# StartupBox FE
Simple application allowing user to create a profile and a diary for their business idea.

### **Main flow:**


User can register and login with Email or SSO
After registration user has to complete his profile to. When profile is created, user has to go through onboarding. A set of questions defining his progress with his project.
Based on these answers a segment is calculated, this is then used to select a group of recommended tasks that will be highlighted in the diary for the first project.

After completing onboarding, user is routed to his first diary, that automatically generated after onboarding flow.

Structure of the diary is defined in the GraphCMS (more below) and specific version is picked by the BE based on the domain user is at. 

Diary is a structured set of tasks, divided into phases, where each task is linked to specific content page created. Content pages are composed in GraphCMS.

In diary, user can navigate between phases and open a detail of specific task/content page to consume content, mark task as done and add notes.


Admin section offers overview of all projects to user with admin rights. THey can do the same as above and also add comments to users notes.








### Key Tech

- This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).
- [TypeScript](https://www.typescriptlang.org/) because it is good to use TypeScript :)
- [Apollo](https://www.apollographql.com/) for data handling
- [MUI](https://mui.com/) for UI components
- [styled-components](https://styled-components.com/) for styling
- [Formik](https://formik.org/docs/overview) to handle forms without the tears
- [Lingui](https://lingui.js.org/) for translations

## Getting Started
```bash
yarn install
```

add env vars file:

```bash
touch .env.local
```

add

`NEXT_PUBLIC_API_URL=https://sbox-dev.herokuapp.com`

`NEXT_PUBLIC_GRAPH_CMS_API_URL=https://api-eu-central-1.graphcms.com/v2/ckumkxn5s3fho01z0b3kj4183/master`

`NEXT_PUBLIC_GRAPH_CMS_TOKEN=` _get it in GraphCMS_

run the development server:

```bash
yarn dev
```

Open [http://localhost:3001](http://localhost:3001) with your browser to see the result.



## GraphCMS

**Very important piece without that it will not work.** GCMS holds the structure of the diary that is served from BE. 

There is a few important pieces: 



- **Diary Scenario** - Defines the structure of diary for specific domain. Consists of phases fetched through BE. 


- **Phase** - represents a phase in product lifecycle. Consist of components 


- **Component** - Mostly visual separation of groups of tasks within a phase. Consist of tasks.


- **Tasks** - definition of a specific task in diary, contains pointer to content page


- **Recommended Tasks** - an array of tasks that are recommended and highlighted in the diary for specific onboarding result. fetched through BE

Other parts:

- **DashboardBanner** - banner that is displayed on index page. Click on this banner opens new tab with specified page. 
Banner can be changed for different domains. Fetched directly 


- **Whitelabel** - Simple customization setup, that allows change of logo and link to community page. Fetched directly 



## Translations

apply changes in translation changes:

```bash
yarn compile
```

extract translation keys:

```bash
yarn extract
```

## Sentry
Basic setup logging only production errors. Sends reports to produkt@startupbox.cz
