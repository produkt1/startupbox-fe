import { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  generates: {
    'src/apollo/types.ts': {
      schema: 'https://startupbox-staging.herokuapp.com/api/graphql',
      plugins: ['typescript'],
    },
    './src': {
      schema: 'https://startupbox-staging.herokuapp.com/api/graphql',
      documents: [
        'src/pages/admin/**/*.ts',
        // 'src/common/**/*.ts',
        // 'src/apollo/fragments/**/*.ts',
        // 'src/apollo/queries/**/*.ts',
        // 'src/apollo/mutations/**/*.ts',
      ],
      preset: 'near-operation-file',
      presetConfig: {
        folder: '__generatedNew__',
        extension: '.types.ts',
        baseTypesPath: 'apollo/types.ts',
      },
      plugins: ['typescript-operations'],
    },

    './src/apollo/cms/GraphCMS/__generatedTypesNew.ts': {
      documents: ['src/apollo/cms/GraphCMS/**/*.ts'],
      schema:
        'https://api-eu-central-1.graphcms.com/v2/ckumkxn5s3fho01z0b3kj4183/master',
      plugins: ['typescript'],
    },
  },
};

export default config;
