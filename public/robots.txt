# *
User-agent: *
Allow: /

# Host
Host: https://startupbox.app

# Sitemaps
Sitemap: https://startupbox.app/sitemap.xml
Sitemap: https://startupbox.app/server-sitemap.xml
