// @ts-ignore

import { FormikHelpers } from 'formik';

export type FormikSubmitHandler<Values> = (
  values: Values,
  formikHelpers: FormikHelpers<Values>
) => void | Promise<any>;

export type LabelValuePair = {
  label: string;
  value: any;
};
