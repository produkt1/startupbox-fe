import React, { FC } from 'react';
import ReactPlayer, { ReactPlayerProps } from 'react-player';
import styled from 'styled-components';

export const VideoPlayer: FC<ReactPlayerProps> = (props) => {
  const { url } = props;

  return (
    <Wrapper>
      <Player
        width={'100%'}
        height={'100%'}
        url={url}
        controls={true}
        // playing={stateOfPlayer.playing}
        // ref={player}
        // onProgress={handleProgress}
        // onPlay={handlePlay}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  padding-top: 56.25%;
`;
const Player = styled(ReactPlayer)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
