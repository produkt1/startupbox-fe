import { FC, default as React } from 'react';
import FormControl from '@mui/material/FormControl';

import {
  Checkbox,
  ListItemText,
  OutlinedInput,
  Select,
  Typography,
} from '@mui/material';
import { InputLabelCustom, InputWrapper } from '../UI';
import { LabelValuePair } from '../../types/commonTypes';
import styled from 'styled-components';
import { StyledMenuItem } from './DropDownInput';

type Props = {
  handleChange<T = string | React.ChangeEvent<any>>(
    field: T
  ): T extends React.ChangeEvent<any>
    ? void
    : (e: string | React.ChangeEvent<any>) => void;
  name: string;
  value: string[];
  label: string;
  options: LabelValuePair[];
  placeholder?: string;
  handleBlur;
};

export const MultiselectDropDownInput: FC<Props> = (props) => {
  const { handleChange, value, label, options, name, handleBlur } = props;

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  // TODO:consider search in list on type
  return (
    <InputWrapper>
      <InputLabelCustom>{label}</InputLabelCustom>
      <FormControl fullWidth>
        <Select
          id={name}
          name={name}
          fullWidth
          multiple
          color="primary"
          value={value}
          onBlur={handleBlur}
          onChange={handleChange}
          input={<OutlinedInput />}
          displayEmpty
          renderValue={(selected) => {
            if ((selected as string[]).length === 0) {
              return <em>Vyber 1 nebo více</em>;
            }

            return (
              <ChipsWrapper>
                <Typography
                  variant="body2"
                  style={{
                    margin: '2px',
                  }}
                >
                  {selected.join(', ')}
                </Typography>
              </ChipsWrapper>
            );
          }}
          MenuProps={MenuProps}
        >
          {options.map((option) => (
            <StyledMenuItem key={option.value} value={option.value}>
              <Checkbox checked={value.indexOf(option.value) > -1} />
              <ListItemText primary={option.value} />
            </StyledMenuItem>
          ))}
        </Select>
      </FormControl>
    </InputWrapper>
  );
};

const ChipsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
