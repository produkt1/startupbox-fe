import { FC } from 'react';
import Button from '@mui/material/Button';
import * as React from 'react';
import { ButtonProps } from '@mui/material/Button/Button';

export type SubmitButtonLabels = {
  submit: string;
  isSubmitting: string;
};

type Props = ButtonProps & {
  isSubmitting;
  labels: SubmitButtonLabels;
};

export const SubmitButton: FC<Props> = (props) => {
  const { isSubmitting, labels, ...rest } = props;

  return (
    <Button
      fullWidth
      color="success"
      size="large"
      variant="contained"
      disabled={isSubmitting}
      type="submit"
      {...rest}
    >
      {isSubmitting ? labels.isSubmitting : labels.submit}
    </Button>
  );
};
