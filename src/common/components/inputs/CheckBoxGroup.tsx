import { FC } from 'react';
import styled from 'styled-components';
import Checkbox from '@mui/material/Checkbox';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { LabelValuePair } from '../../types/commonTypes';
import { InputLabelCustom, InputWrapper } from '../UI';

type Props = {
  name: string;
  value: string[];
  label: string;
  setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
  options: LabelValuePair[];
};

// FIXME: component is using same array logic as skill selector, use filter in remove func

export const CheckBoxGroup: FC<Props> = (props) => {
  const { value, setFieldValue, name, options, label } = props;

  const add = (item) => {
    setFieldValue(name, [...value, item]);
  };

  const remove = (item) => {
    const oldValue = [...value];

    const itemToRemoveIndex = oldValue.indexOf(item);

    setFieldValue(name, [
      ...oldValue.slice(0, itemToRemoveIndex),
      ...oldValue.slice(itemToRemoveIndex + 1, oldValue.length),
    ]);
  };

  const handleClick = (item) => {
    if (value.includes(item)) {
      remove(item);
      return;
    }
    add(item);
  };

  return (
    <InputWrapper>
      <FormControl component="fieldset" fullWidth>
        <InputLabelCustom>{label}</InputLabelCustom>
        <StyledFormGroup>
          {options.map((item) => {
            return (
              <StyledLabel
                key={item.value}
                label={item.label}
                control={
                  <Checkbox
                    color="primary"
                    checked={value.includes(item.value)}
                    // onChange={handleChange}
                    onClick={() => handleClick(item.value)}
                  />
                }
              />
            );
          })}
        </StyledFormGroup>
      </FormControl>
    </InputWrapper>
  );
};

const StyledLabel = styled(FormControlLabel)`
  width: 40%;
`;

const StyledFormGroup = styled(FormGroup)`
  display: flex;
  flex-direction: row;
`;
