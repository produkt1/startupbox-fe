import { FC, default as React } from 'react';
import styled from 'styled-components';
import { InputLabelCustom, InputWrapper } from '../UI';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import { TextareaAutosizeProps } from '@mui/material';

type Props = {
  name: string;
  value: string;
  label: string;
  rowsMin: number;
  handleBlur?(e: React.FocusEvent<any>): void;
  handleChange: any;
};

export const TextAreaInput: FC<Props & TextareaAutosizeProps> = (props) => {
  const { value, rowsMin, label, handleChange, name, ...rest } = props;

  return (
    <InputWrapper>
      <InputLabelCustom> {label}</InputLabelCustom>
      <TextAreaDescription
        name={name}
        minRows={rowsMin}
        value={value}
        onChange={handleChange}
        {...rest}
      />
    </InputWrapper>
  );
};

const TextAreaDescription = styled(TextareaAutosize)`
  width: 100%;
  border-radius: 4px;
  border-style: 1px;
  border-color: rgba(0, 0, 0, 0.23);
  padding: 18.5px 14px;
`;
