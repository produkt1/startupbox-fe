import { FC, default as React } from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import { InputLabelCustom, InputWrapper } from '../UI';
import { LabelValuePair } from '../../types/commonTypes';

type Props = {
  name: string;
  options: LabelValuePair[];
  handleChange(e: React.ChangeEvent<any>): void;
  value: string;
  label: string;
};

export const RadioInput: FC<Props> = (props) => {
  const { name, handleChange, options, value, label } = props;

  return (
    <InputWrapper>
      <InputLabelCustom>{label}</InputLabelCustom>
      <FormControl component="fieldset">
        <RadioGroup
          aria-label={name}
          name={name}
          value={value}
          onChange={handleChange}
        >
          {options.map((option) => (
            <FormControlLabel
              key={option.value}
              value={option.value}
              label={option.label}
              control={<Radio color="primary" />}
            />
          ))}
        </RadioGroup>
      </FormControl>
    </InputWrapper>
  );
};
