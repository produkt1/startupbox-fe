import { FC } from 'react';
import { InputWrapper } from '../UI';
import { FormControlLabel, FormHelperText, Typography } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { useField } from 'formik';
import { FormControlLabelProps } from '@mui/material/FormControlLabel/FormControlLabel';

type Props = Omit<FormControlLabelProps, 'control'>;

export const FormikCheckbox: FC<Props> = (props) => {
  const { name, label, ...rest } = props;
  const [field, meta] = useField(name);

  const { onChange, value, onBlur } = field;
  const { error, touched } = meta;
  const hasError = !!error && touched;

  return (
    <InputWrapper>
      <FormControlLabel
        label={label}
        control={
          <Checkbox
            checked={value}
            onChange={onChange}
            name="agreedToTermsAndConditions"
            color="primary"
          />
        }
        {...rest}
      />
      {hasError && <FormHelperText error={hasError}>{error}</FormHelperText>}
    </InputWrapper>
  );
};
