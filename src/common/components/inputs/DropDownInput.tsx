import { FC, default as React } from 'react';
import FormControl from '@mui/material/FormControl';

import { Select } from '@mui/material';
import { InputLabelCustom, InputWrapper } from '../UI';
import MenuItem from '@mui/material/MenuItem';
import { LabelValuePair } from '../../types/commonTypes';
import { ErrorLabel } from './ErrorLabel';
import { useField } from 'formik';
import { SelectProps } from '@mui/material/Select/Select';
import styled from 'styled-components';

type Props = {
  name: string;
  label: string;
  options: LabelValuePair[];
  placeholder?: string;
};

export const DropDownInput: FC<SelectProps & Props> = (props) => {
  const { label, options, placeholder, name } = props;

  const [field, meta] = useField(name);

  const { onChange, value, onBlur } = field;
  const { error, touched } = meta;
  const hasError = error && touched;

  return (
    <InputWrapper>
      <InputLabelCustom>{label}</InputLabelCustom>
      <FormControl variant="outlined" fullWidth error={hasError}>
        <Select
          name={name}
          value={value}
          fullWidth
          displayEmpty
          onChange={onChange}
          onBlur={onBlur}
        >
          <StyledMenuItem value="" disabled>
            {placeholder}
          </StyledMenuItem>

          {options.map((option) => (
            <StyledMenuItem key={option.value} value={option.value}>
              {option.label}
            </StyledMenuItem>
          ))}
        </Select>
      </FormControl>
      {hasError && <ErrorLabel error={error} />}
    </InputWrapper>
  );
};

export const StyledMenuItem = styled(MenuItem)`
  display: flex;
  justify-content: flex-start;
  padding: 6px 16px;
  line-height: 1.5;

  &:hover {
    background-color: rgba(0, 0, 0, 0.04);
  }
`;
