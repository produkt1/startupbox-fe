import React, { useState, useCallback, useRef, useEffect } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import styled, { css } from 'styled-components';
import FileResizer from 'react-image-file-resizer';
import { Button } from '@mui/material';
import { t, Trans } from '@lingui/macro';
import { minWidth } from '../styles/helpers';

export const ImageCropper = ({
  setFile,
  setImageToProcess,
  imageToProcess,
  onSelectFile,
}) => {
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({
    x: 130,
    y: 50,
    unit: 'px',
    width: 100,
    height: 100,
    // aspect: 4 / 4,
  });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    );
  }, [completedCrop]);

  const generateDownload = (canvas, crop) => {
    if (!crop || !canvas) {
      return;
    }

    canvas.toBlob(
      async (blob) => {
        const resizeFile = async (file: File) =>
          new Promise<Blob>((resolve) => {
            FileResizer.imageFileResizer(
              file,
              500,
              500,
              'JPEG',
              100,
              0,
              (uri) => {
                resolve(uri as Blob);
              },
              'blob'
            );
          });

        const newFile = await resizeFile(blob);

        setFile(newFile);
      },
      'image/png',
      1
    );
  };

  return (
    <Wrapper className="App" $imageToProcess={imageToProcess}>
      {/*<Grid container justifyContent="space-between">*/}
      <StyledButton
        variant={imageToProcess ? 'outlined' : 'contained'}
        component="label"
      >
        Vybrat jiný soubor
        <input type="file" accept="image/*" onChange={onSelectFile} hidden />
      </StyledButton>

      {imageToProcess && (
        <Button
          variant="contained"
          color="success"
          disabled={!completedCrop?.width || !completedCrop?.height}
          onClick={() =>
            generateDownload(previewCanvasRef.current, completedCrop)
          }
        >
          <Trans>Použít</Trans>
        </Button>
      )}
      {/*</Grid>*/}

      {imageToProcess && (
        <>
          <ReactCrop
            className="CropArea"
            src={imageToProcess}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={(c) => setCrop(c)}
            onComplete={(c) => setCompletedCrop(c)}
          />

          <PreviewWrapper>
            <StyledPreviewCanvas ref={previewCanvasRef} />
          </PreviewWrapper>
        </>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div<{ $imageToProcess: boolean }>`
  width: 80vw;

  ${minWidth.mobile} {
    width: auto;
  }

  ${(props) =>
    props.$imageToProcess &&
    css`
      display: grid;
      grid-gap: 24px;

      ${minWidth.mobile} {
        grid-template-columns: auto 1fr;
        grid-template-rows: 1fr 400px;
      }
    `}

  .CropArea {
    height: 100%;

    > div:not(.ReactCrop__crop-selection) {
      height: 100%;
    }

    .ReactCrop__image {
      height: 100%;
    }
  }
`;

const PreviewWrapper = styled.div`
  height: 150px;
  width: 150px;
  border: 1px solid #edf1f7;
  border-radius: 4px;
  overflow: hidden;

  display: flex;
  align-items: center;
`;

const StyledPreviewCanvas = styled.canvas`
  display: block;
  width: 100%;
  height: auto;
`;

const StyledButton = styled(Button)`
  justify-self: start;
`;
