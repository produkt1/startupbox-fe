import React, { FC } from 'react';
import { Link, Typography } from '@mui/material';
import { RichText } from '@graphcms/rich-text-react-renderer';
import styled from 'styled-components';
import { RichTextProps } from '@graphcms/rich-text-types';
import { VideoPlayer } from './VideoPlayer';
import { squareSize } from '../styles/helpers';

export const RichTextCustom: FC<RichTextProps> = (props) => {
  const { content, references } = props;

  return (
    <StyledRT>
      <RichText
        content={content}
        references={references}
        renderers={{
          ul: ({ children }) => <StyledUl>{children}</StyledUl>,
          p: ({ children }) => <Typography gutterBottom>{children}</Typography>,
          h1: ({ children }) => (
            <Typography gutterBottom variant="h1">
              {children}
            </Typography>
          ),
          h2: ({ children }) => (
            <Typography gutterBottom variant="h2">
              {children}
            </Typography>
          ),
          h3: ({ children }) => (
            <Typography gutterBottom variant="h3">
              {children}
            </Typography>
          ),
          h4: ({ children }) => (
            <Typography gutterBottom variant="h4">
              {children}
            </Typography>
          ),
          h5: ({ children }) => (
            <Typography gutterBottom variant="h5">
              {children}
            </Typography>
          ),

          a: ({ children, href }) => (
            <Link href={href} target="_blank">
              {children}
            </Link>
          ),

          embed: {
            VideoLesson: (item) => {
              return <VideoPlayer url={item.url} />;
            },
          },
        }}
      />
    </StyledRT>
  );
};

const StyledRT = styled.div`
  ul,
  li,
  ol,
  h1,
  h2,
  h3,
  h4,
  h5 {
    margin-bottom: 1rem;
  }

  h1,
  h2,
  h3,
  h4,
  h5 {
    margin-top: 2rem;
  }

  p {
    min-height: 1rem;
  }

  a > b {
    &:before {
      display: inline-block;
      content: ' ';
      margin-right: 8px;
      background-image: url('/dl.png');
      background-repeat: no-repeat;
      background-size: 30px 30px;
      ${squareSize('25px')};

      transform: translateY(5px);
    }
  }

  img {
    width: 100%;
    height: auto;
  }
`;

const StyledUl = styled.ul`
  list-style: none;

  li {
    position: relative;
  }
  li::before {
    position: absolute;
    content: url('/check_24px.svg');
    //color: red; /* Change the color */
    //font-weight: bold; /* If you want it to be bold */
    display: inline-block; /* Needed to add space between the bullet and the text */
    width: 1em; /* Also needed for space (tweak if needed) */
    margin-left: -1em; /* Also needed for space (tweak if needed) */
    top: 50%;
    left: -16px;
    transform: translateY(-50%);
  }
`;
