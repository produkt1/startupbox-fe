import { Dispatch, FC, SetStateAction, useRef, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import FileResizer from 'react-image-file-resizer';
import styled from 'styled-components';
import { colors } from '../styles/colors';
import { Button, Typography } from '@mui/material';

type Props = {
  setFile: Dispatch<SetStateAction<Blob | null>>;
  caption: string;
  profilePictureUrl?: string | null;
  setFieldValue?: (field: string, value: string | null) => void;
};

export const ImageUploader: FC<Props> = ({
  setFile,
  caption,
  profilePictureUrl,
  setFieldValue,
}) => {
  const [empty, setEmpty] = useState(false);

  const resizeFile = async (file: File) =>
    new Promise<Blob>((resolve) => {
      FileResizer.imageFileResizer(
        file,
        500,
        500,
        'JPEG',
        100,
        0,
        (uri) => {
          resolve(uri as Blob);
        },
        'blob'
      );
    });

  const onDrop = async (files: File[]) => {
    const newFile = await resizeFile(files[0]);

    setFile(newFile);
    setEmpty(false);
  };

  const dropzoneRef: any = useRef(null);

  const openDialog = () => {
    if (dropzoneRef.current) {
      dropzoneRef.current.click();
    }
  };

  const removePhoto = () => {
    setFieldValue!('profilePictureUrl', '');
    setFile(null);
    setEmpty(true);
  };

  const { getRootProps, getInputProps, acceptedFiles, isDragActive } =
    useDropzone({ onDrop, accept: 'image/*', multiple: false });

  return (
    <>
      {profilePictureUrl && (
        <Container>
          <Image
            src={
              acceptedFiles[0] && !empty
                ? window.URL.createObjectURL(acceptedFiles[0])
                : profilePictureUrl
            }
            alt={''}
          />
          <ButtonWrapper>
            <Button type="button" color="secondary" onClick={removePhoto}>
              Smazat
            </Button>
            <Button type="button" color="secondary" onClick={openDialog}>
              Vybrat jinou fotku
            </Button>
          </ButtonWrapper>
        </Container>
      )}

      {!profilePictureUrl && (
        <Dropzone
          isDragActive={isDragActive}
          {...getRootProps()}
          onClick={openDialog}
        >
          {acceptedFiles[0] && !empty ? (
            <ImageWrapper>
              <Image
                src={window.URL.createObjectURL(acceptedFiles[0])}
                alt={acceptedFiles[0]?.name}
              />
            </ImageWrapper>
          ) : (
            <div>placeholder</div>
            // <StyledIcon size={40} component={<ImagePlaceholderIcon />} />
          )}
          <Typography variant="body1" color="textSecondary">
            {(acceptedFiles[0]?.name && !empty) ?? caption}
          </Typography>
        </Dropzone>
      )}
      <input {...getInputProps()} ref={dropzoneRef} />
    </>
  );
};

const Dropzone = styled.div<{ isDragActive: boolean }>`
  width: 100%;
  height: 280px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  cursor: pointer;
  padding: 50px 20px;
  border: ${({ isDragActive }) =>
    isDragActive ? `2px dashed ${colors.brand}` : `2px dashed ${colors.brand}`};
  text-align: center;
  border-radius: 4px;

  :focus {
    outline: 0;
    border: 2px dashed ${colors.brand};
  }
`;

const ImageWrapper = styled.div`
  margin: 0 auto 10px;
`;

const Image = styled.img`
  width: auto;
  max-height: 200px;
  max-width: 200px;
`;

const Container = styled.div`
  margin-top: 20px;
`;

const ButtonWrapper = styled.div`
  margin: 20px 0;
`;
