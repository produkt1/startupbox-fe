import React, { FC, useRef, useState } from 'react';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import styled from 'styled-components';
import Collapse from '@mui/material/Collapse';
import Button from '@mui/material/Button';
import { t } from '@lingui/macro';

type Props = { collapsedSize?: number };

export const ExpandCustom: FC<Props> = ({ collapsedSize, children }) => {
  const [expanded, setExpanded] = useState(false);
  const myRef = useRef(null);

  const scrollToRef = (ref) =>
    window.scrollTo({
      top: ref.current.offsetTop - 150,
      left: 0,
    });
  // behavior: 'smooth'
  const executeScroll = () => scrollToRef(myRef);

  const toggleExpanded = () => {
    if (expanded) {
      executeScroll();
    }

    setExpanded(!expanded);
  };

  const handleBodyClick = () => (expanded ? null : toggleExpanded);

  // General scroll to element function

  return (
    <StyleCollapse
      in={expanded}
      ref={myRef}
      collapsedSize={collapsedSize || 180}
      component="div"
      onClick={handleBodyClick}
    >
      {children}

      <ExpandButton
        variant="outlined"
        color="primary"
        $expanded={expanded}
        onClick={toggleExpanded}
        startIcon={
          expanded ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />
        }
      >
        {expanded ? t`Méně` : t`Více`}
      </ExpandButton>
    </StyleCollapse>
  );
};

const StyleCollapse = styled(Collapse)<{ in }>`
  margin: 24px 0 48px;
  position: relative;
  padding-bottom: 24px;
  cursor: ${(props) => (props.in ? 'text' : 'pointer')};

  &:after {
    display: ${(props) => (props.in ? 'none' : 'block')};
    position: absolute;
    bottom: 0;
    height: 100%;
    width: 100%;
    content: '';
    background: linear-gradient(
      to top,
      rgba(255, 255, 255, 1) 20%,
      rgba(255, 255, 255, 0) 80%
    );
    //pointer-events: none; /* so the text is still selectable */
  }
`;

const ExpandButton = styled(Button)<{ $expanded: boolean }>`
  position: absolute;
  bottom: ${(props) => (props.$expanded ? -24 : 0)}px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 10;
`;
