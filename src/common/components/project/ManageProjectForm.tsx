import * as React from 'react';
import FormHelperText from '@mui/material/FormHelperText';
import styled from 'styled-components';
import {
  createProject,
  createProjectVariables,
} from '../../../apollo/mutations/__generated__/createProject';
import { FC } from 'react';
import { Form, Formik } from 'formik';
import { FormikSubmitHandler } from '../../types/commonTypes';
import { getProjectById_getProjectById } from '../../../apollo/queries/__generated__/getProjectById';
import { industry, projectPhaseWithLabels } from '../../constants';
import { MultiselectDropDownInput } from '../inputs/MultiselectDropDownInput';
import { MutationFunction } from '@apollo/client/react/types/types';
import {
  editProjectFormValidationSchema,
  newProjectFormValidationSchema,
} from '../../../pages/project/new/components/newProjectForm.validationSchema';
import { t, Trans } from '@lingui/macro';
import { TextInput } from '../inputs/TextInput';
import { UploadModal } from '../../../pages/project/new/components/UploadModal';
import {
  editProject,
  editProjectVariables,
} from '../../../apollo/mutations/__generated__/editProject';
import { SubmitButton } from '../inputs/SubmitButton';
import { InputLabelCustom, InputWrapper, OptionSelector } from '../UI';
import { minWidth } from '../../styles/helpers';

type Props = {
  editProject?: MutationFunction<editProject, editProjectVariables>;
  createProject?: MutationFunction<createProject, createProjectVariables>;
  project?: getProjectById_getProjectById;
  editMode?: boolean;
};

export type FormValues = Omit<createProjectVariables, 'projectLogoUrl'> & {
  name: string;
  industry: string[];
  projectPhase?: string;
  projectLogoUrl?: string;
};

export const ManageProjectForm: FC<Props> = (props) => {
  const { createProject, project, editMode, editProject } = props;

  const buttonLabels = {
    edit: {
      isSubmitting: t`Ukládání změn...`,
      submit: t`Uložit změny`,
    },

    create: {
      submit: t`Vytvořit projekt`,
      isSubmitting: t`Vytváření projektu...`,
    },
  };

  const initialValues: FormValues = {
    name: project?.name || '',
    industry: project?.industry || [],
    projectPhase: '',
    projectLogoUrl: project?.projectLogoUrl,
  };

  const onSubmitHandler: FormikSubmitHandler<FormValues> = async (values) => {
    if (editMode) {
      await editProject({
        variables: {
          ...values,
          name: values.name.trim(),
          projectId: project.id,
        },
      });

      return;
    }

    await createProject({
      variables: {
        ...values,
      },
    });
  };
  const validationSchema = editMode
    ? editProjectFormValidationSchema
    : newProjectFormValidationSchema;

  return (
    <Formik
      onSubmit={onSubmitHandler}
      initialValues={initialValues}
      validationSchema={validationSchema}
      validateOnBlur={false}
      validateOnChange={false}
    >
      {({
        values,
        handleBlur,
        handleChange,
        setFieldValue,
        isSubmitting,
        errors,
      }) => (
        <StyledForm>
          <TextInput
            name="name"
            label={t`Jméno projektu`}
            value={values.name}
            handleChange={handleChange}
            handleBlur={handleBlur}
            error={!!errors.name}
            helperText={errors.name}
          />

          <UploadModal
            setFieldValue={setFieldValue}
            uploadedImageUrl={values.projectLogoUrl}
            name="projectLogoUrl"
            label="Logo"
            isProject
          />

          {editMode || (
            <InputWrapper>
              <InputLabelCustom>
                <Trans>Jak jsi daleko</Trans>
              </InputLabelCustom>

              <OptionsGrid>
                {projectPhaseWithLabels()?.map((option) => (
                  <OptionSelector
                    key={option.value}
                    variant="outlined"
                    value={option.value}
                    $selected={values.projectPhase === option.value}
                    onClick={() => setFieldValue('projectPhase', option.value)}
                  >
                    {option.label}
                  </OptionSelector>
                ))}
              </OptionsGrid>
            </InputWrapper>
          )}

          <MultiselectDropDownInput
            options={industry}
            name="industry"
            value={values.industry}
            label={t`obor`}
            handleChange={handleChange}
            handleBlur={handleBlur}
          />

          {Object.values(errors).map((error) => (
            <FormHelperText key={error as string} error>
              {error}
            </FormHelperText>
          ))}

          <SubmitButton
            labels={editMode ? buttonLabels.edit : buttonLabels.create}
            isSubmitting={isSubmitting}
          />
        </StyledForm>
      )}
    </Formik>
  );
};

const StyledForm = styled(Form)`
  width: 100%;
`;

const OptionsGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(3, 130px);
  grid-gap: 6px;

  ${minWidth.mobile} {
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: repeat(2, 130px);
  }
`;
