import { PhaseStage } from '../../../../../__generated__/globalTypes';
import { CardsIcon } from '../../Icon/Icons/Cards.icon';
import { DocumentIcon } from '../../Icon/Icons/Document.icon';
import { BulbIcon } from '../../Icon/Icons/Bulb.icon';
import { GraphIcon } from '../../Icon/Icons/Graph.icon';
import { FC } from 'react';
import { NewIdeaIcon } from '../../Icon/Icons/NewIdea.icon';

type HeaderIconsByPhase = {
  [key: string]: FC;
};
export const headerIconsByPhase: HeaderIconsByPhase = {
  [PhaseStage.IDEATION]: NewIdeaIcon,
  [PhaseStage.IDEA]: BulbIcon,
  [PhaseStage.BUSINESS]: GraphIcon,
  [PhaseStage.MVP]: DocumentIcon,
  [PhaseStage.PROTOTYPE]: CardsIcon,
};
