import { default as React, FC } from 'react';
import { PhaseProgressHeader, ProgressBar } from './stageProgress.styles';
import { Hidden, Typography } from '@mui/material';
import { Icon } from '../../Icon/Icon';
import { headerIconsByPhase } from './utils';
import { setLastVisitedPhase } from '../../../utils/helpers';
import { PhaseStage } from '../../../../../__generated__/globalTypes';
import { getDiaryByProjectId_getDiaryByProjectId_phases } from '../../../../apollo/queries/__generated__/getDiaryByProjectId';
import styled from 'styled-components';
import { minWidth } from '../../../styles/helpers';

type StageProgressProps = {
  phases?: getDiaryByProjectId_getDiaryByProjectId_phases[];
  setSelectedPhase?(key: PhaseStage);
  selectedPhase?;
  isOnDashboard?: boolean;
  className?: string;
};

export const DiaryStageProgress: FC<StageProgressProps> = (props) => {
  const { setSelectedPhase, selectedPhase, phases, className } = props;

  const getPhaseCompletionInfo = (phase) => {
    const totalTaskCount = phase.components
      .map((component) => component.tasks.length)
      .reduce((tasksInOnPhase, acc) => tasksInOnPhase + acc, 0);

    const totalCompletedTaskCount = phase.components
      .map(
        (component) =>
          component.tasks.filter((task) => task.completed === true).length
      )
      .reduce((tasksInOnPhase, acc) => tasksInOnPhase + acc, 0);

    return {
      percentage: (totalCompletedTaskCount / totalTaskCount) * 100,
      progressLabel: `${totalCompletedTaskCount} / ${totalTaskCount}`,
    };
  };

  const handleClick = (phase) => {
    setSelectedPhase(phase.title);

    setLastVisitedPhase(phase.title);
  };

  return (
    <GridWrapper className={className} $items={phases.length}>
      {phases.map((phase, key) => {
        return (
          <React.Fragment key={key}>
            <PhaseProgressHeader
              className={key === 0 && 'demo__section diary-tutorial-step-3'}
              selected={phase.title === selectedPhase}
              onClick={() => handleClick(phase)}
              key={key}
            >
              <Hidden smDown>
                <Icon Component={headerIconsByPhase[phase.title]} size={36} />
              </Hidden>

              <PhaseHeaderTitle className="span-two">
                {phase.label}
              </PhaseHeaderTitle>

              <Typography variant="body2" className="span-three">
                {getPhaseCompletionInfo(phase).progressLabel}
              </Typography>

              <ProgressBar
                progress={getPhaseCompletionInfo(phase).percentage}
              />
            </PhaseProgressHeader>
          </React.Fragment>
        );
      })}
    </GridWrapper>
  );
};

const PhaseHeaderTitle = styled(Typography)`
  grid-column: span 3;
  justify-self: start;
  text-align: start;
  font-size: 14px;

  ${minWidth.tablet} {
    grid-column: span 2;
  }
`;

const GridWrapper = styled.div<{ $items: number }>`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: auto auto;
  grid-gap: 8px;

  ${minWidth.mobile} {
    grid-template-columns: repeat(3, 1fr);
    grid-column-gap: 24px;
  }

  ${minWidth.tablet} {
    grid-template-columns: repeat(${({ $items }) => $items}, 1fr);
    grid-column-gap: 24px;
  }
`;
