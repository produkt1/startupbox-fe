import { default as React, FC, useEffect, useRef, useState } from 'react';
import Joyride, { CallBackProps, STATUS, Step } from 'react-joyride';
import { t } from '@lingui/macro';
import { TutorialTooltip } from './TutorialTooltip';
import { getDiaryTutorialSteps, getHowToPageSteps } from './tutorials';
import { checkTourFinish, getTours, setTour } from '../../utils/helpers';

export enum TourCase {
  Diary = 'diary',
  HowTo = 'howto',
}
type Props = {
  tourCase: TourCase;
};

export const Tutorial: FC<Props> = ({ tourCase }) => {
  const [run, setRun] = useState(false);
  const takenTours = useRef<string[]>();

  const caseToSteps = {
    [TourCase.Diary]: getDiaryTutorialSteps(),
    [TourCase.HowTo]: getHowToPageSteps(),
  };

  const steps: Step[] = caseToSteps[tourCase];

  useEffect(() => {
    takenTours.current = getTours();

    function checkIfTourWasDoneFor(selectedTourCase) {
      if (!takenTours?.current?.includes(selectedTourCase)) {
        setRun(true);
      }
    }

    checkIfTourWasDoneFor(tourCase);
  }, []);

  const handleJoyrideCallback = (data: CallBackProps) => {
    const tourFinished = checkTourFinish(data.status);

    if (tourFinished) {
      setRun(false);
      setTour(takenTours.current, tourCase);
    }
  };

  return (
    <>
      <Joyride
        run={run}
        steps={steps}
        continuous={true}
        callback={handleJoyrideCallback}
        scrollToFirstStep={true}
        showSkipButton={true}
        showProgress={true}
        scrollOffset={300}
        styles={{
          options: {
            zIndex: 10000,
          },
        }}
        locale={{
          back: t`zpět`,
          next: t`Pokračovat`,
          last: t`Zavřít`,
          close: t`zavřít`,
          skip: t`zavřít`,
        }}
        tooltipComponent={(tooltipProps) => (
          <TutorialTooltip {...tooltipProps} stepsCount={steps.length} />
        )}
      />
    </>
  );
};
