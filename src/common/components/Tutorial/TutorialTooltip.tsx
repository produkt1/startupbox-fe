import { default as React, FC } from 'react';
import { TooltipRenderProps } from 'react-joyride';
import { Button, Card, Grid, IconButton, Typography } from '@mui/material';
import Close from '@mui/icons-material/Close';
import styled from 'styled-components';
import { minWidth } from '../../styles/helpers';
import { colors } from '../../styles/colors';
import { AiAdviserIcon } from '../Icon/Icons/AiAdviser.icon';
import { spacings } from '../../styles/spacings';
import { StyledProgressBar } from '../../styles/tooltipStyles';

type Props = TooltipRenderProps & {
  stepsCount: number;
};

export const TutorialTooltip: FC<Props> = ({
  continuous,
  index,
  step,
  backProps,
  closeProps,
  primaryProps,
  tooltipProps,
  skipProps,
  stepsCount,
}) => {
  const currentStepNumber = index + 1;

  return (
    <StyledCard {...tooltipProps}>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        direction="row-reverse"
        marginBottom={1}
      >
        <IconButton color="primary" {...skipProps}>
          <Close />
        </IconButton>

        {step.title && <Typography variant="h4">{step.title}</Typography>}
      </Grid>

      <StyledProgressBar
        variant="determinate"
        value={(currentStepNumber / stepsCount) * 100}
      />

      {step.target === '.ai-button' && <StyledIcon width={41} height={39} />}

      <Typography marginY={1}>{step.content}</Typography>

      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        marginTop={5}
      >
        <Typography color="primary" fontWeight={600}>
          {currentStepNumber}/{stepsCount}
        </Typography>

        <Grid display="flex" alignItems="center" direction="row-reverse">
          {continuous && (
            <Button variant="contained" {...primaryProps}>
              {primaryProps.title}
            </Button>
          )}

          {index > 0 && <Button {...backProps}>{backProps.title}</Button>}

          {index === 0 && <Button {...skipProps}>{skipProps.title}</Button>}
        </Grid>
      </Grid>
    </StyledCard>
  );
};

const StyledCard = styled(Card)`
  border: none;
  position: relative;
  width: 90vw;
  background: ${colors.cardLightBlue},
  border-radius: 10px;

  ${minWidth.mobile} {
    min-width: 300px;
    max-width: 450px;
  }
`;

const StyledIcon = styled(AiAdviserIcon)`
  margin-bottom: ${spacings.px16};
`;
