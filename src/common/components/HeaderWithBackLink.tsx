import { FC } from 'react';
import Link from 'next/link';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Typography } from '@mui/material';
import styled from 'styled-components';

type Props = { header: string; to: string; as?: string };

export const HeaderWithBackLink: FC<Props> = (props) => {
  const { header, to, as } = props;

  return (
    <Row>
      <Link href={to} as={as}>
        <IconButton aria-label="back">
          <ArrowBackIcon style={{ fontSize: 30 }} />
        </IconButton>
      </Link>
      <Typography variant="h2">{header}</Typography>
    </Row>
  );
};

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;
