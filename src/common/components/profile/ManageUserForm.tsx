import * as React from 'react';
import { Form, Formik } from 'formik';
import { getUser_getUser } from '../../../apollo/queries/__generated__/getUser';
import { FormikTextInput } from '../inputs/TextInput';
import { DropDownInput } from '../inputs/DropDownInput';
import { czechRegions, slovakRegions } from '../../constants';
import { SkillSelector } from '../../../pages/user-profile/creation/components/SkillSelector';
import { InputWrapper } from '../UI';
import { t } from '@lingui/macro';
import { MutationFunction } from '@apollo/client/react/types/types';
import { profileCreationSchema } from '../../../pages/user-profile/creation/components/profileCreation.schema';
import {
  createUserProfile,
  createUserProfileVariables,
} from '../../../apollo/mutations/__generated__/createUserProfile';
import {
  editUserProfile,
  editUserProfileVariables,
} from '../../../apollo/mutations/__generated__/editUserProfile';
import { SubmitButton, SubmitButtonLabels } from '../inputs/SubmitButton';
import { UploadModal } from '../../../pages/project/new/components/UploadModal';
import { useMemo } from 'react';
import { useLingui } from '@lingui/react';

type Props = {
  editMode?: boolean;
  createProfile?: MutationFunction<
    createUserProfile,
    createUserProfileVariables
  >;
  editProfile?: MutationFunction<editUserProfile, editUserProfileVariables>;
  user: getUser_getUser;
};

export type FormValues = createUserProfileVariables & {
  firstName: string;
  lastName: string;
  profilePicture: string;
};

export const ManageUserProfileForm: React.FunctionComponent<Props> = (
  props
) => {
  const { createProfile, editProfile, user, editMode } = props;
  const { i18n } = useLingui();

  const regions = useMemo(() => {
    const options = {
      cs: czechRegions,
      en: czechRegions,
      sk: slovakRegions,
    };

    return options[i18n.locale];
  }, [i18n]);

  const buttonLabels: { [key: string]: SubmitButtonLabels } = {
    edit: {
      submit: t`Uložit změny`,
      isSubmitting: t`Ukládání změn...`,
    },
    create: {
      submit: t`Vytvořit profil`,
      isSubmitting: t`Vytváření profilu...`,
    },
  };

  const onSubmitHandler = async (values, { setSubmitting }) => {
    if (editMode) {
      await editProfile({
        variables: {
          ...values,
          firstName: values.firstName.trim(),
          lastName: values.lastName.trim(),
        },
      });
    } else {
      await createProfile({
        variables: {
          ...values,
          firstName: values.firstName.trim(),
          lastName: values.lastName.trim(),
        },
      });
    }

    setSubmitting(false);
  };

  const initialValues: FormValues = {
    firstName: user?.firstName || '',
    lastName: user?.lastName || '',
    profilePicture: user?.profilePicture || '',
    region: user?.profile?.region || '',
    skills: user?.profile?.skills || [],
  };

  return (
    <Formik
      onSubmit={onSubmitHandler}
      initialValues={initialValues}
      validationSchema={profileCreationSchema}
      render={(formikProps) => {
        const { values, setFieldValue, isSubmitting } = formikProps;
        return (
          <Form>
            <InputWrapper>
              <UploadModal
                setFieldValue={setFieldValue}
                uploadedImageUrl={values.profilePicture}
                name="profilePicture"
                label={t`Profilová fotka`}
              />

              {/*<StyledProfilePicture profilePictureUrl={values.profilePicture} />*/}
            </InputWrapper>

            <FormikTextInput name="firstName" label={t`Jméno`} />

            <FormikTextInput name="lastName" label={t`Příjmení`} />

            <DropDownInput
              name="region"
              placeholder={t`Vyber kraj`}
              label={t`kraj`}
              options={regions}
            />

            <SkillSelector
              value={values.skills}
              setFieldValue={setFieldValue}
            />

            <SubmitButton
              labels={editMode ? buttonLabels.edit : buttonLabels.create}
              isSubmitting={isSubmitting}
            />
          </Form>
        );
      }}
    />
  );
};
