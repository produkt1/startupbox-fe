import React, { Dispatch, FC, SetStateAction } from 'react';
import styled from 'styled-components';
import { ImageUploader } from '../ImageUploader';
import { Typography } from '@mui/material';

type Props = {
  setFile: Dispatch<SetStateAction<Blob | null>>;
  profilePictureUrl?: string | null;
  setFieldValue?: (field: string, value: string | null) => void;
  registrationLabel?: string;
};

export const ProfilePictureUploader: FC<Props> = ({
  setFile,
  profilePictureUrl,
  setFieldValue,
  registrationLabel,
}) => {
  return (
    <Wrapper>
      <Label variant="caption" color="textSecondary">
        {registrationLabel || 'Profilový obrázek'}
      </Label>
      <ImageUploader
        setFieldValue={setFieldValue}
        profilePictureUrl={profilePictureUrl}
        caption="Nahrát profilový obrázek"
        setFile={setFile}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin-bottom: 35px;
`;

const Label = styled(Typography)`
  display: block;
  padding: 0 0 10px 14px;
`;
