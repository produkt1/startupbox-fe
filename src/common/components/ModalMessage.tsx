import { FC, Dispatch, SetStateAction, default as React } from 'react';
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Grid,
} from '@mui/material';
import { useRouter } from 'next/router';

type Props = {
  message?: string;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
};

export const ModalMessage: FC<Props> = (props: Props) => {
  const router = useRouter();
  const { id } = router.query;

  const handleClose = () => {
    props.setOpen(false);
  };

  const onClickHandler = () => router.push('/project/[id]', `/project/${id}`);

  return (
    <Dialog
      open={props.open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      {/* <DialogTitle id="alert-dialog-title">
       {"Use Google's location service?"}
       </DialogTitle> */}
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {props.message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Grid container justifyItems="center">
          <Button color="success" onClick={onClickHandler} variant="contained">
            {' Zpět Do deníku'}
          </Button>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};
