import { default as React, FC } from 'react';
import { useQuery } from '@apollo/client';
import { GET_RECOMMENDED_TASKS_BY_PROJECT_ID } from '../../apollo/queries/getRecommendedTasks.query';
import { Trans } from '@lingui/macro';
import styled from 'styled-components';
import {
  getRecommendedTasksByProjectId,
  getRecommendedTasksByProjectIdVariables,
} from '../../apollo/queries/__generated__/getRecommendedTasksByProjectId';

type Props = { taskPointer: string; projectId: string };

export const RecommendedLabel: FC<Props> = (props) => {
  const { taskPointer, projectId } = props;

  const { data } = useQuery<
    getRecommendedTasksByProjectId,
    getRecommendedTasksByProjectIdVariables
  >(GET_RECOMMENDED_TASKS_BY_PROJECT_ID, {
    variables: {
      id: projectId,
    },
    skip: !projectId || !taskPointer,
  });

  const taskIsRecommended =
    data?.getDiaryByProjectId.recommendedTasks.includes(taskPointer);

  if (taskIsRecommended) {
    return (
      <Label>
        <Trans>Doporučujeme</Trans>
      </Label>
    );
  }

  return null;
};

const Label = styled.span`
  color: white;
  background: #ffa200;
  padding: 4px 9px;
  font-size: 12px;
  border-radius: 4px;
`;
