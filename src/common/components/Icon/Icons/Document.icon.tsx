import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const DocumentIcon: FC<IconSVGProps> = ({ width = 24, height = 24 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="document with lines, list, page, file">
      <path
        id="Icon"
        d="M8.75 6.75H15.25M8.75 10.75H15.25M8.75 14.75H11.25M6.75 21.25H17.25C18.3546 21.25 19.25 20.3546 19.25 19.25V4.75C19.25 3.64543 18.3546 2.75 17.25 2.75H6.75C5.64543 2.75 4.75 3.64543 4.75 4.75V19.25C4.75 20.3546 5.64543 21.25 6.75 21.25Z"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
