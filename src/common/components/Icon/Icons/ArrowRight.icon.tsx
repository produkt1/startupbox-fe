import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const ArrowRightIcon: FC<IconSVGProps> = ({
  width = 24,
  height = 24,
}) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="arrow-corner-down-right">
      <path
        id="vector"
        d="M3.75 4.75V12.25C3.75 13.9069 5.09315 15.25 6.75 15.25H19.25M16.25 11.25L20.25 15.25L16.25 19.25"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
