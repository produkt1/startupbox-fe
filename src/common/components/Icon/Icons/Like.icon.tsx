import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const LikeIcon: FC<IconSVGProps> = ({ width = 22, height = 21 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 22 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="thumbs up, thumb, hand, yes, pro">
      <path
        id="Icon"
        d="M6.6317 9.25119H4.11831C3.65561 9.25119 3.28052 9.62628 3.28052 10.089V16.3724C3.28052 16.8351 3.65561 17.2102 4.11831 17.2102H6.6317M6.6317 17.2102V9.46064L9.95838 3.00295C10.102 2.72408 10.3905 2.54883 10.7042 2.54883C11.7325 2.54883 12.5221 3.46651 12.3637 4.48255L11.8814 7.5756H16.2738C17.8126 7.5756 18.9901 8.94586 18.7586 10.4671L18.0574 15.075C17.8705 16.3029 16.8147 17.2102 15.5726 17.2102H6.6317Z"
        stroke="currentColor"
        strokeWidth="1.25669"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
