import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const ChevronRightIcon: FC<IconSVGProps> = ({
  width = 24,
  height = 24,
}) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 7 12"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      d="M0.244076 11.6531C-0.0813588 11.3275 -0.0813588 10.7996 0.244076 10.474L4.65479 6.06123L0.244076 1.64843C-0.0813592 1.32284 -0.0813592 0.794961 0.244076 0.469373C0.569511 0.143784 1.09714 0.143784 1.42258 0.469373L6.42255 5.4717C6.74798 5.79729 6.74798 6.32517 6.42255 6.65076L1.42258 11.6531C1.09715 11.9787 0.569511 11.9787 0.244076 11.6531Z"
      fill="currentColor"
    />
  </svg>
);
