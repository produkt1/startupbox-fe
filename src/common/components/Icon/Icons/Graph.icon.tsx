import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const GraphIcon: FC<IconSVGProps> = ({ width = 24, height = 24 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="line chart, statistics, graph">
      <path
        id="Icon"
        d="M9.25 10.75V16.25M14.25 4.75V16.25M19.25 12.75V16.25M3.75 3.75V20.25H21.25"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="square"
      />
    </g>
  </svg>
);
