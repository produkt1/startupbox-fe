import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const NewIdeaIcon: FC<IconSVGProps> = ({ width = 24, height = 24 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="image-avatar-sparkle, generated avatar, profile ai, magic avatar">
      <path
        id="vector"
        d="M11.25 3.75H4.75C4.19772 3.75 3.75 4.19772 3.75 4.75V19.25C3.75 19.8023 4.19771 20.25 4.75 20.25H7.25647M20.25 12.75V19.25C20.25 19.8023 19.8023 20.25 19.25 20.25H16.7435M7.25647 20.25C7.38647 17.7429 9.46051 15.75 12 15.75C14.5395 15.75 16.6135 17.7429 16.7435 20.25M7.25647 20.25H16.7435M19 1.75L20.0833 3.91667L22.25 5L20.0833 6.08333L19 8.25L17.9167 6.08333L15.75 5L17.9167 3.91667L19 1.75ZM14.75 10.5C14.75 12.0188 13.5188 13.25 12 13.25C10.4812 13.25 9.25 12.0188 9.25 10.5C9.25 8.98122 10.4812 7.75 12 7.75C13.5188 7.75 14.75 8.98122 14.75 10.5Z"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
