import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const ChevronLeftIcon: FC<IconSVGProps> = ({
  width = 24,
  height = 24,
}) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 7 12"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      d="M6.42255 0.469533C6.74799 0.795121 6.74799 1.323 6.42255 1.64859L2.01181 6.06138L6.42255 10.4742C6.74798 10.7998 6.74798 11.3276 6.42255 11.6532C6.09711 11.9788 5.56948 11.9788 5.24404 11.6532L0.244047 6.65091C-0.0813893 6.32532 -0.0813893 5.79744 0.244047 5.47185L5.24404 0.469533C5.56948 0.143945 6.09711 0.143945 6.42255 0.469533Z"
      fill="currentColor"
    />
  </svg>
);
