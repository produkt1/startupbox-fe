import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const SendIcon: FC<IconSVGProps> = ({ width = 24, height = 24 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13.1336 21.5343C13.3789 22.5154 14.7591 22.554 15.0588 21.5881L20.1869 5.06448C20.4255 4.29567 19.7042 3.57442 18.9354 3.81301L2.41175 8.94104C1.44586 9.2408 1.48447 10.621 2.46561 10.8662L10.4179 12.8543C10.7762 12.9439 11.056 13.2237 11.1455 13.5819L13.1336 21.5343Z"
      stroke="currentColor"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
