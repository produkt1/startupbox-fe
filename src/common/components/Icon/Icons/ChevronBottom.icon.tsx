import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const ChevronBottomIcon: FC<IconSVGProps> = ({
  width = 24,
  height = 24,
}) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="chevron-down-small">
      <path
        id="Icon"
        d="M8 10L12 14L16 10"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="square"
      />
    </g>
  </svg>
);
