import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const CardsIcon: FC<IconSVGProps> = ({ width = 24, height = 24 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="postcard, card, news">
      <path
        id="Icon"
        d="M6.75 10.25H9.25M6.75 13.75H9.25M16.25 14.25H13.75C13.1977 14.25 12.75 13.8023 12.75 13.25V10.75C12.75 10.1977 13.1977 9.75 13.75 9.75H16.25C16.8023 9.75 17.25 10.1977 17.25 10.75V13.25C17.25 13.8023 16.8023 14.25 16.25 14.25ZM4.75 19.25H19.25C20.3546 19.25 21.25 18.3546 21.25 17.25V6.75C21.25 5.64543 20.3546 4.75 19.25 4.75H4.75C3.64543 4.75 2.75 5.64543 2.75 6.75V17.25C2.75 18.3546 3.64543 19.25 4.75 19.25Z"
        stroke="currentColor"
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
