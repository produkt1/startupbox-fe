import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const LinkIcon: FC<IconSVGProps> = ({ width = 18, height = 18 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="square-arrow-top-right, open, new, link, open link, box, arrow">
      <path
        id="vector"
        d="M13.6875 10.5V12.7875C13.6875 13.6276 13.6875 14.0476 13.524 14.3685C13.3802 14.6507 13.1507 14.8802 12.8685 15.024C12.5476 15.1875 12.1276 15.1875 11.2875 15.1875H5.2125C4.37242 15.1875 3.95238 15.1875 3.63151 15.024C3.34927 14.8802 3.1198 14.6507 2.97599 14.3685C2.8125 14.0476 2.8125 13.6276 2.8125 12.7875V6.65625C2.8125 5.86919 2.8125 5.47566 2.95652 5.17117C3.10489 4.85745 3.35745 4.60489 3.67117 4.45652C3.97566 4.3125 4.36919 4.3125 5.15625 4.3125H6.9375M10.3125 2.8125H15.1875M15.1875 2.8125V7.6875M15.1875 2.8125L8.25 9.75"
        stroke="currentColor"
        strokeWidth="1.125"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
