import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const DislikeIcon: FC<IconSVGProps> = ({ width = 21, height = 21 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 21 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="thumbs down, thumb, hand, no, contra">
      <path
        id="Icon"
        d="M14.6444 11.3458H17.1561C17.6188 11.3458 17.9939 10.9707 17.9939 10.508V4.22451C17.9939 3.76181 17.6188 3.38672 17.1561 3.38672H14.6444M14.6444 3.38672V11.1363L11.3196 17.5939C11.1759 17.8728 10.8839 18.0481 10.5702 18.0481V18.0481C9.54239 18.0481 8.7571 17.1304 8.91543 16.1144L9.39743 13.0214H5.0073C3.46934 13.0214 2.29244 11.6511 2.52381 10.1299L3.22465 5.52198C3.41141 4.29403 4.46669 3.38672 5.70814 3.38672H14.6444Z"
        stroke="currentColor"
        strokeWidth="1.25669"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
