import React, { FC } from 'react';
import { IconSVGProps } from '../Icon';

export const CopyIcon: FC<IconSVGProps> = ({ width = 22, height = 21 }) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 22 21"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="pages, boards">
      <path
        id="Icon"
        d="M13.7199 5.90001V4.22442C13.7199 3.29902 12.9697 2.54883 12.0443 2.54883H6.59859C5.67319 2.54883 4.923 3.29902 4.923 4.22442V13.0213C4.923 13.9467 5.67319 14.6969 6.59859 14.6969H8.27418M15.3954 18.048H9.94977C9.02437 18.048 8.27418 17.2978 8.27418 16.3724V7.5756C8.27418 6.6502 9.02437 5.90001 9.94977 5.90001H15.3954C16.3208 5.90001 17.071 6.6502 17.071 7.5756V16.3724C17.071 17.2978 16.3208 18.048 15.3954 18.048Z"
        stroke="currentColor"
        strokeWidth="1.25669"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </g>
  </svg>
);
