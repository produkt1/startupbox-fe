export const uploadImageToGraphCMS = async (file: Blob) => {
  const formData = new FormData();

  formData.append('fileUpload', file);
  // formData.append('upload_preset', 'dtqkvqnl');

  try {
    const data = await fetch('/api/cms/assets/upload', {
      method: 'POST',
      body: formData,
    });
    const response = await data.json();

    return response;
  } catch (err) {
    console.error(err);

    return null;
  }
};
