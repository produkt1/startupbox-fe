import { FC, useCallback } from 'react';
import { Button, Card, Link, Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import styled from 'styled-components';
import { colors } from '../../styles/colors';
import { minWidth } from '../../styles/helpers';

export type SpecialOfferItem = {
  name: string;
  subtitle?: string;
  description: string;
  tags: string;
  link: string;
  logo: string;
};

type Props = {
  items: SpecialOfferItem[];
  buttonLabel: string;
  pageHeader: string;
  gaTrackingFunc?: (param: string) => void;
};

export const SpecialOfferCardPageTemplate: FC<Props> = (props) => {
  const { items, buttonLabel, pageHeader, gaTrackingFunc } = props;

  const handleClick = useCallback(
    (name: string) => {
      if (gaTrackingFunc) {
        gaTrackingFunc(name);
      }
    },
    [gaTrackingFunc]
  );

  return (
    <>
      <Typography variant="h2" align="center" gutterBottom>
        {pageHeader}
      </Typography>

      <CardsGrid>
        {items.map(({ logo, tags, name, subtitle, description, link }) => (
          <Link
            key={name || subtitle}
            href={link}
            target="_blank"
            rel="noreferrer noopener"
            underline="none"
          >
            <ContentCard onClick={() => handleClick(name)}>
              <StyledImage src={logo} alt="" />
              {name && (
                <Typography variant="h3" fontWeight={800}>
                  {name}
                </Typography>
              )}
              {subtitle && (
                <Typography variant="h4" fontWeight={800}>
                  {subtitle}
                </Typography>
              )}
              <Typography variant="body2">{description}</Typography>

              {tags && (
                <Test>
                  {tags.split(`, `).map((tag, index) => (
                    <Tag key={name + tag + index}>{tag}</Tag>
                  ))}
                </Test>
              )}

              <Button variant="contained">
                <Trans>{buttonLabel}</Trans>
              </Button>
            </ContentCard>
          </Link>
        ))}
      </CardsGrid>
    </>
  );
};

const CardsGrid = styled.div`
  display: grid;
  grid-auto-columns: min-content;
  grid-auto-rows: min-content;
  grid-gap: 40px 20px;
  grid-template-columns: 1fr;

  ${minWidth.mobile} {
    grid-template-columns: repeat(2, 1fr);
  }

  ${minWidth.tablet} {
    grid-template-columns: repeat(3, 1fr);
  }
`;

const ContentCard = styled(Card)`
  cursor: pointer;
  height: 100%;
  width: 100%;
  border-radius: 5px;
  border: 1px none #000;

  display: grid;
  grid-template-rows: 70px repeat(6, auto);
  grid-row-gap: 8px;

  &.MuiPaper-elevation1 {
    box-shadow: 0 6px 25px 3px rgb(0 0 0 / 15%);
  }

  h3,
  h4 {
    align-self: end;
  }

  button {
    height: 40px;
    grid-row: -1;
    align-self: end;
  }
`;

// FIXME: image not ideal size
const StyledImage = styled.img`
  margin: 0 auto;
  height: auto;
  width: auto;
  max-width: 155px;
  max-height: 50px;

  align-self: center;
`;

const Tag = styled.span`
  height: 26px;
  font-size: 10px;
  line-height: 20px;
  white-space: nowrap;

  color: ${colors.brand};
  background-color: #ddf1e6;
  margin-right: 10px;
  margin-bottom: 5px;
  padding: 3px 5px;
  border-radius: 20px;
`;

const Test = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
