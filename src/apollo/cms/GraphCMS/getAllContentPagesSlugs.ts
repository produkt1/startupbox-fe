import { gql } from '@apollo/client';

export const GET_ALL_CONTENT_PAGE_SLUGS = gql`
  query getAllContentPagesSlugs {
    contentPages(stage: PUBLISHED) {
      id
      slug
    }
  }
`;
