import { gql } from '@apollo/client';

export const GET_CONTENT_PAGE_FULL = gql`
  query GetContentPageFull($slug: String!, $stage: Stage!) {
    contentPages(where: { slug: $slug }, stage: $stage) {
      id
      slug
      title

      mainVideo {
        url
        mainActor
      }

      mainRichText {
        json
        references {
          ... on Video {
            id
            url
            mainActor
          }
        }
      }

      contentPageSections {
        id
        itemsLayout
        richText {
          json
          references {
            ... on Video {
              id
              url
              mainActor
            }
          }
        }
        title
        contentPageSectionSponsor {
          link
          logo {
            id
            url
          }
        }
        contentPageSectionItems {
          id
          title
          link
          image {
            id
            url
          }
          description
        }
      }
    }
  }
`;
