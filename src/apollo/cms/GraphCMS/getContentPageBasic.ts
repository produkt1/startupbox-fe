import { gql } from '@apollo/client';

export const GET_CONTENT_PAGE_BASIC = gql`
  query GetContentPageBasic($slug: String!, $stage: Stage!) {
    contentPages(where: { slug: $slug }, stage: $stage) {
      id
      slug
      title

      mainVideo {
        url
        mainActor
      }

      mainRichText {
        json
        references {
          ... on Video {
            id
            url
            mainActor
          }
        }
      }

      contentPageSections {
        title
      }
    }
  }
`;
