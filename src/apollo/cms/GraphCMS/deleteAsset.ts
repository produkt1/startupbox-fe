import { gql } from '@apollo/client';

export const DELETE_ASSET = gql`
  mutation deleteAsset($id: ID!) {
    deleteAsset(where: { id: $id }) {
      id
    }
  }
`;
