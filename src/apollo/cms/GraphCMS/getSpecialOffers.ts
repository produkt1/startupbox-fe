import { gql } from '@apollo/client';

export const GET_SPECIAL_OFFERS = gql`
  query getSpecialOffers($where: SpecialOfferWhereInput) {
    specialOffers(where: $where) {
      id
      description
      link
      logo
      name
      subtitle
      tags
    }
  }
`;
