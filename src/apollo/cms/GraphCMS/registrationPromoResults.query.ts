import { gql } from '@apollo/client';

export const REGISTRATION_PROMO_RESULTS_QUERY = gql`
  query registrationPromoResults {
    registrationPromoResults {
      id
      count
      textCs
      textEn
      textSk
      countColor
    }
  }
`;
