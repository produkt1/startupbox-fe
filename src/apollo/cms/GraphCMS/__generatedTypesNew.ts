export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  Date: { input: any; output: any };
  DateTime: { input: any; output: any };
  Hex: { input: any; output: any };
  Json: { input: any; output: any };
  Long: { input: any; output: any };
  RGBAHue: { input: any; output: any };
  RGBATransparency: { input: any; output: any };
  RichTextAST: { input: any; output: any };
};

export type Aggregate = {
  __typename?: 'Aggregate';
  count: Scalars['Int']['output'];
};

/** Asset system model */
export type Asset = Entity &
  Node & {
    __typename?: 'Asset';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    desktopImageDashboardBanner: Array<DashboardBanner>;
    /** Get the document in other stages */
    documentInStages: Array<Asset>;
    /** The file name */
    fileName: Scalars['String']['output'];
    /** The file handle */
    handle: Scalars['String']['output'];
    /** The height of the file */
    height?: Maybe<Scalars['Float']['output']>;
    /** List of Asset versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    imageContentPageSectionItem: Array<ContentPageSectionItem>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<Asset>;
    logoContentPageSectionSponsor: Array<ContentPageSectionSponsor>;
    logoWhitelabel: Array<Whitelabel>;
    /** The mime type of the file */
    mimeType?: Maybe<Scalars['String']['output']>;
    mobileImageDashboardBanner: Array<DashboardBanner>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** The file size */
    size?: Maybe<Scalars['Float']['output']>;
    /** System stage field */
    stage: Stage;
    thumbnailTask: Array<Task>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
    /** Get the url for the asset with provided transformations applied. */
    url: Scalars['String']['output'];
    /** The file width */
    width?: Maybe<Scalars['Float']['output']>;
  };

/** Asset system model */
export type AssetCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Asset system model */
export type AssetDesktopImageDashboardBannerArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<DashboardBannerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DashboardBannerWhereInput>;
};

/** Asset system model */
export type AssetDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

/** Asset system model */
export type AssetHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

/** Asset system model */
export type AssetImageContentPageSectionItemArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ContentPageSectionItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionItemWhereInput>;
};

/** Asset system model */
export type AssetLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

/** Asset system model */
export type AssetLogoContentPageSectionSponsorArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ContentPageSectionSponsorOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionSponsorWhereInput>;
};

/** Asset system model */
export type AssetLogoWhitelabelArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<WhitelabelOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WhitelabelWhereInput>;
};

/** Asset system model */
export type AssetMobileImageDashboardBannerArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<DashboardBannerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DashboardBannerWhereInput>;
};

/** Asset system model */
export type AssetPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Asset system model */
export type AssetScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

/** Asset system model */
export type AssetThumbnailTaskArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<TaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<TaskWhereInput>;
};

/** Asset system model */
export type AssetUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

/** Asset system model */
export type AssetUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Asset system model */
export type AssetUrlArgs = {
  transformation?: InputMaybe<AssetTransformationInput>;
};

export type AssetConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: AssetWhereUniqueInput;
};

/** A connection to a list of items. */
export type AssetConnection = {
  __typename?: 'AssetConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<AssetEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type AssetCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  desktopImageDashboardBanner?: InputMaybe<DashboardBannerCreateManyInlineInput>;
  fileName: Scalars['String']['input'];
  handle: Scalars['String']['input'];
  height?: InputMaybe<Scalars['Float']['input']>;
  imageContentPageSectionItem?: InputMaybe<ContentPageSectionItemCreateManyInlineInput>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<AssetCreateLocalizationsInput>;
  logoContentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorCreateManyInlineInput>;
  logoWhitelabel?: InputMaybe<WhitelabelCreateManyInlineInput>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  mobileImageDashboardBanner?: InputMaybe<DashboardBannerCreateManyInlineInput>;
  size?: InputMaybe<Scalars['Float']['input']>;
  thumbnailTask?: InputMaybe<TaskCreateManyInlineInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  fileName: Scalars['String']['input'];
  handle: Scalars['String']['input'];
  height?: InputMaybe<Scalars['Float']['input']>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Float']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetCreateLocalizationInput = {
  /** Localization input */
  data: AssetCreateLocalizationDataInput;
  locale: Locale;
};

export type AssetCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<AssetCreateLocalizationInput>>;
};

export type AssetCreateManyInlineInput = {
  /** Connect multiple existing Asset documents */
  connect?: InputMaybe<Array<AssetWhereUniqueInput>>;
  /** Create and connect multiple existing Asset documents */
  create?: InputMaybe<Array<AssetCreateInput>>;
};

export type AssetCreateOneInlineInput = {
  /** Connect one existing Asset document */
  connect?: InputMaybe<AssetWhereUniqueInput>;
  /** Create and connect one Asset document */
  create?: InputMaybe<AssetCreateInput>;
};

/** An edge in a connection. */
export type AssetEdge = {
  __typename?: 'AssetEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Asset;
};

/** Identifies documents */
export type AssetManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<AssetWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<AssetWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<AssetWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  desktopImageDashboardBanner_every?: InputMaybe<DashboardBannerWhereInput>;
  desktopImageDashboardBanner_none?: InputMaybe<DashboardBannerWhereInput>;
  desktopImageDashboardBanner_some?: InputMaybe<DashboardBannerWhereInput>;
  documentInStages_every?: InputMaybe<AssetWhereStageInput>;
  documentInStages_none?: InputMaybe<AssetWhereStageInput>;
  documentInStages_some?: InputMaybe<AssetWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  imageContentPageSectionItem_every?: InputMaybe<ContentPageSectionItemWhereInput>;
  imageContentPageSectionItem_none?: InputMaybe<ContentPageSectionItemWhereInput>;
  imageContentPageSectionItem_some?: InputMaybe<ContentPageSectionItemWhereInput>;
  logoContentPageSectionSponsor_every?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoContentPageSectionSponsor_none?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoContentPageSectionSponsor_some?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoWhitelabel_every?: InputMaybe<WhitelabelWhereInput>;
  logoWhitelabel_none?: InputMaybe<WhitelabelWhereInput>;
  logoWhitelabel_some?: InputMaybe<WhitelabelWhereInput>;
  mobileImageDashboardBanner_every?: InputMaybe<DashboardBannerWhereInput>;
  mobileImageDashboardBanner_none?: InputMaybe<DashboardBannerWhereInput>;
  mobileImageDashboardBanner_some?: InputMaybe<DashboardBannerWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  thumbnailTask_every?: InputMaybe<TaskWhereInput>;
  thumbnailTask_none?: InputMaybe<TaskWhereInput>;
  thumbnailTask_some?: InputMaybe<TaskWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum AssetOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  FileNameAsc = 'fileName_ASC',
  FileNameDesc = 'fileName_DESC',
  HandleAsc = 'handle_ASC',
  HandleDesc = 'handle_DESC',
  HeightAsc = 'height_ASC',
  HeightDesc = 'height_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  MimeTypeAsc = 'mimeType_ASC',
  MimeTypeDesc = 'mimeType_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  SizeAsc = 'size_ASC',
  SizeDesc = 'size_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
  WidthAsc = 'width_ASC',
  WidthDesc = 'width_DESC',
}

/** Transformations for Assets */
export type AssetTransformationInput = {
  document?: InputMaybe<DocumentTransformationInput>;
  image?: InputMaybe<ImageTransformationInput>;
  /** Pass true if you want to validate the passed transformation parameters */
  validateOptions?: InputMaybe<Scalars['Boolean']['input']>;
};

export type AssetUpdateInput = {
  desktopImageDashboardBanner?: InputMaybe<DashboardBannerUpdateManyInlineInput>;
  fileName?: InputMaybe<Scalars['String']['input']>;
  handle?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Float']['input']>;
  imageContentPageSectionItem?: InputMaybe<ContentPageSectionItemUpdateManyInlineInput>;
  /** Manage document localizations */
  localizations?: InputMaybe<AssetUpdateLocalizationsInput>;
  logoContentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorUpdateManyInlineInput>;
  logoWhitelabel?: InputMaybe<WhitelabelUpdateManyInlineInput>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  mobileImageDashboardBanner?: InputMaybe<DashboardBannerUpdateManyInlineInput>;
  size?: InputMaybe<Scalars['Float']['input']>;
  thumbnailTask?: InputMaybe<TaskUpdateManyInlineInput>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetUpdateLocalizationDataInput = {
  fileName?: InputMaybe<Scalars['String']['input']>;
  handle?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Float']['input']>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Float']['input']>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetUpdateLocalizationInput = {
  data: AssetUpdateLocalizationDataInput;
  locale: Locale;
};

export type AssetUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<AssetCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<AssetUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<AssetUpsertLocalizationInput>>;
};

export type AssetUpdateManyInlineInput = {
  /** Connect multiple existing Asset documents */
  connect?: InputMaybe<Array<AssetConnectInput>>;
  /** Create and connect multiple Asset documents */
  create?: InputMaybe<Array<AssetCreateInput>>;
  /** Delete multiple Asset documents */
  delete?: InputMaybe<Array<AssetWhereUniqueInput>>;
  /** Disconnect multiple Asset documents */
  disconnect?: InputMaybe<Array<AssetWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Asset documents */
  set?: InputMaybe<Array<AssetWhereUniqueInput>>;
  /** Update multiple Asset documents */
  update?: InputMaybe<Array<AssetUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Asset documents */
  upsert?: InputMaybe<Array<AssetUpsertWithNestedWhereUniqueInput>>;
};

export type AssetUpdateManyInput = {
  fileName?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Float']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<AssetUpdateManyLocalizationsInput>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Float']['input']>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetUpdateManyLocalizationDataInput = {
  fileName?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Float']['input']>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Float']['input']>;
  width?: InputMaybe<Scalars['Float']['input']>;
};

export type AssetUpdateManyLocalizationInput = {
  data: AssetUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type AssetUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<AssetUpdateManyLocalizationInput>>;
};

export type AssetUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: AssetUpdateManyInput;
  /** Document search */
  where: AssetWhereInput;
};

export type AssetUpdateOneInlineInput = {
  /** Connect existing Asset document */
  connect?: InputMaybe<AssetWhereUniqueInput>;
  /** Create and connect one Asset document */
  create?: InputMaybe<AssetCreateInput>;
  /** Delete currently connected Asset document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Asset document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Asset document */
  update?: InputMaybe<AssetUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Asset document */
  upsert?: InputMaybe<AssetUpsertWithNestedWhereUniqueInput>;
};

export type AssetUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: AssetUpdateInput;
  /** Unique document search */
  where: AssetWhereUniqueInput;
};

export type AssetUpsertInput = {
  /** Create document if it didn't exist */
  create: AssetCreateInput;
  /** Update document if it exists */
  update: AssetUpdateInput;
};

export type AssetUpsertLocalizationInput = {
  create: AssetCreateLocalizationDataInput;
  locale: Locale;
  update: AssetUpdateLocalizationDataInput;
};

export type AssetUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: AssetUpsertInput;
  /** Unique document search */
  where: AssetWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type AssetWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type AssetWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<AssetWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<AssetWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<AssetWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  desktopImageDashboardBanner_every?: InputMaybe<DashboardBannerWhereInput>;
  desktopImageDashboardBanner_none?: InputMaybe<DashboardBannerWhereInput>;
  desktopImageDashboardBanner_some?: InputMaybe<DashboardBannerWhereInput>;
  documentInStages_every?: InputMaybe<AssetWhereStageInput>;
  documentInStages_none?: InputMaybe<AssetWhereStageInput>;
  documentInStages_some?: InputMaybe<AssetWhereStageInput>;
  fileName?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  fileName_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  fileName_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  fileName_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  fileName_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  fileName_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  fileName_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  fileName_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  fileName_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  fileName_starts_with?: InputMaybe<Scalars['String']['input']>;
  handle?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  handle_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  handle_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  handle_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  handle_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  handle_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  handle_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  handle_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  handle_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  handle_starts_with?: InputMaybe<Scalars['String']['input']>;
  height?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than the given value. */
  height_gt?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than or equal the given value. */
  height_gte?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are contained in given list. */
  height_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  /** All values less than the given value. */
  height_lt?: InputMaybe<Scalars['Float']['input']>;
  /** All values less than or equal the given value. */
  height_lte?: InputMaybe<Scalars['Float']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  height_not?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are not contained in given list. */
  height_not_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  imageContentPageSectionItem_every?: InputMaybe<ContentPageSectionItemWhereInput>;
  imageContentPageSectionItem_none?: InputMaybe<ContentPageSectionItemWhereInput>;
  imageContentPageSectionItem_some?: InputMaybe<ContentPageSectionItemWhereInput>;
  logoContentPageSectionSponsor_every?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoContentPageSectionSponsor_none?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoContentPageSectionSponsor_some?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  logoWhitelabel_every?: InputMaybe<WhitelabelWhereInput>;
  logoWhitelabel_none?: InputMaybe<WhitelabelWhereInput>;
  logoWhitelabel_some?: InputMaybe<WhitelabelWhereInput>;
  mimeType?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  mimeType_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  mimeType_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  mimeType_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  mimeType_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  mimeType_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  mimeType_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  mimeType_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  mimeType_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  mimeType_starts_with?: InputMaybe<Scalars['String']['input']>;
  mobileImageDashboardBanner_every?: InputMaybe<DashboardBannerWhereInput>;
  mobileImageDashboardBanner_none?: InputMaybe<DashboardBannerWhereInput>;
  mobileImageDashboardBanner_some?: InputMaybe<DashboardBannerWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  size?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than the given value. */
  size_gt?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than or equal the given value. */
  size_gte?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are contained in given list. */
  size_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  /** All values less than the given value. */
  size_lt?: InputMaybe<Scalars['Float']['input']>;
  /** All values less than or equal the given value. */
  size_lte?: InputMaybe<Scalars['Float']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  size_not?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are not contained in given list. */
  size_not_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  thumbnailTask_every?: InputMaybe<TaskWhereInput>;
  thumbnailTask_none?: InputMaybe<TaskWhereInput>;
  thumbnailTask_some?: InputMaybe<TaskWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  width?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than the given value. */
  width_gt?: InputMaybe<Scalars['Float']['input']>;
  /** All values greater than or equal the given value. */
  width_gte?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are contained in given list. */
  width_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
  /** All values less than the given value. */
  width_lt?: InputMaybe<Scalars['Float']['input']>;
  /** All values less than or equal the given value. */
  width_lte?: InputMaybe<Scalars['Float']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  width_not?: InputMaybe<Scalars['Float']['input']>;
  /** All values that are not contained in given list. */
  width_not_in?: InputMaybe<Array<InputMaybe<Scalars['Float']['input']>>>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type AssetWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<AssetWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<AssetWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<AssetWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<AssetWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Asset record uniquely */
export type AssetWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type BatchPayload = {
  __typename?: 'BatchPayload';
  /** The number of nodes that have been affected by the Batch operation. */
  count: Scalars['Long']['output'];
};

export enum CmsProjectPhase {
  Business = 'Business',
  Idea = 'Idea',
  Mvp = 'MVP',
  Prototype = 'Prototype',
}

/** Representing a color value comprising of HEX, RGBA and css color values */
export type Color = {
  __typename?: 'Color';
  css: Scalars['String']['output'];
  hex: Scalars['Hex']['output'];
  rgba: Rgba;
};

/** Accepts either HEX or RGBA color value. At least one of hex or rgba value should be passed. If both are passed RGBA is used. */
export type ColorInput = {
  hex?: InputMaybe<Scalars['Hex']['input']>;
  rgba?: InputMaybe<RgbaInput>;
};

export type Component = Entity &
  Node & {
    __typename?: 'Component';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<Component>;
    /** List of Component versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    label?: Maybe<Scalars['String']['output']>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<Component>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    tasks: Array<Task>;
    /** Title of the component to display in the app.  */
    title?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type ComponentCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ComponentCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ComponentDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type ComponentHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type ComponentLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type ComponentPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ComponentPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ComponentScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type ComponentTasksArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<TaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<TaskWhereInput>;
};

export type ComponentUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ComponentUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ComponentConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ComponentWhereUniqueInput;
};

/** A connection to a list of items. */
export type ComponentConnection = {
  __typename?: 'ComponentConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ComponentEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ComponentCreateInput = {
  ckv87qcnb5ywg01xlbarefddt?: InputMaybe<PhaseCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<ComponentCreateLocalizationsInput>;
  tasks?: InputMaybe<TaskCreateManyInlineInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ComponentCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ComponentCreateLocalizationInput = {
  /** Localization input */
  data: ComponentCreateLocalizationDataInput;
  locale: Locale;
};

export type ComponentCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<ComponentCreateLocalizationInput>>;
};

export type ComponentCreateManyInlineInput = {
  /** Connect multiple existing Component documents */
  connect?: InputMaybe<Array<ComponentWhereUniqueInput>>;
  /** Create and connect multiple existing Component documents */
  create?: InputMaybe<Array<ComponentCreateInput>>;
};

export type ComponentCreateOneInlineInput = {
  /** Connect one existing Component document */
  connect?: InputMaybe<ComponentWhereUniqueInput>;
  /** Create and connect one Component document */
  create?: InputMaybe<ComponentCreateInput>;
};

/** An edge in a connection. */
export type ComponentEdge = {
  __typename?: 'ComponentEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Component;
};

/** Identifies documents */
export type ComponentManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ComponentWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ComponentWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ComponentWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ComponentWhereStageInput>;
  documentInStages_none?: InputMaybe<ComponentWhereStageInput>;
  documentInStages_some?: InputMaybe<ComponentWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tasks_every?: InputMaybe<TaskWhereInput>;
  tasks_none?: InputMaybe<TaskWhereInput>;
  tasks_some?: InputMaybe<TaskWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ComponentOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LabelAsc = 'label_ASC',
  LabelDesc = 'label_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type ComponentUpdateInput = {
  ckv87qcnb5ywg01xlbarefddt?: InputMaybe<PhaseUpdateManyInlineInput>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<ComponentUpdateLocalizationsInput>;
  tasks?: InputMaybe<TaskUpdateManyInlineInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ComponentUpdateLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ComponentUpdateLocalizationInput = {
  data: ComponentUpdateLocalizationDataInput;
  locale: Locale;
};

export type ComponentUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<ComponentCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<ComponentUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<ComponentUpsertLocalizationInput>>;
};

export type ComponentUpdateManyInlineInput = {
  /** Connect multiple existing Component documents */
  connect?: InputMaybe<Array<ComponentConnectInput>>;
  /** Create and connect multiple Component documents */
  create?: InputMaybe<Array<ComponentCreateInput>>;
  /** Delete multiple Component documents */
  delete?: InputMaybe<Array<ComponentWhereUniqueInput>>;
  /** Disconnect multiple Component documents */
  disconnect?: InputMaybe<Array<ComponentWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Component documents */
  set?: InputMaybe<Array<ComponentWhereUniqueInput>>;
  /** Update multiple Component documents */
  update?: InputMaybe<Array<ComponentUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Component documents */
  upsert?: InputMaybe<Array<ComponentUpsertWithNestedWhereUniqueInput>>;
};

export type ComponentUpdateManyInput = {
  label?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<ComponentUpdateManyLocalizationsInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ComponentUpdateManyLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ComponentUpdateManyLocalizationInput = {
  data: ComponentUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ComponentUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<ComponentUpdateManyLocalizationInput>>;
};

export type ComponentUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ComponentUpdateManyInput;
  /** Document search */
  where: ComponentWhereInput;
};

export type ComponentUpdateOneInlineInput = {
  /** Connect existing Component document */
  connect?: InputMaybe<ComponentWhereUniqueInput>;
  /** Create and connect one Component document */
  create?: InputMaybe<ComponentCreateInput>;
  /** Delete currently connected Component document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Component document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Component document */
  update?: InputMaybe<ComponentUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Component document */
  upsert?: InputMaybe<ComponentUpsertWithNestedWhereUniqueInput>;
};

export type ComponentUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ComponentUpdateInput;
  /** Unique document search */
  where: ComponentWhereUniqueInput;
};

export type ComponentUpsertInput = {
  /** Create document if it didn't exist */
  create: ComponentCreateInput;
  /** Update document if it exists */
  update: ComponentUpdateInput;
};

export type ComponentUpsertLocalizationInput = {
  create: ComponentCreateLocalizationDataInput;
  locale: Locale;
  update: ComponentUpdateLocalizationDataInput;
};

export type ComponentUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ComponentUpsertInput;
  /** Unique document search */
  where: ComponentWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type ComponentWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ComponentWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ComponentWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ComponentWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ComponentWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ComponentWhereStageInput>;
  documentInStages_none?: InputMaybe<ComponentWhereStageInput>;
  documentInStages_some?: InputMaybe<ComponentWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tasks_every?: InputMaybe<TaskWhereInput>;
  tasks_none?: InputMaybe<TaskWhereInput>;
  tasks_some?: InputMaybe<TaskWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type ComponentWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ComponentWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ComponentWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ComponentWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<ComponentWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Component record uniquely */
export type ComponentWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ConnectPositionInput = {
  /** Connect document after specified document */
  after?: InputMaybe<Scalars['ID']['input']>;
  /** Connect document before specified document */
  before?: InputMaybe<Scalars['ID']['input']>;
  /** Connect document at last position */
  end?: InputMaybe<Scalars['Boolean']['input']>;
  /** Connect document at first position */
  start?: InputMaybe<Scalars['Boolean']['input']>;
};

export enum ContentOwner {
  InnovateSlovakia = 'innovate_slovakia',
  SboxDefault = 'sbox_default',
}

export type ContentPage = Entity &
  Node & {
    __typename?: 'ContentPage';
    contentOwner?: Maybe<ContentOwner>;
    contentPageSections: Array<ContentPageSection>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<ContentPage>;
    /** List of ContentPage versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    language?: Maybe<Language>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<ContentPage>;
    mainRichText?: Maybe<ContentPageMainRichTextRichText>;
    mainVideo?: Maybe<Video>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    slug: Scalars['String']['output'];
    /** System stage field */
    stage: Stage;
    title: Scalars['String']['output'];
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type ContentPageContentPageSectionsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ContentPageSectionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionWhereInput>;
};

export type ContentPageCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type ContentPageHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type ContentPageLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type ContentPageMainVideoArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPagePublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPagePublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type ContentPageUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ContentPageWhereUniqueInput;
};

/** A connection to a list of items. */
export type ContentPageConnection = {
  __typename?: 'ContentPageConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ContentPageEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ContentPageCreateInput = {
  ckwm0i00u22rd01xu0q3h54fi?: InputMaybe<ContentPageSectionCreateManyInlineInput>;
  contentOwner?: InputMaybe<ContentOwner>;
  contentPageSections?: InputMaybe<ContentPageSectionCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  language?: InputMaybe<Language>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<ContentPageCreateLocalizationsInput>;
  /** mainRichText input for default locale (cz) */
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  mainVideo?: InputMaybe<VideoCreateOneInlineInput>;
  /** slug input for default locale (cz) */
  slug: Scalars['String']['input'];
  /** title input for default locale (cz) */
  title: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  slug: Scalars['String']['input'];
  title: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageCreateLocalizationInput = {
  /** Localization input */
  data: ContentPageCreateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<ContentPageCreateLocalizationInput>>;
};

export type ContentPageCreateManyInlineInput = {
  /** Connect multiple existing ContentPage documents */
  connect?: InputMaybe<Array<ContentPageWhereUniqueInput>>;
  /** Create and connect multiple existing ContentPage documents */
  create?: InputMaybe<Array<ContentPageCreateInput>>;
};

export type ContentPageCreateOneInlineInput = {
  /** Connect one existing ContentPage document */
  connect?: InputMaybe<ContentPageWhereUniqueInput>;
  /** Create and connect one ContentPage document */
  create?: InputMaybe<ContentPageCreateInput>;
};

/** An edge in a connection. */
export type ContentPageEdge = {
  __typename?: 'ContentPageEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ContentPage;
};

export type ContentPageMainRichTextRichText = {
  __typename?: 'ContentPageMainRichTextRichText';
  /** Returns HTMl representation */
  html: Scalars['String']['output'];
  json: Scalars['RichTextAST']['output'];
  /** Returns Markdown representation */
  markdown: Scalars['String']['output'];
  /** @deprecated Please use the 'json' field */
  raw: Scalars['RichTextAST']['output'];
  references: Array<ContentPageMainRichTextRichTextEmbeddedTypes>;
  /** Returns plain-text contents of RichText */
  text: Scalars['String']['output'];
};

export type ContentPageMainRichTextRichTextReferencesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
};

export type ContentPageMainRichTextRichTextEmbeddedTypes = Video;

/** Identifies documents */
export type ContentPageManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentOwner?: InputMaybe<ContentOwner>;
  /** All values that are contained in given list. */
  contentOwner_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  /** Any other value that exists and is not equal to the given value. */
  contentOwner_not?: InputMaybe<ContentOwner>;
  /** All values that are not contained in given list. */
  contentOwner_not_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  contentPageSections_every?: InputMaybe<ContentPageSectionWhereInput>;
  contentPageSections_none?: InputMaybe<ContentPageSectionWhereInput>;
  contentPageSections_some?: InputMaybe<ContentPageSectionWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  language?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  language_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  language_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  language_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  mainVideo?: InputMaybe<VideoWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ContentPageOrderByInput {
  ContentOwnerAsc = 'contentOwner_ASC',
  ContentOwnerDesc = 'contentOwner_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LanguageAsc = 'language_ASC',
  LanguageDesc = 'language_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  SlugAsc = 'slug_ASC',
  SlugDesc = 'slug_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type ContentPageSection = Entity &
  Node & {
    __typename?: 'ContentPageSection';
    contentPage?: Maybe<ContentPage>;
    contentPageSectionItems: Array<ContentPageSectionItem>;
    contentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<ContentPageSection>;
    /** List of ContentPageSection versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    itemsLayout: ContentPageSectionLayout;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<ContentPageSection>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    richText?: Maybe<ContentPageSectionRichTextRichText>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    title?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type ContentPageSectionContentPageArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionContentPageSectionItemsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ContentPageSectionItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionItemWhereInput>;
};

export type ContentPageSectionContentPageSectionSponsorArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type ContentPageSectionHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type ContentPageSectionLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type ContentPageSectionPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type ContentPageSectionUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ContentPageSectionWhereUniqueInput;
};

/** A connection to a list of items. */
export type ContentPageSectionConnection = {
  __typename?: 'ContentPageSectionConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ContentPageSectionEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ContentPageSectionCreateInput = {
  ckwl34v2111mp01zd6y71edyk?: InputMaybe<ContentPageCreateManyInlineInput>;
  contentPage?: InputMaybe<ContentPageCreateOneInlineInput>;
  contentPageSectionItems?: InputMaybe<ContentPageSectionItemCreateManyInlineInput>;
  contentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorCreateOneInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  itemsLayout: ContentPageSectionLayout;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<ContentPageSectionCreateLocalizationsInput>;
  richText?: InputMaybe<Scalars['RichTextAST']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionCreateLocalizationInput = {
  /** Localization input */
  data: ContentPageSectionCreateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<ContentPageSectionCreateLocalizationInput>>;
};

export type ContentPageSectionCreateManyInlineInput = {
  /** Connect multiple existing ContentPageSection documents */
  connect?: InputMaybe<Array<ContentPageSectionWhereUniqueInput>>;
  /** Create and connect multiple existing ContentPageSection documents */
  create?: InputMaybe<Array<ContentPageSectionCreateInput>>;
};

export type ContentPageSectionCreateOneInlineInput = {
  /** Connect one existing ContentPageSection document */
  connect?: InputMaybe<ContentPageSectionWhereUniqueInput>;
  /** Create and connect one ContentPageSection document */
  create?: InputMaybe<ContentPageSectionCreateInput>;
};

/** An edge in a connection. */
export type ContentPageSectionEdge = {
  __typename?: 'ContentPageSectionEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ContentPageSection;
};

export type ContentPageSectionItem = Entity &
  Node & {
    __typename?: 'ContentPageSectionItem';
    contentOwner?: Maybe<ContentOwner>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    description?: Maybe<Scalars['String']['output']>;
    /** Get the document in other stages */
    documentInStages: Array<ContentPageSectionItem>;
    /** List of ContentPageSectionItem versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    image?: Maybe<Asset>;
    language?: Maybe<Language>;
    link?: Maybe<Scalars['String']['output']>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<ContentPageSectionItem>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    title: Scalars['String']['output'];
    toDelete?: Maybe<Scalars['Boolean']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type ContentPageSectionItemCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionItemCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionItemDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type ContentPageSectionItemHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type ContentPageSectionItemImageArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionItemLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type ContentPageSectionItemPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionItemPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionItemScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type ContentPageSectionItemUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionItemUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionItemConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ContentPageSectionItemWhereUniqueInput;
};

/** A connection to a list of items. */
export type ContentPageSectionItemConnection = {
  __typename?: 'ContentPageSectionItemConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ContentPageSectionItemEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ContentPageSectionItemCreateInput = {
  ckwkqio5x0m0801zdaky5akj0?: InputMaybe<ContentPageSectionCreateManyInlineInput>;
  contentOwner?: InputMaybe<ContentOwner>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** description input for default locale (cz) */
  description?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<AssetCreateOneInlineInput>;
  language?: InputMaybe<Language>;
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<ContentPageSectionItemCreateLocalizationsInput>;
  /** title input for default locale (cz) */
  title: Scalars['String']['input'];
  toDelete?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionItemCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  title: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionItemCreateLocalizationInput = {
  /** Localization input */
  data: ContentPageSectionItemCreateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionItemCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<ContentPageSectionItemCreateLocalizationInput>>;
};

export type ContentPageSectionItemCreateManyInlineInput = {
  /** Connect multiple existing ContentPageSectionItem documents */
  connect?: InputMaybe<Array<ContentPageSectionItemWhereUniqueInput>>;
  /** Create and connect multiple existing ContentPageSectionItem documents */
  create?: InputMaybe<Array<ContentPageSectionItemCreateInput>>;
};

export type ContentPageSectionItemCreateOneInlineInput = {
  /** Connect one existing ContentPageSectionItem document */
  connect?: InputMaybe<ContentPageSectionItemWhereUniqueInput>;
  /** Create and connect one ContentPageSectionItem document */
  create?: InputMaybe<ContentPageSectionItemCreateInput>;
};

/** An edge in a connection. */
export type ContentPageSectionItemEdge = {
  __typename?: 'ContentPageSectionItemEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ContentPageSectionItem;
};

/** Identifies documents */
export type ContentPageSectionItemManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentOwner?: InputMaybe<ContentOwner>;
  /** All values that are contained in given list. */
  contentOwner_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  /** Any other value that exists and is not equal to the given value. */
  contentOwner_not?: InputMaybe<ContentOwner>;
  /** All values that are not contained in given list. */
  contentOwner_not_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  image?: InputMaybe<AssetWhereInput>;
  language?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  language_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  language_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  language_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  toDelete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  toDelete_not?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ContentPageSectionItemOrderByInput {
  ContentOwnerAsc = 'contentOwner_ASC',
  ContentOwnerDesc = 'contentOwner_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LanguageAsc = 'language_ASC',
  LanguageDesc = 'language_DESC',
  LinkAsc = 'link_ASC',
  LinkDesc = 'link_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  ToDeleteAsc = 'toDelete_ASC',
  ToDeleteDesc = 'toDelete_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type ContentPageSectionItemUpdateInput = {
  ckwkqio5x0m0801zdaky5akj0?: InputMaybe<ContentPageSectionUpdateManyInlineInput>;
  contentOwner?: InputMaybe<ContentOwner>;
  /** description input for default locale (cz) */
  description?: InputMaybe<Scalars['String']['input']>;
  image?: InputMaybe<AssetUpdateOneInlineInput>;
  language?: InputMaybe<Language>;
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<ContentPageSectionItemUpdateLocalizationsInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  toDelete?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ContentPageSectionItemUpdateLocalizationDataInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionItemUpdateLocalizationInput = {
  data: ContentPageSectionItemUpdateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionItemUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<ContentPageSectionItemCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageSectionItemUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<ContentPageSectionItemUpsertLocalizationInput>>;
};

export type ContentPageSectionItemUpdateManyInlineInput = {
  /** Connect multiple existing ContentPageSectionItem documents */
  connect?: InputMaybe<Array<ContentPageSectionItemConnectInput>>;
  /** Create and connect multiple ContentPageSectionItem documents */
  create?: InputMaybe<Array<ContentPageSectionItemCreateInput>>;
  /** Delete multiple ContentPageSectionItem documents */
  delete?: InputMaybe<Array<ContentPageSectionItemWhereUniqueInput>>;
  /** Disconnect multiple ContentPageSectionItem documents */
  disconnect?: InputMaybe<Array<ContentPageSectionItemWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ContentPageSectionItem documents */
  set?: InputMaybe<Array<ContentPageSectionItemWhereUniqueInput>>;
  /** Update multiple ContentPageSectionItem documents */
  update?: InputMaybe<
    Array<ContentPageSectionItemUpdateWithNestedWhereUniqueInput>
  >;
  /** Upsert multiple ContentPageSectionItem documents */
  upsert?: InputMaybe<
    Array<ContentPageSectionItemUpsertWithNestedWhereUniqueInput>
  >;
};

export type ContentPageSectionItemUpdateManyInput = {
  contentOwner?: InputMaybe<ContentOwner>;
  /** description input for default locale (cz) */
  description?: InputMaybe<Scalars['String']['input']>;
  language?: InputMaybe<Language>;
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<ContentPageSectionItemUpdateManyLocalizationsInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  toDelete?: InputMaybe<Scalars['Boolean']['input']>;
};

export type ContentPageSectionItemUpdateManyLocalizationDataInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionItemUpdateManyLocalizationInput = {
  data: ContentPageSectionItemUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionItemUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageSectionItemUpdateManyLocalizationInput>>;
};

export type ContentPageSectionItemUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ContentPageSectionItemUpdateManyInput;
  /** Document search */
  where: ContentPageSectionItemWhereInput;
};

export type ContentPageSectionItemUpdateOneInlineInput = {
  /** Connect existing ContentPageSectionItem document */
  connect?: InputMaybe<ContentPageSectionItemWhereUniqueInput>;
  /** Create and connect one ContentPageSectionItem document */
  create?: InputMaybe<ContentPageSectionItemCreateInput>;
  /** Delete currently connected ContentPageSectionItem document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected ContentPageSectionItem document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single ContentPageSectionItem document */
  update?: InputMaybe<ContentPageSectionItemUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ContentPageSectionItem document */
  upsert?: InputMaybe<ContentPageSectionItemUpsertWithNestedWhereUniqueInput>;
};

export type ContentPageSectionItemUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ContentPageSectionItemUpdateInput;
  /** Unique document search */
  where: ContentPageSectionItemWhereUniqueInput;
};

export type ContentPageSectionItemUpsertInput = {
  /** Create document if it didn't exist */
  create: ContentPageSectionItemCreateInput;
  /** Update document if it exists */
  update: ContentPageSectionItemUpdateInput;
};

export type ContentPageSectionItemUpsertLocalizationInput = {
  create: ContentPageSectionItemCreateLocalizationDataInput;
  locale: Locale;
  update: ContentPageSectionItemUpdateLocalizationDataInput;
};

export type ContentPageSectionItemUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ContentPageSectionItemUpsertInput;
  /** Unique document search */
  where: ContentPageSectionItemWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type ContentPageSectionItemWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ContentPageSectionItemWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionItemWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentOwner?: InputMaybe<ContentOwner>;
  /** All values that are contained in given list. */
  contentOwner_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  /** Any other value that exists and is not equal to the given value. */
  contentOwner_not?: InputMaybe<ContentOwner>;
  /** All values that are not contained in given list. */
  contentOwner_not_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  documentInStages_every?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionItemWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  image?: InputMaybe<AssetWhereInput>;
  language?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  language_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  language_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  language_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  toDelete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  toDelete_not?: InputMaybe<Scalars['Boolean']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type ContentPageSectionItemWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionItemWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionItemWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionItemWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<ContentPageSectionItemWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References ContentPageSectionItem record uniquely */
export type ContentPageSectionItemWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export enum ContentPageSectionLayout {
  Cards = 'Cards',
  ListOnCard = 'ListOnCard',
  PartnersFlatCards = 'PartnersFlatCards',
  SecondaryRichText = 'SecondaryRichText',
}

/** Identifies documents */
export type ContentPageSectionManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentPage?: InputMaybe<ContentPageWhereInput>;
  contentPageSectionItems_every?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionItems_none?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionItems_some?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageSectionWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  itemsLayout?: InputMaybe<ContentPageSectionLayout>;
  /** All values that are contained in given list. */
  itemsLayout_in?: InputMaybe<Array<InputMaybe<ContentPageSectionLayout>>>;
  /** Any other value that exists and is not equal to the given value. */
  itemsLayout_not?: InputMaybe<ContentPageSectionLayout>;
  /** All values that are not contained in given list. */
  itemsLayout_not_in?: InputMaybe<Array<InputMaybe<ContentPageSectionLayout>>>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ContentPageSectionOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  ItemsLayoutAsc = 'itemsLayout_ASC',
  ItemsLayoutDesc = 'itemsLayout_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type ContentPageSectionRichTextRichText = {
  __typename?: 'ContentPageSectionRichTextRichText';
  /** Returns HTMl representation */
  html: Scalars['String']['output'];
  json: Scalars['RichTextAST']['output'];
  /** Returns Markdown representation */
  markdown: Scalars['String']['output'];
  /** @deprecated Please use the 'json' field */
  raw: Scalars['RichTextAST']['output'];
  references: Array<ContentPageSectionRichTextRichTextEmbeddedTypes>;
  /** Returns plain-text contents of RichText */
  text: Scalars['String']['output'];
};

export type ContentPageSectionRichTextRichTextReferencesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
};

export type ContentPageSectionRichTextRichTextEmbeddedTypes = Video;

export type ContentPageSectionSponsor = Entity &
  Node & {
    __typename?: 'ContentPageSectionSponsor';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<ContentPageSectionSponsor>;
    /** List of ContentPageSectionSponsor versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    link?: Maybe<Scalars['String']['output']>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<ContentPageSectionSponsor>;
    logo: Asset;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type ContentPageSectionSponsorCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionSponsorCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionSponsorDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type ContentPageSectionSponsorHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type ContentPageSectionSponsorLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type ContentPageSectionSponsorLogoArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionSponsorPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionSponsorPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionSponsorScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type ContentPageSectionSponsorUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type ContentPageSectionSponsorUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ContentPageSectionSponsorConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ContentPageSectionSponsorWhereUniqueInput;
};

/** A connection to a list of items. */
export type ContentPageSectionSponsorConnection = {
  __typename?: 'ContentPageSectionSponsorConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ContentPageSectionSponsorEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ContentPageSectionSponsorCreateInput = {
  ckww6bzyw75ja01z1chv93ohf?: InputMaybe<ContentPageSectionCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<ContentPageSectionSponsorCreateLocalizationsInput>;
  logo: AssetCreateOneInlineInput;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionSponsorCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ContentPageSectionSponsorCreateLocalizationInput = {
  /** Localization input */
  data: ContentPageSectionSponsorCreateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionSponsorCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<ContentPageSectionSponsorCreateLocalizationInput>>;
};

export type ContentPageSectionSponsorCreateManyInlineInput = {
  /** Connect multiple existing ContentPageSectionSponsor documents */
  connect?: InputMaybe<Array<ContentPageSectionSponsorWhereUniqueInput>>;
  /** Create and connect multiple existing ContentPageSectionSponsor documents */
  create?: InputMaybe<Array<ContentPageSectionSponsorCreateInput>>;
};

export type ContentPageSectionSponsorCreateOneInlineInput = {
  /** Connect one existing ContentPageSectionSponsor document */
  connect?: InputMaybe<ContentPageSectionSponsorWhereUniqueInput>;
  /** Create and connect one ContentPageSectionSponsor document */
  create?: InputMaybe<ContentPageSectionSponsorCreateInput>;
};

/** An edge in a connection. */
export type ContentPageSectionSponsorEdge = {
  __typename?: 'ContentPageSectionSponsorEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ContentPageSectionSponsor;
};

/** Identifies documents */
export type ContentPageSectionSponsorManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  logo?: InputMaybe<AssetWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ContentPageSectionSponsorOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LinkAsc = 'link_ASC',
  LinkDesc = 'link_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type ContentPageSectionSponsorUpdateInput = {
  ckww6bzyw75ja01z1chv93ohf?: InputMaybe<ContentPageSectionUpdateManyInlineInput>;
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<ContentPageSectionSponsorUpdateLocalizationsInput>;
  logo?: InputMaybe<AssetUpdateOneInlineInput>;
};

export type ContentPageSectionSponsorUpdateLocalizationDataInput = {
  link?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionSponsorUpdateLocalizationInput = {
  data: ContentPageSectionSponsorUpdateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionSponsorUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<ContentPageSectionSponsorCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageSectionSponsorUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<ContentPageSectionSponsorUpsertLocalizationInput>>;
};

export type ContentPageSectionSponsorUpdateManyInlineInput = {
  /** Connect multiple existing ContentPageSectionSponsor documents */
  connect?: InputMaybe<Array<ContentPageSectionSponsorConnectInput>>;
  /** Create and connect multiple ContentPageSectionSponsor documents */
  create?: InputMaybe<Array<ContentPageSectionSponsorCreateInput>>;
  /** Delete multiple ContentPageSectionSponsor documents */
  delete?: InputMaybe<Array<ContentPageSectionSponsorWhereUniqueInput>>;
  /** Disconnect multiple ContentPageSectionSponsor documents */
  disconnect?: InputMaybe<Array<ContentPageSectionSponsorWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ContentPageSectionSponsor documents */
  set?: InputMaybe<Array<ContentPageSectionSponsorWhereUniqueInput>>;
  /** Update multiple ContentPageSectionSponsor documents */
  update?: InputMaybe<
    Array<ContentPageSectionSponsorUpdateWithNestedWhereUniqueInput>
  >;
  /** Upsert multiple ContentPageSectionSponsor documents */
  upsert?: InputMaybe<
    Array<ContentPageSectionSponsorUpsertWithNestedWhereUniqueInput>
  >;
};

export type ContentPageSectionSponsorUpdateManyInput = {
  /** link input for default locale (cz) */
  link?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<ContentPageSectionSponsorUpdateManyLocalizationsInput>;
};

export type ContentPageSectionSponsorUpdateManyLocalizationDataInput = {
  link?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionSponsorUpdateManyLocalizationInput = {
  data: ContentPageSectionSponsorUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionSponsorUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<
    Array<ContentPageSectionSponsorUpdateManyLocalizationInput>
  >;
};

export type ContentPageSectionSponsorUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ContentPageSectionSponsorUpdateManyInput;
  /** Document search */
  where: ContentPageSectionSponsorWhereInput;
};

export type ContentPageSectionSponsorUpdateOneInlineInput = {
  /** Connect existing ContentPageSectionSponsor document */
  connect?: InputMaybe<ContentPageSectionSponsorWhereUniqueInput>;
  /** Create and connect one ContentPageSectionSponsor document */
  create?: InputMaybe<ContentPageSectionSponsorCreateInput>;
  /** Delete currently connected ContentPageSectionSponsor document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected ContentPageSectionSponsor document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single ContentPageSectionSponsor document */
  update?: InputMaybe<ContentPageSectionSponsorUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ContentPageSectionSponsor document */
  upsert?: InputMaybe<ContentPageSectionSponsorUpsertWithNestedWhereUniqueInput>;
};

export type ContentPageSectionSponsorUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ContentPageSectionSponsorUpdateInput;
  /** Unique document search */
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type ContentPageSectionSponsorUpsertInput = {
  /** Create document if it didn't exist */
  create: ContentPageSectionSponsorCreateInput;
  /** Update document if it exists */
  update: ContentPageSectionSponsorUpdateInput;
};

export type ContentPageSectionSponsorUpsertLocalizationInput = {
  create: ContentPageSectionSponsorCreateLocalizationDataInput;
  locale: Locale;
  update: ContentPageSectionSponsorUpdateLocalizationDataInput;
};

export type ContentPageSectionSponsorUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ContentPageSectionSponsorUpsertInput;
  /** Unique document search */
  where: ContentPageSectionSponsorWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type ContentPageSectionSponsorWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ContentPageSectionSponsorWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionSponsorWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionSponsorWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<AssetWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type ContentPageSectionSponsorWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionSponsorWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionSponsorWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionSponsorWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<ContentPageSectionSponsorWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References ContentPageSectionSponsor record uniquely */
export type ContentPageSectionSponsorWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ContentPageSectionUpdateInput = {
  ckwl34v2111mp01zd6y71edyk?: InputMaybe<ContentPageUpdateManyInlineInput>;
  contentPage?: InputMaybe<ContentPageUpdateOneInlineInput>;
  contentPageSectionItems?: InputMaybe<ContentPageSectionItemUpdateManyInlineInput>;
  contentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorUpdateOneInlineInput>;
  itemsLayout?: InputMaybe<ContentPageSectionLayout>;
  /** Manage document localizations */
  localizations?: InputMaybe<ContentPageSectionUpdateLocalizationsInput>;
  richText?: InputMaybe<Scalars['RichTextAST']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionUpdateLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionUpdateLocalizationInput = {
  data: ContentPageSectionUpdateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<ContentPageSectionCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageSectionUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<ContentPageSectionUpsertLocalizationInput>>;
};

export type ContentPageSectionUpdateManyInlineInput = {
  /** Connect multiple existing ContentPageSection documents */
  connect?: InputMaybe<Array<ContentPageSectionConnectInput>>;
  /** Create and connect multiple ContentPageSection documents */
  create?: InputMaybe<Array<ContentPageSectionCreateInput>>;
  /** Delete multiple ContentPageSection documents */
  delete?: InputMaybe<Array<ContentPageSectionWhereUniqueInput>>;
  /** Disconnect multiple ContentPageSection documents */
  disconnect?: InputMaybe<Array<ContentPageSectionWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ContentPageSection documents */
  set?: InputMaybe<Array<ContentPageSectionWhereUniqueInput>>;
  /** Update multiple ContentPageSection documents */
  update?: InputMaybe<
    Array<ContentPageSectionUpdateWithNestedWhereUniqueInput>
  >;
  /** Upsert multiple ContentPageSection documents */
  upsert?: InputMaybe<
    Array<ContentPageSectionUpsertWithNestedWhereUniqueInput>
  >;
};

export type ContentPageSectionUpdateManyInput = {
  itemsLayout?: InputMaybe<ContentPageSectionLayout>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<ContentPageSectionUpdateManyLocalizationsInput>;
  richText?: InputMaybe<Scalars['RichTextAST']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionUpdateManyLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageSectionUpdateManyLocalizationInput = {
  data: ContentPageSectionUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ContentPageSectionUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageSectionUpdateManyLocalizationInput>>;
};

export type ContentPageSectionUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ContentPageSectionUpdateManyInput;
  /** Document search */
  where: ContentPageSectionWhereInput;
};

export type ContentPageSectionUpdateOneInlineInput = {
  /** Connect existing ContentPageSection document */
  connect?: InputMaybe<ContentPageSectionWhereUniqueInput>;
  /** Create and connect one ContentPageSection document */
  create?: InputMaybe<ContentPageSectionCreateInput>;
  /** Delete currently connected ContentPageSection document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected ContentPageSection document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single ContentPageSection document */
  update?: InputMaybe<ContentPageSectionUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ContentPageSection document */
  upsert?: InputMaybe<ContentPageSectionUpsertWithNestedWhereUniqueInput>;
};

export type ContentPageSectionUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ContentPageSectionUpdateInput;
  /** Unique document search */
  where: ContentPageSectionWhereUniqueInput;
};

export type ContentPageSectionUpsertInput = {
  /** Create document if it didn't exist */
  create: ContentPageSectionCreateInput;
  /** Update document if it exists */
  update: ContentPageSectionUpdateInput;
};

export type ContentPageSectionUpsertLocalizationInput = {
  create: ContentPageSectionCreateLocalizationDataInput;
  locale: Locale;
  update: ContentPageSectionUpdateLocalizationDataInput;
};

export type ContentPageSectionUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ContentPageSectionUpsertInput;
  /** Unique document search */
  where: ContentPageSectionWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type ContentPageSectionWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ContentPageSectionWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentPage?: InputMaybe<ContentPageWhereInput>;
  contentPageSectionItems_every?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionItems_none?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionItems_some?: InputMaybe<ContentPageSectionItemWhereInput>;
  contentPageSectionSponsor?: InputMaybe<ContentPageSectionSponsorWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageSectionWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageSectionWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageSectionWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  itemsLayout?: InputMaybe<ContentPageSectionLayout>;
  /** All values that are contained in given list. */
  itemsLayout_in?: InputMaybe<Array<InputMaybe<ContentPageSectionLayout>>>;
  /** Any other value that exists and is not equal to the given value. */
  itemsLayout_not?: InputMaybe<ContentPageSectionLayout>;
  /** All values that are not contained in given list. */
  itemsLayout_not_in?: InputMaybe<Array<InputMaybe<ContentPageSectionLayout>>>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type ContentPageSectionWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageSectionWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageSectionWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageSectionWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<ContentPageSectionWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References ContentPageSection record uniquely */
export type ContentPageSectionWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type ContentPageUpdateInput = {
  ckwm0i00u22rd01xu0q3h54fi?: InputMaybe<ContentPageSectionUpdateManyInlineInput>;
  contentOwner?: InputMaybe<ContentOwner>;
  contentPageSections?: InputMaybe<ContentPageSectionUpdateManyInlineInput>;
  language?: InputMaybe<Language>;
  /** Manage document localizations */
  localizations?: InputMaybe<ContentPageUpdateLocalizationsInput>;
  /** mainRichText input for default locale (cz) */
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  mainVideo?: InputMaybe<VideoUpdateOneInlineInput>;
  /** slug input for default locale (cz) */
  slug?: InputMaybe<Scalars['String']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageUpdateLocalizationDataInput = {
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  slug?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageUpdateLocalizationInput = {
  data: ContentPageUpdateLocalizationDataInput;
  locale: Locale;
};

export type ContentPageUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<ContentPageCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<ContentPageUpsertLocalizationInput>>;
};

export type ContentPageUpdateManyInlineInput = {
  /** Connect multiple existing ContentPage documents */
  connect?: InputMaybe<Array<ContentPageConnectInput>>;
  /** Create and connect multiple ContentPage documents */
  create?: InputMaybe<Array<ContentPageCreateInput>>;
  /** Delete multiple ContentPage documents */
  delete?: InputMaybe<Array<ContentPageWhereUniqueInput>>;
  /** Disconnect multiple ContentPage documents */
  disconnect?: InputMaybe<Array<ContentPageWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ContentPage documents */
  set?: InputMaybe<Array<ContentPageWhereUniqueInput>>;
  /** Update multiple ContentPage documents */
  update?: InputMaybe<Array<ContentPageUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple ContentPage documents */
  upsert?: InputMaybe<Array<ContentPageUpsertWithNestedWhereUniqueInput>>;
};

export type ContentPageUpdateManyInput = {
  contentOwner?: InputMaybe<ContentOwner>;
  language?: InputMaybe<Language>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<ContentPageUpdateManyLocalizationsInput>;
  /** mainRichText input for default locale (cz) */
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageUpdateManyLocalizationDataInput = {
  mainRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ContentPageUpdateManyLocalizationInput = {
  data: ContentPageUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type ContentPageUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<ContentPageUpdateManyLocalizationInput>>;
};

export type ContentPageUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ContentPageUpdateManyInput;
  /** Document search */
  where: ContentPageWhereInput;
};

export type ContentPageUpdateOneInlineInput = {
  /** Connect existing ContentPage document */
  connect?: InputMaybe<ContentPageWhereUniqueInput>;
  /** Create and connect one ContentPage document */
  create?: InputMaybe<ContentPageCreateInput>;
  /** Delete currently connected ContentPage document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected ContentPage document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single ContentPage document */
  update?: InputMaybe<ContentPageUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ContentPage document */
  upsert?: InputMaybe<ContentPageUpsertWithNestedWhereUniqueInput>;
};

export type ContentPageUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ContentPageUpdateInput;
  /** Unique document search */
  where: ContentPageWhereUniqueInput;
};

export type ContentPageUpsertInput = {
  /** Create document if it didn't exist */
  create: ContentPageCreateInput;
  /** Update document if it exists */
  update: ContentPageUpdateInput;
};

export type ContentPageUpsertLocalizationInput = {
  create: ContentPageCreateLocalizationDataInput;
  locale: Locale;
  update: ContentPageUpdateLocalizationDataInput;
};

export type ContentPageUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ContentPageUpsertInput;
  /** Unique document search */
  where: ContentPageWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type ContentPageWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ContentPageWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  contentOwner?: InputMaybe<ContentOwner>;
  /** All values that are contained in given list. */
  contentOwner_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  /** Any other value that exists and is not equal to the given value. */
  contentOwner_not?: InputMaybe<ContentOwner>;
  /** All values that are not contained in given list. */
  contentOwner_not_in?: InputMaybe<Array<InputMaybe<ContentOwner>>>;
  contentPageSections_every?: InputMaybe<ContentPageSectionWhereInput>;
  contentPageSections_none?: InputMaybe<ContentPageSectionWhereInput>;
  contentPageSections_some?: InputMaybe<ContentPageSectionWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<ContentPageWhereStageInput>;
  documentInStages_none?: InputMaybe<ContentPageWhereStageInput>;
  documentInStages_some?: InputMaybe<ContentPageWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  language?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  language_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  language_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  language_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  mainVideo?: InputMaybe<VideoWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  slug?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  slug_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  slug_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  slug_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  slug_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  slug_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  slug_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  slug_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  slug_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  slug_starts_with?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type ContentPageWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ContentPageWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ContentPageWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ContentPageWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<ContentPageWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References ContentPage record uniquely */
export type ContentPageWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type DashboardBanner = Entity &
  Node & {
    __typename?: 'DashboardBanner';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    desktopImage: Asset;
    /** Get the document in other stages */
    documentInStages: Array<DashboardBanner>;
    /** list of domains that will show this banner */
    domains: Array<Scalars['String']['output']>;
    /** List of DashboardBanner versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    link: Scalars['String']['output'];
    mobileImage: Asset;
    name: Scalars['String']['output'];
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type DashboardBannerCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DashboardBannerDesktopImageArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DashboardBannerDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type DashboardBannerHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type DashboardBannerMobileImageArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DashboardBannerPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DashboardBannerScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type DashboardBannerUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DashboardBannerConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: DashboardBannerWhereUniqueInput;
};

/** A connection to a list of items. */
export type DashboardBannerConnection = {
  __typename?: 'DashboardBannerConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<DashboardBannerEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type DashboardBannerCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  desktopImage: AssetCreateOneInlineInput;
  domains?: InputMaybe<Array<Scalars['String']['input']>>;
  link: Scalars['String']['input'];
  mobileImage: AssetCreateOneInlineInput;
  name: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type DashboardBannerCreateManyInlineInput = {
  /** Connect multiple existing DashboardBanner documents */
  connect?: InputMaybe<Array<DashboardBannerWhereUniqueInput>>;
  /** Create and connect multiple existing DashboardBanner documents */
  create?: InputMaybe<Array<DashboardBannerCreateInput>>;
};

export type DashboardBannerCreateOneInlineInput = {
  /** Connect one existing DashboardBanner document */
  connect?: InputMaybe<DashboardBannerWhereUniqueInput>;
  /** Create and connect one DashboardBanner document */
  create?: InputMaybe<DashboardBannerCreateInput>;
};

/** An edge in a connection. */
export type DashboardBannerEdge = {
  __typename?: 'DashboardBannerEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: DashboardBanner;
};

/** Identifies documents */
export type DashboardBannerManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  desktopImage?: InputMaybe<AssetWhereInput>;
  documentInStages_every?: InputMaybe<DashboardBannerWhereStageInput>;
  documentInStages_none?: InputMaybe<DashboardBannerWhereStageInput>;
  documentInStages_some?: InputMaybe<DashboardBannerWhereStageInput>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  domains?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains *all* items provided to the filter */
  domains_contains_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  domains_contains_none?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains at least one item provided to the filter */
  domains_contains_some?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  domains_not?: InputMaybe<Array<Scalars['String']['input']>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  mobileImage?: InputMaybe<AssetWhereInput>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum DashboardBannerOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DomainsAsc = 'domains_ASC',
  DomainsDesc = 'domains_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LinkAsc = 'link_ASC',
  LinkDesc = 'link_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type DashboardBannerUpdateInput = {
  desktopImage?: InputMaybe<AssetUpdateOneInlineInput>;
  domains?: InputMaybe<Array<Scalars['String']['input']>>;
  link?: InputMaybe<Scalars['String']['input']>;
  mobileImage?: InputMaybe<AssetUpdateOneInlineInput>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type DashboardBannerUpdateManyInlineInput = {
  /** Connect multiple existing DashboardBanner documents */
  connect?: InputMaybe<Array<DashboardBannerConnectInput>>;
  /** Create and connect multiple DashboardBanner documents */
  create?: InputMaybe<Array<DashboardBannerCreateInput>>;
  /** Delete multiple DashboardBanner documents */
  delete?: InputMaybe<Array<DashboardBannerWhereUniqueInput>>;
  /** Disconnect multiple DashboardBanner documents */
  disconnect?: InputMaybe<Array<DashboardBannerWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing DashboardBanner documents */
  set?: InputMaybe<Array<DashboardBannerWhereUniqueInput>>;
  /** Update multiple DashboardBanner documents */
  update?: InputMaybe<Array<DashboardBannerUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple DashboardBanner documents */
  upsert?: InputMaybe<Array<DashboardBannerUpsertWithNestedWhereUniqueInput>>;
};

export type DashboardBannerUpdateManyInput = {
  domains?: InputMaybe<Array<Scalars['String']['input']>>;
  link?: InputMaybe<Scalars['String']['input']>;
};

export type DashboardBannerUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: DashboardBannerUpdateManyInput;
  /** Document search */
  where: DashboardBannerWhereInput;
};

export type DashboardBannerUpdateOneInlineInput = {
  /** Connect existing DashboardBanner document */
  connect?: InputMaybe<DashboardBannerWhereUniqueInput>;
  /** Create and connect one DashboardBanner document */
  create?: InputMaybe<DashboardBannerCreateInput>;
  /** Delete currently connected DashboardBanner document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected DashboardBanner document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single DashboardBanner document */
  update?: InputMaybe<DashboardBannerUpdateWithNestedWhereUniqueInput>;
  /** Upsert single DashboardBanner document */
  upsert?: InputMaybe<DashboardBannerUpsertWithNestedWhereUniqueInput>;
};

export type DashboardBannerUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: DashboardBannerUpdateInput;
  /** Unique document search */
  where: DashboardBannerWhereUniqueInput;
};

export type DashboardBannerUpsertInput = {
  /** Create document if it didn't exist */
  create: DashboardBannerCreateInput;
  /** Update document if it exists */
  update: DashboardBannerUpdateInput;
};

export type DashboardBannerUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: DashboardBannerUpsertInput;
  /** Unique document search */
  where: DashboardBannerWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type DashboardBannerWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type DashboardBannerWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DashboardBannerWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  desktopImage?: InputMaybe<AssetWhereInput>;
  documentInStages_every?: InputMaybe<DashboardBannerWhereStageInput>;
  documentInStages_none?: InputMaybe<DashboardBannerWhereStageInput>;
  documentInStages_some?: InputMaybe<DashboardBannerWhereStageInput>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  domains?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains *all* items provided to the filter */
  domains_contains_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  domains_contains_none?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains at least one item provided to the filter */
  domains_contains_some?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  domains_not?: InputMaybe<Array<Scalars['String']['input']>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  mobileImage?: InputMaybe<AssetWhereInput>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type DashboardBannerWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DashboardBannerWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DashboardBannerWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DashboardBannerWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<DashboardBannerWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References DashboardBanner record uniquely */
export type DashboardBannerWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type DiaryScenario = Entity &
  Node & {
    __typename?: 'DiaryScenario';
    /** Client is defined by subdomain - e.g fashion.startupbox.app - where client is 'fashion' */
    client: Array<Scalars['String']['output']>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<DiaryScenario>;
    /** List of DiaryScenario versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    name?: Maybe<Scalars['String']['output']>;
    phases: Array<Phase>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type DiaryScenarioCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DiaryScenarioDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type DiaryScenarioHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type DiaryScenarioPhasesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<PhaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<PhaseWhereInput>;
};

export type DiaryScenarioPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DiaryScenarioScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type DiaryScenarioUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type DiaryScenarioConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: DiaryScenarioWhereUniqueInput;
};

/** A connection to a list of items. */
export type DiaryScenarioConnection = {
  __typename?: 'DiaryScenarioConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<DiaryScenarioEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type DiaryScenarioCreateInput = {
  client?: InputMaybe<Array<Scalars['String']['input']>>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  phases?: InputMaybe<PhaseCreateManyInlineInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type DiaryScenarioCreateManyInlineInput = {
  /** Connect multiple existing DiaryScenario documents */
  connect?: InputMaybe<Array<DiaryScenarioWhereUniqueInput>>;
  /** Create and connect multiple existing DiaryScenario documents */
  create?: InputMaybe<Array<DiaryScenarioCreateInput>>;
};

export type DiaryScenarioCreateOneInlineInput = {
  /** Connect one existing DiaryScenario document */
  connect?: InputMaybe<DiaryScenarioWhereUniqueInput>;
  /** Create and connect one DiaryScenario document */
  create?: InputMaybe<DiaryScenarioCreateInput>;
};

/** An edge in a connection. */
export type DiaryScenarioEdge = {
  __typename?: 'DiaryScenarioEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: DiaryScenario;
};

/** Identifies documents */
export type DiaryScenarioManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  client?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains *all* items provided to the filter */
  client_contains_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  client_contains_none?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains at least one item provided to the filter */
  client_contains_some?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  client_not?: InputMaybe<Array<Scalars['String']['input']>>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<DiaryScenarioWhereStageInput>;
  documentInStages_none?: InputMaybe<DiaryScenarioWhereStageInput>;
  documentInStages_some?: InputMaybe<DiaryScenarioWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  phases_every?: InputMaybe<PhaseWhereInput>;
  phases_none?: InputMaybe<PhaseWhereInput>;
  phases_some?: InputMaybe<PhaseWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum DiaryScenarioOrderByInput {
  ClientAsc = 'client_ASC',
  ClientDesc = 'client_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type DiaryScenarioUpdateInput = {
  client?: InputMaybe<Array<Scalars['String']['input']>>;
  name?: InputMaybe<Scalars['String']['input']>;
  phases?: InputMaybe<PhaseUpdateManyInlineInput>;
};

export type DiaryScenarioUpdateManyInlineInput = {
  /** Connect multiple existing DiaryScenario documents */
  connect?: InputMaybe<Array<DiaryScenarioConnectInput>>;
  /** Create and connect multiple DiaryScenario documents */
  create?: InputMaybe<Array<DiaryScenarioCreateInput>>;
  /** Delete multiple DiaryScenario documents */
  delete?: InputMaybe<Array<DiaryScenarioWhereUniqueInput>>;
  /** Disconnect multiple DiaryScenario documents */
  disconnect?: InputMaybe<Array<DiaryScenarioWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing DiaryScenario documents */
  set?: InputMaybe<Array<DiaryScenarioWhereUniqueInput>>;
  /** Update multiple DiaryScenario documents */
  update?: InputMaybe<Array<DiaryScenarioUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple DiaryScenario documents */
  upsert?: InputMaybe<Array<DiaryScenarioUpsertWithNestedWhereUniqueInput>>;
};

export type DiaryScenarioUpdateManyInput = {
  client?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type DiaryScenarioUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: DiaryScenarioUpdateManyInput;
  /** Document search */
  where: DiaryScenarioWhereInput;
};

export type DiaryScenarioUpdateOneInlineInput = {
  /** Connect existing DiaryScenario document */
  connect?: InputMaybe<DiaryScenarioWhereUniqueInput>;
  /** Create and connect one DiaryScenario document */
  create?: InputMaybe<DiaryScenarioCreateInput>;
  /** Delete currently connected DiaryScenario document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected DiaryScenario document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single DiaryScenario document */
  update?: InputMaybe<DiaryScenarioUpdateWithNestedWhereUniqueInput>;
  /** Upsert single DiaryScenario document */
  upsert?: InputMaybe<DiaryScenarioUpsertWithNestedWhereUniqueInput>;
};

export type DiaryScenarioUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: DiaryScenarioUpdateInput;
  /** Unique document search */
  where: DiaryScenarioWhereUniqueInput;
};

export type DiaryScenarioUpsertInput = {
  /** Create document if it didn't exist */
  create: DiaryScenarioCreateInput;
  /** Update document if it exists */
  update: DiaryScenarioUpdateInput;
};

export type DiaryScenarioUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: DiaryScenarioUpsertInput;
  /** Unique document search */
  where: DiaryScenarioWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type DiaryScenarioWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type DiaryScenarioWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DiaryScenarioWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  /** Matches if the field array contains *all* items provided to the filter and order does match */
  client?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains *all* items provided to the filter */
  client_contains_all?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contain any of the items provided to the filter */
  client_contains_none?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array contains at least one item provided to the filter */
  client_contains_some?: InputMaybe<Array<Scalars['String']['input']>>;
  /** Matches if the field array does not contains *all* items provided to the filter or order does not match */
  client_not?: InputMaybe<Array<Scalars['String']['input']>>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<DiaryScenarioWhereStageInput>;
  documentInStages_none?: InputMaybe<DiaryScenarioWhereStageInput>;
  documentInStages_some?: InputMaybe<DiaryScenarioWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  phases_every?: InputMaybe<PhaseWhereInput>;
  phases_none?: InputMaybe<PhaseWhereInput>;
  phases_some?: InputMaybe<PhaseWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type DiaryScenarioWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<DiaryScenarioWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<DiaryScenarioWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<DiaryScenarioWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<DiaryScenarioWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References DiaryScenario record uniquely */
export type DiaryScenarioWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export enum DocumentFileTypes {
  Doc = 'doc',
  Docx = 'docx',
  Html = 'html',
  Jpg = 'jpg',
  Odp = 'odp',
  Ods = 'ods',
  Odt = 'odt',
  Pdf = 'pdf',
  Png = 'png',
  Ppt = 'ppt',
  Pptx = 'pptx',
  Svg = 'svg',
  Txt = 'txt',
  Webp = 'webp',
  Xls = 'xls',
  Xlsx = 'xlsx',
}

export type DocumentOutputInput = {
  /**
   * Transforms a document into a desired file type.
   * See this matrix for format support:
   *
   * PDF:	jpg, odp, ods, odt, png, svg, txt, and webp
   * DOC:	docx, html, jpg, odt, pdf, png, svg, txt, and webp
   * DOCX:	doc, html, jpg, odt, pdf, png, svg, txt, and webp
   * ODT:	doc, docx, html, jpg, pdf, png, svg, txt, and webp
   * XLS:	jpg, pdf, ods, png, svg, xlsx, and webp
   * XLSX:	jpg, pdf, ods, png, svg, xls, and webp
   * ODS:	jpg, pdf, png, xls, svg, xlsx, and webp
   * PPT:	jpg, odp, pdf, png, svg, pptx, and webp
   * PPTX:	jpg, odp, pdf, png, svg, ppt, and webp
   * ODP:	jpg, pdf, png, ppt, svg, pptx, and webp
   * BMP:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * GIF:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * JPG:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * PNG:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * WEBP:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * TIFF:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * AI:	    jpg, odp, ods, odt, pdf, png, svg, and webp
   * PSD:	jpg, odp, ods, odt, pdf, png, svg, and webp
   * SVG:	jpg, odp, ods, odt, pdf, png, and webp
   * HTML:	jpg, odt, pdf, svg, txt, and webp
   * TXT:	jpg, html, odt, pdf, svg, and webp
   */
  format?: InputMaybe<DocumentFileTypes>;
};

/** Transformations for Documents */
export type DocumentTransformationInput = {
  /** Changes the output for the file. */
  output?: InputMaybe<DocumentOutputInput>;
};

export type DocumentVersion = {
  __typename?: 'DocumentVersion';
  createdAt: Scalars['DateTime']['output'];
  data?: Maybe<Scalars['Json']['output']>;
  id: Scalars['ID']['output'];
  revision: Scalars['Int']['output'];
  stage: Stage;
};

export type Eewe = Entity &
  Node & {
    __typename?: 'Eewe';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<Eewe>;
    /** List of Eewe versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    modular: Array<EewemodularUnion>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    text?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
    we?: Maybe<Wee>;
  };

export type EeweCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type EeweDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type EeweHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type EeweModularArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
};

export type EewePublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type EeweScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type EeweUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type EeweWeArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type EeweConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: EeweWhereUniqueInput;
};

/** A connection to a list of items. */
export type EeweConnection = {
  __typename?: 'EeweConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<EeweEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type EeweCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<EeweCreateLocalizationsInput>;
  modular?: InputMaybe<EewemodularUnionCreateManyInlineInput>;
  text?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  we?: InputMaybe<WeeCreateOneInlineInput>;
};

export type EeweCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type EeweCreateLocalizationInput = {
  /** Localization input */
  data: EeweCreateLocalizationDataInput;
  locale: Locale;
};

export type EeweCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<EeweCreateLocalizationInput>>;
};

export type EeweCreateManyInlineInput = {
  /** Connect multiple existing Eewe documents */
  connect?: InputMaybe<Array<EeweWhereUniqueInput>>;
  /** Create and connect multiple existing Eewe documents */
  create?: InputMaybe<Array<EeweCreateInput>>;
};

export type EeweCreateOneInlineInput = {
  /** Connect one existing Eewe document */
  connect?: InputMaybe<EeweWhereUniqueInput>;
  /** Create and connect one Eewe document */
  create?: InputMaybe<EeweCreateInput>;
};

/** An edge in a connection. */
export type EeweEdge = {
  __typename?: 'EeweEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Eewe;
};

/** Identifies documents */
export type EeweManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<EeweWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<EeweWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<EeweWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<EeweWhereStageInput>;
  documentInStages_none?: InputMaybe<EeweWhereStageInput>;
  documentInStages_some?: InputMaybe<EeweWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values in which the union is empty. */
  modular_empty?: InputMaybe<Scalars['Boolean']['input']>;
  /** Matches if the modular component contains at least one connection to the item provided to the filter */
  modular_some?: InputMaybe<EewemodularUnionWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  text?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  text_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  text_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  text_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  text_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  text_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  text_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  text_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  text_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  text_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  we?: InputMaybe<WeeWhereInput>;
};

export enum EeweOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TextAsc = 'text_ASC',
  TextDesc = 'text_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type EeweUpdateInput = {
  /** Manage document localizations */
  localizations?: InputMaybe<EeweUpdateLocalizationsInput>;
  modular?: InputMaybe<EewemodularUnionUpdateManyInlineInput>;
  text?: InputMaybe<Scalars['String']['input']>;
  we?: InputMaybe<WeeUpdateOneInlineInput>;
};

export type EeweUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<EeweCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
};

export type EeweUpdateManyInlineInput = {
  /** Connect multiple existing Eewe documents */
  connect?: InputMaybe<Array<EeweConnectInput>>;
  /** Create and connect multiple Eewe documents */
  create?: InputMaybe<Array<EeweCreateInput>>;
  /** Delete multiple Eewe documents */
  delete?: InputMaybe<Array<EeweWhereUniqueInput>>;
  /** Disconnect multiple Eewe documents */
  disconnect?: InputMaybe<Array<EeweWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Eewe documents */
  set?: InputMaybe<Array<EeweWhereUniqueInput>>;
  /** Update multiple Eewe documents */
  update?: InputMaybe<Array<EeweUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Eewe documents */
  upsert?: InputMaybe<Array<EeweUpsertWithNestedWhereUniqueInput>>;
};

export type EeweUpdateManyInput = {
  text?: InputMaybe<Scalars['String']['input']>;
};

export type EeweUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: EeweUpdateManyInput;
  /** Document search */
  where: EeweWhereInput;
};

export type EeweUpdateOneInlineInput = {
  /** Connect existing Eewe document */
  connect?: InputMaybe<EeweWhereUniqueInput>;
  /** Create and connect one Eewe document */
  create?: InputMaybe<EeweCreateInput>;
  /** Delete currently connected Eewe document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Eewe document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Eewe document */
  update?: InputMaybe<EeweUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Eewe document */
  upsert?: InputMaybe<EeweUpsertWithNestedWhereUniqueInput>;
};

export type EeweUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: EeweUpdateInput;
  /** Unique document search */
  where: EeweWhereUniqueInput;
};

export type EeweUpsertInput = {
  /** Create document if it didn't exist */
  create: EeweCreateInput;
  /** Update document if it exists */
  update: EeweUpdateInput;
};

export type EeweUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: EeweUpsertInput;
  /** Unique document search */
  where: EeweWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type EeweWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type EeweWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<EeweWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<EeweWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<EeweWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<EeweWhereStageInput>;
  documentInStages_none?: InputMaybe<EeweWhereStageInput>;
  documentInStages_some?: InputMaybe<EeweWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values in which the union is empty. */
  modular_empty?: InputMaybe<Scalars['Boolean']['input']>;
  /** Matches if the modular component contains at least one connection to the item provided to the filter */
  modular_some?: InputMaybe<EewemodularUnionWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  text?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  text_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  text_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  text_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  text_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  text_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  text_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  text_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  text_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  text_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  we?: InputMaybe<WeeWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type EeweWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<EeweWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<EeweWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<EeweWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<EeweWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Eewe record uniquely */
export type EeweWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type EewemodularUnion = LinkToFeedbackForm;

export type EewemodularUnionConnectInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormConnectInput>;
};

export type EewemodularUnionCreateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateInput>;
};

export type EewemodularUnionCreateManyInlineInput = {
  /** Create and connect multiple existing EewemodularUnion documents */
  create?: InputMaybe<Array<EewemodularUnionCreateInput>>;
};

export type EewemodularUnionCreateOneInlineInput = {
  /** Create and connect one EewemodularUnion document */
  create?: InputMaybe<EewemodularUnionCreateInput>;
};

export type EewemodularUnionCreateWithPositionInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateWithPositionInput>;
};

export type EewemodularUnionUpdateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateInput>;
};

export type EewemodularUnionUpdateManyInlineInput = {
  /** Create and connect multiple EewemodularUnion component instances */
  create?: InputMaybe<Array<EewemodularUnionCreateWithPositionInput>>;
  /** Delete multiple EewemodularUnion documents */
  delete?: InputMaybe<Array<EewemodularUnionWhereUniqueInput>>;
  /** Update multiple EewemodularUnion component instances */
  update?: InputMaybe<
    Array<EewemodularUnionUpdateWithNestedWhereUniqueAndPositionInput>
  >;
  /** Upsert multiple EewemodularUnion component instances */
  upsert?: InputMaybe<
    Array<EewemodularUnionUpsertWithNestedWhereUniqueAndPositionInput>
  >;
};

export type EewemodularUnionUpdateManyWithNestedWhereInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateManyWithNestedWhereInput>;
};

export type EewemodularUnionUpdateOneInlineInput = {
  /** Create and connect one EewemodularUnion document */
  create?: InputMaybe<EewemodularUnionCreateInput>;
  /** Delete currently connected EewemodularUnion document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single EewemodularUnion document */
  update?: InputMaybe<EewemodularUnionUpdateWithNestedWhereUniqueInput>;
  /** Upsert single EewemodularUnion document */
  upsert?: InputMaybe<EewemodularUnionUpsertWithNestedWhereUniqueInput>;
};

export type EewemodularUnionUpdateWithNestedWhereUniqueAndPositionInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueAndPositionInput>;
};

export type EewemodularUnionUpdateWithNestedWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
};

export type EewemodularUnionUpsertWithNestedWhereUniqueAndPositionInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueAndPositionInput>;
};

export type EewemodularUnionUpsertWithNestedWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
};

export type EewemodularUnionWhereInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
};

export type EewemodularUnionWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereUniqueInput>;
};

/** An object with an ID */
export type Entity = {
  /** The id of the object. */
  id: Scalars['ID']['output'];
  /** The Stage of an object */
  stage: Stage;
};

/** This enumeration holds all typenames that implement the Entity interface. Components and models implement the Entity interface. */
export enum EntityTypeName {
  /** Asset system model */
  Asset = 'Asset',
  Component = 'Component',
  ContentPage = 'ContentPage',
  ContentPageSection = 'ContentPageSection',
  ContentPageSectionItem = 'ContentPageSectionItem',
  ContentPageSectionSponsor = 'ContentPageSectionSponsor',
  DashboardBanner = 'DashboardBanner',
  DiaryScenario = 'DiaryScenario',
  Eewe = 'Eewe',
  LinkToFeedbackForm = 'LinkToFeedbackForm',
  /** jednotlivé položky na stránce Odborníci */
  Partner = 'Partner',
  Phase = 'Phase',
  RecommendedTask = 'RecommendedTask',
  RegistrationPromoResult = 'RegistrationPromoResult',
  /** Scheduled Operation system model */
  ScheduledOperation = 'ScheduledOperation',
  /** Scheduled Release system model */
  ScheduledRelease = 'ScheduledRelease',
  SpecialOffer = 'SpecialOffer',
  Task = 'Task',
  /** User system model */
  User = 'User',
  Video = 'Video',
  VideoLesson = 'VideoLesson',
  Wee = 'Wee',
  /** Whitelabel config for each subdomain */
  Whitelabel = 'Whitelabel',
}

/** Allows to specify input to query models and components directly */
export type EntityWhereInput = {
  /** The ID of an object */
  id: Scalars['ID']['input'];
  locale?: InputMaybe<Locale>;
  stage: Stage;
  /** The Type name of an object */
  typename: EntityTypeName;
};

export enum ImageFit {
  /** Resizes the image to fit within the specified parameters without distorting, cropping, or changing the aspect ratio. */
  Clip = 'clip',
  /** Resizes the image to fit the specified parameters exactly by removing any parts of the image that don't fit within the boundaries. */
  Crop = 'crop',
  /** Resizes the image to fit within the parameters, but as opposed to 'fit:clip' will not scale the image if the image is smaller than the output size. */
  Max = 'max',
  /** Resizes the image to fit the specified parameters exactly by scaling the image to the desired size. The aspect ratio of the image is not respected and the image can be distorted using this method. */
  Scale = 'scale',
}

export type ImageResizeInput = {
  /** The default value for the fit parameter is fit:clip. */
  fit?: InputMaybe<ImageFit>;
  /** The height in pixels to resize the image to. The value must be an integer from 1 to 10000. */
  height?: InputMaybe<Scalars['Int']['input']>;
  /** The width in pixels to resize the image to. The value must be an integer from 1 to 10000. */
  width?: InputMaybe<Scalars['Int']['input']>;
};

/** Transformations for Images */
export type ImageTransformationInput = {
  /** Resizes the image */
  resize?: InputMaybe<ImageResizeInput>;
};

export enum Language {
  Cs = 'cs',
  En = 'en',
  Sk = 'sk',
}

export type LinkToFeedbackForm = Entity & {
  __typename?: 'LinkToFeedbackForm';
  anortherModular: Array<LinkToFeedbackFormanortherModularUnion>;
  eeee?: Maybe<LinkToFeedbackFormeeeeUnion>;
  /** The unique identifier */
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  /** System stage field */
  stage: Stage;
  url?: Maybe<Scalars['String']['output']>;
};

export type LinkToFeedbackFormAnortherModularArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
};

export type LinkToFeedbackFormEeeeArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type LinkToFeedbackFormConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: LinkToFeedbackFormWhereUniqueInput;
};

/** A connection to a list of items. */
export type LinkToFeedbackFormConnection = {
  __typename?: 'LinkToFeedbackFormConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<LinkToFeedbackFormEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type LinkToFeedbackFormCreateInput = {
  anortherModular?: InputMaybe<LinkToFeedbackFormanortherModularUnionCreateManyInlineInput>;
  eeee?: InputMaybe<LinkToFeedbackFormeeeeUnionCreateOneInlineInput>;
  label?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type LinkToFeedbackFormCreateManyInlineInput = {
  /** Create and connect multiple existing LinkToFeedbackForm documents */
  create?: InputMaybe<Array<LinkToFeedbackFormCreateInput>>;
};

export type LinkToFeedbackFormCreateOneInlineInput = {
  /** Create and connect one LinkToFeedbackForm document */
  create?: InputMaybe<LinkToFeedbackFormCreateInput>;
};

export type LinkToFeedbackFormCreateWithPositionInput = {
  /** Document to create */
  data: LinkToFeedbackFormCreateInput;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
};

/** An edge in a connection. */
export type LinkToFeedbackFormEdge = {
  __typename?: 'LinkToFeedbackFormEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: LinkToFeedbackForm;
};

/** Identifies documents */
export type LinkToFeedbackFormManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  /** All values in which the union is empty. */
  anortherModular_empty?: InputMaybe<Scalars['Boolean']['input']>;
  /** Matches if the modular component contains at least one connection to the item provided to the filter */
  anortherModular_some?: InputMaybe<LinkToFeedbackFormanortherModularUnionWhereInput>;
  /** All values in which the modular component is connected to the given models */
  eeee?: InputMaybe<LinkToFeedbackFormeeeeUnionWhereInput>;
  /** All values in which the union is empty. */
  eeee_empty?: InputMaybe<Scalars['Boolean']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  url_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  url_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  url_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  url_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  url_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  url_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  url_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  url_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  url_starts_with?: InputMaybe<Scalars['String']['input']>;
};

export enum LinkToFeedbackFormOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LabelAsc = 'label_ASC',
  LabelDesc = 'label_DESC',
  UrlAsc = 'url_ASC',
  UrlDesc = 'url_DESC',
}

export type LinkToFeedbackFormParent = Eewe | LinkToFeedbackForm | Whitelabel;

export type LinkToFeedbackFormParentConnectInput = {
  Eewe?: InputMaybe<EeweConnectInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormConnectInput>;
  Whitelabel?: InputMaybe<WhitelabelConnectInput>;
};

export type LinkToFeedbackFormParentCreateInput = {
  Eewe?: InputMaybe<EeweCreateInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateInput>;
  Whitelabel?: InputMaybe<WhitelabelCreateInput>;
};

export type LinkToFeedbackFormParentCreateManyInlineInput = {
  /** Connect multiple existing LinkToFeedbackFormParent documents */
  connect?: InputMaybe<Array<LinkToFeedbackFormParentWhereUniqueInput>>;
  /** Create and connect multiple existing LinkToFeedbackFormParent documents */
  create?: InputMaybe<Array<LinkToFeedbackFormParentCreateInput>>;
};

export type LinkToFeedbackFormParentCreateOneInlineInput = {
  /** Connect one existing LinkToFeedbackFormParent document */
  connect?: InputMaybe<LinkToFeedbackFormParentWhereUniqueInput>;
  /** Create and connect one LinkToFeedbackFormParent document */
  create?: InputMaybe<LinkToFeedbackFormParentCreateInput>;
};

export type LinkToFeedbackFormParentUpdateInput = {
  Eewe?: InputMaybe<EeweUpdateInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateInput>;
  Whitelabel?: InputMaybe<WhitelabelUpdateInput>;
};

export type LinkToFeedbackFormParentUpdateManyInlineInput = {
  /** Connect multiple existing LinkToFeedbackFormParent documents */
  connect?: InputMaybe<Array<LinkToFeedbackFormParentConnectInput>>;
  /** Create and connect multiple LinkToFeedbackFormParent documents */
  create?: InputMaybe<Array<LinkToFeedbackFormParentCreateInput>>;
  /** Delete multiple LinkToFeedbackFormParent documents */
  delete?: InputMaybe<Array<LinkToFeedbackFormParentWhereUniqueInput>>;
  /** Disconnect multiple LinkToFeedbackFormParent documents */
  disconnect?: InputMaybe<Array<LinkToFeedbackFormParentWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing LinkToFeedbackFormParent documents */
  set?: InputMaybe<Array<LinkToFeedbackFormParentWhereUniqueInput>>;
  /** Update multiple LinkToFeedbackFormParent documents */
  update?: InputMaybe<
    Array<LinkToFeedbackFormParentUpdateWithNestedWhereUniqueInput>
  >;
  /** Upsert multiple LinkToFeedbackFormParent documents */
  upsert?: InputMaybe<
    Array<LinkToFeedbackFormParentUpsertWithNestedWhereUniqueInput>
  >;
};

export type LinkToFeedbackFormParentUpdateManyWithNestedWhereInput = {
  Eewe?: InputMaybe<EeweUpdateManyWithNestedWhereInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateManyWithNestedWhereInput>;
  Whitelabel?: InputMaybe<WhitelabelUpdateManyWithNestedWhereInput>;
};

export type LinkToFeedbackFormParentUpdateOneInlineInput = {
  /** Connect existing LinkToFeedbackFormParent document */
  connect?: InputMaybe<LinkToFeedbackFormParentWhereUniqueInput>;
  /** Create and connect one LinkToFeedbackFormParent document */
  create?: InputMaybe<LinkToFeedbackFormParentCreateInput>;
  /** Delete currently connected LinkToFeedbackFormParent document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected LinkToFeedbackFormParent document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single LinkToFeedbackFormParent document */
  update?: InputMaybe<LinkToFeedbackFormParentUpdateWithNestedWhereUniqueInput>;
  /** Upsert single LinkToFeedbackFormParent document */
  upsert?: InputMaybe<LinkToFeedbackFormParentUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormParentUpdateWithNestedWhereUniqueInput = {
  Eewe?: InputMaybe<EeweUpdateWithNestedWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
  Whitelabel?: InputMaybe<WhitelabelUpdateWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormParentUpsertWithNestedWhereUniqueInput = {
  Eewe?: InputMaybe<EeweUpsertWithNestedWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
  Whitelabel?: InputMaybe<WhitelabelUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormParentWhereInput = {
  Eewe?: InputMaybe<EeweWhereInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
  Whitelabel?: InputMaybe<WhitelabelWhereInput>;
};

export type LinkToFeedbackFormParentWhereUniqueInput = {
  Eewe?: InputMaybe<EeweWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereUniqueInput>;
  Whitelabel?: InputMaybe<WhitelabelWhereUniqueInput>;
};

export type LinkToFeedbackFormUpdateInput = {
  anortherModular?: InputMaybe<LinkToFeedbackFormanortherModularUnionUpdateManyInlineInput>;
  eeee?: InputMaybe<LinkToFeedbackFormeeeeUnionUpdateOneInlineInput>;
  label?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type LinkToFeedbackFormUpdateManyInlineInput = {
  /** Create and connect multiple LinkToFeedbackForm component instances */
  create?: InputMaybe<Array<LinkToFeedbackFormCreateWithPositionInput>>;
  /** Delete multiple LinkToFeedbackForm documents */
  delete?: InputMaybe<Array<LinkToFeedbackFormWhereUniqueInput>>;
  /** Update multiple LinkToFeedbackForm component instances */
  update?: InputMaybe<
    Array<LinkToFeedbackFormUpdateWithNestedWhereUniqueAndPositionInput>
  >;
  /** Upsert multiple LinkToFeedbackForm component instances */
  upsert?: InputMaybe<
    Array<LinkToFeedbackFormUpsertWithNestedWhereUniqueAndPositionInput>
  >;
};

export type LinkToFeedbackFormUpdateManyInput = {
  label?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type LinkToFeedbackFormUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: LinkToFeedbackFormUpdateManyInput;
  /** Document search */
  where: LinkToFeedbackFormWhereInput;
};

export type LinkToFeedbackFormUpdateOneInlineInput = {
  /** Create and connect one LinkToFeedbackForm document */
  create?: InputMaybe<LinkToFeedbackFormCreateInput>;
  /** Delete currently connected LinkToFeedbackForm document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single LinkToFeedbackForm document */
  update?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
  /** Upsert single LinkToFeedbackForm document */
  upsert?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormUpdateWithNestedWhereUniqueAndPositionInput = {
  /** Document to update */
  data?: InputMaybe<LinkToFeedbackFormUpdateInput>;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Unique component instance search */
  where: LinkToFeedbackFormWhereUniqueInput;
};

export type LinkToFeedbackFormUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: LinkToFeedbackFormUpdateInput;
  /** Unique document search */
  where: LinkToFeedbackFormWhereUniqueInput;
};

export type LinkToFeedbackFormUpsertInput = {
  /** Create document if it didn't exist */
  create: LinkToFeedbackFormCreateInput;
  /** Update document if it exists */
  update: LinkToFeedbackFormUpdateInput;
};

export type LinkToFeedbackFormUpsertWithNestedWhereUniqueAndPositionInput = {
  /** Document to upsert */
  data?: InputMaybe<LinkToFeedbackFormUpsertInput>;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Unique component instance search */
  where: LinkToFeedbackFormWhereUniqueInput;
};

export type LinkToFeedbackFormUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: LinkToFeedbackFormUpsertInput;
  /** Unique document search */
  where: LinkToFeedbackFormWhereUniqueInput;
};

/** Identifies documents */
export type LinkToFeedbackFormWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<LinkToFeedbackFormWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  /** All values in which the union is empty. */
  anortherModular_empty?: InputMaybe<Scalars['Boolean']['input']>;
  /** Matches if the modular component contains at least one connection to the item provided to the filter */
  anortherModular_some?: InputMaybe<LinkToFeedbackFormanortherModularUnionWhereInput>;
  /** All values in which the modular component is connected to the given models */
  eeee?: InputMaybe<LinkToFeedbackFormeeeeUnionWhereInput>;
  /** All values in which the union is empty. */
  eeee_empty?: InputMaybe<Scalars['Boolean']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  url_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  url_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  url_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  url_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  url_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  url_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  url_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  url_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  url_starts_with?: InputMaybe<Scalars['String']['input']>;
};

/** References LinkToFeedbackForm record uniquely */
export type LinkToFeedbackFormWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type LinkToFeedbackFormanortherModularUnion = LinkToFeedbackForm;

export type LinkToFeedbackFormanortherModularUnionConnectInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormConnectInput>;
};

export type LinkToFeedbackFormanortherModularUnionCreateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateInput>;
};

export type LinkToFeedbackFormanortherModularUnionCreateManyInlineInput = {
  /** Create and connect multiple existing LinkToFeedbackFormanortherModularUnion documents */
  create?: InputMaybe<Array<LinkToFeedbackFormanortherModularUnionCreateInput>>;
};

export type LinkToFeedbackFormanortherModularUnionCreateOneInlineInput = {
  /** Create and connect one LinkToFeedbackFormanortherModularUnion document */
  create?: InputMaybe<LinkToFeedbackFormanortherModularUnionCreateInput>;
};

export type LinkToFeedbackFormanortherModularUnionCreateWithPositionInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateWithPositionInput>;
};

export type LinkToFeedbackFormanortherModularUnionUpdateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateInput>;
};

export type LinkToFeedbackFormanortherModularUnionUpdateManyInlineInput = {
  /** Create and connect multiple LinkToFeedbackFormanortherModularUnion component instances */
  create?: InputMaybe<
    Array<LinkToFeedbackFormanortherModularUnionCreateWithPositionInput>
  >;
  /** Delete multiple LinkToFeedbackFormanortherModularUnion documents */
  delete?: InputMaybe<
    Array<LinkToFeedbackFormanortherModularUnionWhereUniqueInput>
  >;
  /** Update multiple LinkToFeedbackFormanortherModularUnion component instances */
  update?: InputMaybe<
    Array<LinkToFeedbackFormanortherModularUnionUpdateWithNestedWhereUniqueAndPositionInput>
  >;
  /** Upsert multiple LinkToFeedbackFormanortherModularUnion component instances */
  upsert?: InputMaybe<
    Array<LinkToFeedbackFormanortherModularUnionUpsertWithNestedWhereUniqueAndPositionInput>
  >;
};

export type LinkToFeedbackFormanortherModularUnionUpdateManyWithNestedWhereInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateManyWithNestedWhereInput>;
  };

export type LinkToFeedbackFormanortherModularUnionUpdateOneInlineInput = {
  /** Create and connect one LinkToFeedbackFormanortherModularUnion document */
  create?: InputMaybe<LinkToFeedbackFormanortherModularUnionCreateInput>;
  /** Delete currently connected LinkToFeedbackFormanortherModularUnion document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single LinkToFeedbackFormanortherModularUnion document */
  update?: InputMaybe<LinkToFeedbackFormanortherModularUnionUpdateWithNestedWhereUniqueInput>;
  /** Upsert single LinkToFeedbackFormanortherModularUnion document */
  upsert?: InputMaybe<LinkToFeedbackFormanortherModularUnionUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormanortherModularUnionUpdateWithNestedWhereUniqueAndPositionInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueAndPositionInput>;
  };

export type LinkToFeedbackFormanortherModularUnionUpdateWithNestedWhereUniqueInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
  };

export type LinkToFeedbackFormanortherModularUnionUpsertWithNestedWhereUniqueAndPositionInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueAndPositionInput>;
  };

export type LinkToFeedbackFormanortherModularUnionUpsertWithNestedWhereUniqueInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
  };

export type LinkToFeedbackFormanortherModularUnionWhereInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
};

export type LinkToFeedbackFormanortherModularUnionWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereUniqueInput>;
};

export type LinkToFeedbackFormeeeeUnion = LinkToFeedbackForm | Wee;

export type LinkToFeedbackFormeeeeUnionConnectInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormConnectInput>;
  Wee?: InputMaybe<WeeConnectInput>;
};

export type LinkToFeedbackFormeeeeUnionCreateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateInput>;
  Wee?: InputMaybe<WeeCreateInput>;
};

export type LinkToFeedbackFormeeeeUnionCreateManyInlineInput = {
  /** Create and connect multiple existing LinkToFeedbackFormeeeeUnion documents */
  create?: InputMaybe<Array<LinkToFeedbackFormeeeeUnionCreateInput>>;
};

export type LinkToFeedbackFormeeeeUnionCreateOneInlineInput = {
  /** Create and connect one LinkToFeedbackFormeeeeUnion document */
  create?: InputMaybe<LinkToFeedbackFormeeeeUnionCreateInput>;
};

export type LinkToFeedbackFormeeeeUnionCreateWithPositionInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateWithPositionInput>;
  Wee?: InputMaybe<WeeCreateWithPositionInput>;
};

export type LinkToFeedbackFormeeeeUnionUpdateInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateInput>;
  Wee?: InputMaybe<WeeUpdateInput>;
};

export type LinkToFeedbackFormeeeeUnionUpdateManyInlineInput = {
  /** Create and connect multiple LinkToFeedbackFormeeeeUnion component instances */
  create?: InputMaybe<
    Array<LinkToFeedbackFormeeeeUnionCreateWithPositionInput>
  >;
  /** Delete multiple LinkToFeedbackFormeeeeUnion documents */
  delete?: InputMaybe<Array<LinkToFeedbackFormeeeeUnionWhereUniqueInput>>;
  /** Update multiple LinkToFeedbackFormeeeeUnion component instances */
  update?: InputMaybe<
    Array<LinkToFeedbackFormeeeeUnionUpdateWithNestedWhereUniqueAndPositionInput>
  >;
  /** Upsert multiple LinkToFeedbackFormeeeeUnion component instances */
  upsert?: InputMaybe<
    Array<LinkToFeedbackFormeeeeUnionUpsertWithNestedWhereUniqueAndPositionInput>
  >;
};

export type LinkToFeedbackFormeeeeUnionUpdateManyWithNestedWhereInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateManyWithNestedWhereInput>;
  Wee?: InputMaybe<WeeUpdateManyWithNestedWhereInput>;
};

export type LinkToFeedbackFormeeeeUnionUpdateOneInlineInput = {
  /** Create and connect one LinkToFeedbackFormeeeeUnion document */
  create?: InputMaybe<LinkToFeedbackFormeeeeUnionCreateInput>;
  /** Delete currently connected LinkToFeedbackFormeeeeUnion document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single LinkToFeedbackFormeeeeUnion document */
  update?: InputMaybe<LinkToFeedbackFormeeeeUnionUpdateWithNestedWhereUniqueInput>;
  /** Upsert single LinkToFeedbackFormeeeeUnion document */
  upsert?: InputMaybe<LinkToFeedbackFormeeeeUnionUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormeeeeUnionUpdateWithNestedWhereUniqueAndPositionInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueAndPositionInput>;
    Wee?: InputMaybe<WeeUpdateWithNestedWhereUniqueAndPositionInput>;
  };

export type LinkToFeedbackFormeeeeUnionUpdateWithNestedWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
  Wee?: InputMaybe<WeeUpdateWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormeeeeUnionUpsertWithNestedWhereUniqueAndPositionInput =
  {
    LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueAndPositionInput>;
    Wee?: InputMaybe<WeeUpsertWithNestedWhereUniqueAndPositionInput>;
  };

export type LinkToFeedbackFormeeeeUnionUpsertWithNestedWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
  Wee?: InputMaybe<WeeUpsertWithNestedWhereUniqueInput>;
};

export type LinkToFeedbackFormeeeeUnionWhereInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
  Wee?: InputMaybe<WeeWhereInput>;
};

export type LinkToFeedbackFormeeeeUnionWhereUniqueInput = {
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereUniqueInput>;
  Wee?: InputMaybe<WeeWhereUniqueInput>;
};

/** Locale system enumeration */
export enum Locale {
  /** System locale */
  Cz = 'cz',
  Sk = 'sk',
}

/** Representing a geolocation point with latitude and longitude */
export type Location = {
  __typename?: 'Location';
  distance: Scalars['Float']['output'];
  latitude: Scalars['Float']['output'];
  longitude: Scalars['Float']['output'];
};

/** Representing a geolocation point with latitude and longitude */
export type LocationDistanceArgs = {
  from: LocationInput;
};

/** Input for a geolocation point with latitude and longitude */
export type LocationInput = {
  latitude: Scalars['Float']['input'];
  longitude: Scalars['Float']['input'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /**
   * Create one asset
   * @deprecated Asset mutations will be overhauled soon
   */
  createAsset?: Maybe<Asset>;
  /** Create one component */
  createComponent?: Maybe<Component>;
  /** Create one contentPage */
  createContentPage?: Maybe<ContentPage>;
  /** Create one contentPageSection */
  createContentPageSection?: Maybe<ContentPageSection>;
  /** Create one contentPageSectionItem */
  createContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Create one contentPageSectionSponsor */
  createContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Create one dashboardBanner */
  createDashboardBanner?: Maybe<DashboardBanner>;
  /** Create one diaryScenario */
  createDiaryScenario?: Maybe<DiaryScenario>;
  /** Create one eewe */
  createEewe?: Maybe<Eewe>;
  /** Create one partner */
  createPartner?: Maybe<Partner>;
  /** Create one phase */
  createPhase?: Maybe<Phase>;
  /** Create one recommendedTask */
  createRecommendedTask?: Maybe<RecommendedTask>;
  /** Create one registrationPromoResult */
  createRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Create one scheduledRelease */
  createScheduledRelease?: Maybe<ScheduledRelease>;
  /** Create one specialOffer */
  createSpecialOffer?: Maybe<SpecialOffer>;
  /** Create one task */
  createTask?: Maybe<Task>;
  /** Create one video */
  createVideo?: Maybe<Video>;
  /** Create one videoLesson */
  createVideoLesson?: Maybe<VideoLesson>;
  /** Create one whitelabel */
  createWhitelabel?: Maybe<Whitelabel>;
  /** Delete one asset from _all_ existing stages. Returns deleted document. */
  deleteAsset?: Maybe<Asset>;
  /** Delete one component from _all_ existing stages. Returns deleted document. */
  deleteComponent?: Maybe<Component>;
  /** Delete one contentPage from _all_ existing stages. Returns deleted document. */
  deleteContentPage?: Maybe<ContentPage>;
  /** Delete one contentPageSection from _all_ existing stages. Returns deleted document. */
  deleteContentPageSection?: Maybe<ContentPageSection>;
  /** Delete one contentPageSectionItem from _all_ existing stages. Returns deleted document. */
  deleteContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Delete one contentPageSectionSponsor from _all_ existing stages. Returns deleted document. */
  deleteContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Delete one dashboardBanner from _all_ existing stages. Returns deleted document. */
  deleteDashboardBanner?: Maybe<DashboardBanner>;
  /** Delete one diaryScenario from _all_ existing stages. Returns deleted document. */
  deleteDiaryScenario?: Maybe<DiaryScenario>;
  /** Delete one eewe from _all_ existing stages. Returns deleted document. */
  deleteEewe?: Maybe<Eewe>;
  /**
   * Delete many Asset documents
   * @deprecated Please use the new paginated many mutation (deleteManyAssetsConnection)
   */
  deleteManyAssets: BatchPayload;
  /** Delete many Asset documents, return deleted documents */
  deleteManyAssetsConnection: AssetConnection;
  /**
   * Delete many Component documents
   * @deprecated Please use the new paginated many mutation (deleteManyComponentsConnection)
   */
  deleteManyComponents: BatchPayload;
  /** Delete many Component documents, return deleted documents */
  deleteManyComponentsConnection: ComponentConnection;
  /**
   * Delete many ContentPageSectionItem documents
   * @deprecated Please use the new paginated many mutation (deleteManyContentPageSectionItemsConnection)
   */
  deleteManyContentPageSectionItems: BatchPayload;
  /** Delete many ContentPageSectionItem documents, return deleted documents */
  deleteManyContentPageSectionItemsConnection: ContentPageSectionItemConnection;
  /**
   * Delete many ContentPageSectionSponsor documents
   * @deprecated Please use the new paginated many mutation (deleteManyContentPageSectionSponsorsConnection)
   */
  deleteManyContentPageSectionSponsors: BatchPayload;
  /** Delete many ContentPageSectionSponsor documents, return deleted documents */
  deleteManyContentPageSectionSponsorsConnection: ContentPageSectionSponsorConnection;
  /**
   * Delete many ContentPageSection documents
   * @deprecated Please use the new paginated many mutation (deleteManyContentPageSectionsConnection)
   */
  deleteManyContentPageSections: BatchPayload;
  /** Delete many ContentPageSection documents, return deleted documents */
  deleteManyContentPageSectionsConnection: ContentPageSectionConnection;
  /**
   * Delete many ContentPage documents
   * @deprecated Please use the new paginated many mutation (deleteManyContentPagesConnection)
   */
  deleteManyContentPages: BatchPayload;
  /** Delete many ContentPage documents, return deleted documents */
  deleteManyContentPagesConnection: ContentPageConnection;
  /**
   * Delete many DashboardBanner documents
   * @deprecated Please use the new paginated many mutation (deleteManyDashboardBannersConnection)
   */
  deleteManyDashboardBanners: BatchPayload;
  /** Delete many DashboardBanner documents, return deleted documents */
  deleteManyDashboardBannersConnection: DashboardBannerConnection;
  /**
   * Delete many DiaryScenario documents
   * @deprecated Please use the new paginated many mutation (deleteManyDiaryScenariosConnection)
   */
  deleteManyDiaryScenarios: BatchPayload;
  /** Delete many DiaryScenario documents, return deleted documents */
  deleteManyDiaryScenariosConnection: DiaryScenarioConnection;
  /**
   * Delete many Eewe documents
   * @deprecated Please use the new paginated many mutation (deleteManyEewesConnection)
   */
  deleteManyEewes: BatchPayload;
  /** Delete many Eewe documents, return deleted documents */
  deleteManyEewesConnection: EeweConnection;
  /**
   * Delete many Partner documents
   * @deprecated Please use the new paginated many mutation (deleteManyPartnersConnection)
   */
  deleteManyPartners: BatchPayload;
  /** Delete many Partner documents, return deleted documents */
  deleteManyPartnersConnection: PartnerConnection;
  /**
   * Delete many Phase documents
   * @deprecated Please use the new paginated many mutation (deleteManyPhasesConnection)
   */
  deleteManyPhases: BatchPayload;
  /** Delete many Phase documents, return deleted documents */
  deleteManyPhasesConnection: PhaseConnection;
  /**
   * Delete many RecommendedTask documents
   * @deprecated Please use the new paginated many mutation (deleteManyRecommendedTasksConnection)
   */
  deleteManyRecommendedTasks: BatchPayload;
  /** Delete many RecommendedTask documents, return deleted documents */
  deleteManyRecommendedTasksConnection: RecommendedTaskConnection;
  /**
   * Delete many RegistrationPromoResult documents
   * @deprecated Please use the new paginated many mutation (deleteManyRegistrationPromoResultsConnection)
   */
  deleteManyRegistrationPromoResults: BatchPayload;
  /** Delete many RegistrationPromoResult documents, return deleted documents */
  deleteManyRegistrationPromoResultsConnection: RegistrationPromoResultConnection;
  /**
   * Delete many SpecialOffer documents
   * @deprecated Please use the new paginated many mutation (deleteManySpecialOffersConnection)
   */
  deleteManySpecialOffers: BatchPayload;
  /** Delete many SpecialOffer documents, return deleted documents */
  deleteManySpecialOffersConnection: SpecialOfferConnection;
  /**
   * Delete many Task documents
   * @deprecated Please use the new paginated many mutation (deleteManyTasksConnection)
   */
  deleteManyTasks: BatchPayload;
  /** Delete many Task documents, return deleted documents */
  deleteManyTasksConnection: TaskConnection;
  /**
   * Delete many VideoLesson documents
   * @deprecated Please use the new paginated many mutation (deleteManyVideoLessonsConnection)
   */
  deleteManyVideoLessons: BatchPayload;
  /** Delete many VideoLesson documents, return deleted documents */
  deleteManyVideoLessonsConnection: VideoLessonConnection;
  /**
   * Delete many Video documents
   * @deprecated Please use the new paginated many mutation (deleteManyVideosConnection)
   */
  deleteManyVideos: BatchPayload;
  /** Delete many Video documents, return deleted documents */
  deleteManyVideosConnection: VideoConnection;
  /**
   * Delete many Whitelabel documents
   * @deprecated Please use the new paginated many mutation (deleteManyWhitelabelsConnection)
   */
  deleteManyWhitelabels: BatchPayload;
  /** Delete many Whitelabel documents, return deleted documents */
  deleteManyWhitelabelsConnection: WhitelabelConnection;
  /** Delete one partner from _all_ existing stages. Returns deleted document. */
  deletePartner?: Maybe<Partner>;
  /** Delete one phase from _all_ existing stages. Returns deleted document. */
  deletePhase?: Maybe<Phase>;
  /** Delete one recommendedTask from _all_ existing stages. Returns deleted document. */
  deleteRecommendedTask?: Maybe<RecommendedTask>;
  /** Delete one registrationPromoResult from _all_ existing stages. Returns deleted document. */
  deleteRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Delete and return scheduled operation */
  deleteScheduledOperation?: Maybe<ScheduledOperation>;
  /** Delete one scheduledRelease from _all_ existing stages. Returns deleted document. */
  deleteScheduledRelease?: Maybe<ScheduledRelease>;
  /** Delete one specialOffer from _all_ existing stages. Returns deleted document. */
  deleteSpecialOffer?: Maybe<SpecialOffer>;
  /** Delete one task from _all_ existing stages. Returns deleted document. */
  deleteTask?: Maybe<Task>;
  /** Delete one video from _all_ existing stages. Returns deleted document. */
  deleteVideo?: Maybe<Video>;
  /** Delete one videoLesson from _all_ existing stages. Returns deleted document. */
  deleteVideoLesson?: Maybe<VideoLesson>;
  /** Delete one whitelabel from _all_ existing stages. Returns deleted document. */
  deleteWhitelabel?: Maybe<Whitelabel>;
  /** Publish one asset */
  publishAsset?: Maybe<Asset>;
  /** Publish one component */
  publishComponent?: Maybe<Component>;
  /** Publish one contentPage */
  publishContentPage?: Maybe<ContentPage>;
  /** Publish one contentPageSection */
  publishContentPageSection?: Maybe<ContentPageSection>;
  /** Publish one contentPageSectionItem */
  publishContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Publish one contentPageSectionSponsor */
  publishContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Publish one dashboardBanner */
  publishDashboardBanner?: Maybe<DashboardBanner>;
  /** Publish one diaryScenario */
  publishDiaryScenario?: Maybe<DiaryScenario>;
  /** Publish one eewe */
  publishEewe?: Maybe<Eewe>;
  /**
   * Publish many Asset documents
   * @deprecated Please use the new paginated many mutation (publishManyAssetsConnection)
   */
  publishManyAssets: BatchPayload;
  /** Publish many Asset documents */
  publishManyAssetsConnection: AssetConnection;
  /**
   * Publish many Component documents
   * @deprecated Please use the new paginated many mutation (publishManyComponentsConnection)
   */
  publishManyComponents: BatchPayload;
  /** Publish many Component documents */
  publishManyComponentsConnection: ComponentConnection;
  /**
   * Publish many ContentPageSectionItem documents
   * @deprecated Please use the new paginated many mutation (publishManyContentPageSectionItemsConnection)
   */
  publishManyContentPageSectionItems: BatchPayload;
  /** Publish many ContentPageSectionItem documents */
  publishManyContentPageSectionItemsConnection: ContentPageSectionItemConnection;
  /**
   * Publish many ContentPageSectionSponsor documents
   * @deprecated Please use the new paginated many mutation (publishManyContentPageSectionSponsorsConnection)
   */
  publishManyContentPageSectionSponsors: BatchPayload;
  /** Publish many ContentPageSectionSponsor documents */
  publishManyContentPageSectionSponsorsConnection: ContentPageSectionSponsorConnection;
  /**
   * Publish many ContentPageSection documents
   * @deprecated Please use the new paginated many mutation (publishManyContentPageSectionsConnection)
   */
  publishManyContentPageSections: BatchPayload;
  /** Publish many ContentPageSection documents */
  publishManyContentPageSectionsConnection: ContentPageSectionConnection;
  /**
   * Publish many ContentPage documents
   * @deprecated Please use the new paginated many mutation (publishManyContentPagesConnection)
   */
  publishManyContentPages: BatchPayload;
  /** Publish many ContentPage documents */
  publishManyContentPagesConnection: ContentPageConnection;
  /**
   * Publish many DashboardBanner documents
   * @deprecated Please use the new paginated many mutation (publishManyDashboardBannersConnection)
   */
  publishManyDashboardBanners: BatchPayload;
  /** Publish many DashboardBanner documents */
  publishManyDashboardBannersConnection: DashboardBannerConnection;
  /**
   * Publish many DiaryScenario documents
   * @deprecated Please use the new paginated many mutation (publishManyDiaryScenariosConnection)
   */
  publishManyDiaryScenarios: BatchPayload;
  /** Publish many DiaryScenario documents */
  publishManyDiaryScenariosConnection: DiaryScenarioConnection;
  /**
   * Publish many Eewe documents
   * @deprecated Please use the new paginated many mutation (publishManyEewesConnection)
   */
  publishManyEewes: BatchPayload;
  /** Publish many Eewe documents */
  publishManyEewesConnection: EeweConnection;
  /**
   * Publish many Partner documents
   * @deprecated Please use the new paginated many mutation (publishManyPartnersConnection)
   */
  publishManyPartners: BatchPayload;
  /** Publish many Partner documents */
  publishManyPartnersConnection: PartnerConnection;
  /**
   * Publish many Phase documents
   * @deprecated Please use the new paginated many mutation (publishManyPhasesConnection)
   */
  publishManyPhases: BatchPayload;
  /** Publish many Phase documents */
  publishManyPhasesConnection: PhaseConnection;
  /**
   * Publish many RecommendedTask documents
   * @deprecated Please use the new paginated many mutation (publishManyRecommendedTasksConnection)
   */
  publishManyRecommendedTasks: BatchPayload;
  /** Publish many RecommendedTask documents */
  publishManyRecommendedTasksConnection: RecommendedTaskConnection;
  /**
   * Publish many RegistrationPromoResult documents
   * @deprecated Please use the new paginated many mutation (publishManyRegistrationPromoResultsConnection)
   */
  publishManyRegistrationPromoResults: BatchPayload;
  /** Publish many RegistrationPromoResult documents */
  publishManyRegistrationPromoResultsConnection: RegistrationPromoResultConnection;
  /**
   * Publish many SpecialOffer documents
   * @deprecated Please use the new paginated many mutation (publishManySpecialOffersConnection)
   */
  publishManySpecialOffers: BatchPayload;
  /** Publish many SpecialOffer documents */
  publishManySpecialOffersConnection: SpecialOfferConnection;
  /**
   * Publish many Task documents
   * @deprecated Please use the new paginated many mutation (publishManyTasksConnection)
   */
  publishManyTasks: BatchPayload;
  /** Publish many Task documents */
  publishManyTasksConnection: TaskConnection;
  /**
   * Publish many VideoLesson documents
   * @deprecated Please use the new paginated many mutation (publishManyVideoLessonsConnection)
   */
  publishManyVideoLessons: BatchPayload;
  /** Publish many VideoLesson documents */
  publishManyVideoLessonsConnection: VideoLessonConnection;
  /**
   * Publish many Video documents
   * @deprecated Please use the new paginated many mutation (publishManyVideosConnection)
   */
  publishManyVideos: BatchPayload;
  /** Publish many Video documents */
  publishManyVideosConnection: VideoConnection;
  /**
   * Publish many Whitelabel documents
   * @deprecated Please use the new paginated many mutation (publishManyWhitelabelsConnection)
   */
  publishManyWhitelabels: BatchPayload;
  /** Publish many Whitelabel documents */
  publishManyWhitelabelsConnection: WhitelabelConnection;
  /** Publish one partner */
  publishPartner?: Maybe<Partner>;
  /** Publish one phase */
  publishPhase?: Maybe<Phase>;
  /** Publish one recommendedTask */
  publishRecommendedTask?: Maybe<RecommendedTask>;
  /** Publish one registrationPromoResult */
  publishRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Publish one specialOffer */
  publishSpecialOffer?: Maybe<SpecialOffer>;
  /** Publish one task */
  publishTask?: Maybe<Task>;
  /** Publish one video */
  publishVideo?: Maybe<Video>;
  /** Publish one videoLesson */
  publishVideoLesson?: Maybe<VideoLesson>;
  /** Publish one whitelabel */
  publishWhitelabel?: Maybe<Whitelabel>;
  /** Schedule to publish one asset */
  schedulePublishAsset?: Maybe<Asset>;
  /** Schedule to publish one component */
  schedulePublishComponent?: Maybe<Component>;
  /** Schedule to publish one contentPage */
  schedulePublishContentPage?: Maybe<ContentPage>;
  /** Schedule to publish one contentPageSection */
  schedulePublishContentPageSection?: Maybe<ContentPageSection>;
  /** Schedule to publish one contentPageSectionItem */
  schedulePublishContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Schedule to publish one contentPageSectionSponsor */
  schedulePublishContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Schedule to publish one dashboardBanner */
  schedulePublishDashboardBanner?: Maybe<DashboardBanner>;
  /** Schedule to publish one diaryScenario */
  schedulePublishDiaryScenario?: Maybe<DiaryScenario>;
  /** Schedule to publish one eewe */
  schedulePublishEewe?: Maybe<Eewe>;
  /** Schedule to publish one partner */
  schedulePublishPartner?: Maybe<Partner>;
  /** Schedule to publish one phase */
  schedulePublishPhase?: Maybe<Phase>;
  /** Schedule to publish one recommendedTask */
  schedulePublishRecommendedTask?: Maybe<RecommendedTask>;
  /** Schedule to publish one registrationPromoResult */
  schedulePublishRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Schedule to publish one specialOffer */
  schedulePublishSpecialOffer?: Maybe<SpecialOffer>;
  /** Schedule to publish one task */
  schedulePublishTask?: Maybe<Task>;
  /** Schedule to publish one video */
  schedulePublishVideo?: Maybe<Video>;
  /** Schedule to publish one videoLesson */
  schedulePublishVideoLesson?: Maybe<VideoLesson>;
  /** Schedule to publish one whitelabel */
  schedulePublishWhitelabel?: Maybe<Whitelabel>;
  /** Unpublish one asset from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishAsset?: Maybe<Asset>;
  /** Unpublish one component from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishComponent?: Maybe<Component>;
  /** Unpublish one contentPage from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishContentPage?: Maybe<ContentPage>;
  /** Unpublish one contentPageSection from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishContentPageSection?: Maybe<ContentPageSection>;
  /** Unpublish one contentPageSectionItem from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Unpublish one contentPageSectionSponsor from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Unpublish one dashboardBanner from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishDashboardBanner?: Maybe<DashboardBanner>;
  /** Unpublish one diaryScenario from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishDiaryScenario?: Maybe<DiaryScenario>;
  /** Unpublish one eewe from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishEewe?: Maybe<Eewe>;
  /** Unpublish one partner from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishPartner?: Maybe<Partner>;
  /** Unpublish one phase from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishPhase?: Maybe<Phase>;
  /** Unpublish one recommendedTask from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishRecommendedTask?: Maybe<RecommendedTask>;
  /** Unpublish one registrationPromoResult from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Unpublish one specialOffer from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishSpecialOffer?: Maybe<SpecialOffer>;
  /** Unpublish one task from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishTask?: Maybe<Task>;
  /** Unpublish one video from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishVideo?: Maybe<Video>;
  /** Unpublish one videoLesson from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishVideoLesson?: Maybe<VideoLesson>;
  /** Unpublish one whitelabel from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  scheduleUnpublishWhitelabel?: Maybe<Whitelabel>;
  /** Unpublish one asset from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishAsset?: Maybe<Asset>;
  /** Unpublish one component from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishComponent?: Maybe<Component>;
  /** Unpublish one contentPage from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishContentPage?: Maybe<ContentPage>;
  /** Unpublish one contentPageSection from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishContentPageSection?: Maybe<ContentPageSection>;
  /** Unpublish one contentPageSectionItem from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Unpublish one contentPageSectionSponsor from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Unpublish one dashboardBanner from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishDashboardBanner?: Maybe<DashboardBanner>;
  /** Unpublish one diaryScenario from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishDiaryScenario?: Maybe<DiaryScenario>;
  /** Unpublish one eewe from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishEewe?: Maybe<Eewe>;
  /**
   * Unpublish many Asset documents
   * @deprecated Please use the new paginated many mutation (unpublishManyAssetsConnection)
   */
  unpublishManyAssets: BatchPayload;
  /** Find many Asset documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyAssetsConnection: AssetConnection;
  /**
   * Unpublish many Component documents
   * @deprecated Please use the new paginated many mutation (unpublishManyComponentsConnection)
   */
  unpublishManyComponents: BatchPayload;
  /** Find many Component documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyComponentsConnection: ComponentConnection;
  /**
   * Unpublish many ContentPageSectionItem documents
   * @deprecated Please use the new paginated many mutation (unpublishManyContentPageSectionItemsConnection)
   */
  unpublishManyContentPageSectionItems: BatchPayload;
  /** Find many ContentPageSectionItem documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyContentPageSectionItemsConnection: ContentPageSectionItemConnection;
  /**
   * Unpublish many ContentPageSectionSponsor documents
   * @deprecated Please use the new paginated many mutation (unpublishManyContentPageSectionSponsorsConnection)
   */
  unpublishManyContentPageSectionSponsors: BatchPayload;
  /** Find many ContentPageSectionSponsor documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyContentPageSectionSponsorsConnection: ContentPageSectionSponsorConnection;
  /**
   * Unpublish many ContentPageSection documents
   * @deprecated Please use the new paginated many mutation (unpublishManyContentPageSectionsConnection)
   */
  unpublishManyContentPageSections: BatchPayload;
  /** Find many ContentPageSection documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyContentPageSectionsConnection: ContentPageSectionConnection;
  /**
   * Unpublish many ContentPage documents
   * @deprecated Please use the new paginated many mutation (unpublishManyContentPagesConnection)
   */
  unpublishManyContentPages: BatchPayload;
  /** Find many ContentPage documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyContentPagesConnection: ContentPageConnection;
  /**
   * Unpublish many DashboardBanner documents
   * @deprecated Please use the new paginated many mutation (unpublishManyDashboardBannersConnection)
   */
  unpublishManyDashboardBanners: BatchPayload;
  /** Find many DashboardBanner documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyDashboardBannersConnection: DashboardBannerConnection;
  /**
   * Unpublish many DiaryScenario documents
   * @deprecated Please use the new paginated many mutation (unpublishManyDiaryScenariosConnection)
   */
  unpublishManyDiaryScenarios: BatchPayload;
  /** Find many DiaryScenario documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyDiaryScenariosConnection: DiaryScenarioConnection;
  /**
   * Unpublish many Eewe documents
   * @deprecated Please use the new paginated many mutation (unpublishManyEewesConnection)
   */
  unpublishManyEewes: BatchPayload;
  /** Find many Eewe documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyEewesConnection: EeweConnection;
  /**
   * Unpublish many Partner documents
   * @deprecated Please use the new paginated many mutation (unpublishManyPartnersConnection)
   */
  unpublishManyPartners: BatchPayload;
  /** Find many Partner documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyPartnersConnection: PartnerConnection;
  /**
   * Unpublish many Phase documents
   * @deprecated Please use the new paginated many mutation (unpublishManyPhasesConnection)
   */
  unpublishManyPhases: BatchPayload;
  /** Find many Phase documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyPhasesConnection: PhaseConnection;
  /**
   * Unpublish many RecommendedTask documents
   * @deprecated Please use the new paginated many mutation (unpublishManyRecommendedTasksConnection)
   */
  unpublishManyRecommendedTasks: BatchPayload;
  /** Find many RecommendedTask documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyRecommendedTasksConnection: RecommendedTaskConnection;
  /**
   * Unpublish many RegistrationPromoResult documents
   * @deprecated Please use the new paginated many mutation (unpublishManyRegistrationPromoResultsConnection)
   */
  unpublishManyRegistrationPromoResults: BatchPayload;
  /** Find many RegistrationPromoResult documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyRegistrationPromoResultsConnection: RegistrationPromoResultConnection;
  /**
   * Unpublish many SpecialOffer documents
   * @deprecated Please use the new paginated many mutation (unpublishManySpecialOffersConnection)
   */
  unpublishManySpecialOffers: BatchPayload;
  /** Find many SpecialOffer documents that match criteria in specified stage and unpublish from target stages */
  unpublishManySpecialOffersConnection: SpecialOfferConnection;
  /**
   * Unpublish many Task documents
   * @deprecated Please use the new paginated many mutation (unpublishManyTasksConnection)
   */
  unpublishManyTasks: BatchPayload;
  /** Find many Task documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyTasksConnection: TaskConnection;
  /**
   * Unpublish many VideoLesson documents
   * @deprecated Please use the new paginated many mutation (unpublishManyVideoLessonsConnection)
   */
  unpublishManyVideoLessons: BatchPayload;
  /** Find many VideoLesson documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyVideoLessonsConnection: VideoLessonConnection;
  /**
   * Unpublish many Video documents
   * @deprecated Please use the new paginated many mutation (unpublishManyVideosConnection)
   */
  unpublishManyVideos: BatchPayload;
  /** Find many Video documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyVideosConnection: VideoConnection;
  /**
   * Unpublish many Whitelabel documents
   * @deprecated Please use the new paginated many mutation (unpublishManyWhitelabelsConnection)
   */
  unpublishManyWhitelabels: BatchPayload;
  /** Find many Whitelabel documents that match criteria in specified stage and unpublish from target stages */
  unpublishManyWhitelabelsConnection: WhitelabelConnection;
  /** Unpublish one partner from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishPartner?: Maybe<Partner>;
  /** Unpublish one phase from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishPhase?: Maybe<Phase>;
  /** Unpublish one recommendedTask from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishRecommendedTask?: Maybe<RecommendedTask>;
  /** Unpublish one registrationPromoResult from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Unpublish one specialOffer from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishSpecialOffer?: Maybe<SpecialOffer>;
  /** Unpublish one task from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishTask?: Maybe<Task>;
  /** Unpublish one video from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishVideo?: Maybe<Video>;
  /** Unpublish one videoLesson from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishVideoLesson?: Maybe<VideoLesson>;
  /** Unpublish one whitelabel from selected stages. Unpublish either the complete document with its relations, localizations and base data or specific localizations only. */
  unpublishWhitelabel?: Maybe<Whitelabel>;
  /** Update one asset */
  updateAsset?: Maybe<Asset>;
  /** Update one component */
  updateComponent?: Maybe<Component>;
  /** Update one contentPage */
  updateContentPage?: Maybe<ContentPage>;
  /** Update one contentPageSection */
  updateContentPageSection?: Maybe<ContentPageSection>;
  /** Update one contentPageSectionItem */
  updateContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Update one contentPageSectionSponsor */
  updateContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Update one dashboardBanner */
  updateDashboardBanner?: Maybe<DashboardBanner>;
  /** Update one diaryScenario */
  updateDiaryScenario?: Maybe<DiaryScenario>;
  /** Update one eewe */
  updateEewe?: Maybe<Eewe>;
  /**
   * Update many assets
   * @deprecated Please use the new paginated many mutation (updateManyAssetsConnection)
   */
  updateManyAssets: BatchPayload;
  /** Update many Asset documents */
  updateManyAssetsConnection: AssetConnection;
  /**
   * Update many components
   * @deprecated Please use the new paginated many mutation (updateManyComponentsConnection)
   */
  updateManyComponents: BatchPayload;
  /** Update many Component documents */
  updateManyComponentsConnection: ComponentConnection;
  /**
   * Update many contentPageSectionItems
   * @deprecated Please use the new paginated many mutation (updateManyContentPageSectionItemsConnection)
   */
  updateManyContentPageSectionItems: BatchPayload;
  /** Update many ContentPageSectionItem documents */
  updateManyContentPageSectionItemsConnection: ContentPageSectionItemConnection;
  /**
   * Update many contentPageSectionSponsors
   * @deprecated Please use the new paginated many mutation (updateManyContentPageSectionSponsorsConnection)
   */
  updateManyContentPageSectionSponsors: BatchPayload;
  /** Update many ContentPageSectionSponsor documents */
  updateManyContentPageSectionSponsorsConnection: ContentPageSectionSponsorConnection;
  /**
   * Update many contentPageSections
   * @deprecated Please use the new paginated many mutation (updateManyContentPageSectionsConnection)
   */
  updateManyContentPageSections: BatchPayload;
  /** Update many ContentPageSection documents */
  updateManyContentPageSectionsConnection: ContentPageSectionConnection;
  /**
   * Update many contentPages
   * @deprecated Please use the new paginated many mutation (updateManyContentPagesConnection)
   */
  updateManyContentPages: BatchPayload;
  /** Update many ContentPage documents */
  updateManyContentPagesConnection: ContentPageConnection;
  /**
   * Update many dashboardBanners
   * @deprecated Please use the new paginated many mutation (updateManyDashboardBannersConnection)
   */
  updateManyDashboardBanners: BatchPayload;
  /** Update many DashboardBanner documents */
  updateManyDashboardBannersConnection: DashboardBannerConnection;
  /**
   * Update many diaryScenarios
   * @deprecated Please use the new paginated many mutation (updateManyDiaryScenariosConnection)
   */
  updateManyDiaryScenarios: BatchPayload;
  /** Update many DiaryScenario documents */
  updateManyDiaryScenariosConnection: DiaryScenarioConnection;
  /**
   * Update many eewes
   * @deprecated Please use the new paginated many mutation (updateManyEewesConnection)
   */
  updateManyEewes: BatchPayload;
  /** Update many Eewe documents */
  updateManyEewesConnection: EeweConnection;
  /**
   * Update many partners
   * @deprecated Please use the new paginated many mutation (updateManyPartnersConnection)
   */
  updateManyPartners: BatchPayload;
  /** Update many Partner documents */
  updateManyPartnersConnection: PartnerConnection;
  /**
   * Update many phases
   * @deprecated Please use the new paginated many mutation (updateManyPhasesConnection)
   */
  updateManyPhases: BatchPayload;
  /** Update many Phase documents */
  updateManyPhasesConnection: PhaseConnection;
  /**
   * Update many recommendedTasks
   * @deprecated Please use the new paginated many mutation (updateManyRecommendedTasksConnection)
   */
  updateManyRecommendedTasks: BatchPayload;
  /** Update many RecommendedTask documents */
  updateManyRecommendedTasksConnection: RecommendedTaskConnection;
  /**
   * Update many registrationPromoResults
   * @deprecated Please use the new paginated many mutation (updateManyRegistrationPromoResultsConnection)
   */
  updateManyRegistrationPromoResults: BatchPayload;
  /** Update many RegistrationPromoResult documents */
  updateManyRegistrationPromoResultsConnection: RegistrationPromoResultConnection;
  /**
   * Update many specialOffers
   * @deprecated Please use the new paginated many mutation (updateManySpecialOffersConnection)
   */
  updateManySpecialOffers: BatchPayload;
  /** Update many SpecialOffer documents */
  updateManySpecialOffersConnection: SpecialOfferConnection;
  /**
   * Update many tasks
   * @deprecated Please use the new paginated many mutation (updateManyTasksConnection)
   */
  updateManyTasks: BatchPayload;
  /** Update many Task documents */
  updateManyTasksConnection: TaskConnection;
  /**
   * Update many videoLessons
   * @deprecated Please use the new paginated many mutation (updateManyVideoLessonsConnection)
   */
  updateManyVideoLessons: BatchPayload;
  /** Update many VideoLesson documents */
  updateManyVideoLessonsConnection: VideoLessonConnection;
  /**
   * Update many videos
   * @deprecated Please use the new paginated many mutation (updateManyVideosConnection)
   */
  updateManyVideos: BatchPayload;
  /** Update many Video documents */
  updateManyVideosConnection: VideoConnection;
  /**
   * Update many whitelabels
   * @deprecated Please use the new paginated many mutation (updateManyWhitelabelsConnection)
   */
  updateManyWhitelabels: BatchPayload;
  /** Update many Whitelabel documents */
  updateManyWhitelabelsConnection: WhitelabelConnection;
  /** Update one partner */
  updatePartner?: Maybe<Partner>;
  /** Update one phase */
  updatePhase?: Maybe<Phase>;
  /** Update one recommendedTask */
  updateRecommendedTask?: Maybe<RecommendedTask>;
  /** Update one registrationPromoResult */
  updateRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Update one scheduledRelease */
  updateScheduledRelease?: Maybe<ScheduledRelease>;
  /** Update one specialOffer */
  updateSpecialOffer?: Maybe<SpecialOffer>;
  /** Update one task */
  updateTask?: Maybe<Task>;
  /** Update one video */
  updateVideo?: Maybe<Video>;
  /** Update one videoLesson */
  updateVideoLesson?: Maybe<VideoLesson>;
  /** Update one whitelabel */
  updateWhitelabel?: Maybe<Whitelabel>;
  /** Upsert one asset */
  upsertAsset?: Maybe<Asset>;
  /** Upsert one component */
  upsertComponent?: Maybe<Component>;
  /** Upsert one contentPage */
  upsertContentPage?: Maybe<ContentPage>;
  /** Upsert one contentPageSection */
  upsertContentPageSection?: Maybe<ContentPageSection>;
  /** Upsert one contentPageSectionItem */
  upsertContentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Upsert one contentPageSectionSponsor */
  upsertContentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Upsert one dashboardBanner */
  upsertDashboardBanner?: Maybe<DashboardBanner>;
  /** Upsert one diaryScenario */
  upsertDiaryScenario?: Maybe<DiaryScenario>;
  /** Upsert one eewe */
  upsertEewe?: Maybe<Eewe>;
  /** Upsert one partner */
  upsertPartner?: Maybe<Partner>;
  /** Upsert one phase */
  upsertPhase?: Maybe<Phase>;
  /** Upsert one recommendedTask */
  upsertRecommendedTask?: Maybe<RecommendedTask>;
  /** Upsert one registrationPromoResult */
  upsertRegistrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Upsert one specialOffer */
  upsertSpecialOffer?: Maybe<SpecialOffer>;
  /** Upsert one task */
  upsertTask?: Maybe<Task>;
  /** Upsert one video */
  upsertVideo?: Maybe<Video>;
  /** Upsert one videoLesson */
  upsertVideoLesson?: Maybe<VideoLesson>;
  /** Upsert one whitelabel */
  upsertWhitelabel?: Maybe<Whitelabel>;
};

export type MutationCreateAssetArgs = {
  data: AssetCreateInput;
};

export type MutationCreateComponentArgs = {
  data: ComponentCreateInput;
};

export type MutationCreateContentPageArgs = {
  data: ContentPageCreateInput;
};

export type MutationCreateContentPageSectionArgs = {
  data: ContentPageSectionCreateInput;
};

export type MutationCreateContentPageSectionItemArgs = {
  data: ContentPageSectionItemCreateInput;
};

export type MutationCreateContentPageSectionSponsorArgs = {
  data: ContentPageSectionSponsorCreateInput;
};

export type MutationCreateDashboardBannerArgs = {
  data: DashboardBannerCreateInput;
};

export type MutationCreateDiaryScenarioArgs = {
  data: DiaryScenarioCreateInput;
};

export type MutationCreateEeweArgs = {
  data: EeweCreateInput;
};

export type MutationCreatePartnerArgs = {
  data: PartnerCreateInput;
};

export type MutationCreatePhaseArgs = {
  data: PhaseCreateInput;
};

export type MutationCreateRecommendedTaskArgs = {
  data: RecommendedTaskCreateInput;
};

export type MutationCreateRegistrationPromoResultArgs = {
  data: RegistrationPromoResultCreateInput;
};

export type MutationCreateScheduledReleaseArgs = {
  data: ScheduledReleaseCreateInput;
};

export type MutationCreateSpecialOfferArgs = {
  data: SpecialOfferCreateInput;
};

export type MutationCreateTaskArgs = {
  data: TaskCreateInput;
};

export type MutationCreateVideoArgs = {
  data: VideoCreateInput;
};

export type MutationCreateVideoLessonArgs = {
  data: VideoLessonCreateInput;
};

export type MutationCreateWhitelabelArgs = {
  data: WhitelabelCreateInput;
};

export type MutationDeleteAssetArgs = {
  where: AssetWhereUniqueInput;
};

export type MutationDeleteComponentArgs = {
  where: ComponentWhereUniqueInput;
};

export type MutationDeleteContentPageArgs = {
  where: ContentPageWhereUniqueInput;
};

export type MutationDeleteContentPageSectionArgs = {
  where: ContentPageSectionWhereUniqueInput;
};

export type MutationDeleteContentPageSectionItemArgs = {
  where: ContentPageSectionItemWhereUniqueInput;
};

export type MutationDeleteContentPageSectionSponsorArgs = {
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type MutationDeleteDashboardBannerArgs = {
  where: DashboardBannerWhereUniqueInput;
};

export type MutationDeleteDiaryScenarioArgs = {
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationDeleteEeweArgs = {
  where: EeweWhereUniqueInput;
};

export type MutationDeleteManyAssetsArgs = {
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationDeleteManyAssetsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationDeleteManyComponentsArgs = {
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationDeleteManyComponentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionItemsArgs = {
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionSponsorsArgs = {
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionSponsorsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionsArgs = {
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationDeleteManyContentPageSectionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationDeleteManyContentPagesArgs = {
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationDeleteManyContentPagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationDeleteManyDashboardBannersArgs = {
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationDeleteManyDashboardBannersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationDeleteManyDiaryScenariosArgs = {
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationDeleteManyDiaryScenariosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationDeleteManyEewesArgs = {
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationDeleteManyEewesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationDeleteManyPartnersArgs = {
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationDeleteManyPartnersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationDeleteManyPhasesArgs = {
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationDeleteManyPhasesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationDeleteManyRecommendedTasksArgs = {
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationDeleteManyRecommendedTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationDeleteManyRegistrationPromoResultsArgs = {
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationDeleteManyRegistrationPromoResultsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationDeleteManySpecialOffersArgs = {
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationDeleteManySpecialOffersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationDeleteManyTasksArgs = {
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationDeleteManyTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationDeleteManyVideoLessonsArgs = {
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationDeleteManyVideoLessonsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationDeleteManyVideosArgs = {
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationDeleteManyVideosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationDeleteManyWhitelabelsArgs = {
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationDeleteManyWhitelabelsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationDeletePartnerArgs = {
  where: PartnerWhereUniqueInput;
};

export type MutationDeletePhaseArgs = {
  where: PhaseWhereUniqueInput;
};

export type MutationDeleteRecommendedTaskArgs = {
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationDeleteRegistrationPromoResultArgs = {
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationDeleteScheduledOperationArgs = {
  where: ScheduledOperationWhereUniqueInput;
};

export type MutationDeleteScheduledReleaseArgs = {
  where: ScheduledReleaseWhereUniqueInput;
};

export type MutationDeleteSpecialOfferArgs = {
  where: SpecialOfferWhereUniqueInput;
};

export type MutationDeleteTaskArgs = {
  where: TaskWhereUniqueInput;
};

export type MutationDeleteVideoArgs = {
  where: VideoWhereUniqueInput;
};

export type MutationDeleteVideoLessonArgs = {
  where: VideoLessonWhereUniqueInput;
};

export type MutationDeleteWhitelabelArgs = {
  where: WhitelabelWhereUniqueInput;
};

export type MutationPublishAssetArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: AssetWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishComponentArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: ComponentWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishContentPageArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: ContentPageWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishContentPageSectionArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishContentPageSectionItemArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionItemWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishContentPageSectionSponsorArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionSponsorWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishDashboardBannerArgs = {
  to?: Array<Stage>;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationPublishDiaryScenarioArgs = {
  to?: Array<Stage>;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationPublishEeweArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: EeweWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyAssetsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<AssetManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyAssetsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<AssetManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyComponentsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ComponentManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyComponentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ComponentManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionItemsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionSponsorsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionSponsorsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPageSectionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPagesArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyContentPagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<ContentPageManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyDashboardBannersArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationPublishManyDashboardBannersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationPublishManyDiaryScenariosArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationPublishManyDiaryScenariosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationPublishManyEewesArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<EeweManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyEewesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<EeweManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyPartnersArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationPublishManyPartnersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationPublishManyPhasesArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<PhaseManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyPhasesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<PhaseManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyRecommendedTasksArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationPublishManyRecommendedTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationPublishManyRegistrationPromoResultsArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationPublishManyRegistrationPromoResultsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationPublishManySpecialOffersArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationPublishManySpecialOffersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationPublishManyTasksArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<TaskManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<TaskManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyVideoLessonsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyVideoLessonsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyVideosArgs = {
  to?: Array<Stage>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationPublishManyVideosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationPublishManyWhitelabelsArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishManyWhitelabelsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: InputMaybe<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  to?: Array<Stage>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishPartnerArgs = {
  to?: Array<Stage>;
  where: PartnerWhereUniqueInput;
};

export type MutationPublishPhaseArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: PhaseWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishRecommendedTaskArgs = {
  to?: Array<Stage>;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationPublishRegistrationPromoResultArgs = {
  to?: Array<Stage>;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationPublishSpecialOfferArgs = {
  to?: Array<Stage>;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationPublishTaskArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: TaskWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishVideoArgs = {
  to?: Array<Stage>;
  where: VideoWhereUniqueInput;
};

export type MutationPublishVideoLessonArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: VideoLessonWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationPublishWhitelabelArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  to?: Array<Stage>;
  where: WhitelabelWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishAssetArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: AssetWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishComponentArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: ComponentWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishContentPageArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: ContentPageWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishContentPageSectionArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishContentPageSectionItemArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionItemWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishContentPageSectionSponsorArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: ContentPageSectionSponsorWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishDashboardBannerArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationSchedulePublishDiaryScenarioArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationSchedulePublishEeweArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: EeweWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishPartnerArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: PartnerWhereUniqueInput;
};

export type MutationSchedulePublishPhaseArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: PhaseWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishRecommendedTaskArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationSchedulePublishRegistrationPromoResultArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationSchedulePublishSpecialOfferArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationSchedulePublishTaskArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: TaskWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishVideoArgs = {
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: VideoWhereUniqueInput;
};

export type MutationSchedulePublishVideoLessonArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: VideoLessonWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationSchedulePublishWhitelabelArgs = {
  locales?: InputMaybe<Array<Locale>>;
  publishBase?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  to?: Array<Stage>;
  where: WhitelabelWhereUniqueInput;
  withDefaultLocale?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationScheduleUnpublishAssetArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: AssetWhereUniqueInput;
};

export type MutationScheduleUnpublishComponentArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ComponentWhereUniqueInput;
};

export type MutationScheduleUnpublishContentPageArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageWhereUniqueInput;
};

export type MutationScheduleUnpublishContentPageSectionArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionWhereUniqueInput;
};

export type MutationScheduleUnpublishContentPageSectionItemArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionItemWhereUniqueInput;
};

export type MutationScheduleUnpublishContentPageSectionSponsorArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type MutationScheduleUnpublishDashboardBannerArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationScheduleUnpublishDiaryScenarioArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationScheduleUnpublishEeweArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: EeweWhereUniqueInput;
};

export type MutationScheduleUnpublishPartnerArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: PartnerWhereUniqueInput;
};

export type MutationScheduleUnpublishPhaseArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: PhaseWhereUniqueInput;
};

export type MutationScheduleUnpublishRecommendedTaskArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationScheduleUnpublishRegistrationPromoResultArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationScheduleUnpublishSpecialOfferArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationScheduleUnpublishTaskArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: TaskWhereUniqueInput;
};

export type MutationScheduleUnpublishVideoArgs = {
  from?: Array<Stage>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  where: VideoWhereUniqueInput;
};

export type MutationScheduleUnpublishVideoLessonArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: VideoLessonWhereUniqueInput;
};

export type MutationScheduleUnpublishWhitelabelArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  releaseId?: InputMaybe<Scalars['String']['input']>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: WhitelabelWhereUniqueInput;
};

export type MutationUnpublishAssetArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: AssetWhereUniqueInput;
};

export type MutationUnpublishComponentArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ComponentWhereUniqueInput;
};

export type MutationUnpublishContentPageArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageWhereUniqueInput;
};

export type MutationUnpublishContentPageSectionArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionWhereUniqueInput;
};

export type MutationUnpublishContentPageSectionItemArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionItemWhereUniqueInput;
};

export type MutationUnpublishContentPageSectionSponsorArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type MutationUnpublishDashboardBannerArgs = {
  from?: Array<Stage>;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationUnpublishDiaryScenarioArgs = {
  from?: Array<Stage>;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationUnpublishEeweArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: EeweWhereUniqueInput;
};

export type MutationUnpublishManyAssetsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationUnpublishManyAssetsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationUnpublishManyComponentsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationUnpublishManyComponentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionItemsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionSponsorsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionSponsorsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationUnpublishManyContentPageSectionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationUnpublishManyContentPagesArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationUnpublishManyContentPagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationUnpublishManyDashboardBannersArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationUnpublishManyDashboardBannersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationUnpublishManyDiaryScenariosArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationUnpublishManyDiaryScenariosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationUnpublishManyEewesArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationUnpublishManyEewesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationUnpublishManyPartnersArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationUnpublishManyPartnersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationUnpublishManyPhasesArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationUnpublishManyPhasesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationUnpublishManyRecommendedTasksArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationUnpublishManyRecommendedTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationUnpublishManyRegistrationPromoResultsArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationUnpublishManyRegistrationPromoResultsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationUnpublishManySpecialOffersArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationUnpublishManySpecialOffersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationUnpublishManyTasksArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationUnpublishManyTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationUnpublishManyVideoLessonsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationUnpublishManyVideoLessonsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationUnpublishManyVideosArgs = {
  from?: Array<Stage>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationUnpublishManyVideosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationUnpublishManyWhitelabelsArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationUnpublishManyWhitelabelsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  from?: Array<Stage>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: InputMaybe<Stage>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationUnpublishPartnerArgs = {
  from?: Array<Stage>;
  where: PartnerWhereUniqueInput;
};

export type MutationUnpublishPhaseArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: PhaseWhereUniqueInput;
};

export type MutationUnpublishRecommendedTaskArgs = {
  from?: Array<Stage>;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationUnpublishRegistrationPromoResultArgs = {
  from?: Array<Stage>;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationUnpublishSpecialOfferArgs = {
  from?: Array<Stage>;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationUnpublishTaskArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: TaskWhereUniqueInput;
};

export type MutationUnpublishVideoArgs = {
  from?: Array<Stage>;
  where: VideoWhereUniqueInput;
};

export type MutationUnpublishVideoLessonArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: VideoLessonWhereUniqueInput;
};

export type MutationUnpublishWhitelabelArgs = {
  from?: Array<Stage>;
  locales?: InputMaybe<Array<Locale>>;
  unpublishBase?: InputMaybe<Scalars['Boolean']['input']>;
  where: WhitelabelWhereUniqueInput;
};

export type MutationUpdateAssetArgs = {
  data: AssetUpdateInput;
  where: AssetWhereUniqueInput;
};

export type MutationUpdateComponentArgs = {
  data: ComponentUpdateInput;
  where: ComponentWhereUniqueInput;
};

export type MutationUpdateContentPageArgs = {
  data: ContentPageUpdateInput;
  where: ContentPageWhereUniqueInput;
};

export type MutationUpdateContentPageSectionArgs = {
  data: ContentPageSectionUpdateInput;
  where: ContentPageSectionWhereUniqueInput;
};

export type MutationUpdateContentPageSectionItemArgs = {
  data: ContentPageSectionItemUpdateInput;
  where: ContentPageSectionItemWhereUniqueInput;
};

export type MutationUpdateContentPageSectionSponsorArgs = {
  data: ContentPageSectionSponsorUpdateInput;
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type MutationUpdateDashboardBannerArgs = {
  data: DashboardBannerUpdateInput;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationUpdateDiaryScenarioArgs = {
  data: DiaryScenarioUpdateInput;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationUpdateEeweArgs = {
  data: EeweUpdateInput;
  where: EeweWhereUniqueInput;
};

export type MutationUpdateManyAssetsArgs = {
  data: AssetUpdateManyInput;
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationUpdateManyAssetsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: AssetUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<AssetManyWhereInput>;
};

export type MutationUpdateManyComponentsArgs = {
  data: ComponentUpdateManyInput;
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationUpdateManyComponentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: ComponentUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ComponentManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionItemsArgs = {
  data: ContentPageSectionItemUpdateManyInput;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: ContentPageSectionItemUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionItemManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionSponsorsArgs = {
  data: ContentPageSectionSponsorUpdateManyInput;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionSponsorsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: ContentPageSectionSponsorUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionSponsorManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionsArgs = {
  data: ContentPageSectionUpdateManyInput;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationUpdateManyContentPageSectionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: ContentPageSectionUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageSectionManyWhereInput>;
};

export type MutationUpdateManyContentPagesArgs = {
  data: ContentPageUpdateManyInput;
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationUpdateManyContentPagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: ContentPageUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ContentPageManyWhereInput>;
};

export type MutationUpdateManyDashboardBannersArgs = {
  data: DashboardBannerUpdateManyInput;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationUpdateManyDashboardBannersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: DashboardBannerUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DashboardBannerManyWhereInput>;
};

export type MutationUpdateManyDiaryScenariosArgs = {
  data: DiaryScenarioUpdateManyInput;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationUpdateManyDiaryScenariosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: DiaryScenarioUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<DiaryScenarioManyWhereInput>;
};

export type MutationUpdateManyEewesArgs = {
  data: EeweUpdateManyInput;
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationUpdateManyEewesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: EeweUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EeweManyWhereInput>;
};

export type MutationUpdateManyPartnersArgs = {
  data: PartnerUpdateManyInput;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationUpdateManyPartnersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: PartnerUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<PartnerManyWhereInput>;
};

export type MutationUpdateManyPhasesArgs = {
  data: PhaseUpdateManyInput;
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationUpdateManyPhasesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: PhaseUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<PhaseManyWhereInput>;
};

export type MutationUpdateManyRecommendedTasksArgs = {
  data: RecommendedTaskUpdateManyInput;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationUpdateManyRecommendedTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: RecommendedTaskUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<RecommendedTaskManyWhereInput>;
};

export type MutationUpdateManyRegistrationPromoResultsArgs = {
  data: RegistrationPromoResultUpdateManyInput;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationUpdateManyRegistrationPromoResultsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: RegistrationPromoResultUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<RegistrationPromoResultManyWhereInput>;
};

export type MutationUpdateManySpecialOffersArgs = {
  data: SpecialOfferUpdateManyInput;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationUpdateManySpecialOffersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: SpecialOfferUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<SpecialOfferManyWhereInput>;
};

export type MutationUpdateManyTasksArgs = {
  data: TaskUpdateManyInput;
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationUpdateManyTasksConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: TaskUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<TaskManyWhereInput>;
};

export type MutationUpdateManyVideoLessonsArgs = {
  data: VideoLessonUpdateManyInput;
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationUpdateManyVideoLessonsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: VideoLessonUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<VideoLessonManyWhereInput>;
};

export type MutationUpdateManyVideosArgs = {
  data: VideoUpdateManyInput;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationUpdateManyVideosConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: VideoUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<VideoManyWhereInput>;
};

export type MutationUpdateManyWhitelabelsArgs = {
  data: WhitelabelUpdateManyInput;
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationUpdateManyWhitelabelsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']['input']>;
  before?: InputMaybe<Scalars['ID']['input']>;
  data: WhitelabelUpdateManyInput;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WhitelabelManyWhereInput>;
};

export type MutationUpdatePartnerArgs = {
  data: PartnerUpdateInput;
  where: PartnerWhereUniqueInput;
};

export type MutationUpdatePhaseArgs = {
  data: PhaseUpdateInput;
  where: PhaseWhereUniqueInput;
};

export type MutationUpdateRecommendedTaskArgs = {
  data: RecommendedTaskUpdateInput;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationUpdateRegistrationPromoResultArgs = {
  data: RegistrationPromoResultUpdateInput;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationUpdateScheduledReleaseArgs = {
  data: ScheduledReleaseUpdateInput;
  where: ScheduledReleaseWhereUniqueInput;
};

export type MutationUpdateSpecialOfferArgs = {
  data: SpecialOfferUpdateInput;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationUpdateTaskArgs = {
  data: TaskUpdateInput;
  where: TaskWhereUniqueInput;
};

export type MutationUpdateVideoArgs = {
  data: VideoUpdateInput;
  where: VideoWhereUniqueInput;
};

export type MutationUpdateVideoLessonArgs = {
  data: VideoLessonUpdateInput;
  where: VideoLessonWhereUniqueInput;
};

export type MutationUpdateWhitelabelArgs = {
  data: WhitelabelUpdateInput;
  where: WhitelabelWhereUniqueInput;
};

export type MutationUpsertAssetArgs = {
  upsert: AssetUpsertInput;
  where: AssetWhereUniqueInput;
};

export type MutationUpsertComponentArgs = {
  upsert: ComponentUpsertInput;
  where: ComponentWhereUniqueInput;
};

export type MutationUpsertContentPageArgs = {
  upsert: ContentPageUpsertInput;
  where: ContentPageWhereUniqueInput;
};

export type MutationUpsertContentPageSectionArgs = {
  upsert: ContentPageSectionUpsertInput;
  where: ContentPageSectionWhereUniqueInput;
};

export type MutationUpsertContentPageSectionItemArgs = {
  upsert: ContentPageSectionItemUpsertInput;
  where: ContentPageSectionItemWhereUniqueInput;
};

export type MutationUpsertContentPageSectionSponsorArgs = {
  upsert: ContentPageSectionSponsorUpsertInput;
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type MutationUpsertDashboardBannerArgs = {
  upsert: DashboardBannerUpsertInput;
  where: DashboardBannerWhereUniqueInput;
};

export type MutationUpsertDiaryScenarioArgs = {
  upsert: DiaryScenarioUpsertInput;
  where: DiaryScenarioWhereUniqueInput;
};

export type MutationUpsertEeweArgs = {
  upsert: EeweUpsertInput;
  where: EeweWhereUniqueInput;
};

export type MutationUpsertPartnerArgs = {
  upsert: PartnerUpsertInput;
  where: PartnerWhereUniqueInput;
};

export type MutationUpsertPhaseArgs = {
  upsert: PhaseUpsertInput;
  where: PhaseWhereUniqueInput;
};

export type MutationUpsertRecommendedTaskArgs = {
  upsert: RecommendedTaskUpsertInput;
  where: RecommendedTaskWhereUniqueInput;
};

export type MutationUpsertRegistrationPromoResultArgs = {
  upsert: RegistrationPromoResultUpsertInput;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type MutationUpsertSpecialOfferArgs = {
  upsert: SpecialOfferUpsertInput;
  where: SpecialOfferWhereUniqueInput;
};

export type MutationUpsertTaskArgs = {
  upsert: TaskUpsertInput;
  where: TaskWhereUniqueInput;
};

export type MutationUpsertVideoArgs = {
  upsert: VideoUpsertInput;
  where: VideoWhereUniqueInput;
};

export type MutationUpsertVideoLessonArgs = {
  upsert: VideoLessonUpsertInput;
  where: VideoLessonWhereUniqueInput;
};

export type MutationUpsertWhitelabelArgs = {
  upsert: WhitelabelUpsertInput;
  where: WhitelabelWhereUniqueInput;
};

/** An object with an ID */
export type Node = {
  /** The id of the object. */
  id: Scalars['ID']['output'];
  /** The Stage of an object */
  stage: Stage;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']['output']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean']['output'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean']['output'];
  /** Number of items in the current page. */
  pageSize?: Maybe<Scalars['Int']['output']>;
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']['output']>;
};

/** jednotlivé položky na stránce Odborníci */
export type Partner = Entity &
  Node & {
    __typename?: 'Partner';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    description: Scalars['String']['output'];
    /** Get the document in other stages */
    documentInStages: Array<Partner>;
    /** List of Partner versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    lang?: Maybe<Language>;
    link: Scalars['String']['output'];
    logo: Scalars['String']['output'];
    name: Scalars['String']['output'];
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** Seznam tagů oddělených čárkou */
    tags?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

/** jednotlivé položky na stránce Odborníci */
export type PartnerCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** jednotlivé položky na stránce Odborníci */
export type PartnerDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

/** jednotlivé položky na stránce Odborníci */
export type PartnerHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

/** jednotlivé položky na stránce Odborníci */
export type PartnerPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** jednotlivé položky na stránce Odborníci */
export type PartnerScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

/** jednotlivé položky na stránce Odborníci */
export type PartnerUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type PartnerConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: PartnerWhereUniqueInput;
};

/** A connection to a list of items. */
export type PartnerConnection = {
  __typename?: 'PartnerConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<PartnerEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type PartnerCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description: Scalars['String']['input'];
  lang?: InputMaybe<Language>;
  link: Scalars['String']['input'];
  logo: Scalars['String']['input'];
  name: Scalars['String']['input'];
  tags?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type PartnerCreateManyInlineInput = {
  /** Connect multiple existing Partner documents */
  connect?: InputMaybe<Array<PartnerWhereUniqueInput>>;
  /** Create and connect multiple existing Partner documents */
  create?: InputMaybe<Array<PartnerCreateInput>>;
};

export type PartnerCreateOneInlineInput = {
  /** Connect one existing Partner document */
  connect?: InputMaybe<PartnerWhereUniqueInput>;
  /** Create and connect one Partner document */
  create?: InputMaybe<PartnerCreateInput>;
};

/** An edge in a connection. */
export type PartnerEdge = {
  __typename?: 'PartnerEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Partner;
};

/** Identifies documents */
export type PartnerManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PartnerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PartnerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PartnerWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  documentInStages_every?: InputMaybe<PartnerWhereStageInput>;
  documentInStages_none?: InputMaybe<PartnerWhereStageInput>;
  documentInStages_some?: InputMaybe<PartnerWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  lang?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  lang_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  lang_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  lang_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  logo_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  logo_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  logo_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  logo_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  logo_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  logo_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  logo_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  logo_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  logo_starts_with?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tags?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  tags_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  tags_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  tags_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  tags_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  tags_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  tags_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum PartnerOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LangAsc = 'lang_ASC',
  LangDesc = 'lang_DESC',
  LinkAsc = 'link_ASC',
  LinkDesc = 'link_DESC',
  LogoAsc = 'logo_ASC',
  LogoDesc = 'logo_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TagsAsc = 'tags_ASC',
  TagsDesc = 'tags_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type PartnerUpdateInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  lang?: InputMaybe<Language>;
  link?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
};

export type PartnerUpdateManyInlineInput = {
  /** Connect multiple existing Partner documents */
  connect?: InputMaybe<Array<PartnerConnectInput>>;
  /** Create and connect multiple Partner documents */
  create?: InputMaybe<Array<PartnerCreateInput>>;
  /** Delete multiple Partner documents */
  delete?: InputMaybe<Array<PartnerWhereUniqueInput>>;
  /** Disconnect multiple Partner documents */
  disconnect?: InputMaybe<Array<PartnerWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Partner documents */
  set?: InputMaybe<Array<PartnerWhereUniqueInput>>;
  /** Update multiple Partner documents */
  update?: InputMaybe<Array<PartnerUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Partner documents */
  upsert?: InputMaybe<Array<PartnerUpsertWithNestedWhereUniqueInput>>;
};

export type PartnerUpdateManyInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  lang?: InputMaybe<Language>;
  link?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
};

export type PartnerUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: PartnerUpdateManyInput;
  /** Document search */
  where: PartnerWhereInput;
};

export type PartnerUpdateOneInlineInput = {
  /** Connect existing Partner document */
  connect?: InputMaybe<PartnerWhereUniqueInput>;
  /** Create and connect one Partner document */
  create?: InputMaybe<PartnerCreateInput>;
  /** Delete currently connected Partner document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Partner document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Partner document */
  update?: InputMaybe<PartnerUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Partner document */
  upsert?: InputMaybe<PartnerUpsertWithNestedWhereUniqueInput>;
};

export type PartnerUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: PartnerUpdateInput;
  /** Unique document search */
  where: PartnerWhereUniqueInput;
};

export type PartnerUpsertInput = {
  /** Create document if it didn't exist */
  create: PartnerCreateInput;
  /** Update document if it exists */
  update: PartnerUpdateInput;
};

export type PartnerUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: PartnerUpsertInput;
  /** Unique document search */
  where: PartnerWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type PartnerWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type PartnerWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PartnerWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PartnerWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PartnerWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  documentInStages_every?: InputMaybe<PartnerWhereStageInput>;
  documentInStages_none?: InputMaybe<PartnerWhereStageInput>;
  documentInStages_some?: InputMaybe<PartnerWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  lang?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  lang_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  lang_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  lang_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  logo_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  logo_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  logo_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  logo_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  logo_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  logo_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  logo_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  logo_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  logo_starts_with?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tags?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  tags_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  tags_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  tags_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  tags_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  tags_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  tags_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type PartnerWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PartnerWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PartnerWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PartnerWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<PartnerWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Partner record uniquely */
export type PartnerWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Phase = Entity &
  Node & {
    __typename?: 'Phase';
    components: Array<Component>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<Phase>;
    /** List of Phase versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    label?: Maybe<Scalars['String']['output']>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<Phase>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    title?: Maybe<PhaseStage>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type PhaseComponentsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ComponentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ComponentWhereInput>;
};

export type PhaseCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type PhaseCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type PhaseDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type PhaseHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type PhaseLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type PhasePublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type PhasePublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type PhaseScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type PhaseUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type PhaseUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type PhaseConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: PhaseWhereUniqueInput;
};

/** A connection to a list of items. */
export type PhaseConnection = {
  __typename?: 'PhaseConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<PhaseEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type PhaseCreateInput = {
  ckv87rrm45uku01yzbphc5ajl?: InputMaybe<DiaryScenarioCreateManyInlineInput>;
  components?: InputMaybe<ComponentCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<PhaseCreateLocalizationsInput>;
  title?: InputMaybe<PhaseStage>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type PhaseCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type PhaseCreateLocalizationInput = {
  /** Localization input */
  data: PhaseCreateLocalizationDataInput;
  locale: Locale;
};

export type PhaseCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<PhaseCreateLocalizationInput>>;
};

export type PhaseCreateManyInlineInput = {
  /** Connect multiple existing Phase documents */
  connect?: InputMaybe<Array<PhaseWhereUniqueInput>>;
  /** Create and connect multiple existing Phase documents */
  create?: InputMaybe<Array<PhaseCreateInput>>;
};

export type PhaseCreateOneInlineInput = {
  /** Connect one existing Phase document */
  connect?: InputMaybe<PhaseWhereUniqueInput>;
  /** Create and connect one Phase document */
  create?: InputMaybe<PhaseCreateInput>;
};

/** An edge in a connection. */
export type PhaseEdge = {
  __typename?: 'PhaseEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Phase;
};

/** Identifies documents */
export type PhaseManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PhaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PhaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PhaseWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  components_every?: InputMaybe<ComponentWhereInput>;
  components_none?: InputMaybe<ComponentWhereInput>;
  components_some?: InputMaybe<ComponentWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<PhaseWhereStageInput>;
  documentInStages_none?: InputMaybe<PhaseWhereStageInput>;
  documentInStages_some?: InputMaybe<PhaseWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<PhaseStage>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<PhaseStage>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<PhaseStage>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<PhaseStage>>>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum PhaseOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LabelAsc = 'label_ASC',
  LabelDesc = 'label_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export enum PhaseStage {
  Business = 'BUSINESS',
  Idea = 'IDEA',
  Ideation = 'IDEATION',
  Mvp = 'MVP',
  Prototype = 'PROTOTYPE',
}

export type PhaseUpdateInput = {
  ckv87rrm45uku01yzbphc5ajl?: InputMaybe<DiaryScenarioUpdateManyInlineInput>;
  components?: InputMaybe<ComponentUpdateManyInlineInput>;
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<PhaseUpdateLocalizationsInput>;
  title?: InputMaybe<PhaseStage>;
};

export type PhaseUpdateLocalizationDataInput = {
  label?: InputMaybe<Scalars['String']['input']>;
};

export type PhaseUpdateLocalizationInput = {
  data: PhaseUpdateLocalizationDataInput;
  locale: Locale;
};

export type PhaseUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<PhaseCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<PhaseUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<PhaseUpsertLocalizationInput>>;
};

export type PhaseUpdateManyInlineInput = {
  /** Connect multiple existing Phase documents */
  connect?: InputMaybe<Array<PhaseConnectInput>>;
  /** Create and connect multiple Phase documents */
  create?: InputMaybe<Array<PhaseCreateInput>>;
  /** Delete multiple Phase documents */
  delete?: InputMaybe<Array<PhaseWhereUniqueInput>>;
  /** Disconnect multiple Phase documents */
  disconnect?: InputMaybe<Array<PhaseWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Phase documents */
  set?: InputMaybe<Array<PhaseWhereUniqueInput>>;
  /** Update multiple Phase documents */
  update?: InputMaybe<Array<PhaseUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Phase documents */
  upsert?: InputMaybe<Array<PhaseUpsertWithNestedWhereUniqueInput>>;
};

export type PhaseUpdateManyInput = {
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<PhaseUpdateManyLocalizationsInput>;
  title?: InputMaybe<PhaseStage>;
};

export type PhaseUpdateManyLocalizationDataInput = {
  label?: InputMaybe<Scalars['String']['input']>;
};

export type PhaseUpdateManyLocalizationInput = {
  data: PhaseUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type PhaseUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<PhaseUpdateManyLocalizationInput>>;
};

export type PhaseUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: PhaseUpdateManyInput;
  /** Document search */
  where: PhaseWhereInput;
};

export type PhaseUpdateOneInlineInput = {
  /** Connect existing Phase document */
  connect?: InputMaybe<PhaseWhereUniqueInput>;
  /** Create and connect one Phase document */
  create?: InputMaybe<PhaseCreateInput>;
  /** Delete currently connected Phase document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Phase document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Phase document */
  update?: InputMaybe<PhaseUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Phase document */
  upsert?: InputMaybe<PhaseUpsertWithNestedWhereUniqueInput>;
};

export type PhaseUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: PhaseUpdateInput;
  /** Unique document search */
  where: PhaseWhereUniqueInput;
};

export type PhaseUpsertInput = {
  /** Create document if it didn't exist */
  create: PhaseCreateInput;
  /** Update document if it exists */
  update: PhaseUpdateInput;
};

export type PhaseUpsertLocalizationInput = {
  create: PhaseCreateLocalizationDataInput;
  locale: Locale;
  update: PhaseUpdateLocalizationDataInput;
};

export type PhaseUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: PhaseUpsertInput;
  /** Unique document search */
  where: PhaseWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type PhaseWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type PhaseWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PhaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PhaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PhaseWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  components_every?: InputMaybe<ComponentWhereInput>;
  components_none?: InputMaybe<ComponentWhereInput>;
  components_some?: InputMaybe<ComponentWhereInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<PhaseWhereStageInput>;
  documentInStages_none?: InputMaybe<PhaseWhereStageInput>;
  documentInStages_some?: InputMaybe<PhaseWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<PhaseStage>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<PhaseStage>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<PhaseStage>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<PhaseStage>>>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type PhaseWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<PhaseWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<PhaseWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<PhaseWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<PhaseWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Phase record uniquely */
export type PhaseWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type PublishLocaleInput = {
  /** Locales to publish */
  locale: Locale;
  /** Stages to publish selected locales to */
  stages: Array<Stage>;
};

export type Query = {
  __typename?: 'Query';
  /** Retrieve a single asset */
  asset?: Maybe<Asset>;
  /** Retrieve document version */
  assetVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple assets */
  assets: Array<Asset>;
  /** Retrieve multiple assets using the Relay connection interface */
  assetsConnection: AssetConnection;
  /** Retrieve a single component */
  component?: Maybe<Component>;
  /** Retrieve document version */
  componentVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple components */
  components: Array<Component>;
  /** Retrieve multiple components using the Relay connection interface */
  componentsConnection: ComponentConnection;
  /** Retrieve a single contentPage */
  contentPage?: Maybe<ContentPage>;
  /** Retrieve a single contentPageSection */
  contentPageSection?: Maybe<ContentPageSection>;
  /** Retrieve a single contentPageSectionItem */
  contentPageSectionItem?: Maybe<ContentPageSectionItem>;
  /** Retrieve document version */
  contentPageSectionItemVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple contentPageSectionItems */
  contentPageSectionItems: Array<ContentPageSectionItem>;
  /** Retrieve multiple contentPageSectionItems using the Relay connection interface */
  contentPageSectionItemsConnection: ContentPageSectionItemConnection;
  /** Retrieve a single contentPageSectionSponsor */
  contentPageSectionSponsor?: Maybe<ContentPageSectionSponsor>;
  /** Retrieve document version */
  contentPageSectionSponsorVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple contentPageSectionSponsors */
  contentPageSectionSponsors: Array<ContentPageSectionSponsor>;
  /** Retrieve multiple contentPageSectionSponsors using the Relay connection interface */
  contentPageSectionSponsorsConnection: ContentPageSectionSponsorConnection;
  /** Retrieve document version */
  contentPageSectionVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple contentPageSections */
  contentPageSections: Array<ContentPageSection>;
  /** Retrieve multiple contentPageSections using the Relay connection interface */
  contentPageSectionsConnection: ContentPageSectionConnection;
  /** Retrieve document version */
  contentPageVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple contentPages */
  contentPages: Array<ContentPage>;
  /** Retrieve multiple contentPages using the Relay connection interface */
  contentPagesConnection: ContentPageConnection;
  /** Retrieve a single dashboardBanner */
  dashboardBanner?: Maybe<DashboardBanner>;
  /** Retrieve document version */
  dashboardBannerVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple dashboardBanners */
  dashboardBanners: Array<DashboardBanner>;
  /** Retrieve multiple dashboardBanners using the Relay connection interface */
  dashboardBannersConnection: DashboardBannerConnection;
  /** Retrieve a single diaryScenario */
  diaryScenario?: Maybe<DiaryScenario>;
  /** Retrieve document version */
  diaryScenarioVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple diaryScenarios */
  diaryScenarios: Array<DiaryScenario>;
  /** Retrieve multiple diaryScenarios using the Relay connection interface */
  diaryScenariosConnection: DiaryScenarioConnection;
  /** Retrieve a single eewe */
  eewe?: Maybe<Eewe>;
  /** Retrieve document version */
  eeweVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple eewes */
  eewes: Array<Eewe>;
  /** Retrieve multiple eewes using the Relay connection interface */
  eewesConnection: EeweConnection;
  /** Fetches an object given its ID */
  entities?: Maybe<Array<Entity>>;
  /** Fetches an object given its ID */
  node?: Maybe<Node>;
  /** Retrieve a single partner */
  partner?: Maybe<Partner>;
  /** Retrieve document version */
  partnerVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple partners */
  partners: Array<Partner>;
  /** Retrieve multiple partners using the Relay connection interface */
  partnersConnection: PartnerConnection;
  /** Retrieve a single phase */
  phase?: Maybe<Phase>;
  /** Retrieve document version */
  phaseVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple phases */
  phases: Array<Phase>;
  /** Retrieve multiple phases using the Relay connection interface */
  phasesConnection: PhaseConnection;
  /** Retrieve a single recommendedTask */
  recommendedTask?: Maybe<RecommendedTask>;
  /** Retrieve document version */
  recommendedTaskVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple recommendedTasks */
  recommendedTasks: Array<RecommendedTask>;
  /** Retrieve multiple recommendedTasks using the Relay connection interface */
  recommendedTasksConnection: RecommendedTaskConnection;
  /** Retrieve a single registrationPromoResult */
  registrationPromoResult?: Maybe<RegistrationPromoResult>;
  /** Retrieve document version */
  registrationPromoResultVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple registrationPromoResults */
  registrationPromoResults: Array<RegistrationPromoResult>;
  /** Retrieve multiple registrationPromoResults using the Relay connection interface */
  registrationPromoResultsConnection: RegistrationPromoResultConnection;
  /** Retrieve a single scheduledOperation */
  scheduledOperation?: Maybe<ScheduledOperation>;
  /** Retrieve multiple scheduledOperations */
  scheduledOperations: Array<ScheduledOperation>;
  /** Retrieve multiple scheduledOperations using the Relay connection interface */
  scheduledOperationsConnection: ScheduledOperationConnection;
  /** Retrieve a single scheduledRelease */
  scheduledRelease?: Maybe<ScheduledRelease>;
  /** Retrieve multiple scheduledReleases */
  scheduledReleases: Array<ScheduledRelease>;
  /** Retrieve multiple scheduledReleases using the Relay connection interface */
  scheduledReleasesConnection: ScheduledReleaseConnection;
  /** Retrieve a single specialOffer */
  specialOffer?: Maybe<SpecialOffer>;
  /** Retrieve document version */
  specialOfferVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple specialOffers */
  specialOffers: Array<SpecialOffer>;
  /** Retrieve multiple specialOffers using the Relay connection interface */
  specialOffersConnection: SpecialOfferConnection;
  /** Retrieve a single task */
  task?: Maybe<Task>;
  /** Retrieve document version */
  taskVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple tasks */
  tasks: Array<Task>;
  /** Retrieve multiple tasks using the Relay connection interface */
  tasksConnection: TaskConnection;
  /** Retrieve a single user */
  user?: Maybe<User>;
  /** Retrieve multiple users */
  users: Array<User>;
  /** Retrieve multiple users using the Relay connection interface */
  usersConnection: UserConnection;
  /** Retrieve a single video */
  video?: Maybe<Video>;
  /** Retrieve a single videoLesson */
  videoLesson?: Maybe<VideoLesson>;
  /** Retrieve document version */
  videoLessonVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple videoLessons */
  videoLessons: Array<VideoLesson>;
  /** Retrieve multiple videoLessons using the Relay connection interface */
  videoLessonsConnection: VideoLessonConnection;
  /** Retrieve document version */
  videoVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple videos */
  videos: Array<Video>;
  /** Retrieve multiple videos using the Relay connection interface */
  videosConnection: VideoConnection;
  /** Retrieve a single whitelabel */
  whitelabel?: Maybe<Whitelabel>;
  /** Retrieve document version */
  whitelabelVersion?: Maybe<DocumentVersion>;
  /** Retrieve multiple whitelabels */
  whitelabels: Array<Whitelabel>;
  /** Retrieve multiple whitelabels using the Relay connection interface */
  whitelabelsConnection: WhitelabelConnection;
};

export type QueryAssetArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: AssetWhereUniqueInput;
};

export type QueryAssetVersionArgs = {
  where: VersionWhereInput;
};

export type QueryAssetsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<AssetOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<AssetWhereInput>;
};

export type QueryAssetsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<AssetOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<AssetWhereInput>;
};

export type QueryComponentArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ComponentWhereUniqueInput;
};

export type QueryComponentVersionArgs = {
  where: VersionWhereInput;
};

export type QueryComponentsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ComponentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ComponentWhereInput>;
};

export type QueryComponentsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ComponentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ComponentWhereInput>;
};

export type QueryContentPageArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ContentPageWhereUniqueInput;
};

export type QueryContentPageSectionArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ContentPageSectionWhereUniqueInput;
};

export type QueryContentPageSectionItemArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ContentPageSectionItemWhereUniqueInput;
};

export type QueryContentPageSectionItemVersionArgs = {
  where: VersionWhereInput;
};

export type QueryContentPageSectionItemsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionItemWhereInput>;
};

export type QueryContentPageSectionItemsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionItemWhereInput>;
};

export type QueryContentPageSectionSponsorArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ContentPageSectionSponsorWhereUniqueInput;
};

export type QueryContentPageSectionSponsorVersionArgs = {
  where: VersionWhereInput;
};

export type QueryContentPageSectionSponsorsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionSponsorOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionSponsorWhereInput>;
};

export type QueryContentPageSectionSponsorsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionSponsorOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionSponsorWhereInput>;
};

export type QueryContentPageSectionVersionArgs = {
  where: VersionWhereInput;
};

export type QueryContentPageSectionsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionWhereInput>;
};

export type QueryContentPageSectionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageSectionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageSectionWhereInput>;
};

export type QueryContentPageVersionArgs = {
  where: VersionWhereInput;
};

export type QueryContentPagesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageWhereInput>;
};

export type QueryContentPagesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ContentPageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ContentPageWhereInput>;
};

export type QueryDashboardBannerArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: DashboardBannerWhereUniqueInput;
};

export type QueryDashboardBannerVersionArgs = {
  where: VersionWhereInput;
};

export type QueryDashboardBannersArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<DashboardBannerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<DashboardBannerWhereInput>;
};

export type QueryDashboardBannersConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<DashboardBannerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<DashboardBannerWhereInput>;
};

export type QueryDiaryScenarioArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: DiaryScenarioWhereUniqueInput;
};

export type QueryDiaryScenarioVersionArgs = {
  where: VersionWhereInput;
};

export type QueryDiaryScenariosArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<DiaryScenarioOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<DiaryScenarioWhereInput>;
};

export type QueryDiaryScenariosConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<DiaryScenarioOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<DiaryScenarioWhereInput>;
};

export type QueryEeweArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: EeweWhereUniqueInput;
};

export type QueryEeweVersionArgs = {
  where: VersionWhereInput;
};

export type QueryEewesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<EeweOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<EeweWhereInput>;
};

export type QueryEewesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<EeweOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<EeweWhereInput>;
};

export type QueryEntitiesArgs = {
  locales?: InputMaybe<Array<Locale>>;
  where: Array<EntityWhereInput>;
};

export type QueryNodeArgs = {
  id: Scalars['ID']['input'];
  locales?: Array<Locale>;
  stage?: Stage;
};

export type QueryPartnerArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: PartnerWhereUniqueInput;
};

export type QueryPartnerVersionArgs = {
  where: VersionWhereInput;
};

export type QueryPartnersArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<PartnerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<PartnerWhereInput>;
};

export type QueryPartnersConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<PartnerOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<PartnerWhereInput>;
};

export type QueryPhaseArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: PhaseWhereUniqueInput;
};

export type QueryPhaseVersionArgs = {
  where: VersionWhereInput;
};

export type QueryPhasesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<PhaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<PhaseWhereInput>;
};

export type QueryPhasesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<PhaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<PhaseWhereInput>;
};

export type QueryRecommendedTaskArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: RecommendedTaskWhereUniqueInput;
};

export type QueryRecommendedTaskVersionArgs = {
  where: VersionWhereInput;
};

export type QueryRecommendedTasksArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<RecommendedTaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<RecommendedTaskWhereInput>;
};

export type QueryRecommendedTasksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<RecommendedTaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<RecommendedTaskWhereInput>;
};

export type QueryRegistrationPromoResultArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: RegistrationPromoResultWhereUniqueInput;
};

export type QueryRegistrationPromoResultVersionArgs = {
  where: VersionWhereInput;
};

export type QueryRegistrationPromoResultsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<RegistrationPromoResultOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<RegistrationPromoResultWhereInput>;
};

export type QueryRegistrationPromoResultsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<RegistrationPromoResultOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<RegistrationPromoResultWhereInput>;
};

export type QueryScheduledOperationArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ScheduledOperationWhereUniqueInput;
};

export type QueryScheduledOperationsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ScheduledOperationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type QueryScheduledOperationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ScheduledOperationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type QueryScheduledReleaseArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: ScheduledReleaseWhereUniqueInput;
};

export type QueryScheduledReleasesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ScheduledReleaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ScheduledReleaseWhereInput>;
};

export type QueryScheduledReleasesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<ScheduledReleaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<ScheduledReleaseWhereInput>;
};

export type QuerySpecialOfferArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: SpecialOfferWhereUniqueInput;
};

export type QuerySpecialOfferVersionArgs = {
  where: VersionWhereInput;
};

export type QuerySpecialOffersArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<SpecialOfferOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<SpecialOfferWhereInput>;
};

export type QuerySpecialOffersConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<SpecialOfferOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<SpecialOfferWhereInput>;
};

export type QueryTaskArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: TaskWhereUniqueInput;
};

export type QueryTaskVersionArgs = {
  where: VersionWhereInput;
};

export type QueryTasksArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<TaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<TaskWhereInput>;
};

export type QueryTasksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<TaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<TaskWhereInput>;
};

export type QueryUserArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: UserWhereUniqueInput;
};

export type QueryUsersArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<UserOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryUsersConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<UserOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryVideoArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: VideoWhereUniqueInput;
};

export type QueryVideoLessonArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: VideoLessonWhereUniqueInput;
};

export type QueryVideoLessonVersionArgs = {
  where: VersionWhereInput;
};

export type QueryVideoLessonsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<VideoLessonOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<VideoLessonWhereInput>;
};

export type QueryVideoLessonsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<VideoLessonOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<VideoLessonWhereInput>;
};

export type QueryVideoVersionArgs = {
  where: VersionWhereInput;
};

export type QueryVideosArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<VideoOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<VideoWhereInput>;
};

export type QueryVideosConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<VideoOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<VideoWhereInput>;
};

export type QueryWhitelabelArgs = {
  locales?: Array<Locale>;
  stage?: Stage;
  where: WhitelabelWhereUniqueInput;
};

export type QueryWhitelabelVersionArgs = {
  where: VersionWhereInput;
};

export type QueryWhitelabelsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<WhitelabelOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<WhitelabelWhereInput>;
};

export type QueryWhitelabelsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: Array<Locale>;
  orderBy?: InputMaybe<WhitelabelOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  stage?: Stage;
  where?: InputMaybe<WhitelabelWhereInput>;
};

/** Representing a RGBA color value: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#rgb()_and_rgba() */
export type Rgba = {
  __typename?: 'RGBA';
  a: Scalars['RGBATransparency']['output'];
  b: Scalars['RGBAHue']['output'];
  g: Scalars['RGBAHue']['output'];
  r: Scalars['RGBAHue']['output'];
};

/** Input type representing a RGBA color value: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#rgb()_and_rgba() */
export type RgbaInput = {
  a: Scalars['RGBATransparency']['input'];
  b: Scalars['RGBAHue']['input'];
  g: Scalars['RGBAHue']['input'];
  r: Scalars['RGBAHue']['input'];
};

export type RecommendedTask = Entity &
  Node & {
    __typename?: 'RecommendedTask';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<RecommendedTask>;
    /** List of RecommendedTask versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /**
     * Name should store the onboarding results. (e.g. 1A, 2A, 1B,...)
     * The default recommended tasks would be stored under 'Default' name.
     */
    name?: Maybe<Scalars['String']['output']>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    recommendedTasks: Array<Task>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type RecommendedTaskCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RecommendedTaskDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type RecommendedTaskHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type RecommendedTaskPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RecommendedTaskRecommendedTasksArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<TaskOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<TaskWhereInput>;
};

export type RecommendedTaskScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type RecommendedTaskUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RecommendedTaskConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: RecommendedTaskWhereUniqueInput;
};

/** A connection to a list of items. */
export type RecommendedTaskConnection = {
  __typename?: 'RecommendedTaskConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<RecommendedTaskEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type RecommendedTaskCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  recommendedTasks?: InputMaybe<TaskCreateManyInlineInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type RecommendedTaskCreateManyInlineInput = {
  /** Connect multiple existing RecommendedTask documents */
  connect?: InputMaybe<Array<RecommendedTaskWhereUniqueInput>>;
  /** Create and connect multiple existing RecommendedTask documents */
  create?: InputMaybe<Array<RecommendedTaskCreateInput>>;
};

export type RecommendedTaskCreateOneInlineInput = {
  /** Connect one existing RecommendedTask document */
  connect?: InputMaybe<RecommendedTaskWhereUniqueInput>;
  /** Create and connect one RecommendedTask document */
  create?: InputMaybe<RecommendedTaskCreateInput>;
};

/** An edge in a connection. */
export type RecommendedTaskEdge = {
  __typename?: 'RecommendedTaskEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: RecommendedTask;
};

/** Identifies documents */
export type RecommendedTaskManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<RecommendedTaskWhereStageInput>;
  documentInStages_none?: InputMaybe<RecommendedTaskWhereStageInput>;
  documentInStages_some?: InputMaybe<RecommendedTaskWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  recommendedTasks_every?: InputMaybe<TaskWhereInput>;
  recommendedTasks_none?: InputMaybe<TaskWhereInput>;
  recommendedTasks_some?: InputMaybe<TaskWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum RecommendedTaskOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type RecommendedTaskUpdateInput = {
  name?: InputMaybe<Scalars['String']['input']>;
  recommendedTasks?: InputMaybe<TaskUpdateManyInlineInput>;
};

export type RecommendedTaskUpdateManyInlineInput = {
  /** Connect multiple existing RecommendedTask documents */
  connect?: InputMaybe<Array<RecommendedTaskConnectInput>>;
  /** Create and connect multiple RecommendedTask documents */
  create?: InputMaybe<Array<RecommendedTaskCreateInput>>;
  /** Delete multiple RecommendedTask documents */
  delete?: InputMaybe<Array<RecommendedTaskWhereUniqueInput>>;
  /** Disconnect multiple RecommendedTask documents */
  disconnect?: InputMaybe<Array<RecommendedTaskWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing RecommendedTask documents */
  set?: InputMaybe<Array<RecommendedTaskWhereUniqueInput>>;
  /** Update multiple RecommendedTask documents */
  update?: InputMaybe<Array<RecommendedTaskUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple RecommendedTask documents */
  upsert?: InputMaybe<Array<RecommendedTaskUpsertWithNestedWhereUniqueInput>>;
};

export type RecommendedTaskUpdateManyInput = {
  name?: InputMaybe<Scalars['String']['input']>;
};

export type RecommendedTaskUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: RecommendedTaskUpdateManyInput;
  /** Document search */
  where: RecommendedTaskWhereInput;
};

export type RecommendedTaskUpdateOneInlineInput = {
  /** Connect existing RecommendedTask document */
  connect?: InputMaybe<RecommendedTaskWhereUniqueInput>;
  /** Create and connect one RecommendedTask document */
  create?: InputMaybe<RecommendedTaskCreateInput>;
  /** Delete currently connected RecommendedTask document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected RecommendedTask document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single RecommendedTask document */
  update?: InputMaybe<RecommendedTaskUpdateWithNestedWhereUniqueInput>;
  /** Upsert single RecommendedTask document */
  upsert?: InputMaybe<RecommendedTaskUpsertWithNestedWhereUniqueInput>;
};

export type RecommendedTaskUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: RecommendedTaskUpdateInput;
  /** Unique document search */
  where: RecommendedTaskWhereUniqueInput;
};

export type RecommendedTaskUpsertInput = {
  /** Create document if it didn't exist */
  create: RecommendedTaskCreateInput;
  /** Update document if it exists */
  update: RecommendedTaskUpdateInput;
};

export type RecommendedTaskUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: RecommendedTaskUpsertInput;
  /** Unique document search */
  where: RecommendedTaskWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type RecommendedTaskWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type RecommendedTaskWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RecommendedTaskWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<RecommendedTaskWhereStageInput>;
  documentInStages_none?: InputMaybe<RecommendedTaskWhereStageInput>;
  documentInStages_some?: InputMaybe<RecommendedTaskWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  recommendedTasks_every?: InputMaybe<TaskWhereInput>;
  recommendedTasks_none?: InputMaybe<TaskWhereInput>;
  recommendedTasks_some?: InputMaybe<TaskWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type RecommendedTaskWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RecommendedTaskWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RecommendedTaskWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RecommendedTaskWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<RecommendedTaskWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References RecommendedTask record uniquely */
export type RecommendedTaskWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type RegistrationPromoResult = Entity &
  Node & {
    __typename?: 'RegistrationPromoResult';
    count: Scalars['String']['output'];
    countColor: Scalars['String']['output'];
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<RegistrationPromoResult>;
    /** List of RegistrationPromoResult versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    textCs: Scalars['String']['output'];
    textEn?: Maybe<Scalars['String']['output']>;
    textSk?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type RegistrationPromoResultCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RegistrationPromoResultDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type RegistrationPromoResultHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type RegistrationPromoResultPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RegistrationPromoResultScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type RegistrationPromoResultUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type RegistrationPromoResultConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: RegistrationPromoResultWhereUniqueInput;
};

/** A connection to a list of items. */
export type RegistrationPromoResultConnection = {
  __typename?: 'RegistrationPromoResultConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<RegistrationPromoResultEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type RegistrationPromoResultCreateInput = {
  count: Scalars['String']['input'];
  countColor: Scalars['String']['input'];
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  textCs: Scalars['String']['input'];
  textEn?: InputMaybe<Scalars['String']['input']>;
  textSk?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type RegistrationPromoResultCreateManyInlineInput = {
  /** Connect multiple existing RegistrationPromoResult documents */
  connect?: InputMaybe<Array<RegistrationPromoResultWhereUniqueInput>>;
  /** Create and connect multiple existing RegistrationPromoResult documents */
  create?: InputMaybe<Array<RegistrationPromoResultCreateInput>>;
};

export type RegistrationPromoResultCreateOneInlineInput = {
  /** Connect one existing RegistrationPromoResult document */
  connect?: InputMaybe<RegistrationPromoResultWhereUniqueInput>;
  /** Create and connect one RegistrationPromoResult document */
  create?: InputMaybe<RegistrationPromoResultCreateInput>;
};

/** An edge in a connection. */
export type RegistrationPromoResultEdge = {
  __typename?: 'RegistrationPromoResultEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: RegistrationPromoResult;
};

/** Identifies documents */
export type RegistrationPromoResultManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  count?: InputMaybe<Scalars['String']['input']>;
  countColor?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  countColor_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  countColor_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  countColor_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  countColor_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  countColor_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  countColor_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  countColor_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  countColor_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  countColor_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  count_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  count_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  count_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  count_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  count_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  count_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  count_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  count_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  count_starts_with?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  documentInStages_none?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  documentInStages_some?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  textCs?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textCs_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textCs_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textCs_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textCs_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textCs_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textCs_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textCs_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textCs_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textCs_starts_with?: InputMaybe<Scalars['String']['input']>;
  textEn?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textEn_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textEn_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textEn_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textEn_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textEn_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textEn_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textEn_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textEn_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textEn_starts_with?: InputMaybe<Scalars['String']['input']>;
  textSk?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textSk_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textSk_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textSk_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textSk_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textSk_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textSk_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textSk_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textSk_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textSk_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum RegistrationPromoResultOrderByInput {
  CountColorAsc = 'countColor_ASC',
  CountColorDesc = 'countColor_DESC',
  CountAsc = 'count_ASC',
  CountDesc = 'count_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TextCsAsc = 'textCs_ASC',
  TextCsDesc = 'textCs_DESC',
  TextEnAsc = 'textEn_ASC',
  TextEnDesc = 'textEn_DESC',
  TextSkAsc = 'textSk_ASC',
  TextSkDesc = 'textSk_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type RegistrationPromoResultUpdateInput = {
  count?: InputMaybe<Scalars['String']['input']>;
  countColor?: InputMaybe<Scalars['String']['input']>;
  textCs?: InputMaybe<Scalars['String']['input']>;
  textEn?: InputMaybe<Scalars['String']['input']>;
  textSk?: InputMaybe<Scalars['String']['input']>;
};

export type RegistrationPromoResultUpdateManyInlineInput = {
  /** Connect multiple existing RegistrationPromoResult documents */
  connect?: InputMaybe<Array<RegistrationPromoResultConnectInput>>;
  /** Create and connect multiple RegistrationPromoResult documents */
  create?: InputMaybe<Array<RegistrationPromoResultCreateInput>>;
  /** Delete multiple RegistrationPromoResult documents */
  delete?: InputMaybe<Array<RegistrationPromoResultWhereUniqueInput>>;
  /** Disconnect multiple RegistrationPromoResult documents */
  disconnect?: InputMaybe<Array<RegistrationPromoResultWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing RegistrationPromoResult documents */
  set?: InputMaybe<Array<RegistrationPromoResultWhereUniqueInput>>;
  /** Update multiple RegistrationPromoResult documents */
  update?: InputMaybe<
    Array<RegistrationPromoResultUpdateWithNestedWhereUniqueInput>
  >;
  /** Upsert multiple RegistrationPromoResult documents */
  upsert?: InputMaybe<
    Array<RegistrationPromoResultUpsertWithNestedWhereUniqueInput>
  >;
};

export type RegistrationPromoResultUpdateManyInput = {
  count?: InputMaybe<Scalars['String']['input']>;
  countColor?: InputMaybe<Scalars['String']['input']>;
  textCs?: InputMaybe<Scalars['String']['input']>;
  textEn?: InputMaybe<Scalars['String']['input']>;
  textSk?: InputMaybe<Scalars['String']['input']>;
};

export type RegistrationPromoResultUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: RegistrationPromoResultUpdateManyInput;
  /** Document search */
  where: RegistrationPromoResultWhereInput;
};

export type RegistrationPromoResultUpdateOneInlineInput = {
  /** Connect existing RegistrationPromoResult document */
  connect?: InputMaybe<RegistrationPromoResultWhereUniqueInput>;
  /** Create and connect one RegistrationPromoResult document */
  create?: InputMaybe<RegistrationPromoResultCreateInput>;
  /** Delete currently connected RegistrationPromoResult document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected RegistrationPromoResult document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single RegistrationPromoResult document */
  update?: InputMaybe<RegistrationPromoResultUpdateWithNestedWhereUniqueInput>;
  /** Upsert single RegistrationPromoResult document */
  upsert?: InputMaybe<RegistrationPromoResultUpsertWithNestedWhereUniqueInput>;
};

export type RegistrationPromoResultUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: RegistrationPromoResultUpdateInput;
  /** Unique document search */
  where: RegistrationPromoResultWhereUniqueInput;
};

export type RegistrationPromoResultUpsertInput = {
  /** Create document if it didn't exist */
  create: RegistrationPromoResultCreateInput;
  /** Update document if it exists */
  update: RegistrationPromoResultUpdateInput;
};

export type RegistrationPromoResultUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: RegistrationPromoResultUpsertInput;
  /** Unique document search */
  where: RegistrationPromoResultWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type RegistrationPromoResultWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type RegistrationPromoResultWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RegistrationPromoResultWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  count?: InputMaybe<Scalars['String']['input']>;
  countColor?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  countColor_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  countColor_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  countColor_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  countColor_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  countColor_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  countColor_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  countColor_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  countColor_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  countColor_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  count_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  count_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  count_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  count_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  count_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  count_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  count_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  count_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  count_starts_with?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  documentInStages_none?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  documentInStages_some?: InputMaybe<RegistrationPromoResultWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  textCs?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textCs_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textCs_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textCs_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textCs_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textCs_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textCs_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textCs_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textCs_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textCs_starts_with?: InputMaybe<Scalars['String']['input']>;
  textEn?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textEn_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textEn_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textEn_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textEn_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textEn_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textEn_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textEn_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textEn_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textEn_starts_with?: InputMaybe<Scalars['String']['input']>;
  textSk?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  textSk_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  textSk_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  textSk_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  textSk_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  textSk_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  textSk_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  textSk_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  textSk_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  textSk_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type RegistrationPromoResultWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<RegistrationPromoResultWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<RegistrationPromoResultWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<RegistrationPromoResultWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<RegistrationPromoResultWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References RegistrationPromoResult record uniquely */
export type RegistrationPromoResultWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

/** Custom type representing a rich text value comprising of raw rich text ast, html, markdown and text values */
export type RichText = {
  __typename?: 'RichText';
  /** Returns HTMl representation */
  html: Scalars['String']['output'];
  /** Returns Markdown representation */
  markdown: Scalars['String']['output'];
  /** Returns AST representation */
  raw: Scalars['RichTextAST']['output'];
  /** Returns plain-text contents of RichText */
  text: Scalars['String']['output'];
};

/** Scheduled Operation system model */
export type ScheduledOperation = Entity &
  Node & {
    __typename?: 'ScheduledOperation';
    affectedDocuments: Array<ScheduledOperationAffectedDocument>;
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Operation description */
    description?: Maybe<Scalars['String']['output']>;
    /** Get the document in other stages */
    documentInStages: Array<ScheduledOperation>;
    /** Operation error message */
    errorMessage?: Maybe<Scalars['String']['output']>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    /** Raw operation payload including all details, this field is subject to change */
    rawPayload: Scalars['Json']['output'];
    /** The release this operation is scheduled for */
    release?: Maybe<ScheduledRelease>;
    /** System stage field */
    stage: Stage;
    /** operation Status */
    status: ScheduledOperationStatus;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

/** Scheduled Operation system model */
export type ScheduledOperationAffectedDocumentsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
};

/** Scheduled Operation system model */
export type ScheduledOperationCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

/** Scheduled Operation system model */
export type ScheduledOperationPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationReleaseArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Scheduled Operation system model */
export type ScheduledOperationUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ScheduledOperationAffectedDocument =
  | Asset
  | Component
  | ContentPage
  | ContentPageSection
  | ContentPageSectionItem
  | ContentPageSectionSponsor
  | DashboardBanner
  | DiaryScenario
  | Eewe
  | Partner
  | Phase
  | RecommendedTask
  | RegistrationPromoResult
  | SpecialOffer
  | Task
  | Video
  | VideoLesson
  | Whitelabel;

export type ScheduledOperationConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ScheduledOperationWhereUniqueInput;
};

/** A connection to a list of items. */
export type ScheduledOperationConnection = {
  __typename?: 'ScheduledOperationConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ScheduledOperationEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ScheduledOperationCreateManyInlineInput = {
  /** Connect multiple existing ScheduledOperation documents */
  connect?: InputMaybe<Array<ScheduledOperationWhereUniqueInput>>;
};

export type ScheduledOperationCreateOneInlineInput = {
  /** Connect one existing ScheduledOperation document */
  connect?: InputMaybe<ScheduledOperationWhereUniqueInput>;
};

/** An edge in a connection. */
export type ScheduledOperationEdge = {
  __typename?: 'ScheduledOperationEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ScheduledOperation;
};

/** Identifies documents */
export type ScheduledOperationManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  errorMessage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  errorMessage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  errorMessage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  /** All values containing the given json path. */
  rawPayload_json_path_exists?: InputMaybe<Scalars['String']['input']>;
  /**
   * Recursively tries to find the provided JSON scalar value inside the field.
   * It does use an exact match when comparing values.
   * If you pass `null` as value the filter will be ignored.
   * Note: This filter fails if you try to look for a non scalar JSON value!
   */
  rawPayload_value_recursive?: InputMaybe<Scalars['Json']['input']>;
  release?: InputMaybe<ScheduledReleaseWhereInput>;
  status?: InputMaybe<ScheduledOperationStatus>;
  /** All values that are contained in given list. */
  status_in?: InputMaybe<Array<InputMaybe<ScheduledOperationStatus>>>;
  /** Any other value that exists and is not equal to the given value. */
  status_not?: InputMaybe<ScheduledOperationStatus>;
  /** All values that are not contained in given list. */
  status_not_in?: InputMaybe<Array<InputMaybe<ScheduledOperationStatus>>>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ScheduledOperationOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  ErrorMessageAsc = 'errorMessage_ASC',
  ErrorMessageDesc = 'errorMessage_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  StatusAsc = 'status_ASC',
  StatusDesc = 'status_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

/** System Scheduled Operation Status */
export enum ScheduledOperationStatus {
  Canceled = 'CANCELED',
  Completed = 'COMPLETED',
  Failed = 'FAILED',
  InProgress = 'IN_PROGRESS',
  Pending = 'PENDING',
}

export type ScheduledOperationUpdateManyInlineInput = {
  /** Connect multiple existing ScheduledOperation documents */
  connect?: InputMaybe<Array<ScheduledOperationConnectInput>>;
  /** Disconnect multiple ScheduledOperation documents */
  disconnect?: InputMaybe<Array<ScheduledOperationWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ScheduledOperation documents */
  set?: InputMaybe<Array<ScheduledOperationWhereUniqueInput>>;
};

export type ScheduledOperationUpdateOneInlineInput = {
  /** Connect existing ScheduledOperation document */
  connect?: InputMaybe<ScheduledOperationWhereUniqueInput>;
  /** Disconnect currently connected ScheduledOperation document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type ScheduledOperationWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ScheduledOperationWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  errorMessage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  errorMessage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  errorMessage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  /** All values containing the given json path. */
  rawPayload_json_path_exists?: InputMaybe<Scalars['String']['input']>;
  /**
   * Recursively tries to find the provided JSON scalar value inside the field.
   * It does use an exact match when comparing values.
   * If you pass `null` as value the filter will be ignored.
   * Note: This filter fails if you try to look for a non scalar JSON value!
   */
  rawPayload_value_recursive?: InputMaybe<Scalars['Json']['input']>;
  release?: InputMaybe<ScheduledReleaseWhereInput>;
  status?: InputMaybe<ScheduledOperationStatus>;
  /** All values that are contained in given list. */
  status_in?: InputMaybe<Array<InputMaybe<ScheduledOperationStatus>>>;
  /** Any other value that exists and is not equal to the given value. */
  status_not?: InputMaybe<ScheduledOperationStatus>;
  /** All values that are not contained in given list. */
  status_not_in?: InputMaybe<Array<InputMaybe<ScheduledOperationStatus>>>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** References ScheduledOperation record uniquely */
export type ScheduledOperationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

/** Scheduled Release system model */
export type ScheduledRelease = Entity &
  Node & {
    __typename?: 'ScheduledRelease';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Release description */
    description?: Maybe<Scalars['String']['output']>;
    /** Get the document in other stages */
    documentInStages: Array<ScheduledRelease>;
    /** Release error message */
    errorMessage?: Maybe<Scalars['String']['output']>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** Whether scheduled release should be run */
    isActive: Scalars['Boolean']['output'];
    /** Whether scheduled release is implicit */
    isImplicit: Scalars['Boolean']['output'];
    /** Operations to run with this release */
    operations: Array<ScheduledOperation>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    /** Release date and time */
    releaseAt?: Maybe<Scalars['DateTime']['output']>;
    /** System stage field */
    stage: Stage;
    /** Release Status */
    status: ScheduledReleaseStatus;
    /** Release Title */
    title?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

/** Scheduled Release system model */
export type ScheduledReleaseCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Scheduled Release system model */
export type ScheduledReleaseDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

/** Scheduled Release system model */
export type ScheduledReleaseOperationsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  orderBy?: InputMaybe<ScheduledOperationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

/** Scheduled Release system model */
export type ScheduledReleasePublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Scheduled Release system model */
export type ScheduledReleaseUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type ScheduledReleaseConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: ScheduledReleaseWhereUniqueInput;
};

/** A connection to a list of items. */
export type ScheduledReleaseConnection = {
  __typename?: 'ScheduledReleaseConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<ScheduledReleaseEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type ScheduledReleaseCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type ScheduledReleaseCreateManyInlineInput = {
  /** Connect multiple existing ScheduledRelease documents */
  connect?: InputMaybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Create and connect multiple existing ScheduledRelease documents */
  create?: InputMaybe<Array<ScheduledReleaseCreateInput>>;
};

export type ScheduledReleaseCreateOneInlineInput = {
  /** Connect one existing ScheduledRelease document */
  connect?: InputMaybe<ScheduledReleaseWhereUniqueInput>;
  /** Create and connect one ScheduledRelease document */
  create?: InputMaybe<ScheduledReleaseCreateInput>;
};

/** An edge in a connection. */
export type ScheduledReleaseEdge = {
  __typename?: 'ScheduledReleaseEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: ScheduledRelease;
};

/** Identifies documents */
export type ScheduledReleaseManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  errorMessage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  errorMessage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  errorMessage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isActive_not?: InputMaybe<Scalars['Boolean']['input']>;
  isImplicit?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isImplicit_not?: InputMaybe<Scalars['Boolean']['input']>;
  operations_every?: InputMaybe<ScheduledOperationWhereInput>;
  operations_none?: InputMaybe<ScheduledOperationWhereInput>;
  operations_some?: InputMaybe<ScheduledOperationWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  releaseAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  releaseAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  releaseAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  releaseAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  releaseAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  releaseAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  releaseAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  status?: InputMaybe<ScheduledReleaseStatus>;
  /** All values that are contained in given list. */
  status_in?: InputMaybe<Array<InputMaybe<ScheduledReleaseStatus>>>;
  /** Any other value that exists and is not equal to the given value. */
  status_not?: InputMaybe<ScheduledReleaseStatus>;
  /** All values that are not contained in given list. */
  status_not_in?: InputMaybe<Array<InputMaybe<ScheduledReleaseStatus>>>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum ScheduledReleaseOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  ErrorMessageAsc = 'errorMessage_ASC',
  ErrorMessageDesc = 'errorMessage_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  IsActiveAsc = 'isActive_ASC',
  IsActiveDesc = 'isActive_DESC',
  IsImplicitAsc = 'isImplicit_ASC',
  IsImplicitDesc = 'isImplicit_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  ReleaseAtAsc = 'releaseAt_ASC',
  ReleaseAtDesc = 'releaseAt_DESC',
  StatusAsc = 'status_ASC',
  StatusDesc = 'status_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

/** System Scheduled Release Status */
export enum ScheduledReleaseStatus {
  Completed = 'COMPLETED',
  Failed = 'FAILED',
  InProgress = 'IN_PROGRESS',
  Pending = 'PENDING',
}

export type ScheduledReleaseUpdateInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ScheduledReleaseUpdateManyInlineInput = {
  /** Connect multiple existing ScheduledRelease documents */
  connect?: InputMaybe<Array<ScheduledReleaseConnectInput>>;
  /** Create and connect multiple ScheduledRelease documents */
  create?: InputMaybe<Array<ScheduledReleaseCreateInput>>;
  /** Delete multiple ScheduledRelease documents */
  delete?: InputMaybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Disconnect multiple ScheduledRelease documents */
  disconnect?: InputMaybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing ScheduledRelease documents */
  set?: InputMaybe<Array<ScheduledReleaseWhereUniqueInput>>;
  /** Update multiple ScheduledRelease documents */
  update?: InputMaybe<Array<ScheduledReleaseUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple ScheduledRelease documents */
  upsert?: InputMaybe<Array<ScheduledReleaseUpsertWithNestedWhereUniqueInput>>;
};

export type ScheduledReleaseUpdateManyInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type ScheduledReleaseUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: ScheduledReleaseUpdateManyInput;
  /** Document search */
  where: ScheduledReleaseWhereInput;
};

export type ScheduledReleaseUpdateOneInlineInput = {
  /** Connect existing ScheduledRelease document */
  connect?: InputMaybe<ScheduledReleaseWhereUniqueInput>;
  /** Create and connect one ScheduledRelease document */
  create?: InputMaybe<ScheduledReleaseCreateInput>;
  /** Delete currently connected ScheduledRelease document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected ScheduledRelease document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single ScheduledRelease document */
  update?: InputMaybe<ScheduledReleaseUpdateWithNestedWhereUniqueInput>;
  /** Upsert single ScheduledRelease document */
  upsert?: InputMaybe<ScheduledReleaseUpsertWithNestedWhereUniqueInput>;
};

export type ScheduledReleaseUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: ScheduledReleaseUpdateInput;
  /** Unique document search */
  where: ScheduledReleaseWhereUniqueInput;
};

export type ScheduledReleaseUpsertInput = {
  /** Create document if it didn't exist */
  create: ScheduledReleaseCreateInput;
  /** Update document if it exists */
  update: ScheduledReleaseUpdateInput;
};

export type ScheduledReleaseUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: ScheduledReleaseUpsertInput;
  /** Unique document search */
  where: ScheduledReleaseWhereUniqueInput;
};

/** Identifies documents */
export type ScheduledReleaseWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<ScheduledReleaseWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  errorMessage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  errorMessage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  errorMessage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  errorMessage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  errorMessage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  errorMessage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  errorMessage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  errorMessage_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  errorMessage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  errorMessage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isActive_not?: InputMaybe<Scalars['Boolean']['input']>;
  isImplicit?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isImplicit_not?: InputMaybe<Scalars['Boolean']['input']>;
  operations_every?: InputMaybe<ScheduledOperationWhereInput>;
  operations_none?: InputMaybe<ScheduledOperationWhereInput>;
  operations_some?: InputMaybe<ScheduledOperationWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  releaseAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  releaseAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  releaseAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  releaseAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  releaseAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  releaseAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  releaseAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  releaseAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  status?: InputMaybe<ScheduledReleaseStatus>;
  /** All values that are contained in given list. */
  status_in?: InputMaybe<Array<InputMaybe<ScheduledReleaseStatus>>>;
  /** Any other value that exists and is not equal to the given value. */
  status_not?: InputMaybe<ScheduledReleaseStatus>;
  /** All values that are not contained in given list. */
  status_not_in?: InputMaybe<Array<InputMaybe<ScheduledReleaseStatus>>>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** References ScheduledRelease record uniquely */
export type ScheduledReleaseWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type SpecialOffer = Entity &
  Node & {
    __typename?: 'SpecialOffer';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    description: Scalars['String']['output'];
    /** Get the document in other stages */
    documentInStages: Array<SpecialOffer>;
    /** List of SpecialOffer versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    lang: Language;
    link: Scalars['String']['output'];
    logo: Scalars['String']['output'];
    name: Scalars['String']['output'];
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    subtitle: Scalars['String']['output'];
    /** Seznam tagů oddělených čárkou */
    tags?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type SpecialOfferCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type SpecialOfferDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type SpecialOfferHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type SpecialOfferPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type SpecialOfferScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type SpecialOfferUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type SpecialOfferConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: SpecialOfferWhereUniqueInput;
};

/** A connection to a list of items. */
export type SpecialOfferConnection = {
  __typename?: 'SpecialOfferConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<SpecialOfferEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type SpecialOfferCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  description: Scalars['String']['input'];
  lang: Language;
  link: Scalars['String']['input'];
  logo: Scalars['String']['input'];
  name: Scalars['String']['input'];
  subtitle: Scalars['String']['input'];
  tags?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type SpecialOfferCreateManyInlineInput = {
  /** Connect multiple existing SpecialOffer documents */
  connect?: InputMaybe<Array<SpecialOfferWhereUniqueInput>>;
  /** Create and connect multiple existing SpecialOffer documents */
  create?: InputMaybe<Array<SpecialOfferCreateInput>>;
};

export type SpecialOfferCreateOneInlineInput = {
  /** Connect one existing SpecialOffer document */
  connect?: InputMaybe<SpecialOfferWhereUniqueInput>;
  /** Create and connect one SpecialOffer document */
  create?: InputMaybe<SpecialOfferCreateInput>;
};

/** An edge in a connection. */
export type SpecialOfferEdge = {
  __typename?: 'SpecialOfferEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: SpecialOffer;
};

/** Identifies documents */
export type SpecialOfferManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  documentInStages_every?: InputMaybe<SpecialOfferWhereStageInput>;
  documentInStages_none?: InputMaybe<SpecialOfferWhereStageInput>;
  documentInStages_some?: InputMaybe<SpecialOfferWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  lang?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  lang_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  lang_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  lang_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  logo_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  logo_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  logo_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  logo_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  logo_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  logo_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  logo_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  logo_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  logo_starts_with?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  subtitle_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  subtitle_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  subtitle_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  subtitle_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  subtitle_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  subtitle_starts_with?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  tags_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  tags_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  tags_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  tags_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  tags_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  tags_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum SpecialOfferOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DescriptionAsc = 'description_ASC',
  DescriptionDesc = 'description_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LangAsc = 'lang_ASC',
  LangDesc = 'lang_DESC',
  LinkAsc = 'link_ASC',
  LinkDesc = 'link_DESC',
  LogoAsc = 'logo_ASC',
  LogoDesc = 'logo_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  SubtitleAsc = 'subtitle_ASC',
  SubtitleDesc = 'subtitle_DESC',
  TagsAsc = 'tags_ASC',
  TagsDesc = 'tags_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type SpecialOfferUpdateInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  lang?: InputMaybe<Language>;
  link?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
};

export type SpecialOfferUpdateManyInlineInput = {
  /** Connect multiple existing SpecialOffer documents */
  connect?: InputMaybe<Array<SpecialOfferConnectInput>>;
  /** Create and connect multiple SpecialOffer documents */
  create?: InputMaybe<Array<SpecialOfferCreateInput>>;
  /** Delete multiple SpecialOffer documents */
  delete?: InputMaybe<Array<SpecialOfferWhereUniqueInput>>;
  /** Disconnect multiple SpecialOffer documents */
  disconnect?: InputMaybe<Array<SpecialOfferWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing SpecialOffer documents */
  set?: InputMaybe<Array<SpecialOfferWhereUniqueInput>>;
  /** Update multiple SpecialOffer documents */
  update?: InputMaybe<Array<SpecialOfferUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple SpecialOffer documents */
  upsert?: InputMaybe<Array<SpecialOfferUpsertWithNestedWhereUniqueInput>>;
};

export type SpecialOfferUpdateManyInput = {
  description?: InputMaybe<Scalars['String']['input']>;
  lang?: InputMaybe<Language>;
  link?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
};

export type SpecialOfferUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: SpecialOfferUpdateManyInput;
  /** Document search */
  where: SpecialOfferWhereInput;
};

export type SpecialOfferUpdateOneInlineInput = {
  /** Connect existing SpecialOffer document */
  connect?: InputMaybe<SpecialOfferWhereUniqueInput>;
  /** Create and connect one SpecialOffer document */
  create?: InputMaybe<SpecialOfferCreateInput>;
  /** Delete currently connected SpecialOffer document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected SpecialOffer document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single SpecialOffer document */
  update?: InputMaybe<SpecialOfferUpdateWithNestedWhereUniqueInput>;
  /** Upsert single SpecialOffer document */
  upsert?: InputMaybe<SpecialOfferUpsertWithNestedWhereUniqueInput>;
};

export type SpecialOfferUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: SpecialOfferUpdateInput;
  /** Unique document search */
  where: SpecialOfferWhereUniqueInput;
};

export type SpecialOfferUpsertInput = {
  /** Create document if it didn't exist */
  create: SpecialOfferCreateInput;
  /** Update document if it exists */
  update: SpecialOfferUpdateInput;
};

export type SpecialOfferUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: SpecialOfferUpsertInput;
  /** Unique document search */
  where: SpecialOfferWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type SpecialOfferWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type SpecialOfferWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<SpecialOfferWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  description?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  description_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  description_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  description_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  description_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  description_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  description_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  description_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  description_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  description_starts_with?: InputMaybe<Scalars['String']['input']>;
  documentInStages_every?: InputMaybe<SpecialOfferWhereStageInput>;
  documentInStages_none?: InputMaybe<SpecialOfferWhereStageInput>;
  documentInStages_some?: InputMaybe<SpecialOfferWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  lang?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  lang_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  lang_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  lang_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  link?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  link_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  link_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  link_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  link_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  link_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  link_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  link_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  link_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  link_starts_with?: InputMaybe<Scalars['String']['input']>;
  logo?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  logo_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  logo_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  logo_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  logo_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  logo_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  logo_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  logo_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  logo_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  logo_starts_with?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  subtitle_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  subtitle_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  subtitle_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  subtitle_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  subtitle_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  subtitle_starts_with?: InputMaybe<Scalars['String']['input']>;
  tags?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  tags_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  tags_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  tags_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  tags_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  tags_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  tags_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type SpecialOfferWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<SpecialOfferWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<SpecialOfferWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<SpecialOfferWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<SpecialOfferWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References SpecialOffer record uniquely */
export type SpecialOfferWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

/** Stage system enumeration */
export enum Stage {
  /** The Draft is the default stage for all your content. */
  Draft = 'DRAFT',
  /** The Published stage is where you can publish your content to. */
  Published = 'PUBLISHED',
}

export enum SystemDateTimeFieldVariation {
  Base = 'BASE',
  Combined = 'COMBINED',
  Localization = 'LOCALIZATION',
}

export type Task = Entity &
  Node & {
    __typename?: 'Task';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<Task>;
    /** Number of spent minutes */
    duration?: Maybe<Scalars['Int']['output']>;
    /** List of Task versions */
    history: Array<Version>;
    /** Link to the content page. */
    howToPage?: Maybe<Scalars['String']['output']>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    label?: Maybe<Scalars['String']['output']>;
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<Task>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    question?: Maybe<Scalars['String']['output']>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** Perex text */
    subtitle?: Maybe<Scalars['String']['output']>;
    thumbnail?: Maybe<Asset>;
    /** Title of the content page and task. */
    title?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

export type TaskCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TaskCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type TaskDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type TaskHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type TaskLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type TaskPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TaskPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type TaskScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type TaskThumbnailArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type TaskUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type TaskUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type TaskConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: TaskWhereUniqueInput;
};

/** A connection to a list of items. */
export type TaskConnection = {
  __typename?: 'TaskConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<TaskEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type TaskCreateInput = {
  ckvcbt64u0uot01wiae277znl?: InputMaybe<ComponentCreateManyInlineInput>;
  ckvce0dei0xft01y0djia1p95?: InputMaybe<RecommendedTaskCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  howToPage?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<TaskCreateLocalizationsInput>;
  question?: InputMaybe<Scalars['String']['input']>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  thumbnail?: InputMaybe<AssetCreateOneInlineInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type TaskCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type TaskCreateLocalizationInput = {
  /** Localization input */
  data: TaskCreateLocalizationDataInput;
  locale: Locale;
};

export type TaskCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<TaskCreateLocalizationInput>>;
};

export type TaskCreateManyInlineInput = {
  /** Connect multiple existing Task documents */
  connect?: InputMaybe<Array<TaskWhereUniqueInput>>;
  /** Create and connect multiple existing Task documents */
  create?: InputMaybe<Array<TaskCreateInput>>;
};

export type TaskCreateOneInlineInput = {
  /** Connect one existing Task document */
  connect?: InputMaybe<TaskWhereUniqueInput>;
  /** Create and connect one Task document */
  create?: InputMaybe<TaskCreateInput>;
};

/** An edge in a connection. */
export type TaskEdge = {
  __typename?: 'TaskEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Task;
};

/** Identifies documents */
export type TaskManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<TaskWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<TaskWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<TaskWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<TaskWhereStageInput>;
  documentInStages_none?: InputMaybe<TaskWhereStageInput>;
  documentInStages_some?: InputMaybe<TaskWhereStageInput>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  /** All values greater than the given value. */
  duration_gt?: InputMaybe<Scalars['Int']['input']>;
  /** All values greater than or equal the given value. */
  duration_gte?: InputMaybe<Scalars['Int']['input']>;
  /** All values that are contained in given list. */
  duration_in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** All values less than the given value. */
  duration_lt?: InputMaybe<Scalars['Int']['input']>;
  /** All values less than or equal the given value. */
  duration_lte?: InputMaybe<Scalars['Int']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  duration_not?: InputMaybe<Scalars['Int']['input']>;
  /** All values that are not contained in given list. */
  duration_not_in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  howToPage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  howToPage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  howToPage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  howToPage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  howToPage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  howToPage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  howToPage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  howToPage_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  howToPage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  howToPage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  question?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  question_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  question_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  question_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  question_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  question_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  question_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  question_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  question_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  question_starts_with?: InputMaybe<Scalars['String']['input']>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  subtitle_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  subtitle_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  subtitle_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  subtitle_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  subtitle_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  subtitle_starts_with?: InputMaybe<Scalars['String']['input']>;
  thumbnail?: InputMaybe<AssetWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum TaskOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  DurationAsc = 'duration_ASC',
  DurationDesc = 'duration_DESC',
  HowToPageAsc = 'howToPage_ASC',
  HowToPageDesc = 'howToPage_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LabelAsc = 'label_ASC',
  LabelDesc = 'label_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  QuestionAsc = 'question_ASC',
  QuestionDesc = 'question_DESC',
  SubtitleAsc = 'subtitle_ASC',
  SubtitleDesc = 'subtitle_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type TaskUpdateInput = {
  ckvcbt64u0uot01wiae277znl?: InputMaybe<ComponentUpdateManyInlineInput>;
  ckvce0dei0xft01y0djia1p95?: InputMaybe<RecommendedTaskUpdateManyInlineInput>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  howToPage?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<TaskUpdateLocalizationsInput>;
  question?: InputMaybe<Scalars['String']['input']>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  thumbnail?: InputMaybe<AssetUpdateOneInlineInput>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type TaskUpdateLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type TaskUpdateLocalizationInput = {
  data: TaskUpdateLocalizationDataInput;
  locale: Locale;
};

export type TaskUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<TaskCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<TaskUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<TaskUpsertLocalizationInput>>;
};

export type TaskUpdateManyInlineInput = {
  /** Connect multiple existing Task documents */
  connect?: InputMaybe<Array<TaskConnectInput>>;
  /** Create and connect multiple Task documents */
  create?: InputMaybe<Array<TaskCreateInput>>;
  /** Delete multiple Task documents */
  delete?: InputMaybe<Array<TaskWhereUniqueInput>>;
  /** Disconnect multiple Task documents */
  disconnect?: InputMaybe<Array<TaskWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Task documents */
  set?: InputMaybe<Array<TaskWhereUniqueInput>>;
  /** Update multiple Task documents */
  update?: InputMaybe<Array<TaskUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Task documents */
  upsert?: InputMaybe<Array<TaskUpsertWithNestedWhereUniqueInput>>;
};

export type TaskUpdateManyInput = {
  duration?: InputMaybe<Scalars['Int']['input']>;
  howToPage?: InputMaybe<Scalars['String']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<TaskUpdateManyLocalizationsInput>;
  question?: InputMaybe<Scalars['String']['input']>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type TaskUpdateManyLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type TaskUpdateManyLocalizationInput = {
  data: TaskUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type TaskUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<TaskUpdateManyLocalizationInput>>;
};

export type TaskUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: TaskUpdateManyInput;
  /** Document search */
  where: TaskWhereInput;
};

export type TaskUpdateOneInlineInput = {
  /** Connect existing Task document */
  connect?: InputMaybe<TaskWhereUniqueInput>;
  /** Create and connect one Task document */
  create?: InputMaybe<TaskCreateInput>;
  /** Delete currently connected Task document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Task document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Task document */
  update?: InputMaybe<TaskUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Task document */
  upsert?: InputMaybe<TaskUpsertWithNestedWhereUniqueInput>;
};

export type TaskUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: TaskUpdateInput;
  /** Unique document search */
  where: TaskWhereUniqueInput;
};

export type TaskUpsertInput = {
  /** Create document if it didn't exist */
  create: TaskCreateInput;
  /** Update document if it exists */
  update: TaskUpdateInput;
};

export type TaskUpsertLocalizationInput = {
  create: TaskCreateLocalizationDataInput;
  locale: Locale;
  update: TaskUpdateLocalizationDataInput;
};

export type TaskUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: TaskUpsertInput;
  /** Unique document search */
  where: TaskWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type TaskWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type TaskWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<TaskWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<TaskWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<TaskWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<TaskWhereStageInput>;
  documentInStages_none?: InputMaybe<TaskWhereStageInput>;
  documentInStages_some?: InputMaybe<TaskWhereStageInput>;
  duration?: InputMaybe<Scalars['Int']['input']>;
  /** All values greater than the given value. */
  duration_gt?: InputMaybe<Scalars['Int']['input']>;
  /** All values greater than or equal the given value. */
  duration_gte?: InputMaybe<Scalars['Int']['input']>;
  /** All values that are contained in given list. */
  duration_in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  /** All values less than the given value. */
  duration_lt?: InputMaybe<Scalars['Int']['input']>;
  /** All values less than or equal the given value. */
  duration_lte?: InputMaybe<Scalars['Int']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  duration_not?: InputMaybe<Scalars['Int']['input']>;
  /** All values that are not contained in given list. */
  duration_not_in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  howToPage?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  howToPage_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  howToPage_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  howToPage_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  howToPage_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  howToPage_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  howToPage_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  howToPage_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  howToPage_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  howToPage_starts_with?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  question?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  question_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  question_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  question_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  question_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  question_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  question_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  question_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  question_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  question_starts_with?: InputMaybe<Scalars['String']['input']>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  subtitle?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  subtitle_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  subtitle_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  subtitle_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  subtitle_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  subtitle_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  subtitle_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  subtitle_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  subtitle_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  subtitle_starts_with?: InputMaybe<Scalars['String']['input']>;
  thumbnail?: InputMaybe<AssetWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type TaskWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<TaskWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<TaskWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<TaskWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<TaskWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Task record uniquely */
export type TaskWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type UnpublishLocaleInput = {
  /** Locales to unpublish */
  locale: Locale;
  /** Stages to unpublish selected locales from */
  stages: Array<Stage>;
};

/** User system model */
export type User = Entity &
  Node & {
    __typename?: 'User';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** Get the document in other stages */
    documentInStages: Array<User>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** Flag to determine if user is active or not */
    isActive: Scalars['Boolean']['output'];
    /** User Kind. Can be either MEMBER, PAT or PUBLIC */
    kind: UserKind;
    /** The username */
    name: Scalars['String']['output'];
    /** Profile Picture url */
    picture?: Maybe<Scalars['String']['output']>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
  };

/** User system model */
export type UserDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type UserConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: UserWhereUniqueInput;
};

/** A connection to a list of items. */
export type UserConnection = {
  __typename?: 'UserConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<UserEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type UserCreateManyInlineInput = {
  /** Connect multiple existing User documents */
  connect?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type UserCreateOneInlineInput = {
  /** Connect one existing User document */
  connect?: InputMaybe<UserWhereUniqueInput>;
};

/** An edge in a connection. */
export type UserEdge = {
  __typename?: 'UserEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: User;
};

/** System User Kind */
export enum UserKind {
  Member = 'MEMBER',
  Pat = 'PAT',
  Public = 'PUBLIC',
  Webhook = 'WEBHOOK',
}

/** Identifies documents */
export type UserManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<UserWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<UserWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<UserWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  documentInStages_every?: InputMaybe<UserWhereStageInput>;
  documentInStages_none?: InputMaybe<UserWhereStageInput>;
  documentInStages_some?: InputMaybe<UserWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isActive_not?: InputMaybe<Scalars['Boolean']['input']>;
  kind?: InputMaybe<UserKind>;
  /** All values that are contained in given list. */
  kind_in?: InputMaybe<Array<InputMaybe<UserKind>>>;
  /** Any other value that exists and is not equal to the given value. */
  kind_not?: InputMaybe<UserKind>;
  /** All values that are not contained in given list. */
  kind_not_in?: InputMaybe<Array<InputMaybe<UserKind>>>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  picture?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  picture_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  picture_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  picture_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  picture_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  picture_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  picture_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  picture_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  picture_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  picture_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
};

export enum UserOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  IsActiveAsc = 'isActive_ASC',
  IsActiveDesc = 'isActive_DESC',
  KindAsc = 'kind_ASC',
  KindDesc = 'kind_DESC',
  NameAsc = 'name_ASC',
  NameDesc = 'name_DESC',
  PictureAsc = 'picture_ASC',
  PictureDesc = 'picture_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type UserUpdateManyInlineInput = {
  /** Connect multiple existing User documents */
  connect?: InputMaybe<Array<UserConnectInput>>;
  /** Disconnect multiple User documents */
  disconnect?: InputMaybe<Array<UserWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing User documents */
  set?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type UserUpdateOneInlineInput = {
  /** Connect existing User document */
  connect?: InputMaybe<UserWhereUniqueInput>;
  /** Disconnect currently connected User document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
};

/** This contains a set of filters that can be used to compare values internally */
export type UserWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type UserWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<UserWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<UserWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<UserWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  documentInStages_every?: InputMaybe<UserWhereStageInput>;
  documentInStages_none?: InputMaybe<UserWhereStageInput>;
  documentInStages_some?: InputMaybe<UserWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isActive?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isActive_not?: InputMaybe<Scalars['Boolean']['input']>;
  kind?: InputMaybe<UserKind>;
  /** All values that are contained in given list. */
  kind_in?: InputMaybe<Array<InputMaybe<UserKind>>>;
  /** Any other value that exists and is not equal to the given value. */
  kind_not?: InputMaybe<UserKind>;
  /** All values that are not contained in given list. */
  kind_not_in?: InputMaybe<Array<InputMaybe<UserKind>>>;
  name?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  name_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  name_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  name_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  name_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  name_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  name_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  name_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  name_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  name_starts_with?: InputMaybe<Scalars['String']['input']>;
  picture?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  picture_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  picture_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  picture_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  picture_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  picture_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  picture_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  picture_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  picture_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  picture_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type UserWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<UserWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<UserWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<UserWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<UserWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References User record uniquely */
export type UserWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Version = {
  __typename?: 'Version';
  createdAt: Scalars['DateTime']['output'];
  id: Scalars['ID']['output'];
  revision: Scalars['Int']['output'];
  stage: Stage;
};

export type VersionWhereInput = {
  id: Scalars['ID']['input'];
  revision: Scalars['Int']['input'];
  stage: Stage;
};

export type Video = Entity &
  Node & {
    __typename?: 'Video';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<Video>;
    /** List of Video versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    mainActor?: Maybe<Scalars['String']['output']>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    title?: Maybe<Scalars['String']['output']>;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
    url: Scalars['String']['output'];
  };

export type VideoCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type VideoHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type VideoPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type VideoUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: VideoWhereUniqueInput;
};

/** A connection to a list of items. */
export type VideoConnection = {
  __typename?: 'VideoConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<VideoEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type VideoCreateInput = {
  ckwxocc3n06ox01z43i9n80cr?: InputMaybe<ContentPageCreateManyInlineInput>;
  ckyw2vmng0tv601xwd7oq48zq?: InputMaybe<VideoLessonCreateManyInlineInput>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  mainActor?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  url: Scalars['String']['input'];
};

export type VideoCreateManyInlineInput = {
  /** Connect multiple existing Video documents */
  connect?: InputMaybe<Array<VideoWhereUniqueInput>>;
  /** Create and connect multiple existing Video documents */
  create?: InputMaybe<Array<VideoCreateInput>>;
};

export type VideoCreateOneInlineInput = {
  /** Connect one existing Video document */
  connect?: InputMaybe<VideoWhereUniqueInput>;
  /** Create and connect one Video document */
  create?: InputMaybe<VideoCreateInput>;
};

/** An edge in a connection. */
export type VideoEdge = {
  __typename?: 'VideoEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Video;
};

export type VideoLesson = Entity &
  Node & {
    __typename?: 'VideoLesson';
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    /** Get the document in other stages */
    documentInStages: Array<VideoLesson>;
    /** List of VideoLesson versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** System Locale field */
    locale: Locale;
    /** Get the other localizations for this document */
    localizations: Array<VideoLesson>;
    primaryRichText?: Maybe<RichText>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    secondaryRichText?: Maybe<RichText>;
    /** System stage field */
    stage: Stage;
    tags: CmsProjectPhase;
    title: Scalars['String']['output'];
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
    video?: Maybe<Video>;
  };

export type VideoLessonCreatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type VideoLessonCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoLessonDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

export type VideoLessonHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

export type VideoLessonLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type VideoLessonPublishedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type VideoLessonPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoLessonScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

export type VideoLessonUpdatedAtArgs = {
  variation?: SystemDateTimeFieldVariation;
};

export type VideoLessonUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoLessonVideoArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type VideoLessonConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: VideoLessonWhereUniqueInput;
};

/** A connection to a list of items. */
export type VideoLessonConnection = {
  __typename?: 'VideoLessonConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<VideoLessonEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type VideoLessonCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<VideoLessonCreateLocalizationsInput>;
  primaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  secondaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  tags: CmsProjectPhase;
  /** title input for default locale (cz) */
  title: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  video?: InputMaybe<VideoCreateOneInlineInput>;
};

export type VideoLessonCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  title: Scalars['String']['input'];
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type VideoLessonCreateLocalizationInput = {
  /** Localization input */
  data: VideoLessonCreateLocalizationDataInput;
  locale: Locale;
};

export type VideoLessonCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<VideoLessonCreateLocalizationInput>>;
};

export type VideoLessonCreateManyInlineInput = {
  /** Connect multiple existing VideoLesson documents */
  connect?: InputMaybe<Array<VideoLessonWhereUniqueInput>>;
  /** Create and connect multiple existing VideoLesson documents */
  create?: InputMaybe<Array<VideoLessonCreateInput>>;
};

export type VideoLessonCreateOneInlineInput = {
  /** Connect one existing VideoLesson document */
  connect?: InputMaybe<VideoLessonWhereUniqueInput>;
  /** Create and connect one VideoLesson document */
  create?: InputMaybe<VideoLessonCreateInput>;
};

/** An edge in a connection. */
export type VideoLessonEdge = {
  __typename?: 'VideoLessonEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: VideoLesson;
};

/** Identifies documents */
export type VideoLessonManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<VideoLessonWhereStageInput>;
  documentInStages_none?: InputMaybe<VideoLessonWhereStageInput>;
  documentInStages_some?: InputMaybe<VideoLessonWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tags?: InputMaybe<CmsProjectPhase>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<CmsProjectPhase>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<CmsProjectPhase>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<CmsProjectPhase>>>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  video?: InputMaybe<VideoWhereInput>;
};

export enum VideoLessonOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TagsAsc = 'tags_ASC',
  TagsDesc = 'tags_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type VideoLessonUpdateInput = {
  /** Manage document localizations */
  localizations?: InputMaybe<VideoLessonUpdateLocalizationsInput>;
  primaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  secondaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  tags?: InputMaybe<CmsProjectPhase>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
  video?: InputMaybe<VideoUpdateOneInlineInput>;
};

export type VideoLessonUpdateLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type VideoLessonUpdateLocalizationInput = {
  data: VideoLessonUpdateLocalizationDataInput;
  locale: Locale;
};

export type VideoLessonUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<VideoLessonCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<VideoLessonUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<VideoLessonUpsertLocalizationInput>>;
};

export type VideoLessonUpdateManyInlineInput = {
  /** Connect multiple existing VideoLesson documents */
  connect?: InputMaybe<Array<VideoLessonConnectInput>>;
  /** Create and connect multiple VideoLesson documents */
  create?: InputMaybe<Array<VideoLessonCreateInput>>;
  /** Delete multiple VideoLesson documents */
  delete?: InputMaybe<Array<VideoLessonWhereUniqueInput>>;
  /** Disconnect multiple VideoLesson documents */
  disconnect?: InputMaybe<Array<VideoLessonWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing VideoLesson documents */
  set?: InputMaybe<Array<VideoLessonWhereUniqueInput>>;
  /** Update multiple VideoLesson documents */
  update?: InputMaybe<Array<VideoLessonUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple VideoLesson documents */
  upsert?: InputMaybe<Array<VideoLessonUpsertWithNestedWhereUniqueInput>>;
};

export type VideoLessonUpdateManyInput = {
  /** Optional updates to localizations */
  localizations?: InputMaybe<VideoLessonUpdateManyLocalizationsInput>;
  primaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  secondaryRichText?: InputMaybe<Scalars['RichTextAST']['input']>;
  tags?: InputMaybe<CmsProjectPhase>;
  /** title input for default locale (cz) */
  title?: InputMaybe<Scalars['String']['input']>;
};

export type VideoLessonUpdateManyLocalizationDataInput = {
  title?: InputMaybe<Scalars['String']['input']>;
};

export type VideoLessonUpdateManyLocalizationInput = {
  data: VideoLessonUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type VideoLessonUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<VideoLessonUpdateManyLocalizationInput>>;
};

export type VideoLessonUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: VideoLessonUpdateManyInput;
  /** Document search */
  where: VideoLessonWhereInput;
};

export type VideoLessonUpdateOneInlineInput = {
  /** Connect existing VideoLesson document */
  connect?: InputMaybe<VideoLessonWhereUniqueInput>;
  /** Create and connect one VideoLesson document */
  create?: InputMaybe<VideoLessonCreateInput>;
  /** Delete currently connected VideoLesson document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected VideoLesson document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single VideoLesson document */
  update?: InputMaybe<VideoLessonUpdateWithNestedWhereUniqueInput>;
  /** Upsert single VideoLesson document */
  upsert?: InputMaybe<VideoLessonUpsertWithNestedWhereUniqueInput>;
};

export type VideoLessonUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: VideoLessonUpdateInput;
  /** Unique document search */
  where: VideoLessonWhereUniqueInput;
};

export type VideoLessonUpsertInput = {
  /** Create document if it didn't exist */
  create: VideoLessonCreateInput;
  /** Update document if it exists */
  update: VideoLessonUpdateInput;
};

export type VideoLessonUpsertLocalizationInput = {
  create: VideoLessonCreateLocalizationDataInput;
  locale: Locale;
  update: VideoLessonUpdateLocalizationDataInput;
};

export type VideoLessonUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: VideoLessonUpsertInput;
  /** Unique document search */
  where: VideoLessonWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type VideoLessonWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type VideoLessonWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoLessonWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<VideoLessonWhereStageInput>;
  documentInStages_none?: InputMaybe<VideoLessonWhereStageInput>;
  documentInStages_some?: InputMaybe<VideoLessonWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  tags?: InputMaybe<CmsProjectPhase>;
  /** All values that are contained in given list. */
  tags_in?: InputMaybe<Array<InputMaybe<CmsProjectPhase>>>;
  /** Any other value that exists and is not equal to the given value. */
  tags_not?: InputMaybe<CmsProjectPhase>;
  /** All values that are not contained in given list. */
  tags_not_in?: InputMaybe<Array<InputMaybe<CmsProjectPhase>>>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  video?: InputMaybe<VideoWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type VideoLessonWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoLessonWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoLessonWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoLessonWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<VideoLessonWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References VideoLesson record uniquely */
export type VideoLessonWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

/** Identifies documents */
export type VideoManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<VideoWhereStageInput>;
  documentInStages_none?: InputMaybe<VideoWhereStageInput>;
  documentInStages_some?: InputMaybe<VideoWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  mainActor?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  mainActor_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  mainActor_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  mainActor_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  mainActor_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  mainActor_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  mainActor_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  mainActor_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  mainActor_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  mainActor_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  url?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  url_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  url_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  url_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  url_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  url_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  url_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  url_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  url_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  url_starts_with?: InputMaybe<Scalars['String']['input']>;
};

export enum VideoOrderByInput {
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  MainActorAsc = 'mainActor_ASC',
  MainActorDesc = 'mainActor_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  TitleAsc = 'title_ASC',
  TitleDesc = 'title_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
  UrlAsc = 'url_ASC',
  UrlDesc = 'url_DESC',
}

export type VideoUpdateInput = {
  ckwxocc3n06ox01z43i9n80cr?: InputMaybe<ContentPageUpdateManyInlineInput>;
  ckyw2vmng0tv601xwd7oq48zq?: InputMaybe<VideoLessonUpdateManyInlineInput>;
  mainActor?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type VideoUpdateManyInlineInput = {
  /** Connect multiple existing Video documents */
  connect?: InputMaybe<Array<VideoConnectInput>>;
  /** Create and connect multiple Video documents */
  create?: InputMaybe<Array<VideoCreateInput>>;
  /** Delete multiple Video documents */
  delete?: InputMaybe<Array<VideoWhereUniqueInput>>;
  /** Disconnect multiple Video documents */
  disconnect?: InputMaybe<Array<VideoWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Video documents */
  set?: InputMaybe<Array<VideoWhereUniqueInput>>;
  /** Update multiple Video documents */
  update?: InputMaybe<Array<VideoUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Video documents */
  upsert?: InputMaybe<Array<VideoUpsertWithNestedWhereUniqueInput>>;
};

export type VideoUpdateManyInput = {
  mainActor?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
};

export type VideoUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: VideoUpdateManyInput;
  /** Document search */
  where: VideoWhereInput;
};

export type VideoUpdateOneInlineInput = {
  /** Connect existing Video document */
  connect?: InputMaybe<VideoWhereUniqueInput>;
  /** Create and connect one Video document */
  create?: InputMaybe<VideoCreateInput>;
  /** Delete currently connected Video document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Video document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Video document */
  update?: InputMaybe<VideoUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Video document */
  upsert?: InputMaybe<VideoUpsertWithNestedWhereUniqueInput>;
};

export type VideoUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: VideoUpdateInput;
  /** Unique document search */
  where: VideoWhereUniqueInput;
};

export type VideoUpsertInput = {
  /** Create document if it didn't exist */
  create: VideoCreateInput;
  /** Update document if it exists */
  update: VideoUpdateInput;
};

export type VideoUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: VideoUpsertInput;
  /** Unique document search */
  where: VideoWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type VideoWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type VideoWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  documentInStages_every?: InputMaybe<VideoWhereStageInput>;
  documentInStages_none?: InputMaybe<VideoWhereStageInput>;
  documentInStages_some?: InputMaybe<VideoWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  mainActor?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  mainActor_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  mainActor_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  mainActor_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  mainActor_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  mainActor_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  mainActor_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  mainActor_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  mainActor_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  mainActor_starts_with?: InputMaybe<Scalars['String']['input']>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  title?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  title_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  title_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  title_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  title_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  title_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  title_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  title_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  title_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  title_starts_with?: InputMaybe<Scalars['String']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
  url?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  url_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  url_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  url_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  url_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  url_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  url_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  url_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  url_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  url_starts_with?: InputMaybe<Scalars['String']['input']>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type VideoWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<VideoWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<VideoWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<VideoWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<VideoWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Video record uniquely */
export type VideoWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Wee = Entity & {
  __typename?: 'Wee';
  /** The unique identifier */
  id: Scalars['ID']['output'];
  label?: Maybe<Scalars['String']['output']>;
  /** System Locale field */
  locale: Locale;
  /** Get the other localizations for this document */
  localizations: Array<Wee>;
  /** System stage field */
  stage: Stage;
};

export type WeeLocalizationsArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  locales?: Array<Locale>;
};

export type WeeConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: WeeWhereUniqueInput;
};

/** A connection to a list of items. */
export type WeeConnection = {
  __typename?: 'WeeConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<WeeEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type WeeCreateInput = {
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<WeeCreateLocalizationsInput>;
};

export type WeeCreateLocalizationDataInput = {
  label?: InputMaybe<Scalars['String']['input']>;
};

export type WeeCreateLocalizationInput = {
  /** Localization input */
  data: WeeCreateLocalizationDataInput;
  locale: Locale;
};

export type WeeCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<WeeCreateLocalizationInput>>;
};

export type WeeCreateManyInlineInput = {
  /** Create and connect multiple existing Wee documents */
  create?: InputMaybe<Array<WeeCreateInput>>;
};

export type WeeCreateOneInlineInput = {
  /** Create and connect one Wee document */
  create?: InputMaybe<WeeCreateInput>;
};

export type WeeCreateWithPositionInput = {
  /** Document to create */
  data: WeeCreateInput;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
};

/** An edge in a connection. */
export type WeeEdge = {
  __typename?: 'WeeEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Wee;
};

/** Identifies documents */
export type WeeManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<WeeWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<WeeWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<WeeWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
};

export enum WeeOrderByInput {
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  LabelAsc = 'label_ASC',
  LabelDesc = 'label_DESC',
}

export type WeeParent = Eewe | LinkToFeedbackForm;

export type WeeParentConnectInput = {
  Eewe?: InputMaybe<EeweConnectInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormConnectInput>;
};

export type WeeParentCreateInput = {
  Eewe?: InputMaybe<EeweCreateInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateInput>;
};

export type WeeParentCreateManyInlineInput = {
  /** Connect multiple existing WeeParent documents */
  connect?: InputMaybe<Array<WeeParentWhereUniqueInput>>;
  /** Create and connect multiple existing WeeParent documents */
  create?: InputMaybe<Array<WeeParentCreateInput>>;
};

export type WeeParentCreateOneInlineInput = {
  /** Connect one existing WeeParent document */
  connect?: InputMaybe<WeeParentWhereUniqueInput>;
  /** Create and connect one WeeParent document */
  create?: InputMaybe<WeeParentCreateInput>;
};

export type WeeParentUpdateInput = {
  Eewe?: InputMaybe<EeweUpdateInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateInput>;
};

export type WeeParentUpdateManyInlineInput = {
  /** Connect multiple existing WeeParent documents */
  connect?: InputMaybe<Array<WeeParentConnectInput>>;
  /** Create and connect multiple WeeParent documents */
  create?: InputMaybe<Array<WeeParentCreateInput>>;
  /** Delete multiple WeeParent documents */
  delete?: InputMaybe<Array<WeeParentWhereUniqueInput>>;
  /** Disconnect multiple WeeParent documents */
  disconnect?: InputMaybe<Array<WeeParentWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing WeeParent documents */
  set?: InputMaybe<Array<WeeParentWhereUniqueInput>>;
  /** Update multiple WeeParent documents */
  update?: InputMaybe<Array<WeeParentUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple WeeParent documents */
  upsert?: InputMaybe<Array<WeeParentUpsertWithNestedWhereUniqueInput>>;
};

export type WeeParentUpdateManyWithNestedWhereInput = {
  Eewe?: InputMaybe<EeweUpdateManyWithNestedWhereInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateManyWithNestedWhereInput>;
};

export type WeeParentUpdateOneInlineInput = {
  /** Connect existing WeeParent document */
  connect?: InputMaybe<WeeParentWhereUniqueInput>;
  /** Create and connect one WeeParent document */
  create?: InputMaybe<WeeParentCreateInput>;
  /** Delete currently connected WeeParent document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected WeeParent document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single WeeParent document */
  update?: InputMaybe<WeeParentUpdateWithNestedWhereUniqueInput>;
  /** Upsert single WeeParent document */
  upsert?: InputMaybe<WeeParentUpsertWithNestedWhereUniqueInput>;
};

export type WeeParentUpdateWithNestedWhereUniqueInput = {
  Eewe?: InputMaybe<EeweUpdateWithNestedWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateWithNestedWhereUniqueInput>;
};

export type WeeParentUpsertWithNestedWhereUniqueInput = {
  Eewe?: InputMaybe<EeweUpsertWithNestedWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpsertWithNestedWhereUniqueInput>;
};

export type WeeParentWhereInput = {
  Eewe?: InputMaybe<EeweWhereInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
};

export type WeeParentWhereUniqueInput = {
  Eewe?: InputMaybe<EeweWhereUniqueInput>;
  LinkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereUniqueInput>;
};

export type WeeUpdateInput = {
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<WeeUpdateLocalizationsInput>;
};

export type WeeUpdateLocalizationDataInput = {
  label?: InputMaybe<Scalars['String']['input']>;
};

export type WeeUpdateLocalizationInput = {
  data: WeeUpdateLocalizationDataInput;
  locale: Locale;
};

export type WeeUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<WeeCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
  /** Localizations to update */
  update?: InputMaybe<Array<WeeUpdateLocalizationInput>>;
  upsert?: InputMaybe<Array<WeeUpsertLocalizationInput>>;
};

export type WeeUpdateManyInlineInput = {
  /** Create and connect multiple Wee component instances */
  create?: InputMaybe<Array<WeeCreateWithPositionInput>>;
  /** Delete multiple Wee documents */
  delete?: InputMaybe<Array<WeeWhereUniqueInput>>;
  /** Update multiple Wee component instances */
  update?: InputMaybe<Array<WeeUpdateWithNestedWhereUniqueAndPositionInput>>;
  /** Upsert multiple Wee component instances */
  upsert?: InputMaybe<Array<WeeUpsertWithNestedWhereUniqueAndPositionInput>>;
};

export type WeeUpdateManyInput = {
  /** label input for default locale (cz) */
  label?: InputMaybe<Scalars['String']['input']>;
  /** Optional updates to localizations */
  localizations?: InputMaybe<WeeUpdateManyLocalizationsInput>;
};

export type WeeUpdateManyLocalizationDataInput = {
  label?: InputMaybe<Scalars['String']['input']>;
};

export type WeeUpdateManyLocalizationInput = {
  data: WeeUpdateManyLocalizationDataInput;
  locale: Locale;
};

export type WeeUpdateManyLocalizationsInput = {
  /** Localizations to update */
  update?: InputMaybe<Array<WeeUpdateManyLocalizationInput>>;
};

export type WeeUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: WeeUpdateManyInput;
  /** Document search */
  where: WeeWhereInput;
};

export type WeeUpdateOneInlineInput = {
  /** Create and connect one Wee document */
  create?: InputMaybe<WeeCreateInput>;
  /** Delete currently connected Wee document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Wee document */
  update?: InputMaybe<WeeUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Wee document */
  upsert?: InputMaybe<WeeUpsertWithNestedWhereUniqueInput>;
};

export type WeeUpdateWithNestedWhereUniqueAndPositionInput = {
  /** Document to update */
  data?: InputMaybe<WeeUpdateInput>;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Unique component instance search */
  where: WeeWhereUniqueInput;
};

export type WeeUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: WeeUpdateInput;
  /** Unique document search */
  where: WeeWhereUniqueInput;
};

export type WeeUpsertInput = {
  /** Create document if it didn't exist */
  create: WeeCreateInput;
  /** Update document if it exists */
  update: WeeUpdateInput;
};

export type WeeUpsertLocalizationInput = {
  create: WeeCreateLocalizationDataInput;
  locale: Locale;
  update: WeeUpdateLocalizationDataInput;
};

export type WeeUpsertWithNestedWhereUniqueAndPositionInput = {
  /** Document to upsert */
  data?: InputMaybe<WeeUpsertInput>;
  /** Position in the list of existing component instances, will default to appending at the end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Unique component instance search */
  where: WeeWhereUniqueInput;
};

export type WeeUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: WeeUpsertInput;
  /** Unique document search */
  where: WeeWhereUniqueInput;
};

/** Identifies documents */
export type WeeWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<WeeWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<WeeWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<WeeWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  label_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  label_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  label_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  label_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  label_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  label_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  label_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  label_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  label_starts_with?: InputMaybe<Scalars['String']['input']>;
};

/** References Wee record uniquely */
export type WeeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']['input']>;
};

/** Whitelabel config for each subdomain */
export type Whitelabel = Entity &
  Node & {
    __typename?: 'Whitelabel';
    /** Client is the subdomain. Name has to exactly match the subdomain */
    client: Scalars['String']['output'];
    communityLink: Scalars['String']['output'];
    communityLinkLabel: Scalars['String']['output'];
    /** The time the document was created */
    createdAt: Scalars['DateTime']['output'];
    /** User that created this document */
    createdBy?: Maybe<User>;
    customPartners?: Maybe<Scalars['String']['output']>;
    /** Optionally set default language of the app */
    defaultLanguage?: Maybe<Language>;
    /** Get the document in other stages */
    documentInStages: Array<Whitelabel>;
    /** List of Whitelabel versions */
    history: Array<Version>;
    /** The unique identifier */
    id: Scalars['ID']['output'];
    /** This toggles the link to partners in navigation */
    isLinkToPartnersVisible: Scalars['Boolean']['output'];
    linkToFeedbackForm?: Maybe<LinkToFeedbackForm>;
    linksToQuiz?: Maybe<Scalars['Json']['output']>;
    /** If logo is entered it will override the default sbox logo */
    logo?: Maybe<Asset>;
    /** The time the document was published. Null on documents in draft stage. */
    publishedAt?: Maybe<Scalars['DateTime']['output']>;
    /** User that last published this document */
    publishedBy?: Maybe<User>;
    scheduledIn: Array<ScheduledOperation>;
    /** System stage field */
    stage: Stage;
    /** The time the document was updated */
    updatedAt: Scalars['DateTime']['output'];
    /** User that last updated this document */
    updatedBy?: Maybe<User>;
  };

/** Whitelabel config for each subdomain */
export type WhitelabelCreatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelDocumentInStagesArgs = {
  includeCurrent?: Scalars['Boolean']['input'];
  inheritLocale?: Scalars['Boolean']['input'];
  stages?: Array<Stage>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelHistoryArgs = {
  limit?: Scalars['Int']['input'];
  skip?: Scalars['Int']['input'];
  stageOverride?: InputMaybe<Stage>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelLinkToFeedbackFormArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelLogoArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelPublishedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelScheduledInArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  locales?: InputMaybe<Array<Locale>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ScheduledOperationWhereInput>;
};

/** Whitelabel config for each subdomain */
export type WhitelabelUpdatedByArgs = {
  forceParentLocale?: InputMaybe<Scalars['Boolean']['input']>;
  locales?: InputMaybe<Array<Locale>>;
};

export type WhitelabelConnectInput = {
  /** Allow to specify document position in list of connected documents, will default to appending at end of list */
  position?: InputMaybe<ConnectPositionInput>;
  /** Document to connect */
  where: WhitelabelWhereUniqueInput;
};

/** A connection to a list of items. */
export type WhitelabelConnection = {
  __typename?: 'WhitelabelConnection';
  aggregate: Aggregate;
  /** A list of edges. */
  edges: Array<WhitelabelEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

export type WhitelabelCreateInput = {
  client: Scalars['String']['input'];
  communityLink: Scalars['String']['input'];
  communityLinkLabel: Scalars['String']['input'];
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  customPartners?: InputMaybe<Scalars['String']['input']>;
  defaultLanguage?: InputMaybe<Language>;
  isLinkToPartnersVisible: Scalars['Boolean']['input'];
  linkToFeedbackForm?: InputMaybe<LinkToFeedbackFormCreateOneInlineInput>;
  linksToQuiz?: InputMaybe<Scalars['Json']['input']>;
  /** Inline mutations for managing document localizations excluding the default locale */
  localizations?: InputMaybe<WhitelabelCreateLocalizationsInput>;
  logo?: InputMaybe<AssetCreateOneInlineInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type WhitelabelCreateLocalizationDataInput = {
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
};

export type WhitelabelCreateLocalizationInput = {
  /** Localization input */
  data: WhitelabelCreateLocalizationDataInput;
  locale: Locale;
};

export type WhitelabelCreateLocalizationsInput = {
  /** Create localizations for the newly-created document */
  create?: InputMaybe<Array<WhitelabelCreateLocalizationInput>>;
};

export type WhitelabelCreateManyInlineInput = {
  /** Connect multiple existing Whitelabel documents */
  connect?: InputMaybe<Array<WhitelabelWhereUniqueInput>>;
  /** Create and connect multiple existing Whitelabel documents */
  create?: InputMaybe<Array<WhitelabelCreateInput>>;
};

export type WhitelabelCreateOneInlineInput = {
  /** Connect one existing Whitelabel document */
  connect?: InputMaybe<WhitelabelWhereUniqueInput>;
  /** Create and connect one Whitelabel document */
  create?: InputMaybe<WhitelabelCreateInput>;
};

/** An edge in a connection. */
export type WhitelabelEdge = {
  __typename?: 'WhitelabelEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Whitelabel;
};

/** Identifies documents */
export type WhitelabelManyWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  client?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  client_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  client_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  client_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  client_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  client_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  client_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  client_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  client_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  client_starts_with?: InputMaybe<Scalars['String']['input']>;
  communityLink?: InputMaybe<Scalars['String']['input']>;
  communityLinkLabel?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  communityLinkLabel_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  communityLinkLabel_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  communityLinkLabel_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** Any other value that exists and is not equal to the given value. */
  communityLinkLabel_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  communityLinkLabel_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  communityLinkLabel_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  communityLinkLabel_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  communityLinkLabel_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  communityLinkLabel_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  communityLink_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  communityLink_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  communityLink_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  communityLink_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  communityLink_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  communityLink_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  communityLink_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  communityLink_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  communityLink_starts_with?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  customPartners?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  customPartners_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  customPartners_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  customPartners_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  customPartners_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  customPartners_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  customPartners_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  customPartners_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  customPartners_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  customPartners_starts_with?: InputMaybe<Scalars['String']['input']>;
  defaultLanguage?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  defaultLanguage_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  defaultLanguage_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  defaultLanguage_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  documentInStages_every?: InputMaybe<WhitelabelWhereStageInput>;
  documentInStages_none?: InputMaybe<WhitelabelWhereStageInput>;
  documentInStages_some?: InputMaybe<WhitelabelWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isLinkToPartnersVisible?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isLinkToPartnersVisible_not?: InputMaybe<Scalars['Boolean']['input']>;
  linkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
  /** All values containing the given json path. */
  linksToQuiz_json_path_exists?: InputMaybe<Scalars['String']['input']>;
  /**
   * Recursively tries to find the provided JSON scalar value inside the field.
   * It does use an exact match when comparing values.
   * If you pass `null` as value the filter will be ignored.
   * Note: This filter fails if you try to look for a non scalar JSON value!
   */
  linksToQuiz_value_recursive?: InputMaybe<Scalars['Json']['input']>;
  logo?: InputMaybe<AssetWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

export enum WhitelabelOrderByInput {
  ClientAsc = 'client_ASC',
  ClientDesc = 'client_DESC',
  CommunityLinkLabelAsc = 'communityLinkLabel_ASC',
  CommunityLinkLabelDesc = 'communityLinkLabel_DESC',
  CommunityLinkAsc = 'communityLink_ASC',
  CommunityLinkDesc = 'communityLink_DESC',
  CreatedAtAsc = 'createdAt_ASC',
  CreatedAtDesc = 'createdAt_DESC',
  CustomPartnersAsc = 'customPartners_ASC',
  CustomPartnersDesc = 'customPartners_DESC',
  DefaultLanguageAsc = 'defaultLanguage_ASC',
  DefaultLanguageDesc = 'defaultLanguage_DESC',
  IdAsc = 'id_ASC',
  IdDesc = 'id_DESC',
  IsLinkToPartnersVisibleAsc = 'isLinkToPartnersVisible_ASC',
  IsLinkToPartnersVisibleDesc = 'isLinkToPartnersVisible_DESC',
  PublishedAtAsc = 'publishedAt_ASC',
  PublishedAtDesc = 'publishedAt_DESC',
  UpdatedAtAsc = 'updatedAt_ASC',
  UpdatedAtDesc = 'updatedAt_DESC',
}

export type WhitelabelUpdateInput = {
  client?: InputMaybe<Scalars['String']['input']>;
  communityLink?: InputMaybe<Scalars['String']['input']>;
  communityLinkLabel?: InputMaybe<Scalars['String']['input']>;
  customPartners?: InputMaybe<Scalars['String']['input']>;
  defaultLanguage?: InputMaybe<Language>;
  isLinkToPartnersVisible?: InputMaybe<Scalars['Boolean']['input']>;
  linkToFeedbackForm?: InputMaybe<LinkToFeedbackFormUpdateOneInlineInput>;
  linksToQuiz?: InputMaybe<Scalars['Json']['input']>;
  /** Manage document localizations */
  localizations?: InputMaybe<WhitelabelUpdateLocalizationsInput>;
  logo?: InputMaybe<AssetUpdateOneInlineInput>;
};

export type WhitelabelUpdateLocalizationsInput = {
  /** Localizations to create */
  create?: InputMaybe<Array<WhitelabelCreateLocalizationInput>>;
  /** Localizations to delete */
  delete?: InputMaybe<Array<Locale>>;
};

export type WhitelabelUpdateManyInlineInput = {
  /** Connect multiple existing Whitelabel documents */
  connect?: InputMaybe<Array<WhitelabelConnectInput>>;
  /** Create and connect multiple Whitelabel documents */
  create?: InputMaybe<Array<WhitelabelCreateInput>>;
  /** Delete multiple Whitelabel documents */
  delete?: InputMaybe<Array<WhitelabelWhereUniqueInput>>;
  /** Disconnect multiple Whitelabel documents */
  disconnect?: InputMaybe<Array<WhitelabelWhereUniqueInput>>;
  /** Override currently-connected documents with multiple existing Whitelabel documents */
  set?: InputMaybe<Array<WhitelabelWhereUniqueInput>>;
  /** Update multiple Whitelabel documents */
  update?: InputMaybe<Array<WhitelabelUpdateWithNestedWhereUniqueInput>>;
  /** Upsert multiple Whitelabel documents */
  upsert?: InputMaybe<Array<WhitelabelUpsertWithNestedWhereUniqueInput>>;
};

export type WhitelabelUpdateManyInput = {
  communityLink?: InputMaybe<Scalars['String']['input']>;
  communityLinkLabel?: InputMaybe<Scalars['String']['input']>;
  customPartners?: InputMaybe<Scalars['String']['input']>;
  defaultLanguage?: InputMaybe<Language>;
  isLinkToPartnersVisible?: InputMaybe<Scalars['Boolean']['input']>;
  linksToQuiz?: InputMaybe<Scalars['Json']['input']>;
};

export type WhitelabelUpdateManyWithNestedWhereInput = {
  /** Update many input */
  data: WhitelabelUpdateManyInput;
  /** Document search */
  where: WhitelabelWhereInput;
};

export type WhitelabelUpdateOneInlineInput = {
  /** Connect existing Whitelabel document */
  connect?: InputMaybe<WhitelabelWhereUniqueInput>;
  /** Create and connect one Whitelabel document */
  create?: InputMaybe<WhitelabelCreateInput>;
  /** Delete currently connected Whitelabel document */
  delete?: InputMaybe<Scalars['Boolean']['input']>;
  /** Disconnect currently connected Whitelabel document */
  disconnect?: InputMaybe<Scalars['Boolean']['input']>;
  /** Update single Whitelabel document */
  update?: InputMaybe<WhitelabelUpdateWithNestedWhereUniqueInput>;
  /** Upsert single Whitelabel document */
  upsert?: InputMaybe<WhitelabelUpsertWithNestedWhereUniqueInput>;
};

export type WhitelabelUpdateWithNestedWhereUniqueInput = {
  /** Document to update */
  data: WhitelabelUpdateInput;
  /** Unique document search */
  where: WhitelabelWhereUniqueInput;
};

export type WhitelabelUpsertInput = {
  /** Create document if it didn't exist */
  create: WhitelabelCreateInput;
  /** Update document if it exists */
  update: WhitelabelUpdateInput;
};

export type WhitelabelUpsertWithNestedWhereUniqueInput = {
  /** Upsert data */
  data: WhitelabelUpsertInput;
  /** Unique document search */
  where: WhitelabelWhereUniqueInput;
};

/** This contains a set of filters that can be used to compare values internally */
export type WhitelabelWhereComparatorInput = {
  /** This field can be used to request to check if the entry is outdated by internal comparison */
  outdated_to?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Identifies documents */
export type WhitelabelWhereInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<WhitelabelWhereInput>>;
  /** Contains search across all appropriate fields. */
  _search?: InputMaybe<Scalars['String']['input']>;
  client?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  client_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  client_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  client_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  client_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  client_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  client_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  client_not_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** All values not starting with the given string. */
  client_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  client_starts_with?: InputMaybe<Scalars['String']['input']>;
  communityLink?: InputMaybe<Scalars['String']['input']>;
  communityLinkLabel?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  communityLinkLabel_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  communityLinkLabel_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  communityLinkLabel_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** Any other value that exists and is not equal to the given value. */
  communityLinkLabel_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  communityLinkLabel_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  communityLinkLabel_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  communityLinkLabel_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  communityLinkLabel_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  communityLinkLabel_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  communityLink_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  communityLink_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  communityLink_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  communityLink_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  communityLink_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  communityLink_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  communityLink_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  communityLink_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  communityLink_starts_with?: InputMaybe<Scalars['String']['input']>;
  createdAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  createdAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  createdAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  createdAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  createdAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  createdAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  createdAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  createdAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  createdBy?: InputMaybe<UserWhereInput>;
  customPartners?: InputMaybe<Scalars['String']['input']>;
  /** All values containing the given string. */
  customPartners_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values ending with the given string. */
  customPartners_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are contained in given list. */
  customPartners_in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  customPartners_not?: InputMaybe<Scalars['String']['input']>;
  /** All values not containing the given string. */
  customPartners_not_contains?: InputMaybe<Scalars['String']['input']>;
  /** All values not ending with the given string */
  customPartners_not_ends_with?: InputMaybe<Scalars['String']['input']>;
  /** All values that are not contained in given list. */
  customPartners_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['String']['input']>>
  >;
  /** All values not starting with the given string. */
  customPartners_not_starts_with?: InputMaybe<Scalars['String']['input']>;
  /** All values starting with the given string. */
  customPartners_starts_with?: InputMaybe<Scalars['String']['input']>;
  defaultLanguage?: InputMaybe<Language>;
  /** All values that are contained in given list. */
  defaultLanguage_in?: InputMaybe<Array<InputMaybe<Language>>>;
  /** Any other value that exists and is not equal to the given value. */
  defaultLanguage_not?: InputMaybe<Language>;
  /** All values that are not contained in given list. */
  defaultLanguage_not_in?: InputMaybe<Array<InputMaybe<Language>>>;
  documentInStages_every?: InputMaybe<WhitelabelWhereStageInput>;
  documentInStages_none?: InputMaybe<WhitelabelWhereStageInput>;
  documentInStages_some?: InputMaybe<WhitelabelWhereStageInput>;
  id?: InputMaybe<Scalars['ID']['input']>;
  /** All values containing the given string. */
  id_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values ending with the given string. */
  id_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are contained in given list. */
  id_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** Any other value that exists and is not equal to the given value. */
  id_not?: InputMaybe<Scalars['ID']['input']>;
  /** All values not containing the given string. */
  id_not_contains?: InputMaybe<Scalars['ID']['input']>;
  /** All values not ending with the given string */
  id_not_ends_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values that are not contained in given list. */
  id_not_in?: InputMaybe<Array<InputMaybe<Scalars['ID']['input']>>>;
  /** All values not starting with the given string. */
  id_not_starts_with?: InputMaybe<Scalars['ID']['input']>;
  /** All values starting with the given string. */
  id_starts_with?: InputMaybe<Scalars['ID']['input']>;
  isLinkToPartnersVisible?: InputMaybe<Scalars['Boolean']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  isLinkToPartnersVisible_not?: InputMaybe<Scalars['Boolean']['input']>;
  linkToFeedbackForm?: InputMaybe<LinkToFeedbackFormWhereInput>;
  /** All values containing the given json path. */
  linksToQuiz_json_path_exists?: InputMaybe<Scalars['String']['input']>;
  /**
   * Recursively tries to find the provided JSON scalar value inside the field.
   * It does use an exact match when comparing values.
   * If you pass `null` as value the filter will be ignored.
   * Note: This filter fails if you try to look for a non scalar JSON value!
   */
  linksToQuiz_value_recursive?: InputMaybe<Scalars['Json']['input']>;
  logo?: InputMaybe<AssetWhereInput>;
  publishedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  publishedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  publishedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  publishedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  publishedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  publishedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  publishedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  publishedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  publishedBy?: InputMaybe<UserWhereInput>;
  scheduledIn_every?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_none?: InputMaybe<ScheduledOperationWhereInput>;
  scheduledIn_some?: InputMaybe<ScheduledOperationWhereInput>;
  updatedAt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than the given value. */
  updatedAt_gt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values greater than or equal the given value. */
  updatedAt_gte?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are contained in given list. */
  updatedAt_in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']['input']>>>;
  /** All values less than the given value. */
  updatedAt_lt?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values less than or equal the given value. */
  updatedAt_lte?: InputMaybe<Scalars['DateTime']['input']>;
  /** Any other value that exists and is not equal to the given value. */
  updatedAt_not?: InputMaybe<Scalars['DateTime']['input']>;
  /** All values that are not contained in given list. */
  updatedAt_not_in?: InputMaybe<
    Array<InputMaybe<Scalars['DateTime']['input']>>
  >;
  updatedBy?: InputMaybe<UserWhereInput>;
};

/** The document in stages filter allows specifying a stage entry to cross compare the same document between different stages */
export type WhitelabelWhereStageInput = {
  /** Logical AND on all given filters. */
  AND?: InputMaybe<Array<WhitelabelWhereStageInput>>;
  /** Logical NOT on all given filters combined by AND. */
  NOT?: InputMaybe<Array<WhitelabelWhereStageInput>>;
  /** Logical OR on all given filters. */
  OR?: InputMaybe<Array<WhitelabelWhereStageInput>>;
  /** This field contains fields which can be set as true or false to specify an internal comparison */
  compareWithParent?: InputMaybe<WhitelabelWhereComparatorInput>;
  /** Specify the stage to compare with */
  stage?: InputMaybe<Stage>;
};

/** References Whitelabel record uniquely */
export type WhitelabelWhereUniqueInput = {
  client?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export enum _FilterKind {
  And = 'AND',
  Not = 'NOT',
  Or = 'OR',
  Contains = 'contains',
  ContainsAll = 'contains_all',
  ContainsNone = 'contains_none',
  ContainsSome = 'contains_some',
  EndsWith = 'ends_with',
  Eq = 'eq',
  EqNot = 'eq_not',
  Gt = 'gt',
  Gte = 'gte',
  In = 'in',
  JsonPathExists = 'json_path_exists',
  JsonValueRecursive = 'json_value_recursive',
  Lt = 'lt',
  Lte = 'lte',
  NotContains = 'not_contains',
  NotEndsWith = 'not_ends_with',
  NotIn = 'not_in',
  NotStartsWith = 'not_starts_with',
  RelationalEvery = 'relational_every',
  RelationalNone = 'relational_none',
  RelationalSingle = 'relational_single',
  RelationalSome = 'relational_some',
  Search = 'search',
  StartsWith = 'starts_with',
  UnionEmpty = 'union_empty',
  UnionEvery = 'union_every',
  UnionNone = 'union_none',
  UnionSingle = 'union_single',
  UnionSome = 'union_some',
}

export enum _MutationInputFieldKind {
  Enum = 'enum',
  Relation = 'relation',
  RichText = 'richText',
  RichTextWithEmbeds = 'richTextWithEmbeds',
  Scalar = 'scalar',
  Union = 'union',
  Virtual = 'virtual',
}

export enum _MutationKind {
  Create = 'create',
  Delete = 'delete',
  DeleteMany = 'deleteMany',
  Publish = 'publish',
  PublishMany = 'publishMany',
  SchedulePublish = 'schedulePublish',
  ScheduleUnpublish = 'scheduleUnpublish',
  Unpublish = 'unpublish',
  UnpublishMany = 'unpublishMany',
  Update = 'update',
  UpdateMany = 'updateMany',
  Upsert = 'upsert',
}

export enum _OrderDirection {
  Asc = 'asc',
  Desc = 'desc',
}

export enum _RelationInputCardinality {
  Many = 'many',
  One = 'one',
}

export enum _RelationInputKind {
  Create = 'create',
  Update = 'update',
}

export enum _RelationKind {
  Regular = 'regular',
  Union = 'union',
}

export enum _SystemDateTimeFieldVariation {
  Base = 'base',
  Combined = 'combined',
  Localization = 'localization',
}
