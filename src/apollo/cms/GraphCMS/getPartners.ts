import { gql } from '@apollo/client';

export const GET_PARTNERS = gql`
  query getPartners($where: PartnerWhereInput) {
    partners(where: $where) {
      id
      link
      logo
      name
      tags
      description
    }
  }
`;
