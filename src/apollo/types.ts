export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  DateTime: { input: any; output: any };
};

export type ActiveUsers = {
  __typename?: 'ActiveUsers';
  months: Array<Count>;
  year: Scalars['Float']['output'];
};

export type AdminProjects = {
  __typename?: 'AdminProjects';
  count: Scalars['Float']['output'];
  projects: Array<Project>;
};

export type AdminStatistics = {
  __typename?: 'AdminStatistics';
  activeUsers: Array<ActiveUsers>;
  onboardingAnswers: Array<OnboardingAnswers>;
  onboardingResults: Array<Count>;
  registrationRegions: Array<RegistrationRegions>;
  registrationsPerMonth: Array<RegistrationsPerMonth>;
  totalRegistedUsersCount: Scalars['Float']['output'];
};

export enum AuthProvider {
  Email = 'EMAIL',
  Facebook = 'FACEBOOK',
  Google = 'GOOGLE',
  Linkedin = 'LINKEDIN',
}

export type BasicResponseType = {
  __typename?: 'BasicResponseType';
  code: Scalars['String']['output'];
  message: Scalars['String']['output'];
  success: Scalars['Boolean']['output'];
};

export type Comment = {
  __typename?: 'Comment';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id?: Maybe<Scalars['ID']['output']>;
  text: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  user: User;
};

export type Component = {
  __typename?: 'Component';
  label: Scalars['String']['output'];
  tasks: Array<Task>;
  title?: Maybe<Scalars['String']['output']>;
};

export type Count = {
  __typename?: 'Count';
  count: Scalars['Float']['output'];
  value: Scalars['String']['output'];
};

export type CreateProjectInput = {
  industry?: InputMaybe<Array<Scalars['String']['input']>>;
  name?: InputMaybe<Scalars['String']['input']>;
  projectLogoUrl?: InputMaybe<Scalars['String']['input']>;
  projectPhase?: InputMaybe<Scalars['String']['input']>;
};

export type CreateUserProfileInput = {
  firstName: Scalars['String']['input'];
  lastName: Scalars['String']['input'];
  profilePicture?: InputMaybe<Scalars['String']['input']>;
  region: Scalars['String']['input'];
  skills?: InputMaybe<Array<Scalars['String']['input']>>;
};

export type Diary = {
  __typename?: 'Diary';
  archived: Scalars['Boolean']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id?: Maybe<Scalars['ID']['output']>;
  phases: Array<Phase>;
  project: Project;
  projectPhase?: Maybe<Scalars['String']['output']>;
  recommendedTasks: Array<Scalars['String']['output']>;
  status: DiaryStatus;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export enum DiaryStatus {
  Done = 'DONE',
  InProgress = 'IN_PROGRESS',
}

export type EditProjectInput = {
  industry?: InputMaybe<Array<Scalars['String']['input']>>;
  name?: InputMaybe<Scalars['String']['input']>;
  projectId: Scalars['String']['input'];
  projectLogoUrl?: InputMaybe<Scalars['String']['input']>;
};

export type EditUserProfileInput = {
  firstName?: InputMaybe<Scalars['String']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  profilePicture?: InputMaybe<Scalars['String']['input']>;
  region?: InputMaybe<Scalars['String']['input']>;
  skills?: InputMaybe<Array<Scalars['String']['input']>>;
};

export enum EmailVerification {
  Done = 'DONE',
  NotNeeded = 'NOT_NEEDED',
  Pending = 'PENDING',
}

export enum Gender {
  Female = 'FEMALE',
  Male = 'MALE',
  Unspecified = 'UNSPECIFIED',
}

export type Identity = {
  __typename?: 'Identity';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id?: Maybe<Scalars['ID']['output']>;
  provider: AuthProvider;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  userId: Scalars['String']['output'];
};

export type LoginInput = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type LoginResponseType = {
  __typename?: 'LoginResponseType';
  accessToken?: Maybe<Scalars['String']['output']>;
  code: Scalars['String']['output'];
  expiresIn?: Maybe<Scalars['Float']['output']>;
  message: Scalars['String']['output'];
  refreshExpiresIn?: Maybe<Scalars['Float']['output']>;
  refreshToken?: Maybe<Scalars['String']['output']>;
  success: Scalars['Boolean']['output'];
  tokenType?: Maybe<Scalars['String']['output']>;
  user?: Maybe<User>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createProject: Project;
  createUserProfile: ProfileResponseType;
  deleteProject: BasicResponseType;
  deleteUserProfile: BasicResponseType;
  editProject: ProjectResponse;
  editUserProfile: ProfileResponseType;
  login: LoginResponseType;
  onboarding: User;
  refreshAccessToken: LoginResponseType;
  register: RegistrationResponseType;
  requestPasswordReset: ResetPasswordMutationResponseType;
  resendPasswordResetEmail: BasicResponseType;
  resendVerficationEmail: BasicResponseType;
  resetPassword: BasicResponseType;
  saveUserGender: User;
  updateTaskById: Task;
  verifyUser: LoginResponseType;
};

export type MutationCreateProjectArgs = {
  createProjectInput: CreateProjectInput;
};

export type MutationCreateUserProfileArgs = {
  createUserProfileInput: CreateUserProfileInput;
};

export type MutationDeleteProjectArgs = {
  projectId: Scalars['String']['input'];
};

export type MutationEditProjectArgs = {
  editProjectInput: EditProjectInput;
};

export type MutationEditUserProfileArgs = {
  editProfileInput: EditUserProfileInput;
};

export type MutationLoginArgs = {
  data: LoginInput;
};

export type MutationOnboardingArgs = {
  data: OnboardingInput;
};

export type MutationRefreshAccessTokenArgs = {
  data: RefreshAccessTokenInput;
};

export type MutationRegisterArgs = {
  data: RegisterInputType;
};

export type MutationRequestPasswordResetArgs = {
  email: Scalars['String']['input'];
};

export type MutationResendPasswordResetEmailArgs = {
  requestId: Scalars['String']['input'];
};

export type MutationResendVerficationEmailArgs = {
  data: ResetVerificationInput;
};

export type MutationResetPasswordArgs = {
  data: ResetPasswordInputType;
};

export type MutationSaveUserGenderArgs = {
  gender: Scalars['String']['input'];
};

export type MutationUpdateTaskByIdArgs = {
  updateTaskInput: UpdateTaskInput;
};

export type MutationVerifyUserArgs = {
  data: VerifyUserInputType;
};

export type OnboardingAnswers = {
  __typename?: 'OnboardingAnswers';
  answers: Array<Count>;
  question: Scalars['String']['output'];
};

export type OnboardingInput = {
  onboardingAnswers: Array<OnboardingInputs>;
  result: Scalars['String']['input'];
};

export type OnboardingInputs = {
  answer: Array<Scalars['String']['input']>;
  question: Scalars['String']['input'];
};

export type OnboardingType = {
  __typename?: 'OnboardingType';
  answer: Array<Scalars['String']['output']>;
  question: Scalars['String']['output'];
};

export type Phase = {
  __typename?: 'Phase';
  completedTasksCount?: Maybe<Scalars['Float']['output']>;
  components: Array<Component>;
  label: Scalars['String']['output'];
  tasksCount?: Maybe<Scalars['Float']['output']>;
  title: PhaseStage;
};

export enum PhaseStage {
  Business = 'BUSINESS',
  Idea = 'IDEA',
  Ideation = 'IDEATION',
  Mvp = 'MVP',
  Prototype = 'PROTOTYPE',
}

export type Profile = {
  __typename?: 'Profile';
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  id?: Maybe<Scalars['ID']['output']>;
  region?: Maybe<Scalars['String']['output']>;
  skills?: Maybe<Array<Scalars['String']['output']>>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  user?: Maybe<User>;
};

export type ProfileResponseType = {
  __typename?: 'ProfileResponseType';
  code: Scalars['String']['output'];
  message: Scalars['String']['output'];
  profile: Profile;
  success: Scalars['Boolean']['output'];
};

export type Project = {
  __typename?: 'Project';
  archived: Scalars['Boolean']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  diary: Array<Diary>;
  id?: Maybe<Scalars['ID']['output']>;
  industry?: Maybe<Array<Scalars['String']['output']>>;
  name: Scalars['String']['output'];
  phase?: Maybe<ProjectPhase>;
  projectLogoUrl?: Maybe<Scalars['String']['output']>;
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
  user: User;
};

export enum ProjectPhase {
  FinishedProduct = 'FINISHED_PRODUCT',
  Idealization = 'IDEALIZATION',
  InvestmentReady = 'INVESTMENT_READY',
  Prototyping = 'PROTOTYPING',
}

export type ProjectResponse = {
  __typename?: 'ProjectResponse';
  code: Scalars['String']['output'];
  message: Scalars['String']['output'];
  project: Project;
  success: Scalars['Boolean']['output'];
};

export type Query = {
  __typename?: 'Query';
  getAdminStatistics: AdminStatistics;
  getAllProjects: Array<Project>;
  getAllProjectsAsAdmin: AdminProjects;
  getDiaryByProjectId: Diary;
  getProfile: Profile;
  getProjectById: Project;
  getTaskById: Task;
  getUser: User;
  viewTask: Task;
};

export type QueryGetAdminStatisticsArgs = {
  subdomain: Scalars['String']['input'];
};

export type QueryGetAllProjectsAsAdminArgs = {
  skip: Scalars['Float']['input'];
  take: Scalars['Float']['input'];
};

export type QueryGetDiaryByProjectIdArgs = {
  projectId: Scalars['String']['input'];
};

export type QueryGetProjectByIdArgs = {
  projectId: Scalars['String']['input'];
};

export type QueryGetTaskByIdArgs = {
  taskId: Scalars['String']['input'];
};

export type QueryViewTaskArgs = {
  taskId: Scalars['String']['input'];
};

export type RefreshAccessTokenInput = {
  refreshToken: Scalars['String']['input'];
  userId: Scalars['String']['input'];
};

export type RegisterInputType = {
  agreedToTermsAndConditions: Scalars['Boolean']['input'];
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type RegistrationRegions = {
  __typename?: 'RegistrationRegions';
  country: Scalars['String']['output'];
  regions: Array<Count>;
};

export type RegistrationResponseType = {
  __typename?: 'RegistrationResponseType';
  accessToken?: Maybe<Scalars['String']['output']>;
  code: Scalars['String']['output'];
  expiresIn?: Maybe<Scalars['Float']['output']>;
  message: Scalars['String']['output'];
  refreshExpiresIn?: Maybe<Scalars['Float']['output']>;
  refreshToken?: Maybe<Scalars['String']['output']>;
  requestId?: Maybe<Scalars['String']['output']>;
  success: Scalars['Boolean']['output'];
  user?: Maybe<User>;
  verificationToken?: Maybe<Scalars['String']['output']>;
};

export type RegistrationsPerMonth = {
  __typename?: 'RegistrationsPerMonth';
  months: Array<Count>;
  year: Scalars['Float']['output'];
};

export type ResetPasswordInputType = {
  code: Scalars['String']['input'];
  id: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type ResetPasswordMutationResponseType = {
  __typename?: 'ResetPasswordMutationResponseType';
  code: Scalars['String']['output'];
  message: Scalars['String']['output'];
  requestId?: Maybe<Scalars['String']['output']>;
  success: Scalars['Boolean']['output'];
  verificationCode?: Maybe<Scalars['String']['output']>;
};

export type ResetVerificationInput = {
  /** Request ID returned from the register mutation */
  requestId: Scalars['String']['input'];
};

export type Task = {
  __typename?: 'Task';
  comments?: Maybe<Array<Comment>>;
  completed: Scalars['Boolean']['output'];
  completedAt?: Maybe<Scalars['DateTime']['output']>;
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  diaryId: Scalars['String']['output'];
  duration?: Maybe<Scalars['Float']['output']>;
  hasNewUpdate: Scalars['Boolean']['output'];
  howToPage: Scalars['String']['output'];
  id?: Maybe<Scalars['ID']['output']>;
  links?: Maybe<Array<Scalars['String']['output']>>;
  pointer: Scalars['String']['output'];
  question?: Maybe<Scalars['String']['output']>;
  subtitle?: Maybe<Scalars['String']['output']>;
  thumbnail?: Maybe<Thumbnail>;
  title: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export type Thumbnail = {
  __typename?: 'Thumbnail';
  height: Scalars['String']['output'];
  id: Scalars['String']['output'];
  url: Scalars['String']['output'];
  width: Scalars['String']['output'];
};

export type UpdateTaskInput = {
  completed: Scalars['Boolean']['input'];
  links?: InputMaybe<Array<Scalars['String']['input']>>;
  taskId: Scalars['String']['input'];
  text?: InputMaybe<Scalars['String']['input']>;
};

export type User = {
  __typename?: 'User';
  agreedToTermsAndConditions: Scalars['Boolean']['output'];
  archived: Scalars['Boolean']['output'];
  createdAt?: Maybe<Scalars['DateTime']['output']>;
  email?: Maybe<Scalars['String']['output']>;
  emailVerification: EmailVerification;
  firstName?: Maybe<Scalars['String']['output']>;
  gender: Gender;
  id?: Maybe<Scalars['ID']['output']>;
  identities?: Maybe<Array<Identity>>;
  lastName?: Maybe<Scalars['String']['output']>;
  onboardingData?: Maybe<Array<OnboardingType>>;
  onboardingResult?: Maybe<Scalars['String']['output']>;
  profile?: Maybe<Profile>;
  profilePicture?: Maybe<Scalars['String']['output']>;
  projects?: Maybe<Array<Project>>;
  role: UserRole;
  subdomain: Scalars['String']['output'];
  updatedAt?: Maybe<Scalars['DateTime']['output']>;
};

export enum UserRole {
  Admin = 'ADMIN',
  Client = 'CLIENT',
  SuperAdmin = 'SUPER_ADMIN',
}

export type VerifyUserInputType = {
  /** Request ID returned from the register mutation */
  requestId: Scalars['String']['input'];
  /** Verification token received via email */
  verificationToken: Scalars['String']['input'];
};
