import { gql } from '@apollo/client';

import { USER_FRAGMENT } from '../fragments/user.fragment';

export const ONBOARDING = gql`
  mutation onboarding(
    $result: String!
    $onboardingAnswers: [OnboardingInputs!]!
  ) {
    onboarding(
      data: { result: $result, onboardingAnswers: $onboardingAnswers }
    ) {
      ...user
      projects {
        id
      }
    }
  }
  ${USER_FRAGMENT}
`;
