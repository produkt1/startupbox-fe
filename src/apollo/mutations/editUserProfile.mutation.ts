import { gql } from '@apollo/client';

export const EDIT_USER_PROFILE = gql`
  mutation editUserProfile(
    $firstName: String!
    $lastName: String!
    $region: String!
    $skills: [String!]!
    $profilePicture: String
  ) {
    editUserProfile(
      editProfileInput: {
        profilePicture: $profilePicture
        firstName: $firstName
        lastName: $lastName
        region: $region
        skills: $skills
      }
    ) {
      code
      success
    }
  }
`;
