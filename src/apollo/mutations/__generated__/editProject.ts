/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectPhase } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: editProject
// ====================================================

export interface editProject_editProject_project {
  __typename: 'Project';
  id: string | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
  projectLogoUrl: string | null;
}

export interface editProject_editProject {
  __typename: 'ProjectResponse';
  code: string;
  success: boolean;
  project: editProject_editProject_project;
}

export interface editProject {
  editProject: editProject_editProject;
}

export interface editProjectVariables {
  projectId: string;
  name?: string | null;
  industry?: string[] | null;
  projectLogoUrl?: string | null;
}
