/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createUserProfile
// ====================================================

export interface createUserProfile_createUserProfile_profile_user {
  __typename: 'User';
  id: string | null;
  firstName: string | null;
  lastName: string | null;
}

export interface createUserProfile_createUserProfile_profile {
  __typename: 'Profile';
  id: string | null;
  region: string | null;
  skills: string[] | null;
  user: createUserProfile_createUserProfile_profile_user | null;
}

export interface createUserProfile_createUserProfile {
  __typename: 'ProfileResponseType';
  profile: createUserProfile_createUserProfile_profile;
  success: boolean;
  code: string;
  message: string;
}

export interface createUserProfile {
  createUserProfile: createUserProfile_createUserProfile;
}

export interface createUserProfileVariables {
  firstName: string;
  lastName: string;
  region: string;
  skills: string[];
  profilePicture?: string | null;
}
