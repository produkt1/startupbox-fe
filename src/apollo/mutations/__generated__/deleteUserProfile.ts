/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteUserProfile
// ====================================================

export interface deleteUserProfile_deleteUserProfile {
  __typename: 'BasicResponseType';
  message: string;
  code: string;
  success: boolean;
}

export interface deleteUserProfile {
  deleteUserProfile: deleteUserProfile_deleteUserProfile;
}
