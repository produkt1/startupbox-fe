/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: editUserProfile
// ====================================================

export interface editUserProfile_editUserProfile {
  __typename: 'ProfileResponseType';
  code: string;
  success: boolean;
}

export interface editUserProfile {
  editUserProfile: editUserProfile_editUserProfile;
}

export interface editUserProfileVariables {
  firstName: string;
  lastName: string;
  region: string;
  skills: string[];
  profilePicture?: string | null;
}
