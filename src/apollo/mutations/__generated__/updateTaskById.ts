/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateTaskById
// ====================================================

export interface updateTaskById_updateTaskById_comments_user {
  __typename: 'User';
  id: string | null;
}

export interface updateTaskById_updateTaskById_comments {
  __typename: 'Comment';
  id: string | null;
  text: string;
  updatedAt: any | null;
  user: updateTaskById_updateTaskById_comments_user;
}

export interface updateTaskById_updateTaskById {
  __typename: 'Task';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  title: string;
  comments: updateTaskById_updateTaskById_comments[] | null;
  links: string[] | null;
  completed: boolean;
  howToPage: string;
}

export interface updateTaskById {
  updateTaskById: updateTaskById_updateTaskById;
}

export interface updateTaskByIdVariables {
  taskId: string;
  text?: string | null;
  links?: string[] | null;
  completed: boolean;
}
