/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  OnboardingInputs,
  UserRole,
  EmailVerification,
  ProjectPhase,
} from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: onboarding
// ====================================================

export interface onboarding_onboarding_profile {
  __typename: 'Profile';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  region: string | null;
  skills: string[] | null;
}

export interface onboarding_onboarding_projects {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
}

export interface onboarding_onboarding_onboardingData {
  __typename: 'OnboardingType';
  answer: string[];
  question: string;
}

export interface onboarding_onboarding {
  __typename: 'User';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  profilePicture: string | null;
  role: UserRole;
  emailVerification: EmailVerification;
  profile: onboarding_onboarding_profile | null;
  projects: onboarding_onboarding_projects[] | null;
  onboardingResult: string | null;
  onboardingData: onboarding_onboarding_onboardingData[] | null;
}

export interface onboarding {
  onboarding: onboarding_onboarding;
}

export interface onboardingVariables {
  result: string;
  onboardingAnswers: OnboardingInputs[];
}
