/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectPhase } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: createProject
// ====================================================

export interface createProject_createProject {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
  projectLogoUrl: string | null;
}

export interface createProject {
  createProject: createProject_createProject;
}

export interface createProjectVariables {
  name: string;
  industry: string[];
  projectPhase: string;
  projectLogoUrl?: string | null;
}
