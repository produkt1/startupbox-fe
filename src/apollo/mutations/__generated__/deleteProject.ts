/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteProject
// ====================================================

export interface deleteProject_deleteProject {
  __typename: 'BasicResponseType';
  message: string;
  code: string;
  success: boolean;
}

export interface deleteProject {
  deleteProject: deleteProject_deleteProject;
}

export interface deleteProjectVariables {
  projectId: string;
}
