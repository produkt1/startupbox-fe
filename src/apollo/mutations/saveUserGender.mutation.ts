import { gql } from '@apollo/client';

export const SAVE_USER_GENDER = gql`
  mutation SaveUserGender($gender: String!) {
    saveUserGender(gender: $gender) {
      gender
    }
  }
`;
