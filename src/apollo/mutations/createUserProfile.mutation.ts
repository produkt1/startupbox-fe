import { gql } from '@apollo/client';

export const CREATE_USER_PROFILE = gql`
  mutation createUserProfile(
    $firstName: String!
    $lastName: String!
    $region: String!
    $skills: [String!]!
    $profilePicture: String
  ) {
    createUserProfile(
      createUserProfileInput: {
        firstName: $firstName
        lastName: $lastName
        region: $region
        skills: $skills
        profilePicture: $profilePicture
      }
    ) {
      profile {
        id
        region
        skills
        user {
          id
          firstName
          lastName
        }
      }

      success
      code
      message
    }
  }
`;
