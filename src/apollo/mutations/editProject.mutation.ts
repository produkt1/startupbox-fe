import { gql } from '@apollo/client';

export const EDIT_PROJECT_MUTATION = gql`
  mutation editProject(
    $projectId: String!
    $name: String
    $industry: [String!]
    $projectLogoUrl: String
  ) {
    editProject(
      editProjectInput: {
        projectId: $projectId
        name: $name
        industry: $industry
        projectLogoUrl: $projectLogoUrl
      }
    ) {
      code
      success
      project {
        id
        name
        industry
        phase
        projectLogoUrl
      }
    }
  }
`;
