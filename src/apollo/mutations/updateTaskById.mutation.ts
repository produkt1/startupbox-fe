import { gql } from '@apollo/client';

export const UPDATE_TASK_BY_ID_MUTATION = gql`
  mutation updateTaskById(
    $taskId: String!
    $text: String
    $links: [String!]
    $completed: Boolean!
  ) {
    updateTaskById(
      updateTaskInput: {
        taskId: $taskId
        text: $text
        links: $links
        completed: $completed
      }
    ) {
      id
      createdAt
      updatedAt
      title
      comments {
        id
        text
        updatedAt
        user {
          id
        }
      }
      links
      completed
      howToPage
    }
  }
`;
