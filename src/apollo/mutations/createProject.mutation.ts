import { gql } from '@apollo/client';

export const CREATE_PROJECT_MUTATION = gql`
  mutation createProject(
    $name: String!
    $industry: [String!]!
    $projectPhase: String!
    $projectLogoUrl: String
  ) {
    createProject(
      createProjectInput: {
        name: $name
        industry: $industry
        projectPhase: $projectPhase
        projectLogoUrl: $projectLogoUrl
      }
    ) {
      id
      createdAt
      updatedAt
      name
      industry
      phase
      projectLogoUrl
    }
  }
`;
