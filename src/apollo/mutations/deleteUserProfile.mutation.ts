import { gql } from '@apollo/client';

export const DELETE_USER_PROFILE = gql`
  mutation deleteUserProfile {
    deleteUserProfile {
      message
      code
      success
    }
  }
`;
