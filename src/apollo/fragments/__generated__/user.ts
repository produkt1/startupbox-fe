/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  UserRole,
  EmailVerification,
  ProjectPhase,
} from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL fragment: user
// ====================================================

export interface user_profile {
  __typename: 'Profile';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  region: string | null;
  skills: string[] | null;
}

export interface user_projects {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
}

export interface user_onboardingData {
  __typename: 'OnboardingType';
  answer: string[];
  question: string;
}

export interface user {
  __typename: 'User';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  profilePicture: string | null;
  role: UserRole;
  emailVerification: EmailVerification;
  profile: user_profile | null;
  projects: user_projects[] | null;
  onboardingResult: string | null;
  onboardingData: user_onboardingData[] | null;
}
