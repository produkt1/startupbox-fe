import { gql } from '@apollo/client';

export const GET_PROJECT_BY_ID = gql`
  query getProjectById($projectId: String!) {
    getProjectById(projectId: $projectId) {
      id
      createdAt
      updatedAt
      name
      industry
      phase
      user {
        id
        profilePicture
      }
      projectLogoUrl
    }
  }
`;
