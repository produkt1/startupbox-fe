import { gql } from '@apollo/client';

export const GET_TASK_BY_ID = gql`
  query getTaskById($taskId: String!) {
    getTaskById(taskId: $taskId) {
      id
      pointer
      createdAt
      updatedAt
      hasNewUpdate
      question
      title
      comments {
        id
        text
        updatedAt
        user {
          firstName
          lastName
          id
          role
          profilePicture
        }
      }
      links
      completed
      howToPage
    }
  }
`;
