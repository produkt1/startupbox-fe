import { gql } from '@apollo/client';

export const GET_DIARY_BY_PROJECT_ID = gql`
  query getDiaryByProjectId($projectId: String!) {
    getDiaryByProjectId(projectId: $projectId) {
      id
      status
      recommendedTasks

      project {
        name
        projectLogoUrl
      }

      phases {
        label
        title
        completedTasksCount
        tasksCount

        components {
          title

          tasks {
            id
            title
            completed
            pointer
            howToPage
            hasNewUpdate
            duration
            subtitle
            thumbnail {
              url
            }
            comments {
              user {
                id
              }
            }
          }
        }
      }
    }
  }
`;
