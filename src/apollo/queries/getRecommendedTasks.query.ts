import { gql } from '@apollo/client';

export const GET_RECOMMENDED_TASKS_BY_PROJECT_ID = gql`
  query getRecommendedTasksByProjectId($id: String!) {
    getDiaryByProjectId(projectId: $id) {
      id
      recommendedTasks
    }
  }
`;
