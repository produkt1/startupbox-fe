/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserRole } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getTaskById
// ====================================================

export interface getTaskById_getTaskById_comments_user {
  __typename: 'User';
  firstName: string | null;
  lastName: string | null;
  id: string | null;
  role: UserRole;
  profilePicture: string | null;
}

export interface getTaskById_getTaskById_comments {
  __typename: 'Comment';
  id: string | null;
  text: string;
  updatedAt: any | null;
  user: getTaskById_getTaskById_comments_user;
}

export interface getTaskById_getTaskById {
  __typename: 'Task';
  id: string | null;
  pointer: string;
  createdAt: any | null;
  updatedAt: any | null;
  hasNewUpdate: boolean;
  question: string | null;
  title: string;
  comments: getTaskById_getTaskById_comments[] | null;
  links: string[] | null;
  completed: boolean;
  howToPage: string;
}

export interface getTaskById {
  getTaskById: getTaskById_getTaskById;
}

export interface getTaskByIdVariables {
  taskId: string;
}
