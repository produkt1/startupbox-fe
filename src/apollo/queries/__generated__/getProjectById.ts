/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectPhase } from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getProjectById
// ====================================================

export interface getProjectById_getProjectById_user {
  __typename: 'User';
  id: string | null;
  profilePicture: string | null;
}

export interface getProjectById_getProjectById {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
  user: getProjectById_getProjectById_user;
  projectLogoUrl: string | null;
}

export interface getProjectById {
  getProjectById: getProjectById_getProjectById;
}

export interface getProjectByIdVariables {
  projectId: string;
}
