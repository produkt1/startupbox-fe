/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  ProjectPhase,
  PhaseStage,
} from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getAllProjects
// ====================================================

export interface getAllProjects_getAllProjects_diary_phases {
  __typename: 'Phase';
  title: PhaseStage;
  label: string;
  tasksCount: number | null;
  completedTasksCount: number | null;
}

export interface getAllProjects_getAllProjects_diary {
  __typename: 'Diary';
  phases: getAllProjects_getAllProjects_diary_phases[];
}

export interface getAllProjects_getAllProjects_user {
  __typename: 'User';
  id: string | null;
  profilePicture: string | null;
}

export interface getAllProjects_getAllProjects {
  __typename: 'Project';
  id: string | null;
  name: string;
  phase: ProjectPhase | null;
  updatedAt: any | null;
  projectLogoUrl: string | null;
  diary: getAllProjects_getAllProjects_diary[];
  user: getAllProjects_getAllProjects_user;
}

export interface getAllProjects {
  getAllProjects: getAllProjects_getAllProjects[];
}
