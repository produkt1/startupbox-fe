/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getRecommendedTasksByProjectId
// ====================================================

export interface getRecommendedTasksByProjectId_getDiaryByProjectId {
  __typename: 'Diary';
  id: string | null;
  recommendedTasks: string[];
}

export interface getRecommendedTasksByProjectId {
  getDiaryByProjectId: getRecommendedTasksByProjectId_getDiaryByProjectId;
}

export interface getRecommendedTasksByProjectIdVariables {
  id: string;
}
