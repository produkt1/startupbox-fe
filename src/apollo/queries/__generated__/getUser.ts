/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  UserRole,
  EmailVerification,
  ProjectPhase,
  AuthProvider,
} from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getUser
// ====================================================

export interface getUser_getUser_profile {
  __typename: 'Profile';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  region: string | null;
  skills: string[] | null;
}

export interface getUser_getUser_projects {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
}

export interface getUser_getUser_onboardingData {
  __typename: 'OnboardingType';
  answer: string[];
  question: string;
}

export interface getUser_getUser_identities {
  __typename: 'Identity';
  id: string | null;
  provider: AuthProvider;
}

export interface getUser_getUser {
  __typename: 'User';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  profilePicture: string | null;
  role: UserRole;
  emailVerification: EmailVerification;
  profile: getUser_getUser_profile | null;
  projects: getUser_getUser_projects[] | null;
  onboardingResult: string | null;
  onboardingData: getUser_getUser_onboardingData[] | null;
  identities: getUser_getUser_identities[] | null;
}

export interface getUser {
  getUser: getUser_getUser;
}
