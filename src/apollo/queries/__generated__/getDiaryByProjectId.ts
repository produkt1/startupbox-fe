/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  DiaryStatus,
  PhaseStage,
} from './../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getDiaryByProjectId
// ====================================================

export interface getDiaryByProjectId_getDiaryByProjectId_project {
  __typename: 'Project';
  name: string;
  projectLogoUrl: string | null;
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_thumbnail {
  __typename: 'Thumbnail';
  url: string;
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_comments_user {
  __typename: 'User';
  id: string | null;
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_comments {
  __typename: 'Comment';
  user: getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_comments_user;
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks {
  __typename: 'Task';
  id: string | null;
  title: string;
  completed: boolean;
  pointer: string;
  howToPage: string;
  hasNewUpdate: boolean;
  duration: number | null;
  subtitle: string | null;
  thumbnail: getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_thumbnail | null;
  comments:
    | getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks_comments[]
    | null;
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases_components {
  __typename: 'Component';
  title: string | null;
  tasks: getDiaryByProjectId_getDiaryByProjectId_phases_components_tasks[];
}

export interface getDiaryByProjectId_getDiaryByProjectId_phases {
  __typename: 'Phase';
  label: string;
  title: PhaseStage;
  completedTasksCount: number | null;
  tasksCount: number | null;
  components: getDiaryByProjectId_getDiaryByProjectId_phases_components[];
}

export interface getDiaryByProjectId_getDiaryByProjectId {
  __typename: 'Diary';
  id: string | null;
  status: DiaryStatus;
  recommendedTasks: string[];
  project: getDiaryByProjectId_getDiaryByProjectId_project;
  phases: getDiaryByProjectId_getDiaryByProjectId_phases[];
}

export interface getDiaryByProjectId {
  getDiaryByProjectId: getDiaryByProjectId_getDiaryByProjectId;
}

export interface getDiaryByProjectIdVariables {
  projectId: string;
}
