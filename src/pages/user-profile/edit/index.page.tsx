import { NextPage } from 'next';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { t, Trans } from '@lingui/macro';
import { useUserData } from '../../../common/hooks/useUserData';
import { Typography } from '@mui/material';
import { FormCard } from '../../../common/components/UI';
import { ManageUserProfileForm } from '../../../common/components/profile/ManageUserForm';
import * as React from 'react';
import { useMutation } from '@apollo/client';
import { EDIT_USER_PROFILE } from '../../../apollo/mutations/editUserProfile.mutation';
import { useSnackbar } from 'notistack';
import {
  editUserProfile,
  editUserProfileVariables,
} from '../../../apollo/mutations/__generated__/editUserProfile';
import { GET_USER_QUERY } from '../../../apollo/queries/getUser';
import { DeleteUserDialog } from './DeleteUsertDialog';
import {
  ERROR_MESSAGES,
  NOTIFICATION_MESSAGES,
} from '../../../common/constants';

const EditUserProfilePage: NextPage = () => {
  const { user } = useUserData();
  const { enqueueSnackbar } = useSnackbar();

  const [editProfile] = useMutation<editUserProfile, editUserProfileVariables>(
    EDIT_USER_PROFILE,
    {
      refetchQueries: [{ query: GET_USER_QUERY }],
      awaitRefetchQueries: true,
      onCompleted: (data) => {
        if (data.editUserProfile.success)
          enqueueSnackbar(NOTIFICATION_MESSAGES.CHANGES_SAVED, {
            variant: 'success',
          });
      },
      onError: (error) =>
        enqueueSnackbar(ERROR_MESSAGES.GENERIC_ERROR, {
          variant: 'error',
        }),
    }
  );

  return (
    <MainLayout title={t`Úprava profilu`}>
      <Typography variant="h2" gutterBottom>
        <Trans>Úprava profilu</Trans>
      </Typography>

      <FormCard>
        {user && (
          <>
            <ManageUserProfileForm
              editProfile={editProfile}
              user={user}
              editMode
            />

            <DeleteUserDialog />
          </>
        )}
      </FormCard>
    </MainLayout>
  );
};

export default EditUserProfilePage;
