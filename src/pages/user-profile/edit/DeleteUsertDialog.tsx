import { FC, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from '@mui/material';
import { t, Trans } from '@lingui/macro';
import { useMutation } from '@apollo/client';
import { useUserData } from '../../../common/hooks/useUserData';
import { DELETE_USER_PROFILE } from '../../../apollo/mutations/deleteUserProfile.mutation';
import { useModalState } from '../../../common/utils/modalHelper';
import { deleteUserProfile } from '../../../apollo/mutations/__generated__/deleteUserProfile';
import { SubmitButton } from '../../../common/components/inputs/SubmitButton';
import { useRouter } from 'next/router';
import { Routes } from '../../../common/enums/routes';
import { logOut } from '../../../common/utils/authHelpers';

export const DeleteUserDialog: FC = () => {
  const { isModalOpen, closeModal, openModal } = useModalState(false);
  const [inputValue, setInputValue] = useState('');
  const { user } = useUserData();
  const router = useRouter();

  const [deleteProfileOnClick, { loading }] = useMutation<deleteUserProfile>(
    DELETE_USER_PROFILE,
    {
      onCompleted: (data) => {
        if (data.deleteUserProfile.success) {
          logOut(() => router.push(Routes.Login));
        }
      },
    }
  );

  const buttonEnabled = inputValue === `${user.firstName} ${user.lastName}`;

  return (
    <>
      <Button color="error" onClick={openModal}>
        <Trans>Smazat profil</Trans>
      </Button>

      <Dialog onClose={closeModal} open={isModalOpen || loading}>
        <DialogTitle>
          <Typography variant="h3" align="center">
            <Trans>Opravdu chcete smazat svůj profil?</Trans>
          </Typography>
        </DialogTitle>

        <DialogContent>
          <DialogContentText>
            <Typography marginBottom={2}>
              <Trans>
                Tato akce je nevratná a příjdete tak o všechny vaše projekty a
                uložné poznámky. Pokud chcete Váš účet skutečně smazat, zadejde
                prosím níže celé Vaše jméno (Křestní jméno a příjmení)
              </Trans>
            </Typography>
          </DialogContentText>

          <TextField
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
            margin="dense"
            label={t`Celé jméno`}
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={closeModal}>
            <Trans>zpět</Trans>
          </Button>

          <SubmitButton
            isSubmitting={loading}
            fullWidth={false}
            size="medium"
            color="error"
            onClick={() => deleteProfileOnClick()}
            disabled={!buttonEnabled || loading}
            labels={{
              isSubmitting: t`mažeme to`,
              submit: t`Opravdu chci svůj profil smazat`,
            }}
          />
        </DialogActions>
      </Dialog>
    </>
  );
};
