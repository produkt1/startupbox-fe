import * as React from 'react';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';

export const MotivationBox: React.FunctionComponent = (props) => {
  return (
    <Text>
      <Trans>
        Je super, že jsi se do toho pustil. Teď nám prosím řekni něco málo o
        sobě. Pamatuj, čím upřímnější a specifičtější budeš, tím větší máš šanci
        na napojení na ty správné lidi a investory, kteří ti pomůžou s
        rozjezdem. Neboj, nezabere to více, než pár minut.
      </Trans>
    </Text>
  );
};

const Text = styled(Typography)`
  line-height: 1.5;
`;
