import { default as React, FC } from 'react';
import styled from 'styled-components';
import { skills } from '../../../../common/constants';
import { InputLabelCustom, SkillItem } from '../../../../common/components/UI';
import { Trans } from '@lingui/macro';
import Close from '@mui/icons-material/Close';

type Props = {
  value: string[];
  setFieldValue;
};

export const SkillSelector: FC<Props> = (props) => {
  const { value, setFieldValue } = props;

  const add = (item) => {
    setFieldValue('skills', [...value, item]);
  };

  const remove = (item) => {
    const oldValue = [...value];

    const itemToRemoveIndex = oldValue.indexOf(item);

    setFieldValue('skills', [
      ...oldValue.slice(0, itemToRemoveIndex),
      ...oldValue.slice(itemToRemoveIndex + 1, oldValue.length),
    ]);
  };

  const handleClick = (item) => {
    if (value.includes(item)) {
      remove(item);
      return;
    }
    add(item);
  };

  return (
    <>
      <InputLabelCustom>
        <Trans>Tvoje schopnosti</Trans>
      </InputLabelCustom>

      <Text>
        <Trans>
          Vyber, co umíš a v jakých oblastech se cítíš silný. Čím upřímnější
          budeš, tím lépe využiješ StartupBox.
        </Trans>
      </Text>
      <FlexWrapper>
        {skills().map((item) => {
          const isSelected = value.includes(item.value);

          return (
            <SkillItem
              $selected={isSelected}
              onClick={() => handleClick(item.value)}
              key={item.value}
            >
              {item.label}

              {isSelected && <Close sx={{ fontSize: 14 }} />}
            </SkillItem>
          );
        })}
      </FlexWrapper>
    </>
  );
};

const FlexWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 2rem 0;
`;

const Text = styled.div`
  font-size: 14px;
`;
