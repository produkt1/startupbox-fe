import * as yup from 'yup';
import { sanitizeInput } from '../../../../common/utils/helpers';

export const profileCreationSchema = yup.object().shape({
  firstName: yup
    .string()
    .transform((value) => sanitizeInput(value))
    .required('potřeba zadat')
    .test((value) => !/[\d@#$%^&*()_+=[\]:;"<>?,.\\|~`!/]/.test(value)),
  lastName: yup
    .string()
    .transform((value) => sanitizeInput(value))
    .required('potřeba zadat')
    .test((value) => !/[\d@#$%^&*()_+=[\]:;"<>?,.\\|~`!/]/.test(value)),
  region: yup.string().required('potřeba zadat'),
});
