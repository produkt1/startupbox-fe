import * as React from 'react';
import { NextPage } from 'next';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { useUserData } from '../../../common/hooks/useUserData';
import { useMutation } from '@apollo/client';
import { useRouter } from 'next/router';
import { Routes } from '../../../common/enums/routes';
import { GET_USER_QUERY } from '../../../apollo/queries/getUser';
import { CREATE_USER_PROFILE } from '../../../apollo/mutations/createUserProfile.mutation';
import { FormCard } from '../../../common/components/UI';
import { MotivationBox } from './components/MotivationBox';
import { Trans } from '@lingui/macro';
import { Typography } from '@mui/material';
import {
  createUserProfile,
  createUserProfileVariables,
} from '../../../apollo/mutations/__generated__/createUserProfile';
import { ManageUserProfileForm } from '../../../common/components/profile/ManageUserForm';
import { useEffect } from 'react';
import { analyticsDataLayerPush } from '../../../common/utils/ga';

export type values = {
  name: string;
  lastName: string;
};

const RegistrationPage: NextPage = () => {
  const { user } = useUserData();
  const router = useRouter();

  const [createProfile] = useMutation<
    createUserProfile,
    createUserProfileVariables
  >(CREATE_USER_PROFILE, {
    refetchQueries: [{ query: GET_USER_QUERY }],
    awaitRefetchQueries: true,

    onCompleted: (data) => {
      if (data.createUserProfile.success) {
        analyticsDataLayerPush({ event: 'profile_created' });
        router.push(Routes.Onboarding);
      }
    },
  });

  useEffect(() => {
    analyticsDataLayerPush({ event: 'profile_creation_view' });
  }, []);

  return (
    <MainLayout title="Registrace">
      <Typography variant="h1" align="center">
        <Trans>Vítej ve StartupBoxu!</Trans>
      </Typography>

      <FormCard>
        <MotivationBox />

        <ManageUserProfileForm createProfile={createProfile} user={user} />
      </FormCard>
    </MainLayout>
  );
};

export default RegistrationPage;
