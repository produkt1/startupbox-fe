import { NextPage } from 'next';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { CardBlue } from '../../common/components/UI';
import { Button, Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import { Icon } from '../../common/components/Icon/Icon';
import { BadgeFirstIcon } from '../../common/components/Icon/Icons/BadgeFirst.icon';
import { Routes } from '../../common/enums/routes';
import Link from 'next/link';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import confetti from 'canvas-confetti';
import { analyticsDataLayerPush } from '../../common/utils/ga';

const OnBoardingSuccessPage: NextPage = () => {
  const { query } = useRouter();

  const shootConfetti = useCallback(() => {
    confetti({
      particleCount: 700,
      spread: 200,
    });
  }, []);

  useEffect(() => {
    shootConfetti();
  }, []);

  const handleClick = () => {
    analyticsDataLayerPush({ event: 'onboarding_enter_app' });
  };

  return (
    <MainLayout title="Onboarding">
      <StyledCard>
        <Typography variant="h3">
          <Trans>Gratulujeme!</Trans>
        </Typography>

        <Icon Component={BadgeFirstIcon} />

        <Typography variant="h4">
          <Trans>První krok</Trans>
        </Typography>

        <Typography align="center">
          <Trans>
            První krok jste udělali pomocí StartupBoxu a získali jste první
            odznak. Pokračujte v této cestě a sbírejte další odznaky.
          </Trans>
        </Typography>

        <Link
          href={{
            pathname: Routes.ProjectDetail,
            query: { projectId: query.projectId as string },
          }}
        >
          <Button
            variant="contained"
            size="large"
            fullWidth
            onClick={handleClick}
          >
            <Trans>Přejít na Dashboard</Trans>
          </Button>
        </Link>
      </StyledCard>
    </MainLayout>
  );
};

const StyledCard = styled(CardBlue)`
  padding: 70px 16px;
  display: grid;
  grid-template-columns: minmax(200px, 420px);
  grid-gap: 24px;
  align-items: center;
  justify-items: center;
  justify-content: center;
`;

export default OnBoardingSuccessPage;
