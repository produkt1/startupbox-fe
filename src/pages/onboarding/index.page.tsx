import { NextPage } from 'next';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { Typography } from '@mui/material';
import { CardBlue } from '../../common/components/UI';
import { Trans } from '@lingui/macro';
import { OnboardingForm } from './components/OnboardingForm';
import { ONBOARDING } from '../../apollo/mutations/onboarding.mutation';
import { useMutation } from '@apollo/client';
import {
  onboarding,
  onboardingVariables,
} from '../../apollo/mutations/__generated__/onboarding';
import { useRouter } from 'next/router';
import { Routes } from '../../common/enums/routes';
import { LoadingScreen } from '../../common/components/LoadingScreen';
import { analyticsDataLayerPush } from '../../common/utils/ga';

const OnboardingPage: NextPage = () => {
  const router = useRouter();

  const [onboard, { loading, data }] = useMutation<
    onboarding,
    onboardingVariables
  >(ONBOARDING, {
    onCompleted: async (data) => {
      analyticsDataLayerPush({ event: 'onboarding_success' });

      await router.push(
        `${Routes.Onboarding + Routes.Success}?projectId=${
          data.onboarding.projects[0].id
        }`
      );
    },
  });

  return (
    <MainLayout title="StartupBox: Onboarding">
      {loading && <LoadingScreen />}

      {!loading && !data && (
        <>
          <Typography variant="h1">
            <Trans>Vítej ve startupbox</Trans>
          </Typography>

          <CardBlue>
            <OnboardingForm onboard={onboard} />
          </CardBlue>
        </>
      )}
    </MainLayout>
  );
};

export default OnboardingPage;
