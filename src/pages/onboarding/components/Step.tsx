import { FC } from 'react';
import { OnboardingAnswer, OnboardingStep } from './stepsContent';
import { resultCounter, StepValue } from './OnboardingForm';
import { StepView } from './StepView';

type Props = {
  step: OnboardingStep;
  setCurrentStepValue(newValue: StepValue);
  currentStepValue: StepValue;
  additionalValue;
  setAdditionalValue;
};

export const Step: FC<Props> = ({
  step,
  setCurrentStepValue,
  currentStepValue,
  ...rest
}) => {
  const { multiselect } = step;

  const handleClick = (answer: OnboardingAnswer) => {
    const getNewValue = (): StepValue => {
      const currentStepValueAnswer = currentStepValue
        ? [...currentStepValue?.onboardingInputs.answer]
        : [];

      if (multiselect && currentStepValueAnswer.includes(answer.text)) {
        const index = currentStepValueAnswer.indexOf(answer.text);
        const currentresultCounter =
          currentStepValue.resultCounter as resultCounter[];

        return {
          ...currentStepValue,
          onboardingInputs: {
            question: currentStepValue?.onboardingInputs.question,
            answer: [
              ...currentStepValueAnswer.slice(0, index),
              ...currentStepValueAnswer.slice(index + 1),
            ],
          },

          resultCounter: [
            ...currentresultCounter.slice(0, index),
            ...currentresultCounter.slice(index + 1),
          ],
        };
      }

      if (multiselect) {
        const currentCounterArray: resultCounter[] =
          (currentStepValue?.resultCounter as resultCounter[]) || [];

        if (currentStepValueAnswer.length < 3) {
          currentStepValueAnswer.push(answer.text);

          currentCounterArray.push({
            answerNumber: answer.answerNumber,
            questionNumber: answer.questionNumber,
            value: answer.value as string,
          });
        }

        return {
          onboardingInputs: {
            question: step.question,
            answer: [...currentStepValueAnswer],
          },
          resultCounter: currentCounterArray,
        };
      }

      return {
        onboardingInputs: {
          question: step.question,
          answer: [answer.text],
        },
        resultCounter: {
          answerNumber: answer.answerNumber,
          questionNumber: answer.questionNumber,
          value: answer.value as string,
        },
      };
    };

    setCurrentStepValue(getNewValue());
  };

  return (
    <StepView
      step={step}
      // setStepValue={setStepValue}
      currentStepValue={currentStepValue}
      handleClick={handleClick}
      {...rest}
    />
  );
};
