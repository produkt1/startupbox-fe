import { t } from '@lingui/macro';
import { otherLabel } from '../../../common/constants';

export type OnboardingStep = {
  question: string;
  note?: string;
  answers?: OnboardingAnswers;
  multiselect?: boolean;
  video?: string;
};

export type OnboardingSteps = () => OnboardingStep[];

export const getOnboardingQuestions: OnboardingSteps = () => [
  {
    question: t`Čeho chceš pomocí StartupBoxu docílit?`,
    note: t`* Vyberte vše, co platí. Maximálně však 3 možnosti.`,
    multiselect: true,
  },
  {
    question: t`Co nejlépe popisuje tvou aktuální situaci?`,
  },
  {
    question: t`Jak jsi na tom se zákazníky?`,
  },
];

export type OnboardingAnswer = {
  questionNumber: number;
  answerNumber: number;
  text: string;
  value: string | number;
  additionalValue?: string;
};

export type OnboardingAnswers = () => OnboardingAnswer[];
export const onboardingAnswers: OnboardingAnswers = () => [
  {
    questionNumber: 0,
    answerNumber: 1,
    text: t`Najít nápad, na kterém budu pracovat`,
    value: 1,
  },
  {
    questionNumber: 0,
    answerNumber: 2,
    text: t`Získat informace o tom, jak úspěšně podnikat`,
    value: 'x',
  },
  {
    questionNumber: 0,
    answerNumber: 3,
    text: t`Zjistit, proč můj produkt nechce nikdo kupovat`,
    value: 3,
  },
  {
    questionNumber: 0,
    answerNumber: 4,
    text: t`Najít spoluzakladatele`,
    value: 'x',
  },
  {
    questionNumber: 0,
    answerNumber: 5,
    text: t`Najít investora`,
    value: 'x',
  },
  {
    questionNumber: 0,
    answerNumber: 6,
    text: t`Najít mentora`,
    value: 'x',
  },
  {
    questionNumber: 0,
    answerNumber: 7,
    text: otherLabel(),
    value: 'x',
  },
  {
    questionNumber: 1,
    answerNumber: 1,
    text: t`Zatím nemám nápad, ale chci ho najít`,
    value: 1,
  },
  {
    questionNumber: 1,
    answerNumber: 2,
    text: t`Mám nápad, o kterém teď přemýšlím`,
    value: 2,
  },
  {
    questionNumber: 1,
    answerNumber: 3,
    text: t`Mám prototyp produktu (vizualizace, modely atp.)`,
    value: 3,
  },
  {
    questionNumber: 1,
    answerNumber: 4,
    text: t`Mám hotový produkt, ale zákazníci o něj nemají zájem`,
    value: 4,
  },
  {
    questionNumber: 1,
    answerNumber: 5,
    text: t`Mám první verzi produktu (MVP), kterou teď vylepšuji`,
    value: 5,
  },
  {
    questionNumber: 1,
    answerNumber: 6,
    text: t`Mám produkt, platící zákazníky a teď chci se svým startupem vyrůst`,
    value: 6,
  },
  {
    questionNumber: 2,
    answerNumber: 1,
    text: t`Pravidelně získávám zpětnou vazbu (ať na hotový či připravovaný produkt)`,
    value: 1,
  },
  {
    questionNumber: 2,
    answerNumber: 2,
    text: t`Mám platící zákazníky`,
    value: 2,
  },
  {
    questionNumber: 2,
    answerNumber: 3,
    text: t`Za zákazníkem půjdu hned jak bude můj produkt hotový`,
    value: 3,
  },
];

export const combinationToResult = [
  {
    combination: '1xx',
    result: 'no idea',
  },
  {
    combination: 'x1x',
    result: 'no idea',
  },
  {
    combination: 'x61',
    result: '4',
  },
  {
    combination: 'x62',
    result: '4',
  },
  {
    combination: 321,
    result: '1A',
  },
  {
    combination: 322,
    result: '1A',
  },
  {
    combination: 323,
    result: '1A',
  },
  {
    combination: 'x23',
    result: '1A',
  },
  {
    combination: 'x21',
    result: '1B',
  },
  {
    combination: 'x22',
    result: '1B',
  },
  {
    combination: 331,
    result: '2A',
  },
  {
    combination: 332,
    result: '2A',
  },
  {
    combination: 333,
    result: '2A',
  },
  {
    combination: 'x33',
    result: '2A',
  },
  {
    combination: 'x31',
    result: '2B',
  },
  {
    combination: 'x32',
    result: '2B',
  },
  {
    combination: 341,
    result: '3A',
  },
  {
    combination: 342,
    result: '3A',
  },
  {
    combination: 343,
    result: '3A',
  },
  {
    combination: 351,
    result: '3A',
  },
  {
    combination: 353,
    result: '3A',
  },
  {
    combination: 361,
    result: '3A',
  },
  {
    combination: 362,
    result: '3A',
  },
  {
    combination: 363,
    result: '3A',
  },
  {
    combination: 'x41',
    result: '3A',
  },
  {
    combination: 'x43',
    result: '3A',
  },
  {
    combination: 'x53',
    result: '3A',
  },
  {
    combination: 'x63',
    result: '3A',
  },
  {
    combination: 352,
    result: '3B',
  },
  {
    combination: 'x42',
    result: '3B',
  },
  {
    combination: 'x51',
    result: '3B',
  },
  {
    combination: 'x52',
    result: '3B',
  },
];
