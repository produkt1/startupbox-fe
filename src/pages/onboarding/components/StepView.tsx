import { FC } from 'react';
import { OnboardingAnswer, OnboardingStep } from './stepsContent';
import styled from 'styled-components';
import { StepValue } from './OnboardingForm';
import { minWidth } from '../../../common/styles/helpers';
import { otherLabel } from '../../../common/constants';
import { TextAreaInput } from '../../../common/components/inputs/TextAreaInput';
import { OptionSelector } from '../../../common/components/UI';
import { t } from '@lingui/macro';

type Props = {
  step: OnboardingStep;
  currentStepValue: StepValue;
  additionalValue;
  setAdditionalValue;
  handleClick(answer: OnboardingAnswer);
};

export const StepView: FC<Props> = ({
  step,
  currentStepValue,
  additionalValue,
  setAdditionalValue,
  handleClick,
}) => {
  const { answers } = step;

  return (
    <>
      <OptionsGrid>
        {/*@ts-ignore*/}
        {answers?.map((answer) => (
          <OptionSelector
            key={answer.answerNumber}
            variant="outlined"
            value={answer.text}
            $selected={currentStepValue?.onboardingInputs.answer.includes(
              answer.text
            )}
            onClick={() => handleClick(answer)}
          >
            {answer.text}
          </OptionSelector>
        ))}
      </OptionsGrid>

      {currentStepValue?.onboardingInputs.answer.includes(otherLabel()) && (
        <TextAreaInput
          label={t`Čeho se ještě snažíte dosáhnout?`}
          value={additionalValue}
          name="other"
          rowsMin={1}
          handleChange={(event) => {
            setAdditionalValue(event.target.value);
          }}
        />
      )}
    </>
  );
};

const OptionsGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-auto-rows: 80px;
  grid-gap: 16px;

  grid-column: 1;

  ${minWidth.mobile} {
    grid-auto-rows: 140px;
    grid-template-columns: repeat(auto-fit, minmax(230px, 1fr));
    grid-column: span 2;
  }
`;
