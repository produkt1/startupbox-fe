import { FC, useEffect, useState } from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import { Alert, Button, Typography } from '@mui/material';
import {
  onboardingAnswers,
  OnboardingStep,
  getOnboardingQuestions,
  combinationToResult,
} from './stepsContent';
import { Step } from './Step';
import { MutationFunction } from '@apollo/client/react/types/types';
import {
  onboarding,
  onboardingVariables,
} from '../../../apollo/mutations/__generated__/onboarding';
import styled from 'styled-components';
import { minWidth } from '../../../common/styles/helpers';
import { OnboardingInputs } from '../../../../__generated__/globalTypes';
import { Trans } from '@lingui/macro';
import { find } from 'rxjs/operators';
import { analyticsDataLayerPush } from '../../../common/utils/ga';

export type resultCounter = {
  questionNumber: number;
  answerNumber: number;
  value: string;
};

export type StepValue = {
  onboardingInputs: OnboardingInputs;
  resultCounter: resultCounter | resultCounter[];
};

type Values = StepValue[];

type Props = {
  onboard: MutationFunction<onboarding, onboardingVariables>;
};

export const OnboardingForm: FC<Props> = ({ onboard }) => {
  const [selectedStepIndex, setSelectedStepIndex] = useState<number>(0);
  const [customAnswer, setCustomAnswer] = useState('');
  const [nothingSelected, setNothingSelected] = useState(false);

  const initialValues: Values = [];

  const [values, setValues] = useState<Values>(initialValues);

  const onboardingQuestions = getOnboardingQuestions();

  const setCurrentStepValue = (newValue: StepValue) => {
    setValues((prevValues) => [
      ...prevValues.slice(0, selectedStepIndex),
      newValue,
      ...prevValues.slice(selectedStepIndex + 1),
    ]);
    setNothingSelected(false);
  };

  const currentStepAnswers = onboardingAnswers().filter(
    (answer) => answer.questionNumber === selectedStepIndex
  );

  const currentStep: OnboardingStep = {
    ...onboardingQuestions[selectedStepIndex],
    // @ts-ignore
    answers: currentStepAnswers,
  };

  const handleNextStepClick = () => {
    if (!values[selectedStepIndex]?.onboardingInputs.answer.length) {
      setNothingSelected(true);
      return;
    }

    if (selectedStepIndex + 1 === onboardingQuestions.length) {
      const combinationString = values
        .map((value) => {
          if (Array.isArray(value.resultCounter)) {
            const sorted: string[] = value.resultCounter
              .map((item) => item.value)
              .sort();

            return sorted[0];
          }

          return value.resultCounter.value;
        })
        .join('');

      const getResult = () => {
        if (combinationString[0] === '1' || combinationString[1] === '1') {
          return "'no idea'";
        }

        return combinationToResult.find(
          (combination) => combinationString == combination.combination
        ).result;
      };

      return onboard({
        variables: {
          onboardingAnswers: values.map((value) => value.onboardingInputs),
          result: getResult() as string,
        },
      });
    }

    if (customAnswer && onboardingQuestions[selectedStepIndex].multiselect) {
      const currentValue = values[selectedStepIndex].onboardingInputs
        .answer as string[];
      currentValue.push(`Jiný - ${customAnswer}`);
    }

    // setCustomAnswer('');
    window.scrollTo({ top: 0, behavior: 'smooth' });
    return setSelectedStepIndex((prevState) => prevState + 1);
  };

  const handlePrevStepClick = () =>
    setSelectedStepIndex((prevState) => prevState - 1);

  useEffect(() => {
    analyticsDataLayerPush({
      event: 'onboarding',
      onboarding_step: selectedStepIndex + 1,
    });
  }, [selectedStepIndex]);

  return (
    <div>
      <Typography align="right">
        {selectedStepIndex + 1}/{onboardingQuestions.length}
      </Typography>
      <LinearProgress
        variant="determinate"
        value={((selectedStepIndex + 1) / onboardingQuestions.length) * 100}
      />
      <StepWrapper>
        <div>
          <Typography variant="h3">{currentStep.question}</Typography>
          {currentStep?.note && <Typography>{currentStep.note}</Typography>}
        </div>

        <ButtonsWrapper>
          {selectedStepIndex > 0 && (
            <Button fullWidth onClick={handlePrevStepClick}>
              <Typography>
                <Trans>zpět</Trans>
              </Typography>
            </Button>
          )}
          <Button variant="contained" fullWidth onClick={handleNextStepClick}>
            <Typography>
              <Trans>Pokračovat</Trans>
            </Typography>
          </Button>
        </ButtonsWrapper>

        {nothingSelected && (
          <Alert severity="error" className="span-two">
            <Trans>Vyberte prosím alespoň jednu možnost </Trans>
          </Alert>
        )}

        <Step
          step={currentStep}
          currentStepValue={values[selectedStepIndex]}
          setCurrentStepValue={setCurrentStepValue}
          additionalValue={customAnswer}
          setAdditionalValue={setCustomAnswer}
        />
      </StepWrapper>
    </div>
  );
};

const StepWrapper = styled.div`
  margin-top: 24px;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 16px;
  align-items: start;

  ${minWidth.mobile} {
    grid-template-columns: 1fr auto;

    .span-two {
      grid-column: span 2;
    }
  }
`;

const ButtonsWrapper = styled.div`
  display: grid;
  grid-template-columns: auto;

  width: 100%;

  ${minWidth.mobile} {
    grid-template-columns: auto auto;

    width: auto;
  }
`;
