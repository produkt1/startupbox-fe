import { NextPage } from 'next';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { spacings } from '../../common/styles/spacings';
import { useQuery } from '@apollo/client';
import { getAllProjects } from '../../apollo/queries/__generated__/getAllProjects';
import { GET_ALL_PROJECTS } from '../../apollo/queries/getAllProjects.query';
import { useChatbotContext } from '../../common/containers/ChatbotContext';
import { t } from '@lingui/macro';
import { IconButton } from '@mui/material';
import { minWidth } from '../../common/styles/helpers';
import { useRouter } from 'next/router';
import { getStoredUserId } from '../../common/utils/authHelpers';
import { CustomTooltip } from './components/ChatComponents/common/CustomTooltip';
import { SettingsIcon } from '../../common/components/Icon/Icons/Settings.icon';
import { ChatSettings } from './components/ChatComponents/ChatSettings';
import { Chat } from './components/ChatComponents/Chat';
import { colors } from '../../common/styles/colors';

const ChatbotPage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [activeProject, setActiveProject] = useState('');
  const { setState } = useChatbotContext();
  const { data } = useQuery<getAllProjects>(GET_ALL_PROJECTS);
  const router = useRouter();
  const userId = getStoredUserId();

  useEffect(() => {
    if (data?.getAllProjects?.length > 0) {
      const savedProject = localStorage.getItem(`activeProject-${userId}`);
      setActiveProject(savedProject || data.getAllProjects[0].id);
    }
  }, [userId, data, router.asPath]);

  useEffect(() => {
    if (activeProject) {
      localStorage.setItem(`activeProject-${userId}`, activeProject);
      setState((prevState) => ({
        ...prevState,
        project_id: activeProject,
      }));
    }
  }, [activeProject, userId]);

  return (
    <MainLayout title="StartupBox: AI adviser" paddingTop={0} paddingBottom={0}>
      <SelectorWrapper>
        <CustomTooltip title={t`Změnit nápad`}>
          <StyledIconButton onClick={() => setOpenModal(true)}>
            <SettingsIcon />
          </StyledIconButton>
        </CustomTooltip>
        <ChatSettings
          openModal={openModal}
          setOpenModal={setOpenModal}
          activeProject={activeProject}
          setActiveProject={setActiveProject}
          data={data}
        />
      </SelectorWrapper>
      <Chat />
    </MainLayout>
  );
};

const SelectorWrapper = styled.div`
  display: flex;
  width: 100%;
  margin: ${spacings.px8} 0 ${spacings.px24} auto;
  justify-content: flex-end;
  gap: ${spacings.px8};

  ${minWidth.desktop} {
    margin: ${spacings.px32} 0 ${spacings.px32} auto;
  }

  button {
    display: flex;
    align-self: flex-end;
  }
`;

const StyledIconButton = styled(IconButton)`
  background: ${colors.lightBlue};
  color: ${colors.brand};
  border-radius: 4px;
`;

export default ChatbotPage;
