import React, { Dispatch, SetStateAction } from 'react';
import { Box, Dialog, Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import { spacings } from '../../../../common/styles/spacings';
import styled from 'styled-components';
import { ProjectSelector } from './ProjectSelector';
import { getAllProjects } from '../../../../apollo/queries/__generated__/getAllProjects';
import { SettingsIcon } from '../../../../common/components/Icon/Icons/Settings.icon';
import { colors } from '../../../../common/styles/colors';

export type ChatSettingsProps = {
  openModal: boolean;
  setOpenModal: (bool: boolean) => void;
  activeProject: string;
  setActiveProject: Dispatch<SetStateAction<string>>;
  data: getAllProjects;
};

export const ChatSettings: React.FC<ChatSettingsProps> = ({
  openModal,
  setOpenModal,
  activeProject,
  setActiveProject,
  data,
}) => {
  const closeModal = () => {
    setOpenModal(false);
  };

  return (
    <Dialog onClose={closeModal} open={openModal}>
      <Wrapper>
        <IconWrapper>
          <SettingsIcon />
        </IconWrapper>
        <Typography variant="h5" textTransform="none">
          <Trans>Změnit nápad</Trans>
        </Typography>
        <ProjectSelector
          activeProject={activeProject}
          setActiveProject={setActiveProject}
          projects={data && data?.getAllProjects}
          maxWidth={250}
          closeModal={closeModal}
        />
      </Wrapper>
    </Dialog>
  );
};

const Wrapper = styled(Box)`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${spacings.px24};
  max-width: 350px;
  width: 100%;
  padding: ${spacings.px24} ${spacings.px48};
`;

const IconWrapper = styled.div`
  display: flex;
  background: ${colors.lightBlue};
  padding: ${spacings.px12};
  border-radius: 4px;
  color: ${colors.brand};
`;
