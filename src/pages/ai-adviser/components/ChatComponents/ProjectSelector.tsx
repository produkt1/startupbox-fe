import React from 'react';
import styled from 'styled-components';
import { FormControl, InputLabel, Select } from '@mui/material';
import { Trans, t } from '@lingui/macro';
import { getAllProjects_getAllProjects } from '../../../../apollo/queries/__generated__/getAllProjects';
import { StyledMenuItem } from '../../../../common/components/inputs/DropDownInput';

export type ProjectSelectorProps = {
  activeProject: string;
  setActiveProject: (project: string) => void;
  projects: getAllProjects_getAllProjects[];
  withLabel?: boolean;
  size?: 'small' | 'medium';
  maxWidth?: number;
  closeModal: () => void;
};

export const ProjectSelector: React.FC<ProjectSelectorProps> = ({
  activeProject,
  setActiveProject,
  projects,
  withLabel,
  size = 'medium',
  maxWidth,
  closeModal,
}) => {
  const menuProps = {
    MenuListProps: {
      disablePadding: true,
      sx: {
        maxWidth: maxWidth ? `${maxWidth}px` : '100%',
      },
    },
  };

  const handleClick = (projectId: string) => {
    setActiveProject(projectId);
    closeModal();
  };

  return (
    <FormControl fullWidth size={size} sx={{ width: '250px' }}>
      {withLabel && (
        <InputLabel id="project-select-label">
          <Trans>Vyber Projekt</Trans>
        </InputLabel>
      )}

      <Select
        labelId={withLabel && 'project-select-label'}
        id="project-select"
        value={activeProject}
        label={withLabel && t`Vyber Projekt`}
        MenuProps={menuProps}
        fullWidth
      >
        {projects?.map((project) => (
          <StyledMenuItem
            title={project.name}
            key={project.id}
            value={project.id}
            onClick={() => handleClick(project.id)}
          >
            <ItemText>{project.name}</ItemText>
          </StyledMenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

const ItemText = styled.span`
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;
