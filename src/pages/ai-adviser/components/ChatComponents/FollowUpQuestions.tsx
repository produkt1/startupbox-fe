import React from 'react';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import { useCopilotChat } from '@copilotkit/react-core';
import { Role, TextMessage } from '@copilotkit/runtime-client-gql';
import { colors } from '../../../../common/styles/colors';
import { spacings } from '../../../../common/styles/spacings';
import { ArrowRightIcon } from '../../../../common/components/Icon/Icons/ArrowRight.icon';

export type FollowUpQuestionsProps = {
  questions: string[];
};

export const FollowUpQuestions: React.FC<FollowUpQuestionsProps> = ({
  questions,
}) => {
  const { appendMessage } = useCopilotChat();

  const handleClick = (content: string) => {
    appendMessage(
      new TextMessage({
        content: content,
        role: Role.User,
      })
    );
  };

  return (
    <Container style={{ gap: spacings.px16 }}>
      <Typography fontWeight={500}>
        <Trans>Navazující otázky</Trans>
      </Typography>
      <Container>
        {questions?.map((question, i) => (
          <Question key={i} onClick={() => handleClick(question)}>
            <ArrowRightIcon />
            <span>{question}</span>
          </Question>
        ))}
      </Container>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${spacings.px8};
`;

const Question = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  max-width: max-content;
  background: ${colors.greyInput};
  padding: ${spacings.px8} ${spacings.px12};
  border-radius: 8px;
  cursor: pointer;
  font-size: 14px;
  margin-right: 10px;
  width: 90%;

  svg {
    flex: none;
    color: ${colors.darkerGrey};
  }
`;
