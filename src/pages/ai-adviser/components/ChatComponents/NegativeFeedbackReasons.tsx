import React, { useState } from 'react';
import styled from 'styled-components';
import { useSnackbar } from 'notistack';
import { Trans, t } from '@lingui/macro';
import {
  Button,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from '@mui/material';
import { useChatbotContext } from '../../../../common/containers/ChatbotContext';
import { colors } from '../../../../common/styles/colors';
import { Thumbs } from './ResponseFeedback';
import { SendIcon } from '../../../../common/components/Icon/Icons/Send.icon';
import { spacings } from '../../../../common/styles/spacings';
import { NOTIFICATION_MESSAGES } from '../../../../common/constants';
import { ChatbotSnackbarOptions } from './ChatSnackbar';

const reasons = [
  t`Odpověď mi nepomohla`,
  t`Odpověď neodpovídá mojí otázce`,
  t`Odpověď není fakticky správná`,
  t`Odpověď je neúplná`,
  t`Citované zdroje nesedí k odpovědi`,
];

export const NegativeFeedbackReasons = ({ setShowReasons }) => {
  const [showCustomReason, setShowCustomReason] = useState(false);
  const [customReason, setCustomReason] = useState(null);
  const [error, setError] = useState(false);
  const { getLastAgentMessage, feedback, setFeedback } = useChatbotContext();
  const { enqueueSnackbar } = useSnackbar();
  const lastMessage = getLastAgentMessage();

  const submitFeedback = (reason: string) => {
    if (!reason || reason.trim() === '') {
      setError(true);
    } else {
      setError(false);

      setFeedback({
        thumbs: Thumbs.Down,
        message_id: lastMessage.id,
        reason: reason,
      });

      setShowReasons(false);

      enqueueSnackbar(
        NOTIFICATION_MESSAGES.FEEDBACK_ACKNOWLEDGEMENT,
        ChatbotSnackbarOptions
      );
    }
  };

  const sendFeedback = (
    event: React.KeyboardEvent<HTMLDivElement>,
    reason: string
  ) => {
    if (event.key === 'Enter' && !event.shiftKey) {
      event.preventDefault();
      submitFeedback(reason);
    }
  };

  return (
    <Wrapper>
      <Typography variant="body2">
        <Trans>Řekni nám trochu víc</Trans>:
      </Typography>

      <Reasons>
        {reasons.map((reason, i) => (
          <Reason key={i} onClick={() => submitFeedback(reason)}>
            {reason}
          </Reason>
        ))}

        <Reason
          onClick={() => setShowCustomReason(true)}
          $selected={showCustomReason}
        >
          {t`Jiné`}
        </Reason>
      </Reasons>

      {showCustomReason && (
        <StyledTextField
          variant="outlined"
          placeholder={t`Napiš nám svůj názor`}
          fullWidth
          multiline
          value={feedback?.reason}
          size="small"
          onChange={(e) => setCustomReason(e.target.value)}
          error={error}
          helperText={error ? t`Zadejte prosím důvod` : ''}
          onKeyDown={(e) => sendFeedback(e, customReason)}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disableRipple
                  onClick={() => submitFeedback(customReason)}
                  edge="end"
                >
                  <SendIcon width={20} height={20} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${spacings.px20};
  padding: ${spacings.px12};
  border: 1px solid ${colors.buttonLightBlue};
  border-radius: 8px;
`;

const Reasons = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${spacings.px8};
`;

const Reason = styled(Button)<{ $selected?: boolean }>`
  background: ${colors.buttonLightBlue};
  color: ${colors.black};
  text-align: left;
  border: 1px solid
    ${(props) => (props.$selected ? colors.brand : 'transparent')};
`;

const StyledTextField = styled(TextField)`
  & .MuiOutlinedInput-root {
    font-size: 14px;
    border-radius: 8px;
    background-color: ${colors.buttonLightBlue} !important;

    fieldset {
      border: 1px solid ${colors.blueInputBorder};
    }

    &:hover fieldset {
      border-color: ${colors.blueInputBorder};
    }

    &.Mui-focused fieldset {
      border-color: ${colors.blueInputBorder};
    }
  }
`;
