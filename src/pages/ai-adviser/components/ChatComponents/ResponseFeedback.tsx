import React, { useEffect } from 'react';
import styled, { css } from 'styled-components';
import { useSnackbar } from 'notistack';
import { IconButton } from '@mui/material';
import { LikeIcon } from '../../../../common/components/Icon/Icons/Like.icon';
import { DislikeIcon } from '../../../../common/components/Icon/Icons/Dislike.icon';
import { ButtonsWrapper } from './ResponseActions';
import { useChatbotContext } from '../../../../common/containers/ChatbotContext';
import { colors } from '../../../../common/styles/colors';
import { NOTIFICATION_MESSAGES } from '../../../../common/constants';
import { ChatbotSnackbarOptions } from './ChatSnackbar';
import { t } from '@lingui/macro';
import { CustomTooltip } from './common/CustomTooltip';

export enum Thumbs {
  Up = 'up',
  Down = 'down',
}

export type MessageFeedback = {
  thumbs: Thumbs;
  message_id: string;
  reason?: string;
};

export const ResponseFeedback = ({ showReasons, setShowReasons }) => {
  const { getLastAgentMessage, feedback, setFeedback } = useChatbotContext();
  const { enqueueSnackbar } = useSnackbar();
  const lastMessage = getLastAgentMessage();
  const isLiked = feedback?.thumbs === Thumbs.Up;
  const isDisliked = feedback?.thumbs === Thumbs.Down;

  useEffect(() => {
    if (lastMessage?.id != feedback?.message_id) {
      setFeedback(null);
    }
  }, [lastMessage]);

  const sendPositiveFeedback = () => {
    if (showReasons) {
      setShowReasons(false);
    }

    if (!feedback) {
      setFeedback({
        thumbs: Thumbs.Up,
        message_id: lastMessage.id,
      });

      enqueueSnackbar(
        NOTIFICATION_MESSAGES.FEEDBACK_ACKNOWLEDGEMENT,
        ChatbotSnackbarOptions
      );
    }
  };

  const showFeedbackReasons = () => {
    if (!feedback) {
      setShowReasons(true);
    }
  };

  return (
    <ButtonsWrapper>
      <CustomTooltip title={t`Toto se povedlo`}>
        <FeedbackButton
          onClick={sendPositiveFeedback}
          $active={isLiked}
          disableRipple={isLiked}
          disabled={isDisliked}
        >
          <LikeIcon />
        </FeedbackButton>
      </CustomTooltip>
      <CustomTooltip title={t`Toto se nepovedlo`}>
        <FeedbackButton
          onClick={showFeedbackReasons}
          disableRipple={showReasons || isDisliked}
          $active={showReasons || isDisliked}
          disabled={isLiked}
        >
          <DislikeIcon />
        </FeedbackButton>
      </CustomTooltip>
    </ButtonsWrapper>
  );
};

const FeedbackButton = styled(IconButton)<{ $active?: boolean }>`
  ${(props) =>
    props.$active &&
    css`
      cursor: default;
      background-color: ${colors.buttonLightBlue};
      border: 1px solid ${colors.brand} !important;
      svg {
        color: ${colors.brand};
      }
    `}
`;
