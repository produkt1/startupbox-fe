import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import { colors } from '../../../../common/styles/colors';
import { spacings } from '../../../../common/styles/spacings';
import { LinkIcon } from '../../../../common/components/Icon/Icons/Link.icon';

export type Reference = {
  title: string;
  url: string;
};

type ReferencesProps = {
  references?: Reference[];
};

export const References: React.FC<ReferencesProps> = ({ references }) => (
  <Wrapper>
    <Typography fontWeight={500}>
      <Trans>Zdroje</Trans>
    </Typography>
    <ReferencesContainer>
      {references?.map((reference, i) => (
        <StyledReference key={i} href={reference.url} target="_blank">
          <Typography fontSize={12}>{reference.title}</Typography>
          <LinkIcon width={20} height={20} />
        </StyledReference>
      ))}
    </ReferencesContainer>
  </Wrapper>
);

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  gap: ${spacings.px16};
  overflow-x: scroll;
`;

const ReferencesContainer = styled.div`
  display: flex;
  gap: ${spacings.px20};
`;

const StyledReference = styled(Link)`
  width: 100%;
  max-width: 180px;
  border-radius: 4px;
  padding: ${spacings.px8} ${spacings.px12};
  color: ${colors.brand};
  border: 1px solid ${colors.blueActiveBorder};
  text-decoration: none;
  cursor: pointer;
  display: flex;

  p {
    width: 90%;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  svg {
    visibility: hidden;
    display: flex;
  }

  &:hover {
    text-decoration: underline;
    svg {
      visibility: visible;
    }
  }
`;
