import React, { ReactElement } from 'react';
import { Tooltip } from '@mui/material';
import { colors } from '../../../../../common/styles/colors';

type CustomTooltipProps = {
  children: ReactElement;
  title: string;
  offset?: number;
};

export const CustomTooltip: React.FC<CustomTooltipProps> = ({
  children,
  title,
  offset = -5,
}) => (
  <Tooltip
    title={title}
    arrow
    placement="top"
    slotProps={{
      arrow: {
        style: {
          color: colors.blueActiveBorder,
        },
      },
      popper: {
        modifiers: [
          {
            name: 'offset',
            options: {
              offset: [0, offset],
            },
          },
        ],
      },
    }}
  >
    {children}
  </Tooltip>
);
