import React from 'react';
import styled from 'styled-components';
import { IconButton } from '@mui/material';
import { RegenerateIcon } from '../../../../common/components/Icon/Icons/Regenerate.icon';
import { CopyIcon } from '../../../../common/components/Icon/Icons/Copy.icon';
import { useCopilotChat } from '@copilotkit/react-core';
import { colors } from '../../../../common/styles/colors';
import { useSnackbar } from 'notistack';
import { useChatbotContext } from '../../../../common/containers/ChatbotContext';
import { ERROR_MESSAGES } from '../../../../common/constants';
import { t } from '@lingui/macro';
import { CustomTooltip } from './common/CustomTooltip';

export const ResponseActions: React.FC = () => {
  const { reloadMessages } = useCopilotChat();
  const { getLastAgentMessage } = useChatbotContext();
  const { enqueueSnackbar } = useSnackbar();

  const regenerateResponse = () => {
    try {
      reloadMessages();
    } catch (e) {
      console.error(e);
      enqueueSnackbar(ERROR_MESSAGES.GENERIC_ERROR, {
        variant: 'error',
      });
    }
  };

  const copyResponse = () => {
    const latestMessage = getLastAgentMessage();
    navigator.clipboard.writeText(
      latestMessage.isTextMessage() && latestMessage.content
    );
  };

  return (
    <ButtonsWrapper>
      <CustomTooltip title={t`Obnovit`}>
        <IconButton onClick={regenerateResponse}>
          <RegenerateIcon />
        </IconButton>
      </CustomTooltip>
      <CustomTooltip title={t`Zkopírovat`}>
        <IconButton onClick={copyResponse}>
          <CopyIcon />
        </IconButton>
      </CustomTooltip>
    </ButtonsWrapper>
  );
};

export const ButtonsWrapper = styled.div`
  display: flex;
  gap: 3px;
  button {
    color: ${colors.darkerGrey};
    border-radius: 6.4px;
    border: 1px solid transparent;

    &:hover {
      color: ${colors.black};
    }
  }
`;
