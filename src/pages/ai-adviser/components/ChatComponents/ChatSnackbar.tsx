import React from 'react';
import {
  OptionsObject,
  SnackbarContent,
  SnackbarKey,
  SnackbarMessage,
  useSnackbar,
} from 'notistack';
import styled from 'styled-components';
import { spacings } from '../../../../common/styles/spacings';
import { colors } from '../../../../common/styles/colors';
import { AiAdviserIcon } from '../../../../common/components/Icon/Icons/AiAdviser.icon';
import { Close } from '@mui/icons-material';

interface SnackbarProps {
  id: SnackbarKey;
  message: SnackbarMessage;
}

export const ChatbotSnackbarOptions: OptionsObject = {
  content: (key, message) => <ChatbotSnackbar id={key} message={message} />,
  anchorOrigin: {
    horizontal: 'right',
    vertical: 'bottom',
  },
};

export const ChatbotSnackbar = React.forwardRef<HTMLDivElement, SnackbarProps>(
  (props, ref) => {
    const { id, message, ...other } = props;
    const { closeSnackbar } = useSnackbar();

    return (
      <Content ref={ref} role="alert" {...other}>
        <AiAdviserIcon />
        {message}
        <CloseIcon onClick={() => closeSnackbar(id)} />
      </Content>
    );
  }
);

const Content = styled(SnackbarContent)`
  display: flex;
  align-items: center;
  padding: ${spacings.px16};
  gap: ${spacings.px8};
  border-radius: 8px;
  background: ${colors.black};
  color: ${colors.white};
  font-size: 14px;
`;

const CloseIcon = styled(Close)`
  width: 18px;
  cursor: pointer;
  color: ${colors.darkGrey};
`;
