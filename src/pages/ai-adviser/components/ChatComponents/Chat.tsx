import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { CopilotChat } from '@copilotkit/react-ui';
import { t } from '@lingui/macro';
import { useRouter } from 'next/router';
import { spacings } from '../../../../common/styles/spacings';
import { colors } from '../../../../common/styles/colors';
import { useChatbotContext } from '../../../../common/containers/ChatbotContext';
import {
  ActionExecutionMessage,
  ResultMessage,
  TextMessage,
  AgentStateMessage,
} from '@copilotkit/runtime-client-gql';
import { WelcomeScreen } from '../WelcomeScreen';
import { SendIcon } from '../../../../common/components/Icon/Icons/Send.icon';
import { Loading } from './Loading';
import { ResponseFollowUp } from './ResponseFollowUp';
import { CustomTooltip } from './common/CustomTooltip';
import { ChevronBottomIcon } from '../../../../common/components/Icon/Icons/ChevronBottom.icon';
import { minWidth } from '../../../../common/styles/helpers';
import { IconButton } from '@mui/material';
import { getStoredUserId } from '../../../../common/utils/authHelpers';
import {
  getStoredFeedback,
  getStoredMessages,
} from '../../../../common/utils/helpers';

export const Chat: React.FC = () => {
  const [isVisible, setIsVisible] = useState(true);
  const { isLoading, setFeedback, setMessages } = useChatbotContext();
  const [showReasons, setShowReasons] = useState(false);
  const [showScrollButton, setShowScrollButton] = useState(false);
  const router = useRouter();
  const userId = getStoredUserId();
  const wrapperRef = useRef(null);
  const getMessagesContainer = () =>
    wrapperRef.current.querySelector('.copilotKitMessages');

  useEffect(() => {
    const storedMessages = getStoredMessages(userId);
    if (storedMessages || isLoading) {
      setIsVisible(false);
    }
  }, [isLoading]);

  useEffect(() => {
    const storedMessages = getStoredMessages(userId);
    if (storedMessages) {
      const parsedMessages = JSON.parse(storedMessages).map((message: any) => {
        switch (message.type) {
          case 'TextMessage':
            return new TextMessage({
              id: message.id,
              role: message.role,
              content: message.content,
              createdAt: message.createdAt,
            });
          case 'ActionExecutionMessage':
            return new ActionExecutionMessage({
              id: message.id,
              name: message.name,
              scope: message.scope,
              arguments: message.arguments,
              createdAt: message.createdAt,
            });
          case 'ResultMessage':
            return new ResultMessage({
              id: message.id,
              actionExecutionId: message.actionExecutionId,
              actionName: message.actionName,
              result: message.result,
              createdAt: message.createdAt,
            });
          case 'AgentStateMessage':
            return new AgentStateMessage({
              id: message.id,
              createdAt: message.createdAt,
              status: message.status,
              agentName: message.agentName,
              state: message.state,
              running: message.running,
              threadId: message.threadId,
              role: message.role,
              nodeName: message.nodeName,
              runId: message.runId,
              active: message.active,
            });
          default:
            throw new Error(`Unknown message type: ${message.type}`);
        }
      });
      setMessages(parsedMessages);
    } else {
      setMessages([]);
    }
    // load feedback with messages
    const feedback = getStoredFeedback(userId);
    if (feedback) {
      setFeedback(JSON.parse(feedback));
    }
  }, [userId, router.asPath]);

  useEffect(() => {
    const messagesContainer = getMessagesContainer();
    if (!messagesContainer) return;
    const handleScroll = () => {
      const { scrollTop, clientHeight, scrollHeight } = messagesContainer;
      const isAtBottom = scrollTop + clientHeight >= scrollHeight - 50;
      setShowScrollButton(!isAtBottom);
    };

    messagesContainer.addEventListener('scroll', handleScroll);
    return () => messagesContainer.removeEventListener('scroll', handleScroll);
  }, []);

  useEffect(() => {
    const messagesContainer = getMessagesContainer();
    if (isLoading && messagesContainer) {
      messagesContainer.scrollTo({
        top: messagesContainer.scrollHeight,
        behavior: 'smooth',
      });
    }
  }, [isLoading]);

  const scrollToBottom = () => {
    const messagesContainer = getMessagesContainer();
    if (messagesContainer) {
      messagesContainer.scrollTo({
        top: messagesContainer.scrollHeight,
        behavior: 'smooth',
      });
      setShowScrollButton(false);
    }
  };

  return (
    <Wrapper ref={wrapperRef}>
      <ChatContainer className="chatbot-tutorial-step-1">
        {isVisible && <WelcomeScreen />}

        <CopilotChat
          icons={{
            sendIcon: (
              <StyledSendIcon disabled={isLoading} disableRipple>
                <SendIcon />
              </StyledSendIcon>
            ),
            activityIcon: <SendIcon />,
            spinnerIcon: <Loading />,
          }}
          labels={{
            placeholder: t`Poraď se ...`,
          }}
          ResponseButton={(buttonProps) => (
            <ResponseFollowUp
              {...buttonProps}
              setShowReasons={setShowReasons}
              showReasons={showReasons}
            />
          )}
        />
      </ChatContainer>
      <CustomTooltip title={t`Jdu na poslední zprávu`}>
        <ScrollToButton
          onClick={scrollToBottom}
          showScrollButton={showScrollButton}
        >
          <ChevronBottomIcon />
        </ScrollToButton>
      </CustomTooltip>
    </Wrapper>
  );
};

const ChatContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${spacings.px32};
  justify-content: end;
  height: 100%;
  width: 100%;
  margin: auto;
  --copilot-kit-primary-color: ${colors.brandLight};

  ${minWidth.mobile} {
    width: 90%;
  }

  ${minWidth.desktop} {
    width: 100%;
    max-width: 700px;
    gap: ${spacings.px64};
  }

  .copilotKitChat {
    overflow-y: scroll !important;
    scroll-behavior: smooth;
  }

  .copilotKitMessages {
    padding: 1rem 0;
    overflow-x: hidden;
    scroll-behavior: smooth;
    overflow-anchor: none;

    ${minWidth.desktop} {
      padding-right: 2rem;
      padding-left: 0;
    }
  }

  .copilotKitMessage {
    max-width: 90%;
  }

  .copilotKitInput {
    border-radius: 8px;
    border: 1px solid ${colors.blueInputBorder};
    background: ${colors.greyInput};
    padding: ${spacings.px8} ${spacings.px16};
    width: 100%;
    textarea {
      background: transparent;
      font-size: 16px;
    }

    button {
      padding: 0;
    }

    ${minWidth.desktop} {
      max-width: 94%;
    }
  }
`;

const ScrollToButton = styled(IconButton)<{ showScrollButton?: boolean }>`
  align-self: flex-end;
  background-color: ${colors.lightBlue};
  color: ${colors.brand};
  transition:
    opacity 0.3s ease,
    transform 0.3s ease;
  opacity: ${(props) => (props.showScrollButton ? '1' : '0')};
  visibility: ${(props) => (props.showScrollButton ? 'visible' : 'hidden')};

  &:hover {
    background-color: ${colors.greyBlue};
  }
`;

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  gap: ${spacings.px12};
  height: 84vh;

  ${minWidth.tablet} {
    padding-bottom: ${spacings.px24};
    gap: 0;
  }
`;

const StyledSendIcon = styled(IconButton)`
  color: ${colors.brand};
`;
