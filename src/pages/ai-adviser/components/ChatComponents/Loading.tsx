import React from 'react';
import styled, { keyframes } from 'styled-components';
import { colors } from '../../../../common/styles/colors';

export const Loading: React.FC = () => (
  <StyledLoading>
    <span />
    <span />
    <span />
  </StyledLoading>
);

const typing = keyframes`
  to {
    opacity: 0.1;
    transform: translateY(-13px);
  }
`;

const StyledLoading = styled.div`
  display: flex;
  gap: 5px;
  margin-top: 5px;
  height: 10px;
  align-items: center;

  > span {
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background-color: ${colors.darkerGrey};
    animation: ${typing} 0.5s infinite alternate;
    opacity: 1;
    will-change: transform, opacity;
    animation-delay: 0.1s;

    &:nth-child(2) {
      animation-delay: 0.2s;
    }

    &:nth-child(3) {
      animation-delay: 0.4s;
    }
  }
`;
