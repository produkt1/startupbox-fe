import React, { Dispatch, SetStateAction } from 'react';
import styled from 'styled-components';
import { ResponseButtonProps } from '@copilotkit/react-ui';
import { ResponseFeedback } from './ResponseFeedback';
import { References } from './References';
import { FollowUpQuestions } from './FollowUpQuestions';
import { ResponseActions } from './ResponseActions';
import { spacings } from '../../../../common/styles/spacings';
import { useChatbotContext } from '../../../../common/containers/ChatbotContext';
import { NegativeFeedbackReasons } from './NegativeFeedbackReasons';

type FollowUpProps = ResponseButtonProps & {
  showReasons: boolean;
  setShowReasons: Dispatch<SetStateAction<boolean>>;
};

export const ResponseFollowUp: React.ComponentType<FollowUpProps> = ({
  inProgress,
  showReasons,
  setShowReasons,
}) => {
  const { chatbotState } = useChatbotContext();

  return (
    <Wrapper>
      {!inProgress && (
        <>
          <Wrapper>
            <ResponseControls>
              <ResponseActions />
              <ResponseFeedback
                showReasons={showReasons}
                setShowReasons={setShowReasons}
              />
            </ResponseControls>
            {showReasons && (
              <NegativeFeedbackReasons setShowReasons={setShowReasons} />
            )}
          </Wrapper>
          <ResponseSuggestions>
            {chatbotState.references?.length && (
              <References references={chatbotState.references} />
            )}

            {chatbotState.follow_up_questions?.length && (
              <FollowUpQuestions questions={chatbotState.follow_up_questions} />
            )}
          </ResponseSuggestions>
        </>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${spacings.px20};
`;

const ResponseSuggestions = styled(Wrapper)`
  margin-bottom: ${spacings.px20};
`;

const ResponseControls = styled.div`
  display: flex;
  align-items: center;
  gap: ${spacings.px32};
`;
