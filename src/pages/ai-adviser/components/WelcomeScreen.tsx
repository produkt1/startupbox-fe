import { Box } from '@mui/material';
import { AiAdviserIcon } from '../../../common/components/Icon/Icons/AiAdviser.icon';
import styled from 'styled-components';
import { colors } from '../../../common/styles/colors';
import { spacings } from '../../../common/styles/spacings';
import { minWidth } from '../../../common/styles/helpers';
import { Trans, t } from '@lingui/macro';
import { useCopilotChat } from '@copilotkit/react-core';
import { Role, TextMessage } from '@copilotkit/runtime-client-gql';

const hints = [
  t`Jaké další kroky mám v mém projektu učinit dál?`,
  t`Jak mám ověřit, že můj nápad dává smysl?`,
  t`Jak mám sestavit byznys plán?`,
];

export const WelcomeScreen: React.FC = () => {
  const { appendMessage } = useCopilotChat();

  const sendHintMessage = (content: string) => {
    appendMessage(
      new TextMessage({
        content: content,
        role: Role.User,
      })
    );
  };

  return (
    <Container>
      <Header>
        <AiAdviserIcon width={60} height={56} />
        <h3>
          <Trans>Ahoj! Jak Ti mohu poradit?</Trans>
        </h3>
      </Header>
      <Boxes className="hints">
        {hints.map((content, i) => (
          <StyledBox onClick={() => sendHintMessage(content)} key={i}>
            {content}
          </StyledBox>
        ))}
      </Boxes>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  ${minWidth.desktop} {
    align-items: flex-start;
  }
`;

const StyledBox = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  background: ${colors.lightBlue};
  width: 210px;
  min-height: 90px;
  border-radius: 8px;
  padding: ${spacings.px16};
  cursor: pointer;
  font-size: 14px;
  &:hover {
    background: ${colors.greyBlue};
  }

  ${minWidth.custom(700)} {
    min-height: 120px;
  }
`;

const Boxes = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: ${spacings.px16};

  ${minWidth.custom(700)} {
    flex-direction: row;
    max-width: 94%;
  }
`;

const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${spacings.px24};
  margin-bottom: ${spacings.px48};
  align-self: center;
`;
