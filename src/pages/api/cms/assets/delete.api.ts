import type { NextApiRequest, NextApiResponse } from 'next';
import { proxyRequest } from '../../../../common/utils/helpers';
import { withAuth } from '../../../../common/utils/authHelpers';

async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== 'POST') {
    res.setHeader('Allow', ['POST']);
    return res.status(405).json({ error: 'Method Not Allowed' });
  }

  try {
    const response = await proxyRequest(
      process.env.GRAPH_CMS_API_URL,
      'POST',
      {
        'Content-Type': req.headers['content-type'] || '',
        Authorization: `Bearer ${process.env.GRAPH_CMS_TOKEN}`,
      },
      JSON.stringify(req.body)
    );

    const responseData = await response.json();
    return res.status(response.status).json(responseData);
  } catch (error: any) {
    console.error('GraphCMS Proxy Error:', error);
    return res.status(500).json({ error: error.message });
  }
}

export default withAuth(handler);
