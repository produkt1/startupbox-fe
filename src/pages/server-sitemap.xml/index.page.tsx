import { getServerSideSitemap, ISitemapField } from 'next-sitemap';
import { GetServerSideProps } from 'next';
import { initializeApollo } from '../../apollo/apollo-client';

import { GRAPH_CMS_CLIENT_NAME } from '../../common/constants';
import { GET_ALL_CONTENT_PAGE_SLUGS } from '../../apollo/cms/GraphCMS/getAllContentPagesSlugs';
import {
  getAllContentPagesSlugs,
  getAllContentPagesSlugs_contentPages,
} from '../../apollo/cms/GraphCMS/__generatedTypes';

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  // Method to source urls from cms
  // const urls = await fetch('https//example.com/api')

  const siteUrl = 'https://startupbox.app';

  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: GET_ALL_CONTENT_PAGE_SLUGS,

    context: {
      clientName: GRAPH_CMS_CLIENT_NAME,
    },
  });

  const pages: getAllContentPagesSlugs = data;

  const fields: ISitemapField[] = pages.contentPages?.map(
    (page: getAllContentPagesSlugs_contentPages) => ({
      loc: `${siteUrl}/howto/${page.slug}`,
      lastmod: new Date().toISOString(),
    })
  );

  return getServerSideSitemap(ctx, fields);
};

// Default export to prevent next.js errors
// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function Sitemap() {}
