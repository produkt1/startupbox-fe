import { FC } from 'react';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Link from 'next/link';
import { Routes } from '../../common/enums/routes';
import Card from '@mui/material/Card';
import styled from 'styled-components';
import { getAllProjects_getAllProjects } from '../../apollo/queries/__generated__/getAllProjects';
import { formatDate } from '../../common/utils/formatters';
import AddIcon from '@mui/icons-material/Add';
import { DashBoardStageProgress } from '../../common/components/project/StageProgress/DashBoardStageProgress';
import { Trans } from '@lingui/macro';
import { ProjectLogo } from '../../common/components/project/ProjectLogo';
import { Grid } from '@mui/material';
import { minWidth } from '../../common/styles/helpers';

type Props = {
  projects: getAllProjects_getAllProjects[];
};
export const Projects: FC<Props> = ({ projects }) => {
  return (
    <>
      <Grid
        container
        justifyContent="space-between"
        marginBottom={4}
        alignItems="flex-end"
      >
        <Typography variant="h2">
          <Trans>Moje projekty</Trans>
        </Typography>

        {!!projects.length && (
          <>
            <Link href={Routes.Project + Routes.New}>
              <Button
                variant="contained"
                color="secondary"
                startIcon={<AddIcon />}
              >
                <Trans>Přidat</Trans>
              </Button>
            </Link>
          </>
        )}
      </Grid>

      {!projects.length && (
        <NoProjectsCard>
          <div>Ještě nemáš žádný projekt</div>

          <Link href={Routes.Project + Routes.New}>
            <Button
              variant="contained"
              color="secondary"
              size="large"
              startIcon={<AddIcon />}
            >
              Přidat projekt
            </Button>
          </Link>
        </NoProjectsCard>
      )}

      {projects.map((project) => (
        <Link
          href={{
            pathname: Routes.ProjectDetail,
            query: {
              projectId: project.id,
            },
          }}
          key={project.id}
        >
          <ProjectThumbnail>
            <GridContainer>
              <StyledProjectLogo url={project?.projectLogoUrl} isProject />
              <Typography variant="h3">{project.name}</Typography>
            </GridContainer>

            <ProgressWrapper>
              <DashBoardStageProgress phases={project.diary[0].phases} />
            </ProgressWrapper>

            <Grider>
              <Button variant="contained">
                <Trans>Deník projektu</Trans>
              </Button>

              <Typography variant="caption">
                <Trans>Poslední update</Trans>
                <div>{formatDate(project.updatedAt)}</div>
              </Typography>
            </Grider>
          </ProjectThumbnail>
        </Link>
      ))}
    </>
  );
};

const Grider = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 14px;
  width: 100%;
  max-width: 400px;
`;

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;

  grid-column-gap: 1rem;
  width: 100%;
  align-items: center;
  justify-items: start;
`;

const StyledProjectLogo = styled(ProjectLogo)`
  grid-row: 1 / -1;
`;

const ProjectThumbnail = styled(Card)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 25px;
  padding: 15px;
  width: 100%;
  cursor: pointer;

  ${minWidth.mobile} {
    align-items: start;
  }
`;

const ProgressWrapper = styled.div`
  margin: 25px 0;
  width: 100%;
`;

const NoProjectsCard = styled(Card)`
  width: 100%;
  display: grid;
  grid-template-rows: 1fr 1fr;
  justify-content: center;
  align-items: center;

  > div {
    margin-bottom: 2rem;
  }
`;
