import React, { FC } from 'react';
import { Projects } from './Projects';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { useQuery } from '@apollo/client';
import { getAllProjects } from '../../apollo/queries/__generated__/getAllProjects';
import { GET_ALL_PROJECTS } from '../../apollo/queries/getAllProjects.query';
import { LoadingScreen } from '../../common/components/LoadingScreen';

const MyProjects: FC = () => {
  const { data, loading } = useQuery<getAllProjects>(GET_ALL_PROJECTS);

  return (
    <MainLayout protectedRoute title="Moje Projekty">
      {loading && <LoadingScreen logo={false} />}

      {data && <Projects projects={data?.getAllProjects} />}
    </MainLayout>
  );
};

export default MyProjects;
