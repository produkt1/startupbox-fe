import { FC, useEffect, useRef, useState } from 'react';
import { InputLabelCustom } from '../../../../common/components/UI';
import { Trans } from '@lingui/macro';
import { ProjectLogo } from '../../../../common/components/project/ProjectLogo';
import Button from '@mui/material/Button';
import { Card, Modal, Typography } from '@mui/material';
import { ImageCropper } from '../../../../common/components/ImageCropper';
import * as React from 'react';
import { useModalState } from '../../../../common/utils/modalHelper';
import styled from 'styled-components';
import { uploadImageToGraphCMS } from '../../../../common/utils/fileHandlers';
import CircularProgress from '@mui/material/CircularProgress';
import { useMutation } from '@apollo/client';
import { DELETE_ASSET } from '../../../../apollo/cms/GraphCMS/deleteAsset';
import { UploadIcon } from '../../../../common/components/Icon/Icons/Upload.icon';
import { TrashIcon } from '../../../../common/components/Icon/Icons/Trash.icon';
import { minWidth } from '../../../../common/styles/helpers';
import { useSnackbar } from 'notistack';
import { ERROR_MESSAGES, GRAPH_CMS_ASSET } from '../../../../common/constants';

type Props = {
  uploadedImageUrl: string;
  setFieldValue;
  name: string;
  label: string;
  isProject?: boolean;
};

export const UploadModal: FC<Props> = ({
  uploadedImageUrl,
  setFieldValue,
  name,
  label,
  isProject,
}) => {
  const [file, setFile] = useState<Blob | null>(null);
  const [imageToProcess, setImageToProcess] = useState();
  const { openModal, closeModal, isModalOpen } = useModalState(false);
  const [uploading, setUploading] = useState(false);
  const prevUploadedFile = useRef(null);
  const { enqueueSnackbar } = useSnackbar();

  const [deletePrevImage] = useMutation(DELETE_ASSET, {
    variables: {
      id: prevUploadedFile?.current?.id,
    },
    context: {
      clientName: GRAPH_CMS_ASSET,
    },
    onError: (error) => {
      enqueueSnackbar(ERROR_MESSAGES.GENERIC_ERROR, {
        variant: 'error',
      });
    },
  });

  const handleUpload = async () => {
    setUploading(true);

    if (file) {
      if (prevUploadedFile.current) {
        await deletePrevImage();
      }

      const response = await uploadImageToGraphCMS(file);

      if (!response) {
        console.log('error');
        return;
      }

      if (response.url) {
        setFieldValue(name, response.url);
        prevUploadedFile.current = response;
        setFile(null);
        closeModal();
      }
    }

    setUploading(false);
  };

  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      // FIXME:
      // @ts-ignore
      reader.addEventListener('load', () => setImageToProcess(reader.result));
      reader.readAsDataURL(e.target.files[0]);
      openModal();
    }
  };

  const deletePhoto = () => {
    if (uploadedImageUrl) {
      setFieldValue(name, null);
    }
  };

  useEffect(() => {
    handleUpload();
  }, [file]);

  return (
    <>
      <InputLabelCustom>
        <Trans>{label}</Trans>
      </InputLabelCustom>

      <UploadWrapper>
        <ProjectLogo url={uploadedImageUrl} isProject={isProject} />

        <ButtonContainer>
          <Button
            variant="contained"
            component="label"
            startIcon={<UploadIcon />}
          >
            <Trans>Nahrát</Trans>
            <input
              type="file"
              accept="image/*"
              onChange={onSelectFile}
              hidden
            />
          </Button>
          <Button
            variant="text"
            component="label"
            startIcon={<TrashIcon />}
            onClick={deletePhoto}
          >
            <Trans>Smazat</Trans>
          </Button>
        </ButtonContainer>

        <StyledModal open={isModalOpen} onClose={closeModal}>
          <Card style={{ position: 'relative' }}>
            {uploading && (
              <Overlay>
                <Typography variant="h4">
                  <Trans>Nahrávání</Trans>
                </Typography>
                <CircularProgress color="primary" />
              </Overlay>
            )}

            <ImageCropper
              setFile={setFile}
              imageToProcess={imageToProcess}
              setImageToProcess={setImageToProcess}
              onSelectFile={onSelectFile}
            />
          </Card>
        </StyledModal>
      </UploadWrapper>
    </>
  );
};

const UploadWrapper = styled.div`
  background-color: #fff;
  padding: 20px;
  display: grid;
  grid-template-columns: auto 1fr;
  grid-column-gap: 16px;
  border: 1px dashed grey;
  align-items: center;
  gap: 20px;
`;

const StyledModal = styled(Modal)`
  display: grid;
  place-items: center;
`;

const Overlay = styled.div`
  z-index: 2000;
  //color: white;
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.64);
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  gap: 10px;

  ${minWidth.custom(430)} {
    flex-direction: row;
  }
`;
