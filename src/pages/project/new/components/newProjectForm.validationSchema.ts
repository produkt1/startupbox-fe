import * as yup from 'yup';
import { sanitizeInput } from '../../../../common/utils/helpers';

export const newProjectFormValidationSchema = yup.object().shape({
  name: yup
    .string()
    .transform((value) => sanitizeInput(value))
    .required('Zadej prosím jméno projektu.')
    .test((value) => !/[@#$%^&*+=[\];"<>?\\|~`!/]/.test(value)),
  industry: yup.array().min(1, 'Vyber obor'),
  projectPhase: yup.string().required('Vyber fázi projektu.'),
});

export const editProjectFormValidationSchema = yup.object().shape({
  name: yup
    .string()
    .transform((value) => sanitizeInput(value))
    .required('Zadej prosím jméno projektu.')
    .test((value) => !/[@#$%^&*+=[\];"<>?\\|~`!/]/.test(value)),
  industry: yup.array().min(1, 'Vyber obor'),
});
