import { NextPage } from 'next';
import { useMutation } from '@apollo/client';
import { GET_USER_QUERY } from '../../../apollo/queries/getUser';
import { useRouter } from 'next/router';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { HeaderWithBackLink } from '../../../common/components/HeaderWithBackLink';
import * as React from 'react';
import {
  createProject,
  createProjectVariables,
} from '../../../apollo/mutations/__generated__/createProject';
import { CREATE_PROJECT_MUTATION } from '../../../apollo/mutations/createProject.mutation';
import { ManageProjectForm } from '../../../common/components/project/ManageProjectForm';
import { GET_ALL_PROJECTS } from '../../../apollo/queries/getAllProjects.query';
import { t, Trans } from '@lingui/macro';
import { FormCard } from '../../../common/components/UI';
import { Typography } from '@mui/material';

const NewProjectPage: NextPage = () => {
  const router = useRouter();

  const [submitProjectData] = useMutation<
    createProject,
    createProjectVariables
  >(CREATE_PROJECT_MUTATION, {
    refetchQueries: [{ query: GET_USER_QUERY }, { query: GET_ALL_PROJECTS }],
    onCompleted: (data) => {
      router.push('/project/[id]', `/project/${data.createProject.id}`);
    },
  });

  return (
    <MainLayout title={t`Nový projekt`}>
      <HeaderWithBackLink header={t`Vytvořit projekt`} to="/" />

      <FormCard>
        <Typography>
          <Trans>
            Prosím vyplň základní informace o projektu. Díky nim dokážeme
            personalizovat doporučení a napojit na relevantní partnery.
          </Trans>
        </Typography>

        <ManageProjectForm createProject={submitProjectData} />
      </FormCard>
    </MainLayout>
  );
};

export default NewProjectPage;
