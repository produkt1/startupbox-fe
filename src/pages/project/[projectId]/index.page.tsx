import { default as React, useEffect, useState } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { useQuery } from '@apollo/client';
import { HeaderWithBackLink } from '../../../common/components/HeaderWithBackLink';
import Typography from '@mui/material/Typography';
import styled from 'styled-components';
import { DiaryStageProgress } from '../../../common/components/project/StageProgress/DiaryStageProgress';
import { GET_DIARY_BY_PROJECT_ID } from '../../../apollo/queries/getDiaryByProjectId.query';
import { ProjectDiary } from './components/ProjectDiary';
import { LoadingScreen } from '../../../common/components/LoadingScreen';
import { ProjectLogo } from '../../../common/components/project/ProjectLogo';
import { getLastVisitedPhase } from '../../../common/utils/helpers';
import { t } from '@lingui/macro';
import { IconButton } from '@mui/material';
import Link from 'next/link';
import { Routes } from '../../../common/enums/routes';
import { PhaseStage } from '../../../../__generated__/globalTypes';
import {
  TourCase,
  Tutorial,
} from '../../../common/components/Tutorial/Tutorial';
import {
  getDiaryByProjectId,
  getDiaryByProjectIdVariables,
} from '../../../apollo/queries/__generated__/getDiaryByProjectId';
import { SuccessBanner } from './components/SuccessBanner';
import { EditIcon } from '../../../common/components/Icon/Icons/Edit.icon';

const ProjectDetailPage: NextPage = () => {
  const router = useRouter();
  const { projectId } = router.query;
  const [selectedPhase, setSelectedPhase] = useState(PhaseStage.IDEA);

  const { data, loading } = useQuery<
    getDiaryByProjectId,
    getDiaryByProjectIdVariables
  >(GET_DIARY_BY_PROJECT_ID, {
    variables: { projectId: projectId as string },
    skip: !router.isReady,
  });

  useEffect(() => {
    if (!loading && !!getLastVisitedPhase()) {
      setSelectedPhase(getLastVisitedPhase() as PhaseStage);
    }
  }, [data]);

  const getCompleted = () => {
    const totalCompleted = data?.getDiaryByProjectId.phases.reduce(
      (acc, phase) => {
        return acc + phase.completedTasksCount;
      },
      0
    );

    const totalTaskCount = data?.getDiaryByProjectId.phases.reduce(
      (acc, phase) => {
        return acc + phase.tasksCount;
      },
      0
    );

    return totalCompleted === totalTaskCount;
  };

  const diaryCompleted = getCompleted();

  return (
    <MainLayout
      protectedRoute
      title={`Deník projektu ${data?.getDiaryByProjectId.project.name}`}
    >
      <Tutorial tourCase={TourCase.Diary} />

      <HeaderWithBackLink header={t`Deník projektu`} to="/" />

      {loading && <LoadingScreen logo={false} />}

      {data && (
        <>
          <GridContainer>
            <ProjectLogo
              url={data?.getDiaryByProjectId.project.projectLogoUrl}
              size="regular"
              isProject
            />

            <Typography variant="h3">
              {data?.getDiaryByProjectId.project.name}
            </Typography>

            <Link
              href={{
                pathname: Routes.ProjectEdit,
                query: { projectId },
              }}
            >
              <IconButton color="primary">
                <EditIcon />
              </IconButton>
            </Link>
          </GridContainer>

          <DiaryStageProgress
            className="demo__section diary-tutorial-step-1"
            setSelectedPhase={setSelectedPhase}
            selectedPhase={selectedPhase}
            phases={data?.getDiaryByProjectId.phases}
          />

          {diaryCompleted && <SuccessBanner />}

          <div className="demo__section diary-tutorial-step-2">
            <ProjectDiary
              recommendedTasks={data.getDiaryByProjectId.recommendedTasks}
              projectId={projectId as string}
              phases={data?.getDiaryByProjectId.phases}
              selectedPhase={selectedPhase}
            />
          </div>
        </>
      )}
    </MainLayout>
  );
};

export default ProjectDetailPage;

const GridContainer = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  margin: 2rem 0;

  > * {
    margin-right: 1rem;
  }
`;
