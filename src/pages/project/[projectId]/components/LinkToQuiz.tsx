import Button from '@mui/material/Button';
import { ErrorBoundary } from '../../../../common/containers/ErrorBoundary';
import { default as React, useMemo } from 'react';
import { useWhiteLabelContext } from '../../../../common/containers/WhitelabeProvider';
import { getDiaryByProjectId_getDiaryByProjectId_phases } from '../../../../apollo/queries/__generated__/getDiaryByProjectId';

type Props = {
  currentPhase: getDiaryByProjectId_getDiaryByProjectId_phases;
};
export const LinkToQuiz = ({ currentPhase }: Props) => {
  //link to quiz is used to show external links to get feedback for the app
  // or in case of innovate slovakia it leads to a quiz after a chapter
  // linksToQuiz can be defined in hygraph in whitelabel section
  const { linksToQuiz } = useWhiteLabelContext();

  const { href, buttonLabel } = useMemo(() => {
    if (!!linksToQuiz && !!linksToQuiz[currentPhase.title]) {
      return {
        buttonLabel: linksToQuiz[currentPhase.title].buttonLabel,
        href: linksToQuiz[currentPhase.title].url,
      };
    }

    return {
      buttonLabel: 'FEEDBACK | Pomoz nám. Dej nám zpětnou vazbu na StartupBox',
      href: 'https://forms.gle/FHqzjciRK6hWuXwG6',
    };
  }, []);

  return (
    <ErrorBoundary>
      <a href={href} target="_blank" rel="noreferrer noopener">
        <Button variant="contained" fullWidth>
          {buttonLabel}
        </Button>
      </a>
    </ErrorBoundary>
  );
};
