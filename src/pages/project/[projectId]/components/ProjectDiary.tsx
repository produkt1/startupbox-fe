import { default as React, FC, useCallback } from 'react';
import { Card, Grid, Hidden } from '@mui/material';
import Link from 'next/link';
import styled, { css } from 'styled-components';
import Button from '@mui/material/Button';
import { colors } from '../../../../common/styles/colors';
import Typography from '@mui/material/Typography';
import ChatIcon from '@mui/icons-material/Chat';
import { useUserData } from '../../../../common/hooks/useUserData';
import { PhaseStage } from '../../../../../__generated__/globalTypes';
import { Routes } from '../../../../common/enums/routes';
import { Trans } from '@lingui/macro';
import CheckIcon from '@mui/icons-material/Check';
import { minWidth } from '../../../../common/styles/helpers';
import { RecommendedLabel } from '../../../../common/components/RecommendedLabel';
import { getDiaryByProjectId_getDiaryByProjectId_phases } from '../../../../apollo/queries/__generated__/getDiaryByProjectId';
import { analyticsDataLayerPush } from '../../../../common/utils/ga';

import { LinkToQuiz } from './LinkToQuiz';
import { spacings } from '../../../../common/styles/spacings';

type Props = {
  selectedPhase: PhaseStage;
  phases: getDiaryByProjectId_getDiaryByProjectId_phases[];
  projectId: string;
  recommendedTasks: string[];
};

export const ProjectDiary: FC<Props> = (props) => {
  const { selectedPhase, phases, projectId, recommendedTasks } = props;
  const { user } = useUserData();

  const currentPhase = phases.find((phase) => phase.title === selectedPhase);
  const hasNewComment = (task) =>
    !!task.hasNewUpdate && task.comments[0]?.user.id !== user.id;

  const taskIsRecommended = (taskId) => recommendedTasks.includes(taskId);

  const handleClick = useCallback(
    (taskPath) => {
      analyticsDataLayerPush({
        event: 'howto_click',
        howto_click_metrics: 1,
        click_url: taskPath,
      });
    },
    [analyticsDataLayerPush]
  );

  return (
    <>
      {currentPhase.components.map((component, key) => (
        <SectionCard key={key}>
          <SectionHeader variant="h2">{component.title}</SectionHeader>

          {component.tasks.map((task) => (
            <React.Fragment key={task.id}>
              <Divider />

              <Link
                href={{
                  pathname: Routes.HowToPage,
                  query: {
                    projectId,
                    taskId: task.id,
                    pageId: task.howToPage,
                  },
                }}
              >
                <TaskLinkButton
                  $taskIsRecommended={taskIsRecommended(task.pointer)}
                  color="primary"
                  onClick={() => handleClick(task.howToPage)}
                >
                  <TaskRow
                    hasNewComment={hasNewComment(task)}
                    completed={task.completed}
                  >
                    <Grid container alignItems="center">
                      {task.thumbnail && (
                        <Hidden smDown>
                          <ImageThumbnail
                            src={task.thumbnail.url}
                            $taskIsRecommended={taskIsRecommended(task.pointer)}
                          />
                        </Hidden>
                      )}

                      <div>
                        <Typography variant="body2" fontWeight={500}>
                          {task.title}
                        </Typography>

                        {task.subtitle && (
                          <Typography variant="body2" color={'#141125'}>
                            {task.subtitle}
                          </Typography>
                        )}

                        <RecommendedLabel
                          taskPointer={task.pointer}
                          projectId={projectId as string}
                        />
                      </div>
                    </Grid>

                    {hasNewComment(task) && <ChatIcon color="secondary" />}

                    {!task.completed && task.duration && (
                      <Typography variant="body2" color={'#AAAAAA'}>
                        {task.duration} minut
                      </Typography>
                    )}

                    {task.completed && (
                      <CompletedLabel>
                        <CheckIcon />

                        <Hidden smDown>
                          <Trans>Dokončeno</Trans>
                        </Hidden>
                      </CompletedLabel>
                    )}
                  </TaskRow>
                </TaskLinkButton>
              </Link>
            </React.Fragment>
          ))}
        </SectionCard>
      ))}

      <LinkToQuiz currentPhase={currentPhase} />
    </>
  );
};

const SectionHeader = styled(Typography)`
  margin: 1rem 2rem;
`;

const TaskRow = styled.div<{ hasNewComment: boolean; completed: boolean }>`
  width: 100%;
  display: grid;
  justify-content: space-between;
  align-items: center;
  grid-column-gap: 4px;

  color: ${(props) => (props.completed ? colors.green : 'inherit')};

  grid-template-columns: ${(props) =>
    props.hasNewComment ? '1fr 25px  auto' : '1fr auto'};

  ${minWidth.mobile} {
    grid-column-gap: 16px;
  }
`;

const Divider = styled.div`
  height: 1px;
  background-color: #cccccc;
  margin: 0 1rem;

  ${minWidth.mobile} {
    margin: 0 2rem;
  }
`;

const RecommendedLabelWrapper = styled.span`
  position: absolute;
  top: -10px;
  left: 0;
  margin-left: 8px;

  ${minWidth.mobile} {
    position: relative;
    top: unset;
    left: unset;
  }
`;

const TaskLinkButton = styled(Button)<{ $taskIsRecommended: boolean }>`
  padding: 1rem 0.5rem;
  font-size: 14px;
  width: 100%;
  border-radius: 0;
  justify-content: flex-start;
  text-align: start;

  ${minWidth.mobile} {
    padding: 1rem 2rem;
    font-size: 16px;
  }

  ${(props) =>
    props.$taskIsRecommended &&
    css`
      background-color: #ffa2001d;
    `}
`;

const ImageThumbnail = styled.img<{ $taskIsRecommended: boolean }>`
  width: ${({ $taskIsRecommended }) => ($taskIsRecommended ? '50px' : '40px')};
  margin-right: 16px;
`;

const SectionCard = styled(Card)`
  margin: 0.5rem 0;
  padding: 16px 0 8px;
  border: none;
  box-shadow: 0 15px 20px 0 rgba(9, 60, 143, 0.2);

  ${minWidth.mobile} {
    margin: 2rem 0;
  }
`;

const CompletedLabel = styled.div`
  display: flex;
  align-items: center;
  padding: ${spacings.px4} ${spacings.px16};

  color: ${colors.green};

  ${minWidth.mobile} {
    border-radius: 8px;
    border: 1px solid ${colors.green};
    gap: ${spacings.px8};
  }

  svg {
    width: 20px;
    height: 20px;
  }
`;
