import { FC } from 'react';
import styled from 'styled-components';
import { colors } from '../../../../common/styles/colors';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';

export const SuccessBanner: FC = () => {
  return (
    <BannerWrapper>
      <Typography gutterBottom>
        <Typography component="span" fontSize={24}>
          <span role="img" aria-label="tada icon">
            🎉
          </span>{' '}
        </Typography>

        <Trans>
          Jupí, tvůj Deník je kompletně hotový! Skvělá práce! Celý tým
          StartupBoxu ti od srdce gratuluje!
        </Trans>
      </Typography>

      <Typography>
        <Trans>
          Ale nezapomeň, tohle stále jen začátek. Moc ti držíme palce v dalším
          budování tvého nápadu. Dej nám vědět, jak ti to jde na
          hello@startupbox.cz.
        </Trans>
      </Typography>
    </BannerWrapper>
  );
};

const BannerWrapper = styled.div`
  margin-top: 24px;
  padding: 16px;
  border-radius: 10px;
  color: ${colors.brand};
  background-color: ${colors.greenBanner};
`;
