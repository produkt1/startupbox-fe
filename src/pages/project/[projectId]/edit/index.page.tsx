import { NextPage } from 'next';
import { MainLayout } from '../../../../common/containers/MainLayout/MainLayout';
import { t } from '@lingui/macro';
import { HeaderWithBackLink } from '../../../../common/components/HeaderWithBackLink';
import * as React from 'react';
import { FormCard } from '../../../../common/components/UI';
import { ManageProjectForm } from '../../../../common/components/project/ManageProjectForm';
import { useMutation, useQuery } from '@apollo/client';
import { GET_PROJECT_BY_ID } from '../../../../apollo/queries/getProjectById.query';
import { useRouter } from 'next/router';
import {
  getProjectById,
  getProjectByIdVariables,
} from '../../../../apollo/queries/__generated__/getProjectById';
import {
  editProject,
  editProjectVariables,
} from '../../../../apollo/mutations/__generated__/editProject';
import { EDIT_PROJECT_MUTATION } from '../../../../apollo/mutations/editProject.mutation';
import { DeleteProjectDialog } from './DeleteProjectDialog';
import { useSnackbar } from 'notistack';
import { Routes } from '../../../../common/enums/routes';
import { GET_DIARY_BY_PROJECT_ID } from '../../../../apollo/queries/getDiaryByProjectId.query';
import {
  ERROR_MESSAGES,
  NOTIFICATION_MESSAGES,
} from '../../../../common/constants';

const EditProjectPage: NextPage = () => {
  const router = useRouter();
  const { projectId } = router.query;
  const { enqueueSnackbar } = useSnackbar();

  const { data } = useQuery<getProjectById, getProjectByIdVariables>(
    GET_PROJECT_BY_ID,
    {
      variables: { projectId: projectId as string },
    }
  );

  const [editProject] = useMutation<editProject, editProjectVariables>(
    EDIT_PROJECT_MUTATION,
    {
      onCompleted: (data) => {
        if (data.editProject.success) {
          enqueueSnackbar(NOTIFICATION_MESSAGES.CHANGES_SAVED, {
            variant: 'success',
            preventDuplicate: true,
          });
        } else {
          enqueueSnackbar(ERROR_MESSAGES.GENERIC_ERROR, {
            variant: 'error',
            preventDuplicate: true,
          });
        }
      },
      awaitRefetchQueries: true,
      refetchQueries: [
        {
          query: GET_DIARY_BY_PROJECT_ID,
          variables: {
            projectId: projectId as string,
          },
        },
      ],
    }
  );

  return (
    <MainLayout title={t`Upravit projekt`}>
      <HeaderWithBackLink
        header={t`Upravit projekt`}
        to={`${Routes.Project}/${projectId}`}
      />

      {data && (
        <FormCard>
          <ManageProjectForm
            editMode
            project={data?.getProjectById}
            editProject={editProject}
          />

          <DeleteProjectDialog
            projectId={projectId as string}
            projectName={data?.getProjectById.name}
          />
        </FormCard>
      )}
    </MainLayout>
  );
};

export default EditProjectPage;
