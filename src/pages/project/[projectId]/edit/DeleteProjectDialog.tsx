import { FC, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from '@mui/material';
import { t, Trans } from '@lingui/macro';
import { useModalState } from '../../../../common/utils/modalHelper';
import { useMutation } from '@apollo/client';
import { DELETE_PROJECT } from '../../../../apollo/mutations/deleteProject.mutation';
import {
  deleteProject,
  deleteProjectVariables,
} from '../../../../apollo/mutations/__generated__/deleteProject';
import { GET_ALL_PROJECTS } from '../../../../apollo/queries/getAllProjects.query';
import { Routes } from '../../../../common/enums/routes';
import { SubmitButton } from '../../../../common/components/inputs/SubmitButton';
import Link from 'next/link';
import styled from 'styled-components';

type Props = {
  projectId: string;
  projectName: string;
};

export const DeleteProjectDialog: FC<Props> = (props) => {
  const { projectId, projectName } = props;

  const { isModalOpen, closeModal, openModal } = useModalState(false);
  const [inputValue, setInputValue] = useState('');
  const [isProjectDeleted, setIsProjectDeleted] = useState(false);

  const [deleteProjectOnClick, { loading }] = useMutation<
    deleteProject,
    deleteProjectVariables
  >(DELETE_PROJECT, {
    variables: { projectId },
    refetchQueries: [{ query: GET_ALL_PROJECTS }],
    awaitRefetchQueries: true,
    onCompleted: (data) => {
      if (data.deleteProject.success) {
        setIsProjectDeleted(true);
      }
    },
  });

  const buttonEnabled = inputValue === projectName;

  return (
    <>
      <Button color="error" onClick={openModal}>
        <Trans>Smazat projekt</Trans>
      </Button>

      <Dialog onClose={closeModal} open={isModalOpen || loading}>
        {isProjectDeleted && (
          <>
            <DialogTitle>
              <Typography variant="h3" align="center">
                <Trans>Projekt byl smazán</Trans>
              </Typography>
            </DialogTitle>

            <ButtonsWrapper>
              <Link href={Routes.ProjectNew}>
                <Button variant="contained">
                  <Trans>Vytvořit jiný projekt </Trans>
                </Button>
              </Link>

              <Link href={Routes.Dashboard}>
                <Button variant="contained">
                  <Trans>Přejít na Dashboard po smazání</Trans>
                </Button>
              </Link>
            </ButtonsWrapper>
          </>
        )}

        {!isProjectDeleted && (
          <>
            <DialogTitle>
              <Typography variant="h3" align="center">
                <Trans>Opravdu chcete projekt {projectName} smazat?</Trans>
              </Typography>
            </DialogTitle>

            <DialogContent>
              <DialogContentText>
                <Typography>
                  <Trans>
                    Tato akce je nevratná a příjdete tak o všechny uložené
                    poznámky. Pokud chcete projekt skutečně smazat, zadejde
                    prosím níže název projektu
                  </Trans>
                </Typography>
              </DialogContentText>

              <TextField
                value={inputValue}
                onChange={(e) => setInputValue(e.target.value)}
                margin="dense"
                id="name"
                label={t`Název projektu`}
                fullWidth
                variant="standard"
              />
            </DialogContent>

            <DialogActions>
              <Button onClick={closeModal}>
                <Trans>zpět</Trans>
              </Button>

              <SubmitButton
                isSubmitting={loading}
                fullWidth={false}
                color="error"
                size="medium"
                onClick={() => deleteProjectOnClick()}
                disabled={!buttonEnabled || loading}
                labels={{
                  isSubmitting: t`mažeme to`,
                  submit: t`Opravdu chci projekt smazat`,
                }}
              />
            </DialogActions>
          </>
        )}
      </Dialog>
    </>
  );
};

const ButtonsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 32px;
  padding: 16px 32px;
`;
