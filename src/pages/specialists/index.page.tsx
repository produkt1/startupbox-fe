import { NextPage } from 'next';
import { SpecialOfferCardPageTemplate } from '../../common/containers/SpecialOfferCardPageTemplate';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { t } from '@lingui/macro';
import {
  getPartners,
  getPartners_partners,
  getPartnersVariables,
  Language,
} from '../../apollo/cms/GraphCMS/__generatedTypes';
import { GRAPH_CMS_CLIENT_NAME } from '../../common/constants';
import { GET_PARTNERS } from '../../apollo/cms/GraphCMS/getPartners';
import * as React from 'react';
import { useCallback } from 'react';
import { analyticsDataLayerPush } from '../../common/utils/ga';
import { useQuery } from '@apollo/client';
import { LoadingScreen } from '../../common/components/LoadingScreen';
import { useWhiteLabelContext } from '../../common/containers/WhitelabeProvider';

const Partners: NextPage = () => {
  const gaTrackingFunc = useCallback(
    (expertName) => {
      analyticsDataLayerPush({
        event: 'expert_click',
        expert_name: expertName,
      });
    },
    [analyticsDataLayerPush]
  );

  const { customPartnersLang } = useWhiteLabelContext();

  const { data, loading } = useQuery<getPartners, getPartnersVariables>(
    GET_PARTNERS,
    {
      variables: {
        where: {
          lang: customPartnersLang,
        },
      },
      context: {
        clientName: GRAPH_CMS_CLIENT_NAME,
      },
    }
  );

  return (
    <MainLayout title={t`Odborníci`}>
      {loading && <LoadingScreen logo={false} />}
      {data && (
        <SpecialOfferCardPageTemplate
          items={data.partners}
          buttonLabel={t`Profil partnera`}
          pageHeader={t`Odborníci`}
          gaTrackingFunc={gaTrackingFunc}
        />
      )}
    </MainLayout>
  );
};

export default Partners;
