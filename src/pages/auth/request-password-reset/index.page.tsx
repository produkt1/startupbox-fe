import { NextPage } from 'next';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { t, Trans } from '@lingui/macro';
import { useMutation } from '@apollo/client';
import { REQUEST_PASSWORD_RESET } from './gql/requestPasswordReset';
import {
  requestPasswordReset,
  requestPasswordResetVariables,
} from './gql/__generated__/requestPasswordReset';
import { Typography } from '@mui/material';
import React, { useRef, useState } from 'react';
import styled from 'styled-components';
import { RequestPasswordResetForm } from './components/RequestPasswordResetForm';
import { SuccessView } from './components/SuccessView';
import { CardBlue } from '../../../common/components/UI';

const RequestPasswordReset: NextPage = () => {
  const usedEmail = useRef<string | undefined>(undefined);
  const [error, setError] = useState(null);

  const [requestPasswordReset, { data }] = useMutation<
    requestPasswordReset,
    requestPasswordResetVariables
  >(REQUEST_PASSWORD_RESET, {
    onCompleted: (data1) => {
      if (!data1.requestPasswordReset.success) {
        setError('Heslo pro tento email nelze obnovit');
      }
    },
  });

  const success = data?.requestPasswordReset.success;

  return (
    <MainLayout title={t`Obnovit heslo`} protectedRoute={false} maxWidth={450}>
      <Typography variant="h2" align="center">
        <Trans>Zapomenuté heslo</Trans>
      </Typography>

      <StyledCard>
        {!success && (
          <RequestPasswordResetForm
            requestPasswordReset={requestPasswordReset}
            usedEmail={usedEmail}
            error={error}
          />
        )}

        {success && <SuccessView usedEmail={usedEmail.current} />}
      </StyledCard>
    </MainLayout>
  );
};

export default RequestPasswordReset;

const StyledCard = styled(CardBlue)`
  margin-top: 24px;
  display: grid;
  grid-column: 1;
  grid-row-gap: 24px;
`;
