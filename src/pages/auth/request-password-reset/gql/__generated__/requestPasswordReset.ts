/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: requestPasswordReset
// ====================================================

export interface requestPasswordReset_requestPasswordReset {
  __typename: 'ResetPasswordMutationResponseType';
  message: string;
  success: boolean;
  requestId: string | null;
  verificationCode: string | null;
}

export interface requestPasswordReset {
  requestPasswordReset: requestPasswordReset_requestPasswordReset;
}

export interface requestPasswordResetVariables {
  email: string;
}
