import * as yup from 'yup';

export const requestPasswordResetValidationSchema = yup.object().shape({
  email: yup
    .string()
    .email('zadejte prosím email v platném formátu')
    .required('potřeba zadat'),
});
