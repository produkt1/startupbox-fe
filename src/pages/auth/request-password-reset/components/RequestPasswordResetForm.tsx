import React, { FC, MutableRefObject } from 'react';
import { Button, Typography, Link as MuiLink } from '@mui/material';
import { Trans } from '@lingui/macro';
import { Form, Formik } from 'formik';
import { FormikTextInput } from '../../../../common/components/inputs/TextInput';
import { FormikSubmitHandler } from '../../../../common/types/commonTypes';
import {
  requestPasswordReset,
  requestPasswordResetVariables,
} from '../gql/__generated__/requestPasswordReset';
import { MutationFunction } from '@apollo/client/react/types/types';
import Link from 'next/link';
import { Routes } from '../../../../common/enums/routes';
import { requestPasswordResetValidationSchema } from './validation.schema';

type Props = {
  error: string;
  usedEmail: MutableRefObject<string>;
  requestPasswordReset: MutationFunction<
    requestPasswordReset,
    requestPasswordResetVariables
  >;
};

export const RequestPasswordResetForm: FC<Props> = (props) => {
  const { requestPasswordReset, usedEmail, error } = props;

  const handleSubmit: FormikSubmitHandler<requestPasswordResetVariables> =
    async ({ email }) => {
      usedEmail.current = email;
      await requestPasswordReset({ variables: { email } });
    };

  const initialValues: requestPasswordResetVariables = { email: '' };

  return (
    <>
      <Typography variant="body2" align="center">
        <Trans>
          Zadejte prosím emailovou adresu se kterou jste se registrovali a
          zašleme vám link pro obnovení hesla
        </Trans>
      </Typography>

      <Formik
        onSubmit={handleSubmit}
        initialValues={initialValues}
        validationSchema={requestPasswordResetValidationSchema}
      >
        {({ isSubmitting }) => (
          <Form>
            <FormikTextInput
              name="email"
              label="email pro obnovení hesla"
              type="email"
            />

            {error && (
              <Typography
                fontWeight={500}
                gutterBottom
                align="center"
                color="error"
              >
                {error}
              </Typography>
            )}

            <Button
              variant="contained"
              color="success"
              fullWidth
              size="large"
              disabled={isSubmitting}
              type="submit"
            >
              <Trans>Odeslat</Trans>
            </Button>
          </Form>
        )}
      </Formik>

      <Link href={Routes.Login}>
        <MuiLink align="center" variant="body2" fontWeight={500}>
          <Trans>Zpět na přihlášení</Trans>
        </MuiLink>
      </Link>
    </>
  );
};
