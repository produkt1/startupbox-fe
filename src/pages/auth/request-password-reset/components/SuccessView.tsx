import { FC } from 'react';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';

type Props = { usedEmail: string };

export const SuccessView: FC<Props> = ({ usedEmail }) => {
  return (
    <>
      <Typography variant="h3" align="center" gutterBottom>
        <Trans>Odesláno</Trans>
      </Typography>

      <Typography>
        Na adresu <b>{usedEmail}</b> jsme vám poslali email s ověřovacím kódem
        pro obnovení hesla.
      </Typography>
    </>
  );
};
