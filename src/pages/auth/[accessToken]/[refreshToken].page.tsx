/* eslint-disable no-undef */
import * as React from 'react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { LoadingScreen } from '../../../common/components/LoadingScreen';
import {
  getStoredAccessToken,
  storeNewTokens,
} from '../../../common/utils/authHelpers';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';

const Auth: React.FunctionComponent = () => {
  const router = useRouter();
  const { accessToken, refreshToken } = router.query;
  storeNewTokens(accessToken as string, refreshToken as string);

  const handleSignIn = async () => {
    router.push('/');
  };
  //
  useEffect(() => {
    handleSignIn();
  }, []);

  return (
    // <MainLayout title={''} protectedRoute={false}>
    <LoadingScreen logo={false} />
    // </MainLayout>
  );
};

export default Auth;
