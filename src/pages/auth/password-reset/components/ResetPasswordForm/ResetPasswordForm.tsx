import { Formik } from 'formik';
import { ResetPasswordFormView } from './ResetPasswordFormView';
import { ResetPasswordFuncType } from '../../[id].page';
import { resetPasswordFormSchema } from './resetPasswordForm.schema';
import { useRouter } from 'next/dist/client/router';
import { FC } from 'react';
import { FormikSubmitHandler } from '../../../../../common/types/commonTypes';
import { t } from '@lingui/macro';

type Props = {
  resetPassword: ResetPasswordFuncType;
};

export type resetPasswordFormValues = {
  password: string;
  passwordRepeat: string;
  code: string;
};

export const ResetPasswordForm: FC<Props> = (props) => {
  const { resetPassword } = props;
  const { query } = useRouter();
  const { id } = query;

  const initialValues: resetPasswordFormValues = {
    password: '',
    passwordRepeat: '',
    code: '',
  };

  const handleSubmit: FormikSubmitHandler<resetPasswordFormValues> = async (
    values,
    { setFieldError, setSubmitting }
  ) => {
    const { password, passwordRepeat, code } = values;

    if (password === passwordRepeat) {
      await resetPassword({
        code,
        password,
        id: id as string,
      });
    } else {
      setFieldError('passwordRepeat', t`hesla nejsou stejná`);
    }

    setSubmitting(false);
  };

  return (
    <Formik
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={resetPasswordFormSchema}
      component={(formikBag) => <ResetPasswordFormView {...formikBag} />}
    />
  );
};
