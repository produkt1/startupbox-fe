import { Form, FormikProps } from 'formik';
import { resetPasswordFormValues } from './ResetPasswordForm';
import { PasswordField } from '../../../../../common/components/inputs/PasswordField';
import { Button } from '@mui/material';
import { FormikTextInput } from '../../../../../common/components/inputs/TextInput';
import { t } from '@lingui/macro';
import { FC } from 'react';

type Props = FormikProps<resetPasswordFormValues>;

export const ResetPasswordFormView: FC<Props> = ({ isSubmitting }) => {
  return (
    <Form title="Nové heslo" autoComplete="off">
      <FormikTextInput
        id="code"
        name="code"
        label={t`Kód pro obnovení z emailu`}
        autoComplete="code"
        InputProps={{
          autoComplete: 'code',
        }}
      />

      <PasswordField
        name="password"
        label={t`Nové heslo *`}
        helperText={t`Heslo musí mít alespoň 6 znaků.`}
      />

      <PasswordField
        name="passwordRepeat"
        label={t`Zadejte prosím heslo znovu*`}
      />

      <Button
        variant="contained"
        color="success"
        fullWidth
        size="large"
        disabled={isSubmitting}
        type="submit"
      >
        {isSubmitting ? t`nastavování...` : t`Nastavit nové heslo`}
      </Button>
    </Form>
  );
};
