import * as yup from 'yup';

export const resetPasswordFormSchema = yup.object().shape({
  code: yup.string().min(4, 'moc kratky kod').required('kod je potřeba zadat'),

  password: yup
    .string()
    .min(6, 'Heslo musí mít minimálně 6 znaků')
    .required('pole je potřeba vyplnit'),

  passwordRepeat: yup
    .string()
    .min(6, 'Heslo musí mít minimálně 6 znaků')
    .required('pole je potřeba vyplnit'),
});
