import React, { FC } from 'react';
import { ResetPasswordForm } from './ResetPasswordForm/ResetPasswordForm';
import { ResetPasswordFuncType } from '../[id].page';
import { ResetPasswordSuccessMessage } from './ResetPasswordSuccessMessage';
import { MainLayout } from '../../../../common/containers/MainLayout/MainLayout';
import { t, Trans } from '@lingui/macro';
import { CardBlue } from '../../../../common/components/UI';
import { Typography } from '@mui/material';
import styled from 'styled-components';

type Props = {
  resetPassword: ResetPasswordFuncType;
  success: boolean;
};

export const ResetPasswordPageView: FC<Props> = (props) => {
  const { success, resetPassword } = props;

  return (
    <MainLayout title={t`Obnovení hesla`} protectedRoute={false} maxWidth={450}>
      <Typography variant="h2" align="center" gutterBottom>
        <Trans>Nové Heslo</Trans>
      </Typography>

      <StyledCard>
        {!success && <ResetPasswordForm resetPassword={resetPassword} />}

        {success && <ResetPasswordSuccessMessage />}
      </StyledCard>
    </MainLayout>
  );
};

const StyledCard = styled(CardBlue)`
  display: grid;
  grid-column: 1;
  grid-row-gap: 24px;
`;
