import Link from 'next/link';
import { Routes } from '../../../../common/enums/routes';
import { Button, Typography } from '@mui/material';
import { Trans } from '@lingui/macro';

export const ResetPasswordSuccessMessage: React.FunctionComponent = () => {
  return (
    <>
      <Typography variant="h3" align="center" gutterBottom>
        <Trans>Heslo bylo změněno</Trans>
      </Typography>

      <Typography align="center" gutterBottom>
        <Trans>Nyní se můžete přihlásit s novým heslem</Trans>
      </Typography>

      <Link href={Routes.Login}>
        <Button variant="contained" color="primary" fullWidth>
          <Trans>Přihlásit se</Trans>
        </Button>
      </Link>
    </>
  );
};
