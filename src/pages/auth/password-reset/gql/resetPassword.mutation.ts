import { gql } from '@apollo/client';

export const MUTATION_RESET_PASSWORD = gql`
  mutation resetPassword($code: String!, $password: String!, $id: String!) {
    resetPassword(data: { code: $code, password: $password, id: $id }) {
      code
      success
      message
    }
  }
`;
