import { NextPage } from 'next';
import { MUTATION_RESET_PASSWORD } from './gql/resetPassword.mutation';
import { ResetPasswordPageView } from './components/ResetPasswordPageView';
import {
  resetPassword,
  resetPasswordVariables,
} from './gql/__generated__/resetPassword';
import { FetchResult, useMutation } from '@apollo/client';

export type ResetPasswordFuncType = (
  variables: resetPasswordVariables
) => Promise<FetchResult<resetPassword>>;

const ResetPasswordPage: NextPage = () => {
  const [mutate, { data }] = useMutation<resetPassword, resetPasswordVariables>(
    MUTATION_RESET_PASSWORD
  );

  const resetPassword: ResetPasswordFuncType = async (variables) => {
    return await mutate({ variables: { ...variables } });
  };

  return (
    <ResetPasswordPageView
      resetPassword={resetPassword}
      success={data?.resetPassword?.success}
    />
  );
};

export default ResetPasswordPage;
