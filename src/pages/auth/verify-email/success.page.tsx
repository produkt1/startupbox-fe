import { NextPage } from 'next';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';
import { Button, Card, Typography } from '@mui/material';
import { Icon } from '../../../common/components/Icon/Icon';
import Link from 'next/link';
import { Routes } from '../../../common/enums/routes';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { colors } from '../../../common/styles/colors';
import { squareSize } from '../../../common/styles/helpers';

const EmailVerificationSuccessPage: NextPage = () => {
  return (
    <PageWrapper>
      <Card className="grid-setup">
        <StyledIcon Component={CheckCircleIcon} size={98} />
        <Typography variant="h3">
          <Trans>Ověření dokončeno</Trans>
        </Typography>

        <Typography>
          <Trans>Váš email byl úspěšně ověřen</Trans>
        </Typography>

        <Link href={Routes.Dashboard}>
          <Button variant="contained" color="success">
            <Trans>Přejít na Dashboard</Trans>
          </Button>
        </Link>
      </Card>
    </PageWrapper>
  );
};

export default EmailVerificationSuccessPage;

const PageWrapper = styled.div`
  height: 100vh;
  display: grid;
  place-content: center;

  .grid-setup {
    position: relative;
    display: grid;
    place-content: center;
    place-items: center;
    grid-row-gap: 16px;
    overflow: visible;
    padding: 64px 24px 24px;
`;

const StyledIcon = styled(Icon)`
  position: absolute;
  top: -40px;
  color: ${colors.green};

  > svg {
    ${squareSize('98px')}
  }
`;
