import { gql } from '@apollo/client';

import { USER_FRAGMENT } from '../../../../../apollo/fragments/user.fragment';

export const VERIFY_USER = gql`
  mutation verifyUser($requestId: String!, $verificationToken: String!) {
    verifyUser(
      data: { requestId: $requestId, verificationToken: $verificationToken }
    ) {
      __typename
      message
      code
      success

      accessToken
      refreshExpiresIn
      refreshToken
      expiresIn
      tokenType

      user {
        ...user
      }
    }
  }
  ${USER_FRAGMENT}
`;
