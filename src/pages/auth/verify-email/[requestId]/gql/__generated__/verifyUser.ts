/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  UserRole,
  EmailVerification,
  ProjectPhase,
} from './../../../../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: verifyUser
// ====================================================

export interface verifyUser_verifyUser_user_profile {
  __typename: 'Profile';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  region: string | null;
  skills: string[] | null;
}

export interface verifyUser_verifyUser_user_projects {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
}

export interface verifyUser_verifyUser_user_onboardingData {
  __typename: 'OnboardingType';
  answer: string[];
  question: string;
}

export interface verifyUser_verifyUser_user {
  __typename: 'User';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  profilePicture: string | null;
  role: UserRole;
  emailVerification: EmailVerification;
  profile: verifyUser_verifyUser_user_profile | null;
  projects: verifyUser_verifyUser_user_projects[] | null;
  onboardingResult: string | null;
  onboardingData: verifyUser_verifyUser_user_onboardingData[] | null;
}

export interface verifyUser_verifyUser {
  __typename: 'LoginResponseType';
  message: string;
  code: string;
  success: boolean;
  accessToken: string | null;
  refreshExpiresIn: number | null;
  refreshToken: string | null;
  expiresIn: number | null;
  tokenType: string | null;
  user: verifyUser_verifyUser_user | null;
}

export interface verifyUser {
  verifyUser: verifyUser_verifyUser;
}

export interface verifyUserVariables {
  requestId: string;
  verificationToken: string;
}
