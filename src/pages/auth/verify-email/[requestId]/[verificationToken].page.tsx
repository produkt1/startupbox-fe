import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { LoadingScreen } from '../../../../common/components/LoadingScreen';
import { useMutation } from '@apollo/client';
import { VERIFY_USER } from './gql/verifyUser.mutation';
import {
  verifyUser,
  verifyUserVariables,
} from './gql/__generated__/verifyUser';
import { Routes } from '../../../../common/enums/routes';
import { useEffect } from 'react';

const VerifyEmailPage: NextPage = () => {
  const router = useRouter();
  const { requestId, verificationToken } = router.query;

  const [verify] = useMutation<verifyUser, verifyUserVariables>(VERIFY_USER, {
    onCompleted: async (data) => {
      if (data.verifyUser.success) {
        await router.push(Routes.AuthVerifyEmail + Routes.Success);
      }
    },
  });

  useEffect(() => {
    if (verificationToken && requestId) {
      verify({
        variables: {
          requestId: requestId as string,
          verificationToken: verificationToken as string,
        },
      });
    }
  }, [requestId, verificationToken]);

  return <LoadingScreen message="ověřování emailu..." />;
};

export default VerifyEmailPage;
