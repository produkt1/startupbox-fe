import { GetServerSideProps, NextPage } from 'next';
import React from 'react';

import { GRAPH_CMS_CLIENT_NAME } from '../../common/constants';

import {
  GetContentPageBasic,
  GetContentPageBasic_contentPages,
  GetContentPageFull_contentPages,
  Stage,
} from '../../apollo/cms/GraphCMS/__generatedTypes';

import { GET_CONTENT_PAGE_BASIC } from '../../apollo/cms/GraphCMS/getContentPageBasic';

import { useUserData } from '../../common/hooks/useUserData';
import { useRouter } from 'next/dist/client/router';
import { useQuery } from '@apollo/client';

import { HowToPageFullView } from './components/HowToPageFullView';
import { HowToPagePreviewView } from './components/HowToPagePreviewView';

type Props = {
  page: GetContentPageFull_contentPages | GetContentPageBasic_contentPages;
};

const HowToPage: NextPage<Props> = (props, ctx) => {
  const { user } = useUserData();

  const { query } = useRouter();
  const { pageId } = query;

  const { data, loading } = useQuery<GetContentPageBasic>(
    GET_CONTENT_PAGE_BASIC,
    {
      variables: {
        slug: pageId as string,
        stage: ctx.preview ? Stage.DRAFT : Stage.PUBLISHED,
      },
      context: {
        clientName: GRAPH_CMS_CLIENT_NAME,
      },
    }
  );

  return user ? (
    <HowToPageFullView
      preview={ctx.preview}
      loadingMain={loading}
      page={data?.contentPages[0]}
    />
  ) : (
    <HowToPagePreviewView
      preview={ctx.preview}
      page={data?.contentPages[0]}
      loadingMain={loading}
    />
  );
};

// type Link = { pageId: string; preview?: string };
//
// export const getServerSideProps: GetServerSideProps<unknown, Link> = async (
//   context
// ) => {
// const apolloClient = initializeApollo();
//
// const { data } = await apolloClient.query({
//   query: GET_CONTENT_PAGE_BASIC,
//   variables: {
//     slug: context.query.pageId as string,
//     stage: context.query?.preview ? Stage.DRAFT : Stage.PUBLISHED,
//   },
//   context: {
//     clientName: GRAPH_CMS_CLIENT_NAME,
//   },
// });

// const page = data?.contentPages[0];

// if (!page) {
//   return {
//     redirect: {
//       permanent: false,
//       destination: '/404',
//     },
//   };
// }

//   return {
//     props: {
//       // page,
//       preview: !!context.query.preview,
//     },
//   };
// };

export default HowToPage;
