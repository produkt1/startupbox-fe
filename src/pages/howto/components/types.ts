type ChapterNavigationItem = {
  taskId: string;
  pageId: string;
};

export type ChapterNavigation = {
  previous: ChapterNavigationItem;
  next: ChapterNavigationItem;
};
