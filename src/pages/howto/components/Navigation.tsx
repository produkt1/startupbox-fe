import React from 'react';
import {
  TourCase,
  Tutorial,
} from '../../../common/components/Tutorial/Tutorial';
import { ButtonsSection } from './ButtonsSection';
import { Box, Skeleton } from '@mui/material';
import { RecommendedLabel } from '../../../common/components/RecommendedLabel';
import { useQuery } from '@apollo/client';
import {
  getTaskById,
  getTaskByIdVariables,
} from '../../../apollo/queries/__generated__/getTaskById';
import { GET_TASK_BY_ID } from '../../../apollo/queries/getTaskById.query';
import {
  getDiaryByProjectId,
  getDiaryByProjectIdVariables,
} from '../../../apollo/queries/__generated__/getDiaryByProjectId';
import { GET_DIARY_BY_PROJECT_ID } from '../../../apollo/queries/getDiaryByProjectId.query';
import { getPrevAndNextPageIds } from '../helpers';

import { useRouter } from 'next/router';
import { ChapterNavigation } from './types';

type Props = { preview: boolean };

export const Navigattion = ({ preview }: Props) => {
  const router = useRouter();
  const { taskId, projectId, pageId } = router.query;

  const { data, loading } = useQuery<getTaskById, getTaskByIdVariables>(
    GET_TASK_BY_ID,
    {
      variables: { taskId: taskId as string },
      skip: !taskId || preview,
    }
  );

  const { data: diaryData, loading: diaryLoading } = useQuery<
    getDiaryByProjectId,
    getDiaryByProjectIdVariables
  >(GET_DIARY_BY_PROJECT_ID, {
    variables: { projectId: projectId as string },
    skip: !router.isReady || !projectId,
  });

  const chaptersNavigation: ChapterNavigation = getPrevAndNextPageIds(
    diaryData,
    pageId as string
  );

  const task = data?.getTaskById;

  return (
    <>
      {(loading || diaryLoading) && (
        <>
          <Box marginBottom={3}>
            <Skeleton variant="rectangular" width={'100%'} height={36} />
          </Box>

          <Box marginBottom={2} display="flex" justifyContent="space-between">
            <Skeleton variant="rectangular" width={'20%'} height={36} />
            <Skeleton variant="rectangular" width={'20%'} height={36} />
          </Box>
        </>
      )}

      {task && (
        <>
          <Tutorial tourCase={TourCase.HowTo} />

          <ButtonsSection task={task} chaptersNavigation={chaptersNavigation} />

          <Box marginBottom={1}>
            <RecommendedLabel
              taskPointer={task?.pointer}
              projectId={projectId as string}
            />
          </Box>
        </>
      )}
    </>
  );
};
