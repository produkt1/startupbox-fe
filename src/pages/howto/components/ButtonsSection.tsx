import React, { FC } from 'react';
import Link from 'next/link';
import { Routes } from '../../../common/enums/routes';
import { Button, Grid } from '@mui/material';
import { Trans } from '@lingui/macro';
import { CompleteTaskButton } from './CompleteTaskButton';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { getTaskById_getTaskById } from '../../../apollo/queries/__generated__/getTaskById';
import { DiaryIcon } from '../../../common/components/Icon/Icons/Diary.icon';
import { ChevronLeftIcon } from '../../../common/components/Icon/Icons/ChevronLeft.icon';
import { ChevronRightIcon } from '../../../common/components/Icon/Icons/ChevronRight.icon';
import { ChapterNavigation } from './types';
import { spacings } from '../../../common/styles/spacings';

type Props = {
  task: getTaskById_getTaskById;
  chaptersNavigation: ChapterNavigation;
};

export const ButtonsSection: FC<Props> = ({ task, chaptersNavigation }) => {
  const router = useRouter();
  const { projectId, taskId, pageId } = router.query;

  return (
    <>
      <ButtonsGrid>
        <Link
          href={{
            pathname: Routes.ProjectDetail,
            query: { projectId },
          }}
        >
          <Button aria-label="back" startIcon={<DiaryIcon />}>
            <Trans>Deník projektu</Trans>
          </Button>
        </Link>

        <ButtonsContainer>
          <Link
            href={{
              pathname: Routes.TaskDetail,
              query: {
                taskId,
                projectId,
                pageId,
              },
            }}
          >
            <Button variant="outlined" className="howTo-tutorial-step-2">
              <Trans>Moje Poznámky</Trans>
            </Button>
          </Link>

          <CompleteTaskButton task={task} />
        </ButtonsContainer>
      </ButtonsGrid>

      <Grid container justifyContent="space-between" marginBottom={2}>
        {chaptersNavigation?.previous ? (
          <Link
            href={{
              pathname: Routes.HowToPage,
              query: {
                projectId,
                taskId: chaptersNavigation?.previous.taskId,
                pageId: chaptersNavigation?.previous.pageId,
              },
            }}
          >
            <Button
              aria-label="back"
              startIcon={<ChevronLeftIcon width={7} height={12} />}
            >
              <Trans>Předchozí kapitola</Trans>
            </Button>
          </Link>
        ) : (
          <div />
        )}

        {chaptersNavigation?.next ? (
          <Link
            href={{
              pathname: Routes.HowToPage,
              query: {
                projectId,
                taskId: chaptersNavigation?.next.taskId,
                pageId: chaptersNavigation?.next.pageId,
              },
            }}
          >
            <Button
              aria-label="back"
              endIcon={<ChevronRightIcon width={7} height={12} />}
            >
              <Trans>Další kapitola</Trans>
            </Button>
          </Link>
        ) : (
          <div />
        )}
      </Grid>
    </>
  );
};

const ButtonsGrid = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  gap: ${spacings.px8};
  margin-bottom: 24px;
`;

const ButtonsContainer = styled.div`
  display: flex;
  gap: ${spacings.px16};
`;
