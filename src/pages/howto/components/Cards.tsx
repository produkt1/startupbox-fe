import React, { FC } from 'react';
import Card from '@mui/material/Card';
import styled from 'styled-components';
import Typography from '@mui/material/Typography';
import { UsageExplanation } from './commonUi';
import { minWidth } from '../../../common/styles/helpers';
import { GetContentPageFull_contentPages_contentPageSections } from '../../../apollo/cms/GraphCMS/__generatedTypes';

type Props = { section: GetContentPageFull_contentPages_contentPageSections };

export const Cards: FC<Props> = ({ section }) => {
  return (
    <>
      <GridWWrapper>
        {section.contentPageSectionItems.map((item) => {
          return (
            <StyledCard key={item.link}>
              <Image src={item.image?.url} alt="" />
              <a href={item.link} target="_blank" rel="noreferrer noopener">
                <Typography>{item.title}</Typography>
              </a>
              {item.description && (
                <UsageExplanation>{item.description}</UsageExplanation>
              )}
            </StyledCard>
          );
        })}
      </GridWWrapper>
    </>
  );
};

const GridWWrapper = styled.div`
  display: grid;
  grid-template-rows: repeat(auto-fit, minmax(230px, 1fr));

  justify-content: space-between;
  grid-gap: 44px;

  ${minWidth.mobile} {
    grid-template-columns: repeat(auto-fit, minmax(230px, 1fr));
  }

  a {
    color: black;
  }
`;

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  align-items: center;

  img {
    height: 100px;
  }

  img,
  a {
    margin-bottom: 24px;
  }
  //grid-template-rows: 100px repeat(auto-fill, minmax(58px, 1fr));
  //grid-row-gap: 16px;
  //place-items: center;

  width: 100%;
  transition: 0.3s;

  &:hover {
    transform: translate3d(0, -3px, 0);
    box-shadow: rgba(0, 0, 0, 0.1) 0px 13px 13px;
  }
`;

const Image = styled.img`
  position: relative;
  max-width: 100%;
  height: 100%;
`;
