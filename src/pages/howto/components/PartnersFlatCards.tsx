import React, { FC } from 'react';
import Typography from '@mui/material/Typography';
import { Card } from '@mui/material';
import styled from 'styled-components';
import Button from '@mui/material/Button';
import { GetContentPageFull_contentPages_contentPageSections } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { Trans } from '@lingui/macro';

type Props = { section: GetContentPageFull_contentPages_contentPageSections };

export const PartnersFlatCards: FC<Props> = ({ section }) => {
  return (
    <>
      {section.contentPageSectionItems.map((item) => (
        <div style={{ position: 'relative' }} key={item.link}>
          <SupportOption>
            <Img src={item.image.url} alt="" />

            <Typography>{item.description}</Typography>
          </SupportOption>
          <GetHelpButton
            variant="contained"
            href={item.link}
            // @ts-ignore
            target="_blank"
            rel="noreferrer nopener"
            color="secondary"
          >
            <Trans>Získat konzultaci zdarma</Trans>
          </GetHelpButton>
        </div>
      ))}
    </>
  );
};

const SupportOption = styled(Card)`
  position: relative;
  margin-bottom: 48px;
  display: grid;
  grid-template-columns: 88px 1fr;
  grid-column-gap: 16px;
  padding-bottom: 40px;
`;

const Img = styled.img`
  max-width: 100%;
`;

const GetHelpButton = styled(Button)`
  position: absolute;
  bottom: -20px;
  left: 50%;
  transform: translateX(-50%);
  padding: 10px 40px;
  width: 275px;
`;
