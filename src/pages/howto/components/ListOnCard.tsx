import { FC } from 'react';
import Card from '@mui/material/Card';
import styled, { css } from 'styled-components';
import React from 'react';
import { UsageExplanation } from './commonUi';
import { customMinMediaQuery } from '../../../common/styles/helpers';
import { GetContentPageFull_contentPages_contentPageSections } from '../../../apollo/cms/GraphCMS/__generatedTypes';

type Props = { section: GetContentPageFull_contentPages_contentPageSections };

export const ListOnCard: FC<Props> = ({ section }) => {
  return (
    <>
      <CardWrapper>
        {section.contentPageSectionItems.map((item) => {
          const explanation = item?.description;

          return (
            <RowGrid key={item.link} explanation={!!explanation}>
              <img src={item.image?.url} alt="" />

              <a href={item.link} target="_blank" rel="noreferrer no-opener">
                {item.title}
              </a>

              {explanation && (
                <UsageExplanation>{explanation}</UsageExplanation>
              )}
            </RowGrid>
          );
        })}
      </CardWrapper>
    </>
  );
};

const CardWrapper = styled(Card)`
  padding: 28px 36px;
`;

const RowGrid = styled.div<{ explanation: boolean }>`
  margin-bottom: 24px;
  padding-bottom: 24px;
  border-bottom: 1px solid #979797;
  display: grid;
  grid-template-columns: 121px 1fr;
  grid-gap: 16px;
  align-items: center;
  grid-template-rows: 1fr;

  &:last-of-type {
    margin-bottom: 0;
    padding-bottom: 0;
    border-bottom: none;
  }

  img {
    width: 100%;
  }

  a {
    color: black;
  }

  ${(props) =>
    props.explanation &&
    css`
      grid-template-rows: auto auto;

      > div {
        grid-column: span 2;
      }

      ${customMinMediaQuery(550)} {
        img {
          grid-row: span 2;
        }

        a {
          align-self: end;
        }
        div {
          align-self: start;
          grid-column: span 1;
        }
      }
    `};
`;
