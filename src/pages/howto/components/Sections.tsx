import React from 'react';
import { Section } from './Section';
import { useQuery } from '@apollo/client';
import { GRAPH_CMS_CLIENT_NAME } from '../../../common/constants';
import {
  GetContentPageFull,
  Stage,
} from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { useRouter } from 'next/dist/client/router';
import { GET_CONTENT_PAGE_FULL } from '../../../apollo/cms/GraphCMS/getContentPageFull';
import CircularProgress from '@mui/material/CircularProgress';

type Props = {
  preview: boolean;
};

export const Sections = ({ preview }: Props) => {
  const { query } = useRouter();
  const { pageId } = query;

  const { data, loading } = useQuery<GetContentPageFull>(
    GET_CONTENT_PAGE_FULL,
    {
      variables: {
        slug: pageId as string,
        stage: preview ? Stage.DRAFT : Stage.PUBLISHED,
      },
      context: {
        clientName: GRAPH_CMS_CLIENT_NAME,
      },
    }
  );

  if (loading) {
    return (
      <div>
        <CircularProgress />;
      </div>
    );
  }

  return (
    <>
      {data.contentPages[0].contentPageSections.map((section, index) => (
        <Section section={section} key={index} />
      ))}
    </>
  );
};
