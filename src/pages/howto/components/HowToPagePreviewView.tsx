import { NextPage } from 'next';
import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import {
  MainLayout,
  WidthWrapper,
} from '../../../common/containers/MainLayout/MainLayout';
import { Box, Link } from '@mui/material';
import { AppHead } from '../../../common/components/AppHead';

import { GetContentPageBasic_contentPages } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import Typography from '@mui/material/Typography';
import { RichTextCustom } from '../../../common/components/RichTextCustom';

import { VideoPlayer } from '../../../common/components/VideoPlayer';
import Collapse from '@mui/material/Collapse';

import Registration from '../../registration/Registration/Registration';
import { Trans } from '@lingui/macro';
import { LoadingScreen } from '../../../common/components/LoadingScreen';

type Props = {
  preview: boolean;
  loadingMain: boolean;
  page: GetContentPageBasic_contentPages;
};

export const HowToPagePreviewView: NextPage<Props> = ({
  page,
  loadingMain,
}) => {
  const myRef = useRef(null);

  const scrollToRef = (ref) =>
    window.scrollTo({
      top: ref.current.offsetTop - 150,
      left: 0,
      behavior: 'smooth',
    });

  const executeScroll = () => scrollToRef(myRef);

  const [originHe, setoriginHe] = useState(0);
  const [collapsedHeight, setCollapsedHeight] = useState(0);
  const measureHeigthRef = useRef(null);

  useEffect(() => {
    setoriginHe(measureHeigthRef.current?.clientHeight);
    setCollapsedHeight(measureHeigthRef.current?.clientHeight * 0.9);
  }, [measureHeigthRef]);

  return (
    <>
      <AppHead title={page?.title} />

      <MainLayout
        protectedRoute={false}
        maxWidth={1028}
        showScrollProgress
        title={`Jak na to: ${page?.title}`}
      >
        {loadingMain && <LoadingScreen logo={false} />}

        {!loadingMain && page && (
          <>
            <div className="howTo-tutorial-step-1">
              <Typography variant="h2" gutterBottom marginBottom={3}>
                {page.title}
              </Typography>

              {page.mainVideo && <VideoPlayer url={page.mainVideo.url} />}

              {!!page.contentPageSections.length && (
                <LinksWrapper>
                  {page.contentPageSections.map((section) => (
                    <Link
                      key={section.title}
                      href={'#' + section.title}
                      onClick={executeScroll}
                    >
                      <Typography variant="body2" fontWeight={600} margin={2}>
                        {section.title}
                      </Typography>
                    </Link>
                  ))}
                </LinksWrapper>
              )}
              <WidthWrapper maxWidth={732}>
                {page.mainRichText && (
                  <Box ref={measureHeigthRef} height={originHe}>
                    <Box
                      maxHeight={collapsedHeight}
                      // in={false}
                      // collapsedSize={collapsedHeight}
                      // component="div"
                    >
                      <RichTextCustom
                        content={page.mainRichText?.json}
                        references={page.mainRichText?.references}
                      />
                    </Box>
                  </Box>
                )}
              </WidthWrapper>
              <StyledBox ref={myRef}>
                <Typography
                  align="center"
                  variant="h3"
                  maxWidth={663}
                  fontWeight={700}
                  margin="0 auto"
                  paddingBottom={4}
                >
                  <Trans>
                    KOMPLETNÍ OBSAH KAPITOLY JE DOSTUPNÝ JEN PRO REGISTROVANÉ
                    UŽIVATELE STARTUPBOX
                  </Trans>
                </Typography>

                <Registration />
              </StyledBox>
            </div>
          </>
        )}
      </MainLayout>
    </>
  );
};

const LinksWrapper = styled.div`
  margin: 32px 0;
  flex-wrap: wrap;
  padding: 8px;
  display: flex;
  align-items: center;
  border-radius: 16px;
  background-color: #e7f0fc;
`;

const StyledBox = styled.div`
  position: relative;
  &:before {
    display: block;
    position: absolute;
    top: -200px;
    height: 200px;
    width: 100%;
    content: '';

    background: linear-gradient(
      180deg,
      rgba(252, 252, 252, 0) 0%,
      rgba(255, 255, 255, 0.7525603991596639) 35%,
      rgba(252, 252, 252, 1) 100%
    );
    //pointer-events: none; /* so the text is still selectable */
  }
`;
