import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import { Link, Skeleton } from '@mui/material';

import { AppHead } from '../../../common/components/AppHead';

import { GetContentPageBasic_contentPages } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import Typography from '@mui/material/Typography';
import { RichTextCustom } from '../../../common/components/RichTextCustom';
import { ExpandCustom } from '../../../common/components/ExpandCustom';

import { VideoPlayer } from '../../../common/components/VideoPlayer';

import { Sections } from './Sections';
import { Navigattion } from './Navigation';

// FIXME: ADD TYPES Somehow
type Props = {
  preview: boolean;
  loadingMain: boolean;
  page: GetContentPageBasic_contentPages;
};

export const HowToPageFullView: NextPage<Props> = ({
  page,
  loadingMain,
  preview,
}) => {
  // const router = useRouter();
  // const { projectId } = router.query;

  return (
    <>
      <AppHead title={page?.title || ''} />

      <MainLayout
        protectedRoute={false}
        maxWidth={732}
        showScrollProgress
        title={`Jak na to: ${page?.title}`}
      >
        <Navigattion preview={preview} />

        {loadingMain && (
          <>
            <Skeleton variant="rectangular" width={'100%'} height={36} />
          </>
        )}

        {page && (
          <>
            <div className="howTo-tutorial-step-1">
              <Typography variant="h2" gutterBottom marginBottom={3}>
                {page.title}
              </Typography>

              {page.mainVideo && <VideoPlayer url={page.mainVideo.url} />}

              {!!page.contentPageSections.length && (
                <LinksWrapper>
                  {page.contentPageSections.map((section) => (
                    <Link key={section.title} href={'#' + section.title}>
                      <Typography variant="body2" fontWeight={600} margin={2}>
                        {section.title}
                      </Typography>
                    </Link>
                  ))}
                </LinksWrapper>
              )}

              {page.mainRichText && (
                <ExpandCustom>
                  <RichTextCustom
                    content={page.mainRichText?.json}
                    references={page.mainRichText?.references}
                  />
                </ExpandCustom>
              )}

              <Sections preview={preview} />
            </div>
          </>
        )}
      </MainLayout>
    </>
  );
};

const LinksWrapper = styled.div`
  margin: 32px 0;
  flex-wrap: wrap;
  padding: 8px;
  display: flex;
  align-items: center;
  border-radius: 16px;
  background-color: #e7f0fc;
`;
