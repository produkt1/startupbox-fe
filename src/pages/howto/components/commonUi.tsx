import styled from 'styled-components';
import { colors } from '../../../common/styles/colors';

export const UsageExplanation = styled.div`
  padding: 12px;
  border-radius: 8px;
  background-color: ${colors.lightBlue};
`;
