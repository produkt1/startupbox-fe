import React, { FC } from 'react';
import { GetContentPageFull_contentPages_contentPageSections } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { ExpandCustom } from '../../../common/components/ExpandCustom';
import { RichTextCustom } from '../../../common/components/RichTextCustom';

type Props = { section: GetContentPageFull_contentPages_contentPageSections };

export const SecondaryRichText: FC<Props> = ({ section }) => {
  return (
    <div>
      <ExpandCustom>
        <RichTextCustom
          content={section?.richText?.json}
          references={section?.richText.references}
        />
      </ExpandCustom>
    </div>
  );
};
