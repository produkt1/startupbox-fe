import React, { FC } from 'react';
import {
  ContentPageSectionLayout,
  GetContentPageFull_contentPages_contentPageSections,
} from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { Typography } from '@mui/material';
import { ListOnCard } from './ListOnCard';
import { Cards } from './Cards';
import { PartnersFlatCards } from './PartnersFlatCards';
import { SecondaryRichText } from './SecondaryRichText';
import styled from 'styled-components';
import { minWidth } from '../../../common/styles/helpers';
import Button from '@mui/material/Button';
import { Trans } from '@lingui/macro';

type Props = { section: GetContentPageFull_contentPages_contentPageSections };

export const Section: FC<Props> = ({ section }) => {
  const componentOptions = {
    [ContentPageSectionLayout.ListOnCard]: ListOnCard,
    [ContentPageSectionLayout.Cards]: Cards,
    [ContentPageSectionLayout.PartnersFlatCards]: PartnersFlatCards,
    [ContentPageSectionLayout.SecondaryRichText]: SecondaryRichText,
  };

  const RenderComponent = componentOptions[section.itemsLayout];

  return (
    <SectionWrapper id={section?.title}>
      <SectionHeaderRow>
        {section?.title && (
          <Typography variant="h3">{section?.title}</Typography>
        )}

        {section?.contentPageSectionSponsor && (
          <SponsorLinkButton
            href={section?.contentPageSectionSponsor.link}
            // @ts-ignore
            target="_blank"
            rel="noreferrer nopener"
            variant="outlined"
            color="secondary"
          >
            <Typography variant="body2">
              <Trans>Garant sekce</Trans>
            </Typography>
            <img src={section?.contentPageSectionSponsor.logo.url} alt="" />
          </SponsorLinkButton>
        )}
      </SectionHeaderRow>

      <RenderComponent section={section} />
    </SectionWrapper>
  );
};

const SectionWrapper = styled.section`
  margin: 24px 8px;
  border-top: 1px solid #979797;
  padding-top: 40px;

  ${minWidth.mobile} {
    margin: 50px;
  }
`;

const SponsorLinkButton = styled(Button)`
  height: 52px;
  width: 190px;
  padding: 8px;
  font-size: 14px;
  font-weight: 400;
  flex-shrink: 0;
  display: grid;
  grid-template-columns: 100px 1fr;
  overflow: hidden;
  align-items: center;
  color: black;

  grid-column: 2;
  img {
    width: 100%;
  }
`;

const SectionHeaderRow = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 24px;
`;
