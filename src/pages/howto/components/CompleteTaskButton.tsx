import React, { FC, useCallback } from 'react';
import CheckIcon from '@mui/icons-material/Check';
import { t } from '@lingui/macro';
import { Button } from '@mui/material';
import { getTaskById_getTaskById } from '../../../apollo/queries/__generated__/getTaskById';
import { useMutation } from '@apollo/client';
import { UPDATE_TASK_BY_ID_MUTATION } from '../../../apollo/mutations/updateTaskById.mutation';
import {
  updateTaskById,
  updateTaskByIdVariables,
} from '../../../apollo/mutations/__generated__/updateTaskById';
import { GET_DIARY_BY_PROJECT_ID } from '../../../apollo/queries/getDiaryByProjectId.query';
import { useRouter } from 'next/router';
import { GET_ALL_PROJECTS } from '../../../apollo/queries/getAllProjects.query';
import confetti from 'canvas-confetti';

type Props = { task: getTaskById_getTaskById };

export const CompleteTaskButton: FC<Props> = (props) => {
  const { completed, id, links } = props.task;
  const router = useRouter();
  const { projectId } = router.query;

  const shootConfetti = useCallback(() => {
    confetti({
      particleCount: 250,
      spread: 60,
      origin: { x: 0.8, y: 0.3 },
    });
  }, []);

  const [updateState] = useMutation<updateTaskById, updateTaskByIdVariables>(
    UPDATE_TASK_BY_ID_MUTATION,
    {
      variables: {
        links,
        taskId: id,
        completed: !completed,
      },

      refetchQueries: [
        {
          query: GET_DIARY_BY_PROJECT_ID,
          variables: { projectId: projectId as string },
        },
        {
          query: GET_ALL_PROJECTS,
        },
      ],
    }
  );

  const handleClick = async () => {
    if (!completed) {
      shootConfetti();
    }

    await updateState();
  };

  return (
    <Button
      className="howTo-tutorial-step-3"
      variant={completed ? 'outlined' : 'contained'}
      color={completed ? 'success' : 'primary'}
      startIcon={<CheckIcon />}
      onClick={handleClick}
    >
      {completed ? t`Dokončeno` : t`Dokončit`}
    </Button>
  );
};
