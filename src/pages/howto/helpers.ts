import { ChapterNavigation } from './components/types';
import { getDiaryByProjectId } from '../../apollo/queries/__generated__/getDiaryByProjectId';

type GetPrevAndNextPageIds = (
  diaryData: getDiaryByProjectId,
  pageId: string
) => ChapterNavigation;

export const getPrevAndNextPageIds: GetPrevAndNextPageIds = (
  diaryData,
  pageId
) => {
  if (diaryData) {
    const components = diaryData.getDiaryByProjectId.phases.reduce(
      (acc, phase) => [...acc, ...phase.components],
      []
    );
    const tasks = components.reduce(
      (acc, component) => [...acc, ...component.tasks],
      []
    );

    const tasksSimplified = tasks.map((task) => ({
      taskId: task.id,
      pageId: task.howToPage,
    }));

    const index = tasksSimplified.findIndex((item) => item.pageId === pageId);

    const previous = tasksSimplified[index - 1];
    const next = tasksSimplified[index + 1];

    return { previous, next };
  } else {
    return null;
  }
};
