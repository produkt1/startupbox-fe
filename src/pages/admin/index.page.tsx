import { NextPage } from 'next';

import * as React from 'react';

import { Chart } from 'react-google-charts';

import { AdminPageWrapper } from './components/AdminPageWrapper';

import { Box, Typography } from '@mui/material';
import { useQuery } from '@apollo/client';
import { GET_ADMIN_STATISTICS } from './gql/getAdminStatistics.query';
import {
  GetAdminStatisticsQuery,
  GetAdminStatisticsQueryVariables,
} from './gql/__generatedNew__/getAdminStatistics.query.types';
import { useWhiteLabelContext } from '../../common/containers/WhitelabeProvider';
import { isDev, isStaging } from '../../common/utils/helpers';

import { getOnboardingQuestions } from '../onboarding/components/stepsContent';
import { orderByMonth, translateMonthToCzech } from './utils';

const ProjectsPage: NextPage = () => {
  const { language, domainName } = useWhiteLabelContext();

  const { data } = useQuery<
    GetAdminStatisticsQuery,
    GetAdminStatisticsQueryVariables
  >(GET_ADMIN_STATISTICS, {
    variables: { subdomain: isDev || isStaging ? 'default' : domainName },
  });

  //this is filtering older versions of questions that are still stored. BE stores the questions string
  const onboardingSteps = getOnboardingQuestions();
  const acceptableQuestions = onboardingSteps.map((step) => step.question);

  const transformForGraph = (array?: { value: string; count: number }[]) =>
    array?.map((item) => [item.value, item.count]) ?? [];

  const adjustCzechLabels = (items: (string | number)[][]) => {
    const trasformer = {
      "'no idea'": "'no idea' - Wannabe, začínající podnikatel",
      '1A': '1A - Má nápad, ale nikam dál to neposunul',
      '1B': '1B - Má nápad, provedl první kroky k jeho definování',
      '2A': '2A - Zatím nemá zákazníka, začíná s fází ověřování (validace)',
      '2B': '2B - Zatím nemá zákazníka, ale má POC (proof of concept)',
      '3A': '3A - Zaměřuje se na produkt, ale bez zákazníků a validace',
      '3B': '3B - Má první zákazníky nebo uživatele s MVP',
      '4': '4 - Více rozvinuté startupy s určitými příjmy a stabilním růstem',
    };

    const transformedItems = items.map((item) => [
      trasformer[item[0]],
      item[1],
    ]);

    return transformedItems.sort((a, b) => {
      const orderA = trasformer[a[0].split(' - ')[0]];

      const orderB = trasformer[b[0].split(' - ')[0]];

      return orderA.localeCompare(orderB);
    });
  };

  const onboardingResults = adjustCzechLabels(
    transformForGraph(data?.getAdminStatistics.onboardingResults)
  );

  const selectedLang =
    language === 'cs' ? 'CZ' : language === 'sk' ? 'SK' : 'other';

  const registrationRegion = data?.getAdminStatistics.registrationRegions.find(
    (item) => item.country === selectedLang
  );

  const regs: [string, number][] =
    data?.getAdminStatistics?.registrationsPerMonth.reduce((acc, current) => {
      return [
        ...acc,
        ...orderByMonth(current.months).map((item) => [
          `${translateMonthToCzech(item.value)} ${current.year}`,
          item.count,
        ]),
      ];
    }, []);

  const activeUsers: [string, number][] =
    data?.getAdminStatistics?.activeUsers.reduce((acc, current) => {
      return [
        ...acc,
        ...orderByMonth(current.months).map((item) => [
          `${translateMonthToCzech(item.value)} ${current.year}`,
          item.count,
        ]),
      ];
    }, []);

  const onboardingQuestions =
    data?.getAdminStatistics?.onboardingAnswers.filter((item) =>
      acceptableQuestions.includes(item.question)
    );

  return (
    <AdminPageWrapper>
      <Typography variant="h2">
        Celkový počet uživatelů:{' '}
        {data?.getAdminStatistics.totalRegistedUsersCount}
      </Typography>

      {regs && (
        <Chart
          options={{
            title: 'Nárust registrovaných uživatelů v čase ',
            legend: {
              position: 'right',
              alignment: 'top',
            },
            chartArea: {
              left: 0,
              right: 140,
            },
          }}
          chartType="ColumnChart"
          width="100%"
          height="400px"
          data={[['měsíc', 'počet'], ...regs]}
        />
      )}

      {activeUsers && (
        <Chart
          options={{
            title: 'Počet aktivních uživatelů za měsíc',
            legend: {
              position: 'right',
              alignment: 'top',
            },
            chartArea: {
              left: 0,
              right: 140,
            },
          }}
          chartType="ColumnChart"
          width="100%"
          height="400px"
          data={[['měsíc', 'počet'], ...activeUsers]}
        />
      )}

      <Chart
        chartType="PieChart"
        data={[['result', 'count'], ...onboardingResults]}
        options={{
          title:
            'Výsledky vstupního (onboarding) dotazníku do aplikace StartupBox',
          legend: {
            position: 'right',
            alignment: 'top',
          },
          chartArea: {
            left: 0,
            right: 140,
          },
        }}
        width={'100%'}
        height={'400px'}
      />

      <Chart
        options={{
          title: 'Rozložení registrovaných uživatelů dle regionů ČR',
          legend: {
            position: 'right',
            alignment: 'top',
          },
          chartArea: {
            left: 0,
            right: 140,
          },
        }}
        chartType="PieChart"
        data={[
          ['reagion', 'count'],
          ...transformForGraph(registrationRegion?.regions),
        ]}
        width={'100%'}
        height={'400px'}
      />

      {onboardingQuestions?.map((item, index) => (
        <Box marginTop={5} key={item.question}>
          <Chart
            options={{
              title: `Vstupní otázka č. ${index + 1}: ${item.question}`,
              legend: {
                position: 'right',
                alignment: 'top',
              },
              chartArea: {
                left: 0,
                right: 140,
              },
            }}
            chartType="PieChart"
            data={[['reagion', 'count'], ...transformForGraph(item.answers)]}
            width={'100%'}
            height={'400px'}
          />
        </Box>
      ))}
    </AdminPageWrapper>
  );
};

export default ProjectsPage;
