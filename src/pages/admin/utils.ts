export function translateMonthToCzech(englishMonth) {
  const monthNumber = new Date(`${englishMonth} 1, 2000`).getMonth();

  if (isNaN(monthNumber)) {
    // Handle the case where the input is not a valid month
    return 'Invalid month';
  }

  const czechTranslation = new Intl.DateTimeFormat('cs-CZ', {
    month: 'long',
  }).format(new Date(2000, monthNumber));

  return czechTranslation;
}

export function orderByMonth(
  months: Array<{ __typename?: 'Count'; value: string; count: number }>
) {
  const monthOrder = {
    January: 1,
    February: 2,
    March: 3,
    April: 4,
    May: 5,
    June: 6,
    July: 7,
    August: 8,
    September: 9,
    October: 10,
    November: 11,
    December: 12,
  };

  return [...months].sort((a, b) => monthOrder[a.value] - monthOrder[b.value]);
}
