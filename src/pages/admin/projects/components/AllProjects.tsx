import styled, { css } from 'styled-components';
import Link from 'next/link';
import { useQuery } from '@apollo/client';
import { colors } from '../../../../common/styles/colors';
import { Box, Grid, Pagination, Typography } from '@mui/material';
import { LoadingScreen } from '../../../../common/components/LoadingScreen';
import {
  getAllProjectsAsAdmin,
  getAllProjectsAsAdminVariables,
} from '../gql/__generated__/getAllProjectsAsAdmin';
import { GET_ALL_PROJECTS_AS_ADMIN } from '../gql/getAllProjectsAsAdmin.query';
import { ProfilePicture } from '../../../../common/components/profile/ProfilePicture';

import React, { useMemo, useState } from 'react';

const ITEMS_PER_PAGE = 20;

export const AllProjects = () => {
  const [pagination, setPagination] = useState({
    skip: 0,
    take: ITEMS_PER_PAGE,
  });

  const { data, loading } = useQuery<
    getAllProjectsAsAdmin,
    getAllProjectsAsAdminVariables
  >(GET_ALL_PROJECTS_AS_ADMIN, {
    variables: {
      ...pagination,
    },
  });

  const labels = [
    '#',
    'Project name',
    'owner',
    'onboarding result',
    'last updated',
  ];

  const pagesCount = useMemo(
    () => Math.ceil(data?.getAllProjectsAsAdmin.count / ITEMS_PER_PAGE),
    [data?.getAllProjectsAsAdmin.count]
  );

  const handlePaginationChange = (e, page) => {
    setPagination({
      skip: (page - 1) * ITEMS_PER_PAGE,
      take: ITEMS_PER_PAGE,
    });
  };

  return (
    <>
      <Pagination
        color="primary"
        showFirstButton
        showLastButton
        count={pagesCount}
        onChange={handlePaginationChange}
      />

      {loading && <LoadingScreen />}

      {!loading && (
        <RowWrapper>
          {labels.map((label) => (
            <Box key={label} marginY={3}>
              <b>{label}</b>
            </Box>
          ))}
        </RowWrapper>
      )}

      {data?.getAllProjectsAsAdmin.projects?.map(
        ({ name, id, diary, user, archived }, index) => (
          <RowWrapper isArchived={archived} key={index}>
            <div>{index}</div>

            <Link
              href={`/project/[id]`}
              as={`/project/${id}`}
              key={id}
              legacyBehavior
            >
              <StyledAnchor>
                <b>{name}</b>
              </StyledAnchor>
            </Link>

            <Grid container alignItems={'center'}>
              <ProfilePicture
                profilePictureUrl={user.profilePicture}
                size={24}
              />{' '}
              <Box marginLeft={1}>
                {user.firstName} {user.lastName}
              </Box>
            </Grid>
            <div>{user.onboardingResult}</div>
            <div>
              {archived
                ? 'archived project'
                : new Date(diary[0].updatedAt).toLocaleString()}
            </div>
          </RowWrapper>
        )
      )}
    </>
  );
};

const RowWrapper = styled.div<{ isArchived?: boolean }>`
  margin-bottom: 4px;
  display: grid;
  grid-gap: 8px;
  align-items: center;
  grid-template-columns: 0.5fr 2fr 2fr 1fr 1.5fr;
  ${({ isArchived }) =>
    isArchived &&
    css`
      background-color: darkgray;
    `}
`;

const StyledAnchor = styled.a`
  color: ${colors.brand};
  cursor: pointer;
  display: block;

  text-decoration: none;
`;
