import { NextPage } from 'next';

import * as React from 'react';
import { AllProjects } from './components/AllProjects';

import { AdminPageWrapper } from '../components/AdminPageWrapper';

const Admin: NextPage = () => {
  return (
    <AdminPageWrapper>
      <AllProjects />
    </AdminPageWrapper>
  );
};

export default Admin;
