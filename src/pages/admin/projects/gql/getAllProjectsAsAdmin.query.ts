import { gql } from '@apollo/client';

export const GET_ALL_PROJECTS_AS_ADMIN = gql`
  query getAllProjectsAsAdmin($skip: Float!, $take: Float!) {
    getAllProjectsAsAdmin(skip: $skip, take: $take) {
      count
      projects {
        id
        name
        phase
        archived
        user {
          createdAt
          firstName
          lastName
          onboardingResult
          profilePicture
          email

          onboardingData {
            question
            answer
          }

          profile {
            region
            skills
          }
        }

        diary {
          updatedAt
        }
      }
    }
  }
`;
