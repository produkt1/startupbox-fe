import * as Types from '../../../../../apollo/types';

export type GetAllProjectsAsAdminQueryVariables = Types.Exact<{
  skip: Types.Scalars['Float']['input'];
  take: Types.Scalars['Float']['input'];
}>;

export type GetAllProjectsAsAdminQuery = {
  __typename?: 'Query';
  getAllProjectsAsAdmin: {
    __typename?: 'AdminProjects';
    count: number;
    projects: Array<{
      __typename?: 'Project';
      id?: string | null;
      name: string;
      phase?: Types.ProjectPhase | null;
      archived: boolean;
      user: {
        __typename?: 'User';
        createdAt?: any | null;
        firstName?: string | null;
        lastName?: string | null;
        onboardingResult?: string | null;
        profilePicture?: string | null;
        email?: string | null;
        onboardingData?: Array<{
          __typename?: 'OnboardingType';
          question: string;
          answer: Array<string>;
        }> | null;
        profile?: {
          __typename?: 'Profile';
          region?: string | null;
          skills?: Array<string> | null;
        } | null;
      };
      diary: Array<{ __typename?: 'Diary'; updatedAt?: any | null }>;
    }>;
  };
};
