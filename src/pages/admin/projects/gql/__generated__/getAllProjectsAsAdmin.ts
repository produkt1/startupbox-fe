/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectPhase } from '../../../../../../__generated__/globalTypes';

// ====================================================
// GraphQL query operation: getAllProjectsAsAdmin
// ====================================================

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user_onboardingData {
  __typename: 'OnboardingType';
  question: string;
  answer: string[];
}

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user_profile {
  __typename: 'Profile';
  region: string | null;
  skills: string[] | null;
}

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user {
  __typename: 'User';
  createdAt: any | null;
  firstName: string | null;
  lastName: string | null;
  onboardingResult: string | null;
  profilePicture: string | null;
  email: string | null;
  onboardingData:
    | getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user_onboardingData[]
    | null;
  profile: getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user_profile | null;
}

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_diary {
  __typename: 'Diary';
  updatedAt: any | null;
}

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects {
  __typename: 'Project';
  id: string | null;
  name: string;
  phase: ProjectPhase | null;
  archived: boolean;
  user: getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_user;
  diary: getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects_diary[];
}

export interface getAllProjectsAsAdmin_getAllProjectsAsAdmin {
  __typename: 'AdminProjects';
  count: number;
  projects: getAllProjectsAsAdmin_getAllProjectsAsAdmin_projects[];
}

export interface getAllProjectsAsAdmin {
  getAllProjectsAsAdmin: getAllProjectsAsAdmin_getAllProjectsAsAdmin;
}

export interface getAllProjectsAsAdminVariables {
  skip: number;
  take: number;
}
