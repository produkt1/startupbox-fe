import * as Types from '../../../../apollo/types';

export type GetAdminStatisticsQueryVariables = Types.Exact<{
  subdomain: Types.Scalars['String']['input'];
}>;

export type GetAdminStatisticsQuery = {
  __typename?: 'Query';
  getAdminStatistics: {
    __typename?: 'AdminStatistics';
    totalRegistedUsersCount: number;
    activeUsers: Array<{
      __typename?: 'ActiveUsers';
      year: number;
      months: Array<{ __typename?: 'Count'; count: number; value: string }>;
    }>;
    onboardingResults: Array<{
      __typename?: 'Count';
      count: number;
      value: string;
    }>;
    registrationRegions: Array<{
      __typename?: 'RegistrationRegions';
      country: string;
      regions: Array<{ __typename?: 'Count'; count: number; value: string }>;
    }>;
    registrationsPerMonth: Array<{
      __typename?: 'RegistrationsPerMonth';
      year: number;
      months: Array<{ __typename?: 'Count'; value: string; count: number }>;
    }>;
    onboardingAnswers: Array<{
      __typename?: 'OnboardingAnswers';
      question: string;
      answers: Array<{ __typename?: 'Count'; value: string; count: number }>;
    }>;
  };
};
