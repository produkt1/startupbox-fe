import { gql } from '@apollo/client';

export const GET_ADMIN_STATISTICS = gql`
  query GetAdminStatistics($subdomain: String!) {
    getAdminStatistics(subdomain: $subdomain) {
      totalRegistedUsersCount

      activeUsers {
        year
        months {
          count
          value
        }
      }

      onboardingResults {
        count
        value
      }

      registrationRegions {
        country
        regions {
          count
          value
        }
      }

      registrationsPerMonth {
        months {
          value
          count
        }
        year
      }

      onboardingAnswers {
        answers {
          value
          count
        }
        question
      }
    }
  }
`;
