import { useUserData } from '../../../common/hooks/useUserData';
import { useRouter } from 'next/router';
import { ReactNode, useEffect } from 'react';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import Link from 'next/link';

import { LoadingScreen } from '../../../common/components/LoadingScreen';
import * as React from 'react';

import { Typography } from '@mui/material';
import Grid from '@mui/material/Grid';

type Props = {
  children: ReactNode;
};

const adminRoutes = [
  {
    title: 'Přehled',
    href: '/admin',
  },
  {
    title: 'Všechny projekty',
    href: '/admin/projects',
  },
];
export const AdminPageWrapper = ({ children }: Props) => {
  const { isAdmin, user } = useUserData();
  const router = useRouter();
  const title = adminRoutes.find(
    (route) => route.href === router.pathname
  ).title;

  useEffect(() => {
    if (user && !isAdmin) {
      router.push('/404');
    }
  }, []);

  if (isAdmin) {
    return (
      <MainLayout title="Admin">
        <Grid container spacing={2} marginBottom={4}>
          {adminRoutes.map((route) => (
            <Grid item key={route.href}>
              <Link href={route.href}>{route.title}</Link>
            </Grid>
          ))}
        </Grid>

        <Typography variant="h1" gutterBottom>
          {title}
        </Typography>

        {children}
      </MainLayout>
    );
  }

  return <LoadingScreen />;
};
