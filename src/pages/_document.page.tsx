import Document, { Head, Html, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

import { GA_TRACKING_ID } from '../common/constants';
import { isProduction } from '../common/utils/helpers';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) =>
            sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <link
            href="https://fonts.googleapis.com/css2?family=Atkinson+Hyperlegible+Next:ital,wght@0,200..800;1,200..800&display=swap"
            rel="stylesheet"
          />
          {isProduction && (
            <>
              {/*Google Tag Manager  new*/}
              <script
                dangerouslySetInnerHTML={{
                  __html: `
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-WS227JW');
            `,
                }}
              />

              {/*End Google Tag Manager new*/}

              {/*TODO: remove old GA*/}

              {/*  /!*  Google Tag Manager *!/*/}
              {/*  <script*/}
              {/*    dangerouslySetInnerHTML={{*/}
              {/*      __html: `*/}
              {/*  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({‘gtm.start’:*/}
              {/*  new Date().getTime(),event:‘gtm.js’});var f=d.getElementsByTagName(s)[0],*/}
              {/*  j=d.createElement(s),dl=l!=‘dataLayer’?‘&l=‘+l:‘’;j.async=true;j.src=*/}
              {/*  ’https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);*/}
              {/*})(window,document,‘script’,‘dataLayer’,‘GTM-WGCFBGJ’);*/}
              {/*`,*/}
              {/*    }}*/}
              {/*  />*/}
              {/*  /!*End Google Tag Manager*!/*/}

              {/* Global Site Tag (gtag.js) - Google Analytics */}
              {/*    <script*/}
              {/*      async*/}
              {/*      src={`https://www.googletagmanage~r.com/gtag/js?id=${GA_TRACKING_ID}`}*/}
              {/*    />*/}
              {/*    <script*/}
              {/*      dangerouslySetInnerHTML={{*/}
              {/*        __html: `*/}
              {/*  window.dataLayer = window.dataLayer || [];*/}
              {/*  function gtag(){dataLayer.push(arguments);}*/}
              {/*  gtag('js', new Date());*/}
              {/*  gtag('config', '${GA_TRACKING_ID}', {*/}
              {/*    page_path: window.location.pathname,*/}
              {/*  });*/}
              {/*`,*/}
              {/*      }}*/}
              {/*    />*/}
            </>
          )}
        </Head>

        <body>
          {/*TODO: remove old GA*/}
          {/*/!*Google Tag Manager (noscript)*!/*/}
          {/*<noscript*/}
          {/*  dangerouslySetInnerHTML={{*/}
          {/*    __html: `*/}
          {/*  <iframe src=“https://www.googletagmanager.com/ns.html?id=GTM-WGCFBGJ”*/}
          {/*    height=“0" width=“0” style=“display:none;visibility:hidden”></iframe>*/}
          {/*  `,*/}
          {/*  }}*/}
          {/*/>*/}

          {/*/!*End Google Tag Manager (noscript)*!/*/}

          {/*Google Tag Manager (noscript) new*/}
          <noscript
            dangerouslySetInnerHTML={{
              __html: `
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS227JW"
              height="0" width="0" style="display:none;visibility:hidden"></iframe>
            `,
            }}
          />

          {/*End Google Tag Manager (noscript) new */}

          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
