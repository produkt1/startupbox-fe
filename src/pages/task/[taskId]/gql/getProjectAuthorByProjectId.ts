import { gql } from '@apollo/client';

export const GET_PROJECT_AUTHOR_BY_ID = gql`
  query getProjectAuthorByProjectId($id: String!) {
    getProjectById(projectId: $id) {
      id
      user {
        id
        profilePicture
        firstName
        lastName
      }
    }
  }
`;
