/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getProjectAuthorByProjectId
// ====================================================

export interface getProjectAuthorByProjectId_getProjectById_user {
  __typename: 'User';
  id: string | null;
  profilePicture: string | null;
  firstName: string | null;
  lastName: string | null;
}

export interface getProjectAuthorByProjectId_getProjectById {
  __typename: 'Project';
  id: string | null;
  user: getProjectAuthorByProjectId_getProjectById_user;
}

export interface getProjectAuthorByProjectId {
  getProjectById: getProjectAuthorByProjectId_getProjectById;
}

export interface getProjectAuthorByProjectIdVariables {
  id: string;
}
