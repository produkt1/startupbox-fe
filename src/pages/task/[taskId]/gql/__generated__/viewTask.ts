/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: viewTask
// ====================================================

export interface viewTask_viewTask {
  __typename: 'Task';
  id: string | null;
  completed: boolean;
  hasNewUpdate: boolean;
}

export interface viewTask {
  viewTask: viewTask_viewTask;
}

export interface viewTaskVariables {
  taskId: string;
}
