import { gql } from '@apollo/client';

export const VIEW_TASK_QUERY = gql`
  query viewTask($taskId: String!) {
    viewTask(taskId: $taskId) {
      id
      completed
      hasNewUpdate
    }
  }
`;
