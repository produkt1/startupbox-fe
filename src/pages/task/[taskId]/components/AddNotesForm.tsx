import React, { FC } from 'react';
import { TextAreaInput } from '../../../../common/components/inputs/TextAreaInput';
import { TaskLinks } from './TaskLinks';
import Button from '@mui/material/Button';
import { Formik } from 'formik';
import { TaskFormValues } from '../index.page';
import { isAdmin } from '../../../../common/utils/helpers';
import {
  getTaskById_getTaskById,
  getTaskById_getTaskById_comments,
} from '../../../../apollo/queries/__generated__/getTaskById';
import { getProjectAuthorByProjectId_getProjectById_user } from '../gql/__generated__/getProjectAuthorByProjectId';
import { MutationFunction } from '@apollo/client/react/types/types';
import { useRouter } from 'next/router';
import { Trans } from '@lingui/macro';
import {
  updateTaskById,
  updateTaskByIdVariables,
} from '../../../../apollo/mutations/__generated__/updateTaskById';

type Props = {
  sendNewTaskContent: MutationFunction<updateTaskById, updateTaskByIdVariables>;
  author: getProjectAuthorByProjectId_getProjectById_user;
  task: getTaskById_getTaskById;
};

export const AddNotesForm: FC<Props> = (props) => {
  const { sendNewTaskContent, task } = props;
  const router = useRouter();

  const { taskId } = router.query;

  const { comments, links, completed } = task;

  const userContent: getTaskById_getTaskById_comments[] = comments?.filter(
    (comment) => !isAdmin(comment.user)
  );

  const onSubmitHandler = async (data, { setSubmitting }) => {
    await sendNewTaskContent({ variables: data });
    setSubmitting(false);
  };

  const initialValues: TaskFormValues = {
    taskId: taskId as string,
    text: userContent[0]?.text || '',
    links,
    completed,
  };

  const label = task?.question || 'Okomentujte jak jste došli k danému výstupu';

  return (
    <Formik onSubmit={onSubmitHandler} initialValues={initialValues}>
      {(formikProps) => {
        return (
          <form onSubmit={formikProps.handleSubmit} autoComplete="off">
            <TextAreaInput
              handleChange={formikProps.handleChange}
              rowsMin={5}
              name="text"
              placeholder="Váš text..."
              value={formikProps.values.text}
              label={label}
            />

            <TaskLinks formikProps={formikProps} />

            <Button
              fullWidth
              color="success"
              variant="contained"
              disabled={formikProps.isSubmitting}
              onClick={formikProps.submitForm}
            >
              <Trans>Uložit změny</Trans>
            </Button>
          </form>
        );
      }}
    </Formik>
  );
};
