import { FC } from 'react';
import { getTaskById_getTaskById_comments } from '../../../../apollo/queries/__generated__/getTaskById';
import { Card, Typography } from '@mui/material';
import styled from 'styled-components';
import { ProfilePicture } from '../../../../common/components/profile/ProfilePicture';
import { Trans } from '@lingui/macro';

type Props = {
  adminComments: getTaskById_getTaskById_comments[];
};

export const AdminComment: FC<Props> = ({ adminComments }) => {
  const getTime = (t) => {
    const updatedAt = new Date(Date.parse(t));
    return `${updatedAt.getDate()}. ${updatedAt.getMonth()}. ${updatedAt.getFullYear()} `;
  };

  if (adminComments.length === 0) return <></>;

  return (
    <Wrapper>
      <Typography>
        <Trans>Feedback:</Trans>
      </Typography>

      {adminComments.map((comment, key) => (
        <Card key={key}>
          <GridC>
            <ProfilePicture
              size={40}
              profilePictureUrl={comment.user.profilePicture}
              className="span-two"
            />

            <Typography>{getTime(comment.updatedAt)}</Typography>

            <strong>
              {`${comment.user.firstName} ${comment.user.lastName}: `}
            </strong>
          </GridC>

          <Typography>{comment.text}</Typography>
        </Card>
      ))}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin-top: 24px;
  display: grid;
  grid-row-gap: 16px;
`;

const GridC = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  grid-template-rows: 1fr 1fr;
  grid-column-gap: 8px;

  .span-two {
    grid-row: span 2;
  }
`;
