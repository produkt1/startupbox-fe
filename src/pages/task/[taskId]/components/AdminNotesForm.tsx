import React, { FC } from 'react';
import Card from '@mui/material/Card';
import { AuthorHeader } from './AuthorHeader';
import { TextAreaInput } from '../../../../common/components/inputs/TextAreaInput';
import Button from '@mui/material/Button';
import { Trans } from '@lingui/macro';
import {
  getTaskById_getTaskById,
  getTaskById_getTaskById_comments,
} from '../../../../apollo/queries/__generated__/getTaskById';
import { AdminComment } from './AdminComment';
import { isAdmin } from '../../../../common/utils/helpers';
import { Form, Formik } from 'formik';
import { MutationFunction } from '@apollo/client/react/types/types';

import { Typography } from '@mui/material';
import styled from 'styled-components';
import {
  updateTaskById,
  updateTaskByIdVariables,
} from '../../../../apollo/mutations/__generated__/updateTaskById';
import { FormikSubmitHandler } from '../../../../common/types/commonTypes';

type Props = {
  task: getTaskById_getTaskById;
  sendNewTaskContent: MutationFunction<updateTaskById, updateTaskByIdVariables>;
};

export const AdminNotesForm: FC<Props> = ({ task, sendNewTaskContent }) => {
  const { comments, links } = task;

  const adminComments: getTaskById_getTaskById_comments[] = comments?.filter(
    (comment) => isAdmin(comment.user)
  );
  const userContent: getTaskById_getTaskById_comments[] = comments?.filter(
    (comment) => !isAdmin(comment.user)
  );

  const handleSubmit: FormikSubmitHandler<{ text: string }> = async (
    values
  ) => {
    await sendNewTaskContent({
      variables: {
        completed: task.completed,
        taskId: task.id,
        text: values.text,
      },
    });
  };

  const initialValues = { text: '' };

  return (
    <>
      {!userContent.length && <div>ještě tu nic není</div>}

      {userContent.length && (
        <Card>
          <AuthorHeader author={userContent[0]?.user} />
          <Content>{userContent[0].text}</Content>
        </Card>
      )}

      {links?.map((link) => (
        //TODO: check better way
        <a
          key={link}
          //FIXME: inline styles
          style={{ display: 'block', marginTop: '1rem' }}
          href={link.startsWith('https') ? link : `https://${link}`}
          target="_blank"
          rel="noreferrer noopener"
        >
          {link}
        </a>
      ))}

      <AdminComment adminComments={adminComments} />

      <Formik onSubmit={handleSubmit} initialValues={initialValues}>
        {({ isSubmitting, values, handleChange }) => (
          <Form>
            <TextAreaInput
              handleChange={handleChange}
              rowsMin={5}
              name="text"
              placeholder="Váš text..."
              value={values.text}
              label="Komentář pro autora"
            />

            <Button
              fullWidth
              color="success"
              variant="contained"
              disabled={isSubmitting}
              type="submit"
            >
              <Trans>Uložit změny</Trans>
            </Button>
          </Form>
        )}
      </Formik>
    </>
  );
};

const Content = styled(Typography)`
  margin-top: 16px;
  white-space: break-spaces;
`;
