import { FC } from 'react';
import { Grid } from '@mui/material';
import { ProfilePicture } from '../../../../common/components/profile/ProfilePicture';
import styled from 'styled-components';
import { getTaskById_getTaskById_comments_user } from '../../../../apollo/queries/__generated__/getTaskById';

type Props = {
  author: getTaskById_getTaskById_comments_user;
};

export const AuthorHeader: FC<Props> = (props) => {
  const { author } = props;

  return (
    <Grid container direction="row" alignItems="center">
      <ProfilePicture profilePictureUrl={author?.profilePicture} size={40} />

      <AuthorDiv>
        <p>Autor: {`${author?.firstName} ${author?.lastName} `}</p>
        {/*<Button*/}
        {/*  color="primary"*/}
        {/*  startIcon={<AddCircleOutlineIcon />}*/}
        {/*  onClick={() => ''}*/}
        {/*>*/}
        {/*  Přidat autora*/}
        {/*</Button>*/}
      </AuthorDiv>
    </Grid>
  );
};

const AuthorDiv = styled.div`
  margin: 0rem 2rem;
`;
