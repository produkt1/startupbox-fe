import React, { FC } from 'react';
import { FieldArray, FormikProps } from 'formik';
import { TextInput } from '../../../../common/components/inputs/TextInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import ClearIcon from '@mui/icons-material/Clear';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

import styled from 'styled-components';
import Button from '@mui/material/Button';
import {
  InputLabelCustom,
  InputWrapper,
} from '../../../../common/components/UI';
import { TaskFormValues } from '../index.page';

type Props = {
  formikProps: FormikProps<TaskFormValues>;
};

export const TaskLinks: FC<Props> = (props) => {
  const { formikProps } = props;

  return (
    <InputWrapper>
      <InputLabelCustom>Přidejte odkazy k materiálům</InputLabelCustom>

      <FieldArray
        name="links"
        render={(arrayHelpers) => (
          <div>
            {formikProps.values.links?.map((link, index) => (
              <div key={index}>
                <TextInput
                  name={`links.${index}`}
                  value={link}
                  placeholder="Např. Sketch, nebo link na Google Drive"
                  handleBlur={formikProps.handleBlur}
                  handleChange={formikProps.handleChange}
                  InputProps={{
                    endAdornment:
                      index !== 0 ? (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            <ClearIcon />
                          </IconButton>
                        </InputAdornment>
                      ) : (
                        <div />
                      ),
                  }}
                />
              </div>
            ))}
            <div>
              <AddButton
                color="primary"
                startIcon={<AddCircleOutlineIcon />}
                onClick={() => arrayHelpers.push('')}
              >
                Přidat další odkaz
              </AddButton>
            </div>
          </div>
        )}
      />
    </InputWrapper>
  );
};

const AddButton = styled(Button)`
  margin-bottom: 2rem;
`;
