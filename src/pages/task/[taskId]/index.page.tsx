import React, { useEffect } from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useQuery, useMutation, useLazyQuery } from '@apollo/client';
import { UPDATE_TASK_BY_ID_MUTATION } from '../../../apollo/mutations/updateTaskById.mutation';
import { GET_TASK_BY_ID } from '../../../apollo/queries/getTaskById.query';
import {
  getTaskById,
  getTaskByIdVariables,
} from '../../../apollo/queries/__generated__/getTaskById';
import { GET_DIARY_BY_PROJECT_ID } from '../../../apollo/queries/getDiaryByProjectId.query';
import { CompletionTaskPageView } from './CompletionTaskPageView';
import { LoadingScreen } from '../../../common/components/LoadingScreen';
import { useUserData } from '../../../common/hooks/useUserData';
import { GET_PROJECT_AUTHOR_BY_ID } from './gql/getProjectAuthorByProjectId';
import {
  getProjectAuthorByProjectId,
  getProjectAuthorByProjectIdVariables,
} from './gql/__generated__/getProjectAuthorByProjectId';
import { VIEW_TASK_QUERY } from './gql/viewTask';
import { viewTask, viewTaskVariables } from './gql/__generated__/viewTask';
import { MainLayout } from '../../../common/containers/MainLayout/MainLayout';
import {
  updateTaskById,
  updateTaskByIdVariables,
} from '../../../apollo/mutations/__generated__/updateTaskById';

export type TaskFormValues = {
  taskId: string;
  text: string;
  links: string[];
  completed: boolean;
};

const CompletionTaskPage: NextPage = () => {
  const router = useRouter();
  const { user } = useUserData();
  const { projectId, taskId } = router.query;

  const { data: dataAuthor, loading: loadingAuthor } = useQuery<
    getProjectAuthorByProjectId,
    getProjectAuthorByProjectIdVariables
  >(GET_PROJECT_AUTHOR_BY_ID, {
    variables: { id: projectId as string },
  });

  const { data, loading } = useQuery<getTaskById, getTaskByIdVariables>(
    GET_TASK_BY_ID,
    {
      variables: { taskId: taskId as string },
    }
  );

  const [sendNewTaskContent] = useMutation<
    updateTaskById,
    updateTaskByIdVariables
  >(UPDATE_TASK_BY_ID_MUTATION, {
    refetchQueries: [
      { query: GET_TASK_BY_ID, variables: { taskId } },
      {
        query: GET_DIARY_BY_PROJECT_ID,
        variables: { id: projectId },
      },
    ],
    awaitRefetchQueries: true,
    onCompleted: () => {
      // setConfirmationModalOpen(true);
    },
  });

  const [viewTask, { loading: loadingViewTask }] = useLazyQuery<
    viewTask,
    viewTaskVariables
  >(VIEW_TASK_QUERY, {
    variables: { taskId: taskId as string },
  });

  useEffect(() => {
    if (
      data?.getTaskById.hasNewUpdate &&
      data?.getTaskById.comments.length > 0 &&
      data?.getTaskById.comments[0].user.id !== user.id
    ) {
      viewTask();
    }
  }, [data]);

  if (loading || loadingViewTask || loadingAuthor) return <LoadingScreen />;

  return (
    <MainLayout maxWidth={732} title={`Check: ${data?.getTaskById.title}`}>
      <CompletionTaskPageView
        sendNewTaskContent={sendNewTaskContent}
        task={data?.getTaskById}
        projectId={projectId as string}
        author={dataAuthor.getProjectById.user}
      />
    </MainLayout>
  );
};

export default CompletionTaskPage;
