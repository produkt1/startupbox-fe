import React, { FC } from 'react';
import Button from '@mui/material/Button';
import {
  getTaskById_getTaskById,
  getTaskById_getTaskById_comments,
} from '../../../apollo/queries/__generated__/getTaskById';
import { Typography } from '@mui/material';
import Link from 'next/link';
import Grid from '@mui/material/Grid';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import styled from 'styled-components';
import { MutationFunction } from '@apollo/client/react/types/types';
import { getProjectAuthorByProjectId_getProjectById_user } from './gql/__generated__/getProjectAuthorByProjectId';
import { AddNotesForm } from './components/AddNotesForm';
import { useUserData } from '../../../common/hooks/useUserData';
import { AdminNotesForm } from './components/AdminNotesForm';
import { AdminComment } from './components/AdminComment';
import { isAdmin } from '../../../common/utils/helpers';
import { Routes } from '../../../common/enums/routes';
import { Trans } from '@lingui/macro';
import {
  updateTaskById,
  updateTaskByIdVariables,
} from '../../../apollo/mutations/__generated__/updateTaskById';

type Props = {
  sendNewTaskContent: MutationFunction<updateTaskById, updateTaskByIdVariables>;
  task: getTaskById_getTaskById;
  projectId: string;
  author: getProjectAuthorByProjectId_getProjectById_user;
};

export const CompletionTaskPageView: FC<Props> = (props) => {
  const { task, sendNewTaskContent, projectId, author } = props;
  const { user, isAdmin: currentUserIsAdmin } = useUserData();

  const iAmAuthor = author.id === user.id;
  const adminComments: getTaskById_getTaskById_comments[] =
    task.comments?.filter((comment) => isAdmin(comment.user));

  return (
    <>
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="space-between"
      >
        <Link
          href={{
            pathname: Routes.HowToPage,
            query: {
              projectId,
              taskId: task.id,
              pageId: task.howToPage,
            },
          }}
        >
          <Button aria-label="back" startIcon={<ArrowBackIcon />}>
            <Trans>zpět k úkolu</Trans>
          </Button>
        </Link>
      </Grid>

      <MainHeader variant="h1">{task?.title}</MainHeader>

      {iAmAuthor && (
        <>
          <AddNotesForm
            sendNewTaskContent={sendNewTaskContent}
            task={task}
            author={author}
          />

          <AdminComment adminComments={adminComments} />
        </>
      )}

      {!iAmAuthor && currentUserIsAdmin && (
        <AdminNotesForm sendNewTaskContent={sendNewTaskContent} task={task} />
      )}
    </>
  );
};

const MainHeader = styled(Typography)`
  margin-top: 32px;
`;
