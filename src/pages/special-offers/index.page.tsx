import { NextPage } from 'next';
import { SpecialOfferCardPageTemplate } from '../../common/containers/SpecialOfferCardPageTemplate';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import { t } from '@lingui/macro';
import { GRAPH_CMS_CLIENT_NAME } from '../../common/constants';
import { GET_SPECIAL_OFFERS } from '../../apollo/cms/GraphCMS/getSpecialOffers';
import {
  getSpecialOffers,
  getSpecialOffers_specialOffers,
  getSpecialOffersVariables,
} from '../../apollo/cms/GraphCMS/__generatedTypes';
import { useWhiteLabelContext } from '../../common/containers/WhitelabeProvider';
import { useQuery } from '@apollo/client';
import { LoadingScreen } from '../../common/components/LoadingScreen';
import * as React from 'react';

type Props = {
  specialOffers: getSpecialOffers_specialOffers[];
};
const SpecialOffers: NextPage<Props> = ({ specialOffers }) => {
  const { language } = useWhiteLabelContext();

  const { data, loading } = useQuery<
    getSpecialOffers,
    getSpecialOffersVariables
  >(GET_SPECIAL_OFFERS, {
    variables: {
      where: {
        lang: language,
      },
    },
    context: {
      clientName: GRAPH_CMS_CLIENT_NAME,
    },
  });

  return (
    <MainLayout title={t`Speciální nabídka`}>
      {loading && <LoadingScreen logo={false} />}

      {data && (
        <SpecialOfferCardPageTemplate
          items={data.specialOffers}
          buttonLabel={t`Více`}
          pageHeader={t`Speciální nabídky pro startupy`}
        />
      )}
    </MainLayout>
  );
};

export default SpecialOffers;
