import React from 'react';
import { CheckBig } from './CheckBig';
import { Typography } from '@mui/material';
import styled from 'styled-components';
import { Trans } from '@lingui/macro';

export const TabOne = () => {
  const benefits = [
    <Trans key={0}>
      <b>Ucelený návod</b>, jak projekt nebo startup postavit krok za krokem a{' '}
      <b>jak nedělat zbytečné chyby</b>.
    </Trans>,

    <Trans key={1}>
      <b>Videa úspěšných podnikatelů</b> k jednotlivým krokům s reálnými
      zkušenosti a radami.
    </Trans>,

    <Trans key={2}>
      Pro každý krok ti doporučíme <b>nástroje, experty a podporovatele</b>,
      kteří ti pomohou.
    </Trans>,

    <Trans key={3}>
      Možnost zúčastnit se pravidelných <b>Validation Challenges</b>, kde si
      ověříš svůj nápad a dostaneš zpětnou vazbu od mentorů i investorů.
    </Trans>,

    <Trans key={4}>
      Staneš se <b>součástí komunity startupistů</b>, kteří řeší ty samé věci
      jako ty a dodají ti odvahu a motivaci.
    </Trans>,
  ];

  return (
    <div>
      <Typography variant={'h3'} gutterBottom>
        <Trans>Výhody, za které nemusíš platit</Trans>
      </Typography>
      <Content>
        {benefits.map((line, index) => (
          <BenefitRow key={index}>
            <CheckBig />
            <Typography fontSize={15}>{line}</Typography>
          </BenefitRow>
        ))}
      </Content>
    </div>
  );
};

const BenefitRow = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;

  grid-gap: 16px;
  margin-bottom: 24px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
