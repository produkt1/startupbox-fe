import { FC, useState } from 'react';
import Link from 'next/link';
import { Routes } from '../../../common/enums/routes';
import * as React from 'react';
import {
  Typography,
  Link as MuiLink,
  Card,
  Box,
  Tabs,
  Tab,
} from '@mui/material';
import { RegistrationForm } from './RegistrationForm';
import styled from 'styled-components';
import { Divider } from '../../../common/components/auth/UI';
import { SocialLogin } from '../../../common/components/auth/SocialLogin';

import { minWidth } from '../../../common/styles/helpers';
import { t, Trans } from '@lingui/macro';
import { REGISTER } from './gql/register.mutation';
import { register, registerVariables } from './gql/__generated__/register';
import { useMutation, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';

import { TabOne } from './TabOne';
import { TabTwo } from './TabTwo';
import { TabThree } from './TabThree';
import { registrationPromoResults } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { REGISTRATION_PROMO_RESULTS_QUERY } from '../../../apollo/cms/GraphCMS/registrationPromoResults.query';
import { GRAPH_CMS_CLIENT_NAME } from '../../../common/constants';
import { analyticsDataLayerPush } from '../../../common/utils/ga';

const Registration: FC = () => {
  const { push } = useRouter();

  const [register] = useMutation<register, registerVariables>(REGISTER, {
    onCompleted: async (data) => {
      if (data.register.success) {
        analyticsDataLayerPush({ event: 'registration_submitted' });

        await push(
          Routes.Auth +
            `/${data.register.accessToken}/${data.register.refreshToken} `
        );
      }
    },
  });

  // todo: remove this from trans
  // const text = [t`benefit1`, t`benefit2`, t`benefit3`];

  const { data } = useQuery<registrationPromoResults>(
    REGISTRATION_PROMO_RESULTS_QUERY,
    {
      context: {
        clientName: GRAPH_CMS_CLIENT_NAME,
      },
    }
  );

  const tabs = [
    {
      label: t`Co získáš`,
      compoennt: TabOne,
    },
    {
      label: t`Reference`,
      compoennt: TabTwo,
    },
    {
      label: t`Výsledky`,
      compoennt: () => <TabThree data={data} />,
    },
  ];

  const [value, setValue] = useState(0);
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  return (
    <Box marginTop={4}>
      <TabsWrapper>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          {tabs.map((item, index) => (
            <Tab
              key={index}
              label={<Typography fontWeight={700}>{item.label}</Typography>}
              {...a11yProps(index)}
            />
          ))}
        </Tabs>
      </TabsWrapper>

      <RegistrationCard>
        {tabs[value].compoennt()}

        <DividerLine />

        <SignUpSection>
          <SocialLogin />

          <Divider>
            <Typography className="text">
              <Trans>nebo</Trans>
            </Typography>
          </Divider>

          <RegistrationForm register={register} />

          <Typography variant="body2" align={'center'}>
            <Trans>Pokud už účet máte, stačí se</Trans>{' '}
            <Link href={Routes.Login}>
              <MuiLink align="center" variant="body2" fontWeight={500}>
                <Trans>přihlásit</Trans>
              </MuiLink>
            </Link>
          </Typography>
        </SignUpSection>
      </RegistrationCard>
    </Box>
  );
};

export default Registration;

const RegistrationCard = styled(Card)`
  background-color: #eef1f7;
  padding: 3rem 1rem;
  border-top-left-radius: 0;

  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 24px;
  justify-content: center;

  box-shadow: 0px 0px 20px rgba(9, 60, 143, 0.1077);

  ${minWidth.tablet} {
    grid-template-columns: minmax(100px, 400px) auto minmax(100px, 400px);
    grid-gap: 48px;
    justify-content: center;

    padding: 4rem;
  }
`;

const BenefitRow = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;

  grid-gap: 16px;
  margin-bottom: 24px;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const DividerLine = styled.div`
  background-color: rgba(9, 60, 143, 0.15);
  height: 1px;
  width: 100%;

  ${minWidth.tablet} {
    height: 100%;
    width: 1px;
  }
`;

const SignUpSection = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-gap: 16px;
`;

const TabsWrapper = styled.div`
  background-color: #eef1f7;
  border-radius: 10px 10px 0px 0px;
  max-width: max-content;

  padding: 0 16px;
`;
