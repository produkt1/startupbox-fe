import React from 'react';
import { t, Trans } from '@lingui/macro';
import { Box, Typography } from '@mui/material';
import { QuotaionIcon } from '../../../common/components/Icon/Icons/Quotation.icon';

export const TabTwo = () => {
  const quotes = [
    {
      text: t`Prošla jsem několik akcelerátorů a deník, který je ve StartupBoxu, by měl být základem každého inkubátoru a akcelerátoru.`,
      author: 'Kateřina Eklová, Rethink Architecture',
    },

    {
      text: t`StartupBox nám pomohl s jejich deníkem startupisty, který nás nasměroval a ucelil naše myšlenky a podklady, co jsme měli a máme.`,
      author: 'Marek Erben a Markéta Horáková, SimplyOn',
    },

    {
      text: t`Jestliže chcete podnikat, nedělejte spoustu chyb, které dělat nemusíte. Pojďte na StartupBox, který vás naučí, co dělat správně a jak to dělat, abyste byli úspěšní.`,
      author: 'Juraj Atlas, Mileus a Liftago',
    },
  ];

  //
  return (
    <div>
      <Typography variant={'h3'} gutterBottom>
        <Trans>Co o nás říkají startupisté?</Trans>
      </Typography>

      {quotes.map((quote) => (
        <>
          <Typography fontSize={15} fontStyle="italic" marginBottom={1}>
            “{quote.text}“
          </Typography>

          <Box display="flex">
            <QuotaionIcon />

            <Typography
              fontSize={15}
              fontWeight={600}
              marginBottom={4}
              marginLeft={1}
            >
              {quote.author}
            </Typography>
          </Box>
        </>
      ))}
    </div>
  );
};
