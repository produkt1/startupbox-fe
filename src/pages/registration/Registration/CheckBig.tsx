import React from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Box } from '@mui/material';
import { colors } from '../../../common/styles/colors';

export const CheckBig = () => {
  return (
    <Box
      bgcolor={colors.brand}
      width={32}
      height={32}
      display="flex"
      alignItems="center"
      justifyContent="center"
      borderRadius="50%"
    >
      <Box
        bgcolor={colors.white}
        width={19}
        height={19}
        display="flex"
        alignItems="center"
        justifyContent="center"
        borderRadius="50%"
      >
        <CheckCircleIcon color="primary" />
      </Box>
    </Box>
  );
};
