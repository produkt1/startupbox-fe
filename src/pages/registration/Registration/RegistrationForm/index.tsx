import { FC, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { FormikTextInput } from '../../../../common/components/inputs/TextInput';
import { Alert, Box, Button, Typography } from '@mui/material';
import { PasswordField } from '../../../../common/components/inputs/PasswordField';
import { FormikCheckbox } from '../../../../common/components/inputs/Checkbox';
import { MutationFunction } from '@apollo/client/react/types/types';
import { register, registerVariables } from '../gql/__generated__/register';
import { t, Trans } from '@lingui/macro';

import { registrationValidation } from './validation.schema';

type SubmitHandler<Values> = (
  values: Values,
  formikHelpers: FormikHelpers<Values>
) => void | Promise<any>;

type Props = {
  register: MutationFunction<register, registerVariables>;
};

export const RegistrationForm: FC<Props> = ({ register }) => {
  const [error, setError] = useState(false);
  const initialValues: registerVariables = {
    email: '',
    password: '',
    agreedToTermsAndConditions: false,
  };

  const handleSubmit: SubmitHandler<registerVariables> = async (
    values,
    { setSubmitting }
  ) => {
    const response = await register({ variables: values });
    setSubmitting(false);

    if (!response.data.register.success) {
      setError(true);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validateOnBlur
      validateOnMount={false}
      validationSchema={registrationValidation}
    >
      {({ isSubmitting }) => {
        return (
          <Form>
            <FormikTextInput name="email" type="email" label={t`Váš email`} />

            <PasswordField name="password" label={t`Heslo`} />

            <FormikCheckbox
              name="agreedToTermsAndConditions"
              label={
                <Typography variant="body2" gutterBottom>
                  <Trans>
                    Registrací souhlasím s{' '}
                    <a
                      href="https://www.startupbox.cz/podminky-uziti"
                      target="_blank"
                      rel="noreferrer"
                    >
                      podmínkami použití
                    </a>{' '}
                    a{' '}
                    <a
                      href="https://www.startupbox.cz/zasady-ochrany-osobnich-udaju"
                      target="_blank"
                      rel="noreferrer"
                    >
                      zásadami ochrany osobních údajů
                    </a>
                    .
                  </Trans>
                </Typography>
              }
            />

            {error && (
              <Box marginY={2}>
                <Alert severity="error">
                  <Trans>Registrace s tímto emailem se nezdařila</Trans>
                </Alert>
              </Box>
            )}

            <Button
              color="success"
              variant="contained"
              size="large"
              type="submit"
              fullWidth
              disabled={isSubmitting}
            >
              {isSubmitting ? (
                <Trans>Odesílání...</Trans>
              ) : (
                <Trans>Registrovat se</Trans>
              )}
            </Button>
          </Form>
        );
      }}
    </Formik>
  );
};
