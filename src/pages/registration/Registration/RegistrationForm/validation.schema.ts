import * as yup from 'yup';
import { t } from '@lingui/macro';

export const registrationValidation = yup.object().shape({
  email: yup
    .string()
    .email(t`zadejte prosím email v platném formátu`)
    .required(t`potřeba zadat`),
  password: yup
    .string()
    .required(t`potřeba zadat`)
    .min(6, t`heslo musí mít minimálně 6 znaků`),
  agreedToTermsAndConditions: yup
    .boolean()
    .oneOf([true], t`* potvrďte prosím souhlas`),
});
