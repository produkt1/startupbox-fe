import { gql } from '@apollo/client';

export const REGISTER = gql`
  mutation register(
    $email: String!
    $password: String!
    $agreedToTermsAndConditions: Boolean!
  ) {
    register(
      data: {
        email: $email
        password: $password
        agreedToTermsAndConditions: $agreedToTermsAndConditions
      }
    ) {
      requestId
      verificationToken
      accessToken
      refreshToken
      expiresIn
      success
      refreshExpiresIn
      user {
        id
        email
      }
    }
  }
`;
