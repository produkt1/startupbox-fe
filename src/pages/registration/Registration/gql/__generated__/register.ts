/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: register
// ====================================================

export interface register_register_user {
  __typename: 'User';
  id: string | null;
  email: string | null;
}

export interface register_register {
  __typename: 'RegistrationResponseType';
  requestId: string | null;
  verificationToken: string | null;
  accessToken: string | null;
  refreshToken: string | null;
  expiresIn: number | null;
  success: boolean;
  refreshExpiresIn: number | null;
  user: register_register_user | null;
}

export interface register {
  register: register_register;
}

export interface registerVariables {
  email: string;
  password: string;
  agreedToTermsAndConditions: boolean;
}
