import React from 'react';
import { registrationPromoResults } from '../../../apollo/cms/GraphCMS/__generatedTypes';
import { Typography } from '@mui/material';
import { Trans } from '@lingui/macro';
import { useLingui } from '@lingui/react';

export const TabThree = ({ data }: { data: registrationPromoResults }) => {
  const { i18n } = useLingui();

  return (
    <div>
      <Typography variant="h3" marginBottom={4}>
        <Trans> Co nás nejvíc těší</Trans>
      </Typography>

      {data?.registrationPromoResults?.map((item) => (
        <>
          <Typography
            fontSize={40}
            marginBottom={2}
            fontWeight={600}
            color={item.countColor}
          >
            {item.count}
          </Typography>

          <Typography fontSize={18} marginBottom={6} fontWeight={600}>
            {
              item[
                `text${
                  i18n.locale.charAt(0).toUpperCase() + i18n.locale.slice(1)
                }`
              ]
            }
          </Typography>
        </>
      ))}
    </div>
  );
};
