import { FC } from 'react';
import { MainLayout } from '../../common/containers/MainLayout/MainLayout';
import * as React from 'react';
import { Typography } from '@mui/material';
import { t, Trans } from '@lingui/macro';

import Registration from './Registration/Registration';

const RegistrationPage: FC = () => {
  return (
    <MainLayout title={t`Registrace`} protectedRoute={false}>
      <Typography variant="h2" align="center">
        <Trans>Registrovat se</Trans>
      </Typography>

      <Registration />
    </MainLayout>
  );
};

export default RegistrationPage;
