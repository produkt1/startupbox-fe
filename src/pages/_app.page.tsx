import { ApolloProvider } from '@apollo/client';
import { ThemeProvider } from '@mui/material/styles';
import { StylesProvider } from '@mui/styles';
import { GlobalStyle } from '../common/styles/globalStyles';
import React from 'react';

import { i18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';
import { messages as csMessages } from '../locales/cs';
import { messages as enMessages } from '../locales/en';
import { messages as skMessages } from '../locales/sk';
import { theme } from '../common/styles/muiTheme';
import { useApollo } from '../apollo/apollo-client';
import { SnackProvider } from '../common/containers/SnackProvider';
import { AppHead } from '../common/components/AppHead';
import { WhiteLabelProvider } from '../common/containers/WhitelabeProvider';
import { CopilotKit } from '@copilotkit/react-core';
import { ChatbotProvider } from '../common/containers/ChatbotContext';
import { isDev } from '../common/utils/helpers';
import { getStoredAccessToken } from '../common/utils/authHelpers';

const App = ({ Component, pageProps }) => {
  const apolloClient = useApollo(pageProps);
  const runtimeUrlFallback = `${process.env.NEXT_PUBLIC_API_URL}/copilotkit`;
  const accessToken = getStoredAccessToken() || '';

  i18n.load({
    en: enMessages,
    cs: csMessages,
    sk: skMessages,
  });
  i18n.activate('cs');

  return (
    <>
      <AppHead />

      <ApolloProvider client={apolloClient}>
        <ThemeProvider theme={theme}>
          <StylesProvider injectFirst>
            {/*{isProduction && <GATracker />}*/}

            <GlobalStyle />

            <CopilotKit
              runtimeUrl={
                process.env.NEXT_PUBLIC_RUNTIME_URL || runtimeUrlFallback
              }
              agent="chat_agent"
              showDevConsole={isDev && true}
              headers={{
                Authorization: `Bearer ${accessToken}`,
              }}
            >
              <I18nProvider i18n={i18n}>
                <SnackProvider>
                  <WhiteLabelProvider>
                    <ChatbotProvider>
                      <Component {...pageProps} />
                    </ChatbotProvider>
                  </WhiteLabelProvider>
                </SnackProvider>
              </I18nProvider>
            </CopilotKit>
          </StylesProvider>
        </ThemeProvider>
      </ApolloProvider>
    </>
  );
};

// FIXME: move this inside app, it will break apollo, => update APollo handling = remove HOC for hook

/*TODO: review analytics*/

// const GATracker = () => {
//   // TODO: if prod then
//   const router = useRouter();
//   useEffect(() => {
//     const handleRouteChange = (url) => {
//       ga.pageView(url);
//     };
//     router.events.on('routeChangeComplete', handleRouteChange);
//     return () => {
//       router.events.off('routeChangeComplete', handleRouteChange);
//     };
//   }, [router.events]);
//
//   return null;
// };

export default App;
