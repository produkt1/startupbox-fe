import React, { FC } from 'react';
import styled from 'styled-components';
import {
  Card,
  CardHeader,
  CardContent,
  Button,
  Typography,
} from '@mui/material';
import { colors } from '../../../../common/styles/colors';

type Props = {
  type: string;
  title: string;
  description: string;
  date: string;
  img: string;
};

export const ChallengeCard: FC<Props> = (props) => {
  const { type, title, description, date, img } = props;

  return (
    <CardWrapper>
      {/*<IconWrapper />*/}
      <CardTitle title={<Typography variant="h3">{type}</Typography>} />
      <Content>
        <ChallengeInfo>
          <div>
            <Typography variant="h4">{title}</Typography>
            <Description>{description}</Description>
            <Typography variant="caption">{date}</Typography>
          </div>
          <Logo src={img} alt={title} />
        </ChallengeInfo>

        <Button variant="contained" size="large">
          Více informací
        </Button>
      </Content>
    </CardWrapper>
  );
};

const ICON_SIZE = '60px';

const CardWrapper = styled(Card)`
  margin: calc(${ICON_SIZE} / 2) 0 45px;
  overflow: visible;
  position: relative;
  width: 100%;
  padding: 0;
`;

const CardTitle = styled(CardHeader)`
  background: ${colors.lightGrey};
  padding: 40px 0 20px;
  text-align: center;
  width: 100%;
`;

const Content = styled(CardContent)`
  align-items: center;
  display: flex;
  flex-direction: column;
  padding: 25px;
`;

const IconWrapper = styled.div`
  background: ${colors.brand};
  border-radius: 50%;
  height: ${ICON_SIZE};
  left: 50%;
  position: absolute;
  top: calc(-${ICON_SIZE} / 2);
  transform: translateX(-50%);
  width: ${ICON_SIZE};
`;

const Description = styled(Typography)`
  margin: 15px 0 10px;
`;

const ChallengeInfo = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const Logo = styled.img`
  width: 33%;
`;
