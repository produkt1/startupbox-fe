import React from 'react';
import { ChallengeCard } from './ChallengeCard';
import { Typography } from '@mui/material';

export const Challenges = () => (
  <>
    <Typography variant="h2">Doporučené výzvy</Typography>

    <ChallengeCard
      type="Korporátní výzva"
      description="Optimalizace logistiky na skladech"
      title="Albert"
      date="Květen 2019"
      img="/challenge_placeholder.jpg"
    />
    <ChallengeCard
      type="Korporátní výzva"
      description="Návrh budoucnosti prodejny"
      title="Vodafone"
      date="Duben 2019"
      img="/challenge_placeholder.jpg"
    />
  </>
);
