import { FC } from 'react';
import { Typography } from '@mui/material';
import { ProfilePicture } from '../../../common/components/profile/ProfilePicture';
import { getUser_getUser } from '../../../apollo/queries/__generated__/getUser';
import Button from '@mui/material/Button';
import styled from 'styled-components';
import { SkillItem } from '../../../common/components/UI';
import Grid from '@mui/material/Grid';

type Props = {
  user: getUser_getUser;
};

export const DashboardHead: FC<Props> = (props) => {
  const { user } = props;

  return (
    <>
      <Typography variant="h3" gutterBottom>
        Zaregistrujte se do výzvy
      </Typography>

      <Typography gutterBottom>Výzvy již brzy spustíme</Typography>
    </>
  );
};

const ProfileClaim = styled(Typography)`
  margin: 15px 0;
`;
