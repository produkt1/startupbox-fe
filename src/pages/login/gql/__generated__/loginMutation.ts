/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import {
  UserRole,
  EmailVerification,
  ProjectPhase,
} from './../../../../../__generated__/globalTypes';

// ====================================================
// GraphQL mutation operation: loginMutation
// ====================================================

export interface loginMutation_login_user_profile {
  __typename: 'Profile';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  region: string | null;
  skills: string[] | null;
}

export interface loginMutation_login_user_projects {
  __typename: 'Project';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  name: string;
  industry: string[] | null;
  phase: ProjectPhase | null;
}

export interface loginMutation_login_user_onboardingData {
  __typename: 'OnboardingType';
  answer: string[];
  question: string;
}

export interface loginMutation_login_user {
  __typename: 'User';
  id: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  profilePicture: string | null;
  role: UserRole;
  emailVerification: EmailVerification;
  profile: loginMutation_login_user_profile | null;
  projects: loginMutation_login_user_projects[] | null;
  onboardingResult: string | null;
  onboardingData: loginMutation_login_user_onboardingData[] | null;
}

export interface loginMutation_login {
  __typename: 'LoginResponseType';
  message: string;
  code: string;
  success: boolean;
  accessToken: string | null;
  refreshToken: string | null;
  expiresIn: number | null;
  refreshExpiresIn: number | null;
  tokenType: string | null;
  user: loginMutation_login_user | null;
}

export interface loginMutation {
  login: loginMutation_login;
}

export interface loginMutationVariables {
  email: string;
  password: string;
}
