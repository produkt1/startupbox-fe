/** @type {import('next-sitemap').IConfig} */

module.exports = {
  // siteUrl: process.env.SITE_URL || 'https://example.com',
  siteUrl: 'https://startupbox.app',

  generateRobotsTxt: true, // (optional)
  exclude: ['/server-sitemap.xml'], // <= exclude here
  robotsTxtOptions: {
    additionalSitemaps: [
      'https://startupbox.app/server-sitemap.xml', // <==== Add here
    ],
  },
};
